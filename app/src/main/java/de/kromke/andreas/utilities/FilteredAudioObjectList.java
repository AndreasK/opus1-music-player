/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import java.util.ArrayList;

/**
 * List of audio objects with filtering
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
public class FilteredAudioObjectList
{
    // the unfiltered list, the filtered list and the shown list are all of this type:
    @SuppressWarnings("WeakerAccess")
    static public class AudioObjectList
    {
        public ArrayList<AudioBaseObject> list = null;      // the data

        public int getSize()
        {
            return (list == null) ? -1 : list.size();
        }

        public int selectedIndex = -1;

        public void setSelectedIndex(final AudioBaseObject theObject)
        {
            selectedIndex = -1;     // default
            if (theObject != null)
            {
                // search for object
                for (int i = 0; i < getSize(); i++)
                {
                    if (list.get(i) == theObject)
                    {
                        // found and set index
                        selectedIndex = i;
                        break;
                    }
                }
            }
        }

        public boolean removeObjects(ArrayList<AudioBaseObject> theList)
        {
            boolean bDone = false;
            if (list != null)
            {
                int n = theList.size();
                for (int i = 0; i < n; i++)
                {
                    AudioBaseObject theTrack = theList.get(i);
                    if (theTrack != null)
                    {
                        if (list.remove(theTrack))
                        {
                            bDone = true;
                        }
                    }
                }
            }
            return bDone;
        }
    }

    public AudioObjectList raw = new AudioObjectList(); // all items
    protected AudioObjectList filtered = null;          // those items that match filters, not used by playlist
    protected AudioObjectList shown = null;             // items that match filters and text
    public int scrolledIndexShown = -1;
    public int scrolledTopShown = -1;
    private String searchText = null;
    private boolean mSearchIsCaseSensitive = false;

    // get the filtered list. If there is no filter, the raw list is returned
    public AudioObjectList getListFiltered()
    {
        // if no object filter present, return raw list
        return (filtered == null) ? raw : filtered;
    }

    // get the size of the filtered list
    public int getFilteredSize() { return getListFiltered().getSize(); }
    //public int getFilteredSelectedIndex() { return getListFiltered().selectedIndex; }

    public void setRawSelectedIndex(int index)
    {
        raw.selectedIndex = index;
        if (filtered != null)
        {
            final AudioBaseObject theObject = (index >= 0) ? raw.list.get(index) : null;
            filtered.setSelectedIndex(theObject);
        }
        if (shown != null)
        {
            final AudioBaseObject theObject = (index >= 0) ? raw.list.get(index) : null;
            shown.setSelectedIndex(theObject);
        }
    }

    // get the shown list. If there is none, the filtered resp. raw list is returned
    public AudioObjectList getListShown()
    {
        // if no text filter present, return filtered list
        return (shown == null) ? getListFiltered() : shown;
    }
    //public int getShownSize() { return getListShown().getSize(); }
    public int getRawSelectedIndex() { return raw.selectedIndex; }
    public int getShownSelectedIndex() { return getListShown().selectedIndex; }
    public void setShownSelectedIndex(int index)
    {
        if (index < 0)
        {
            // deselect everything
            setRawSelectedIndex(-1);
        }
        else
        {
            AudioObjectList theList = getListShown();
            AudioBaseObject theObject = theList.list.get(index);
            raw.selectedIndex = -1;
            for (int i = 0; i < raw.getSize(); i++)
            {
                if (raw.list.get(i) == theObject)
                {
                    raw.selectedIndex = i;
                    break;
                }
            }
            theList.selectedIndex = index;
        }
    }

    public int getShownIndexFromRawIndex(int rawIndex)
    {
        if ((rawIndex < 0) || (rawIndex >= raw.getSize()))
        {
            return -1;
        }
        AudioBaseObject theObject = raw.list.get(rawIndex);
        int shownIndex = -1;
        AudioObjectList theShownList = getListShown();
        for (int i = 0; i < theShownList.getSize(); i++)
        {
            if (theShownList.list.get(i) == theObject)
            {
                shownIndex = i;
                break;
            }
        }
        return shownIndex;
    }

    public int getTotalSize() { return (raw == null) ? -1 : raw.getSize(); }
    public int getTotalSizeOfType(AudioBaseObject.AudioObjectType theType)
    {
        int num = 0;
        for (int i = 0; i < raw.getSize(); i++)
        {
            AudioBaseObject theObject = raw.list.get(i);
            if (theObject.objectType == theType)
            {
                num++;
            }
        }
        return num;
    }

    public boolean isInitialised() { return (getTotalSize() >= 0); }


    /**************************************************************************
     * Find object in raw list and return index (-1 if not found)
     *************************************************************************/

    public int findInRawList(AudioBaseObject anObject)
    {
        for (int i = 0; i < raw.getSize(); i++)
        {
            AudioBaseObject theObject = raw.list.get(i);
            if (theObject == anObject)
            {
                // found (direct match)
                return i;
            }
            if ((anObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_WORK_OBJECT) &&
                (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_ALBUM_OBJECT))
            {
                // look for work in album
                AudioAlbum theAlbum = (AudioAlbum) theObject;
                for (int j = 0; j < theAlbum.mWorkList.size(); j++)
                {
                    if (theAlbum.mWorkList.get(j) == anObject)
                    {
                        // found work in album
                        return i;
                    }
                }
            }
        }
        // not found
        return -1;
    }


    public void setRawList(ArrayList<AudioBaseObject> theObjects)
    {
        raw.list = theObjects;
        raw.selectedIndex = -1;
        scrolledIndexShown = -1;
        scrolledTopShown = -1;
        filtered = null;
        shown = null;
    }

    public boolean removeObjects(ArrayList<AudioBaseObject> theObjects)
    {
        boolean bDone = false;
        if (raw != null)
        {
            bDone = raw.removeObjects(theObjects);
            if (bDone)
            {
                raw.selectedIndex = -1;
                scrolledIndexShown = -1;
                scrolledTopShown = -1;
                // save and remove search
                String savedSearchText = searchText;
                setSearchText(null, mSearchIsCaseSensitive);
                // re-apply search
                setSearchText(savedSearchText, mSearchIsCaseSensitive /* stored value */);
            }
        }
        return bDone;
    }


    public void appendRawList(ArrayList<AudioBaseObject> theObjects)
    {
        if (getTotalSize() < 1)
        {
            setRawList(theObjects);
        }
        else
        {
            int size = theObjects.size();
            for (int i = 0; i < size; i++)
            {
                raw.list.add(theObjects.get(i));
            }
            filtered = null;    // sensible?
            shown = null;       // sensible?
        }
    }

    /**************************************************************************
     * set the object filters
     *
     * Basically recreates the "filtered" list, based on the raw list
     * and the filters.
     *************************************************************************/
    public void setFilters(AudioComposer theFilterComposer, AudioGenre theFilterGenre, AudioPerformer theFilterPerformer)
    {
        if ((theFilterComposer == null) && (theFilterGenre == null) && (theFilterPerformer == null))
        {
            // no filter. Is there a previous filter?
            if (filtered != null)
            {
                // remove filter
                filtered = null;
                // remove search
                setSearchText(null, mSearchIsCaseSensitive  /* stored value */);
            }
        }
        else
        {
            // filter by filter objects
            AudioObjectList tempList = new AudioObjectList();
            tempList.list = new ArrayList<>();
            int temp_index = 0;

            int size = getTotalSize();
            for (int i = 0; i < size; i++)
            {
                AudioBaseObject theObject = raw.list.get(i);
                if (theObject.matchesFilter(theFilterComposer, theFilterPerformer, theFilterGenre))
                {
                    tempList.list.add(theObject);
                    if (i == raw.selectedIndex)
                    {
                        // the selected index also matches, keep it
                        tempList.selectedIndex = temp_index;
                    }
                    temp_index++;
                }
            }

            filtered = tempList;
            if (filtered.selectedIndex < 0)
            {
                // remove selection also from raw list
                raw.selectedIndex = -1;
            }
            // re-apply search
            setSearchText(searchText, mSearchIsCaseSensitive /* stored value */);
        }
    }


    /**************************************************************************
     * count matching objects
     *
     * similar to setFilters(), but do not create a list, instead only count
     * the number of matching objects.
     * This is called for the work list to count the number of matching
     * works for a given composer, genre etc.
     *
     *************************************************************************/
    public int countMatchingObjects(AudioComposer theFilterComposer, AudioGenre theFilterGenre, AudioPerformer theFilterPerformer)
    {
        if ((theFilterComposer == null) && (theFilterGenre == null) && (theFilterPerformer == null))
        {
            // no filter, every object in list matches
            return getTotalSize();
        }

        // filter by filter objects
        int count = 0;

        for (int i = 0; i < getTotalSize(); i++)
        {
            AudioBaseObject theObject = raw.list.get(i);
            if (theObject.matchesFilter(theFilterComposer, theFilterPerformer, theFilterGenre))
            {
                count++;
            }
        }

        return count;
    }


    /**************************************************************************
     *
     * Set the search list. As a result the shown list might be recreated.
     *
     *************************************************************************/
    public void setSearchText(String newText, boolean bCaseSensitive)
    {
        mSearchIsCaseSensitive = bCaseSensitive;    // remember for later

        if ((newText != null) && (newText.isEmpty()))
        {
            // treat empty string like null
            newText = null;
        }

        if (newText != null && !bCaseSensitive)
        {
            // For case insensitive search convert search text to upper case.
            // Note that case insensitive string comparison does not work for
            // non-ASCII characters. Instead convert both strings to upper case, and then compare.
            newText = newText.toUpperCase();
        }

        if (newText == null)
        {
            // no text filter
            shown = null;
            searchText = null;
        }
        else
        if ((searchText == null) || !(searchText.equals(newText)))
        {
            // previous search text was not empty, and new one differs

            // get filtered list or raw list, if no filter
            AudioObjectList theList = getListFiltered();

            // filter by search text
            AudioObjectList tempList = new AudioObjectList();
            tempList.list = new ArrayList<>();
            int temp_index = 0;

            for (int i = 0; i < theList.getSize(); i++)
            {
                AudioBaseObject theObject = theList.list.get(i);
                if (theObject.matchesText(newText, mSearchIsCaseSensitive))
                {
                    tempList.list.add(theObject);
                    if (i == theList.selectedIndex)
                    {
                        // the selected index also matches, keep it
                        tempList.selectedIndex = temp_index;
                    }
                    temp_index++;
                }
            }

            searchText = newText;
            shown = tempList;
        }
    }

}
