/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * all data for a single audio genre
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioGenre extends AudioBaseObject
{
    private final String name_localised;
    public ArrayList<AudioTrack> mTrackList = new ArrayList<>();  // tracks with that genre, only group number 0 tracks
    private final String picture_path;

    public AudioGenre(String theName, String theNameLocalised, int theNoOfWorks)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_GENRE_OBJECT;
        name = theName;
        name_localised = theNameLocalised;
        no_of_works = no_of_matching_works = theNoOfWorks;
        picture_path = null;
    }

    // for sorting
    @Override
    public int compareTo(@NonNull AudioBaseObject another)
    {
        return name_localised.compareTo(((AudioGenre) another).name_localised);
    }

    // for display in a list, normal text
    public String getTitle()
    {
        return (name_localised.isEmpty()) ? str_unknown_genre : name_localised;
    }

    // for display in a list, small text
    @Override
    public String getInfo()
    {
        return "(" + getWorkNoInfo() + ")";
    }

    @Override
    public String getPicturePath(Context context)
    {
        return picture_path;
    }


    /**************************************************************************
     *
     * Check, if the object matches the given filter objects
     *
     *************************************************************************/
    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        // theFilterGenre is ignored, because we already have a matching track list
        if ((theFilterComposer == null) && (theFilterPerformer == null))
        {
            // no filter, so always matches
            return true;
        }

        for (AudioTrack theTrack: mTrackList)
        {
            if (theTrack.matchesFilter(theFilterComposer) && theTrack.matchesFilter(theFilterPerformer))
            {
                // one match is sufficient
                return true;
            }
        }

        return false;    // no matches found
    }
}
