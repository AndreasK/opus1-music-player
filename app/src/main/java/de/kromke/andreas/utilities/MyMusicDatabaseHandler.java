/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

/**
 * Combines AudioTrack with the database
 */
public class MyMusicDatabaseHandler
{
    static private final String LOG_TAG = "O1M : MyMusicDbaseHdlr";
    private static MusicEntryReaderDbHelper mDbHelper = null;
    private static SQLiteDatabase db = null;
    private static final boolean dbIsWritable = false;
    public  static long mDbModificationDate = 0;

    // In SAF mode or with own database, bIsOwnDatabase is true.
    // When using the Android database, bIsOwnDatabase is false, and
    // our shadow database "MusicEntry.db" in the app's standard
    // database folder is used (/data/user/0/de.kromke.andreas.opus1musicplayer/databases/MusicEntry.db).
    static void open(Context context, boolean bCreateNew, boolean bIsOwnDatabase)
    {
        mDbHelper = new MusicEntryReaderDbHelper(context);

        if (!bIsOwnDatabase)
        {
            // get date of last change, needed for consistency check
            File dbPath = context.getDatabasePath(mDbHelper.getDbName());
            if (dbPath != null)
            {
                Log.v(LOG_TAG, "music db path = " + dbPath);
                if (bCreateNew)
                {
                    deleteAllRows();
                    Log.v(LOG_TAG, "db to be recreated");
                    mDbModificationDate = 0;
                } else
                {
                    mDbModificationDate = dbPath.lastModified();
                }
                Log.v(LOG_TAG, "music db modification date = " + mDbModificationDate + " (" + Utilities.convertMsToIso(mDbModificationDate) + ")");
            } else
            {
                Log.v(LOG_TAG, "no db found");
                mDbModificationDate = 0;
            }
        }
    }


    static void deleteAllRows()
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(MyMusicDatabase.MusicEntry.TABLE_NAME, null /* where */, null /* whereArgs */);
    }


    static void insert(ArrayList<AudioTrack> theList)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        for (int i = 0; i < theList.size(); i++)
        {
            AudioTrack theTrack = theList.get(i);

            // Create a new map of values, where column names are the keys
            ContentValues values = new ContentValues();

            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_ENTRY_ID, theTrack.id);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_TRACK_NO, theTrack.track_no);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_TITLE, theTrack.name);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM, theTrack.album);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM_ID, theTrack.album_id);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_DURATION, theTrack.duration);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_GROUPING, theTrack.grouping);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_SUBTITLE, theTrack.subtitle);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_COMPOSER, theTrack.composer);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_PERFORMER, theTrack.artist);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM_PERFORMER, theTrack.album_artist);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_CONDUCTOR, theTrack.conductor);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_GENRE, theTrack.genre);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_YEAR, theTrack.year);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_MEDIA_URI, theTrack.mMediaUri);
            values.put(MyMusicDatabase.MusicEntry.COLUMN_NAME_PATH, theTrack.getRawPath());

            // Insert the new row, returning the primary key value of the new row
            /*long newRowId;
            newRowId = */db.insert(
                    MyMusicDatabase.MusicEntry.TABLE_NAME,
                    null,
                    values);
        }
    }

    @SuppressLint("Range")
    static ArrayList<AudioTrack> read()
    {
        // Gets the data repository in read mode
        if ((db == null) || (dbIsWritable))
        {
            db = mDbHelper.getReadableDatabase();
        }

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM + ", " + MyMusicDatabase.MusicEntry.COLUMN_NAME_TRACK_NO;

        Cursor theCursor = db.query(
                MyMusicDatabase.MusicEntry.TABLE_NAME,  // The table to query
                null,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no track found");
            theCursor.close();
            return null;
        }

        ArrayList<AudioTrack> theList = new ArrayList<>();

        do
        {
            AudioTrack theTrack = new AudioTrack(
                    theCursor.getLong  (theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_ENTRY_ID)),
                    theCursor.getInt   (theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_TRACK_NO)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_TITLE)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM)),
                    theCursor.getLong  (theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM_ID)),
                    theCursor.getLong  (theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_DURATION)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_GROUPING)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_SUBTITLE)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_COMPOSER)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_PERFORMER)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_ALBUM_PERFORMER)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_CONDUCTOR)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_GENRE)),
                    theCursor.getInt   (theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_YEAR)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_MEDIA_URI)),
                    theCursor.getString(theCursor.getColumnIndex(MyMusicDatabase.MusicEntry.COLUMN_NAME_PATH)));

            theList.add(theTrack);
        }
        while (theCursor.moveToNext());
        theCursor.close();

        return theList;
    }
}
