/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;

import java.util.ArrayList;

/**
 * all data for a single audio performer
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioPerformer extends AudioBaseObject
{
    public ArrayList<AudioTrack> mTrackList = new ArrayList<>();  // tracks with that performer, only group number 0 tracks
    private final boolean bIsGroupOfPersons;

    public AudioPerformer(String theName, int theNoOfWorks, boolean bIsMoreThanOnePerson)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_PERFORMER_OBJECT;
        no_of_works = no_of_matching_works = theNoOfWorks;
        bIsGroupOfPersons = bIsMoreThanOnePerson;
        if (bIsGroupOfPersons)
        {
            name = theName.replace("\n", "; ");
        }
        else
        {
            name = theName;
        }
    }

    /*
    // for sorting
    @Override
    public int compareTo(@NonNull AudioBaseObject another)
    {
        return name.compareTo(((AudioPerformer) another).name);
    }
    */

    // for display in a list, normal text
    public String getTitle()
    {
        return (name.isEmpty()) ? str_unknown_performer : name;
    }

    // for display in a list, small text
    @Override
    public String getInfo()
    {
        return "(" + getWorkNoInfo() + ")";
    }

    @Override
    public void initPicturePath(Context context)
    {
        initPicturePathForPersons();
    }

    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        // theFilterPerformer is ignored, because we already have a matching track list
        if ((theFilterComposer == null) && (theFilterGenre == null))
        {
            // no filter, so always matches
            return true;
        }

        for (AudioTrack theTrack: mTrackList)
        {
            if (theTrack.matchesFilter(theFilterComposer) && theTrack.matchesFilter(theFilterGenre))
            {
                // one match is sufficient
                return true;
            }
        }

        return false;    // no matches found
    }


    /**************************************************************************
     *
     * Check, if the object symbolises a group of persons (composers or performers)
     * to be overridden
     *************************************************************************/
    @Override
    public boolean isPersonGroup()
    {
        return bIsGroupOfPersons;
    }
}
