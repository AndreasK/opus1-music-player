/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.Log;

//import java.io.FileInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
//import java.util.HashMap;
//import java.util.Map;

/**
 * various useful functions
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
public class BitmapUtils
{
    private static final String LOG_TAG = "O1M : BitmapUtils";


    /**************************************************************************
     *
     * get a resized bitmap from a given bitmap and keep the width/height
     * ratio
     *
     *************************************************************************/
    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        if ((width <=  0) || (height <= 0))
        {
            Log.d(LOG_TAG, "getResizedBitmap() : illegal bitmap size w=" + width + " h=" + height);
            return null;
        }

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        float scale = java.lang.Math.min(scaleWidth, scaleHeight); // keep ratio
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scale, scale);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap;
        try
        {
            resizedBitmap = Bitmap.createBitmap(
                    bm, 0, 0, width, height, matrix, false);
        }
        catch (IllegalArgumentException e)
        {
            Log.e(LOG_TAG, "createBitmap() failed because of: " + e.getMessage());
            resizedBitmap = null;
        }

        //bm.recycle();     // causes crash with error message: trying to use a recycled bitmap
        return resizedBitmap;
    }


    /**************************************************************************
     *
     * create a masked bitmap from a given bitmap
     *
     * Note that the mask has the correct size, while the original must be
     * centered.
     *
     *************************************************************************/
    public static Bitmap makeMaskedBitmap
    (
        Bitmap bmOriginal,
        Bitmap bmMask,      // already scaled
        int width,
        int height
    )
    {
        int xMargin = width/8;
        int yMargin = height/6;

        bmOriginal = getResizedBitmap(bmOriginal, width - 2 * xMargin, height - 2 * yMargin);
        if (bmOriginal != null)
        {
            //bmMask = getResizedBitmap(bmMask, width, height);
            Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas mCanvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            int xOffs = (width - bmOriginal.getWidth()) / 2;
            int yOffs = (height - bmOriginal.getHeight()) / 2;
            mCanvas.drawBitmap(bmOriginal, xOffs, yOffs, null);
            mCanvas.drawBitmap(bmMask, 0, 0, paint);
            paint.setXfermode(null);
            return result;
        }
        else
        {
            return null;
        }
    }


    /* *************************************************************************
     *
     * create a masked bitmap from path
     *
     *************************************************************************/
    /* UNUSED
    public static Bitmap makeMaskedBitmapFromPath
    (
        String path,
        Bitmap bmMask,
        int width,
        int height
    )
    {
        Bitmap result = BitmapFactory.decodeFile(path);
        if (result != null)
        {
            Bitmap bmOriginal = getResizedBitmap(result, width, height);
            bmMask = getResizedBitmap(bmMask, width, height);
            result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas mCanvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            mCanvas.drawBitmap(bmOriginal, 0, 0, null);
            mCanvas.drawBitmap(bmMask, 0, 0, paint);
            paint.setXfermode(null);
        }
        return result;
    }
    */


    /**************************************************************************
     *
     * helper to calculate the optimal sampling size in order to save
     * memory, e.g. reduce 1000x1000 to 128x128 if 120x120 is needed or to
     * 256x256 if 200 is needed.
     *
     *************************************************************************/
    private static int calculateInSampleSize
    (
        BitmapFactory.Options options,
        int reqWidth,
        int reqHeight
    )
    {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if ((height > reqHeight) || (width > reqWidth))
        {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth)
            {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Decode and sample down a bitmap from a file to the requested width and height.
     *
     * @param pathName The full path of the file to decode
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     *         that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromFile
    (
        final String pathName,
        int reqWidth,
        int reqHeight
    )
    {
        // Note: In Android 11, without MANAGE_EXTERNAL_STORAGE permission, album pictures
        // are not accessible. However, we could tell the mediascanner application to use
        // a non-system-compliant naming scheme for album images, e.g. "workaround.jpg". Possibly
        // there must be two images in each album folder, one "albumart.jpg" or "folder.jpg"
        // for Android and another one, like "workaround.jpg", for other applications.

        // We do not use decodeFile(), but directly decodeStream(), for performance reasons

        InputStream stream = null;
        Bitmap bm = null;
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            // First decode with inJustDecodeBounds = true to check dimensions
            options.inJustDecodeBounds = true;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                Path thePath = Paths.get(pathName);
                stream = Files.newInputStream(thePath);
                BitmapFactory.decodeStream(stream, null, options);
                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                // Then decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                // stream.reset();   // does not work, we have to recreate instead
                stream = Files.newInputStream(thePath);
            }
            else
            {
                //noinspection IOStreamConstructor
                stream = new FileInputStream(pathName);
                BitmapFactory.decodeStream(stream, null, options);
                // Calculate inSampleSize
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                // Then decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                // stream.reset();   // does not work, we have to recreate instead
                //noinspection IOStreamConstructor
                stream = new FileInputStream(pathName);

            }
            bm = BitmapFactory.decodeStream(stream, null, options);
        } catch (Exception e) {
            /*  do nothing.
                If the exception happened on open, bm will be null.
            */
            Log.e(LOG_TAG, "decodeSampledBitmapFromFile() : " + e);
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    // do nothing here
                }
            }
        }

        return bm;
    }

}
