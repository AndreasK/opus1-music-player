/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.util.Log;

import java.util.ArrayList;

import de.kromke.andreas.opus1musicplayer.PlayListHandler;

@SuppressWarnings("JavadocBlankLines")
public class Playlists
{
    static private final String LOG_TAG = "O1M : Playlists";


    /**************************************************************************
     *
     * find playlist by name
     *
     *************************************************************************/
    private static long findPlayList(final String name)
    {
        if (AudioFilters.audioPlaylistList == null)
        {
            return 0;
        }
        int size = AudioFilters.audioPlaylistList.raw.getSize();
        if (size <= 0)
        {
            return 0;
        }
        // get complete list of playlists
        ArrayList<AudioBaseObject> theList = AudioFilters.audioPlaylistList.raw.list;
        // check if name already exists
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject theObject = theList.get(i);
            if (theObject.name.equals(name))
            {
                return theObject.id;
            }
        }

        return 0;
    }


    /**************************************************************************
     *
     * add playlist and checks, if already exists
     *
     * return "false" if exists and shall not overwrite
     *
     *************************************************************************/
    public static boolean writePlaylistFromPlayingList(final String name, boolean overwrite)
    {
        FilteredAudioObjectList playingList = AudioFilters.audioPlayingList;
        int size = (playingList == null) ? 0 : playingList.raw.getSize();
        if (size <= 0)
        {
            Log.d(LOG_TAG, "writePlaylistFromPlayingList() : no items");
            return true;
        }

        long playlistId = findPlayList(name);   // 0: not found
        if (!overwrite && playlistId != 0)
        {
            return false;
        }

        if (playlistId >= 0)
        {
            // remove existing
            PrivateDbHandler.deletePlaylistRowEntry(playlistId);
            PrivateDbHandler.deletePlaylistHeaderEntry(playlistId);
        }

        // get num of tracks and duration
        int no_of_tracks = 0;
        long duration = 0;
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject theObject = playingList.raw.list.get(i);
            if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
            {
                no_of_tracks++;
                duration += theObject.duration;
            }
        }
        // save new list and get new id
        long newPlaylistId = PrivateDbHandler.insertPlaylistHeaderEntry(name, no_of_tracks, duration);
        // save items with new id
        PrivateDbHandler.insertPlaylistRowEntry(newPlaylistId, playingList.raw.list);
        // update GUI
        initAudioPlaylistsList();        // TODO: this is brute force, reloads everything
        return true;
    }


    /**************************************************************************
     *
     * delete playlist from list and from database
     *
     *************************************************************************/
    public static void deletePlaylist(final AudioPlaylist thePlaylist)
    {
        // remove rows
        PrivateDbHandler.deletePlaylistRowEntry(thePlaylist.id);
        // remove header
        PrivateDbHandler.deletePlaylistHeaderEntry(thePlaylist.id);
        // remove from GUI
        FilteredAudioObjectList playingList = AudioFilters.audioPlaylistList;
        if (playingList != null)
        {
            int size = playingList.raw.list.size();
            for (int i = 0; i < size; i++)
            {
                AudioBaseObject theObject = playingList.raw.list.get(i);
                if (theObject.id == thePlaylist.id)
                {
                    playingList.raw.list.remove(i);
                    AudioFilters.setPlaylistList(playingList.raw.list);     // force tidy up
                    return;
                }
            }
        }
    }


    /**************************************************************************
     *
     * write current playing list as playlist zero to database
     *
     *************************************************************************/
    static void writePlaylistZero()
    {
        Log.d(LOG_TAG, "writePlaylistZero()");
        final long playlistId = 0;

        // remove existing list, if any
        PrivateDbHandler.deletePlaylistRowEntry(playlistId);
        FilteredAudioObjectList playingList = AudioFilters.audioPlayingList;
        if ((playingList == null) || (playingList.raw.getSize() <= 0))
        {
            Log.d(LOG_TAG, "writePlaylistZero() : no items");
            return;
        }

        // save items
        PrivateDbHandler.insertPlaylistRowEntry(playlistId, playingList.raw.list);
    }


    /**************************************************************************
     *
     * Read tracks of a playlist given by id; id 0 is the current playing list.
     * For each entry, its music file path is extracted and looked up
     * in the list of all audio tracks. Entries, whose path could not be found
     * (e.g. because the path belongs to a different database or is in
     * SAF format instead of file format), are ignored.
     *
     *************************************************************************/
    static ArrayList<AudioBaseObject> readAudioPlaylist(long id)
    {
        ArrayList<PrivateDbHandler.playlistRow> theRowList = PrivateDbHandler.readPlaylistRowTable(id);
        int size = (theRowList == null) ? 0 : theRowList.size();

        int missing = 0;
        ArrayList<AudioBaseObject> tempList = new ArrayList<>();
        for (int i = 0; i < size; i++)
        {
            PrivateDbHandler.playlistRow theObject = theRowList.get(i);
            AudioTrack theTrack = AppGlobals.getAudioTrack(theObject.path);
            if (theTrack != null)
            {
                tempList.add(theTrack);
            }
            else
            {
                missing++;
                Log.d(LOG_TAG, "readAudioPlaylist() : path not found: " + theObject.path);
            }
        }

        if (missing > 0)
        {
            Log.w(LOG_TAG, "readAudioPlaylist() : " + missing + " tracks of playlist " + id + " could not be found and are ignored.");
        }

        return tempList;
    }


    /**************************************************************************
     *
     * get playlists from private db and initialise playing list with
     * playlist 0
     *
     * Note that all playlists may contain invalid entries in case the
     * database was changed. A path can be invalid or can have the wrong
     * format, e.g. when switched between File and Saf.
     *
     *  Warning: This runs in a background task!!!
     *
     *************************************************************************/
    public static void initAudioPlaylistsList()
    {
        // get complete list of playlists
        ArrayList<AudioBaseObject> theList = PrivateDbHandler.readPlaylistHeaderTable();
        AudioFilters.setPlaylistList(theList);

        // get all tracks of playlist 0, which is the playing list
        ArrayList<AudioBaseObject> tempList = readAudioPlaylist(0);
        Log.d(LOG_TAG, "initAudioPlaylistsList() : number of playing list items: " + tempList.size());

        // just initialise the playing list, do not check if the paths are valid
        if (!tempList.isEmpty())
        {
            AudioFilters.setPlayingList(tempList, false /* do not append */);
            PlayListHandler.normalisePlayingList();    // insert album and work objects
        }
    }
}
