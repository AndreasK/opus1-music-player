/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;
import androidx.annotation.NonNull;

import java.io.File;
import java.text.Normalizer;
import java.util.ArrayList;

import de.kromke.andreas.opus1musicplayer.R;

/**
 * Parent class of AudioAlbum, AudioComposer, AudioFolder, AudioGenre, AudioWork, AudioPerformer, AudioTrack
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioBaseObject implements Comparable<AudioBaseObject>
{
    public enum AudioObjectType
    {
        AUDIO_UNKNOWN_OBJECT,
        AUDIO_ALBUM_OBJECT,
        AUDIO_COMPOSER_OBJECT,
        AUDIO_FOLDER_OBJECT,
        AUDIO_GENRE_OBJECT,
        AUDIO_WORK_OBJECT,
        AUDIO_PERFORMER_OBJECT,
        AUDIO_TRACK_OBJECT,
        AUDIO_PLAYLIST_OBJECT
    }

    // static variables
    @SuppressWarnings("WeakerAccess")
    public static boolean sShowAlbumAndWorkDuration = false;
    public static int sWorkPictureMode = 0;
    public static String sOwnPersonsPicturePath = null;
    @SuppressWarnings("WeakerAccess")
    public static boolean sDecomposeComposerName = true;
    @SuppressWarnings("WeakerAccess")
    public static boolean sShowComposerLastNameFirst = true;
    @SuppressWarnings("WeakerAccess")
    public static boolean sShowAlbumYear = true;
    private static long lastindex = 0;   // for global object counting
    public static String str_album_uc = null;   // for creating info lines
    public static String str_parts = null;      // for creating info lines
    private static String str_track = null;      // for creating info lines
    private static String str_tracks = null;      // for creating info lines
    private static String str_work = null;      // for creating info lines
    private static String str_works = null;      // for creating info lines
    public static String str_folders_uc_dativ = null;      // for creating info lines
    public static String str_unknown_composer = null;
    public static String str_unknown_genre = null;
    public static String str_unknown_performer = null;
    @SuppressWarnings("WeakerAccess")
    public static String str_various_performers = null;

    // object attributes
    public AudioObjectType objectType;
    public long id;                 // from MediaStore.Audio.Albums._ID, not "ALBUM_ID" (Android BUG!)
    public long index;              // needed for filtered list to find position in original list
    public String name;             // album name, track title, composer name etc.
    public int no_of_tracks;
    @SuppressWarnings("WeakerAccess")
    public int no_of_works;
    @SuppressWarnings("WeakerAccess")
    public int no_of_matching_works;   //  in case of filters
    public long duration;
    public int alternating_colour;      // 0 or 1, used in list only
    protected String picture_path;
    private boolean mbPictureInitialised = false;

    public AudioBaseObject()
    {
        objectType = AudioObjectType.AUDIO_UNKNOWN_OBJECT;
        index = lastindex++;    // count the objects
        id = 0;
        name = null;
        no_of_tracks = 0;
        no_of_works = 0;
        duration = 0;
    }

    public static void SetContext(Context theContext, int genderismMode)
    {
        str_album_uc = theContext.getString(R.string.str_album_uc);
        str_parts = theContext.getString(R.string.str_parts);
        str_track = theContext.getString(R.string.str_track);
        str_tracks = theContext.getString(R.string.str_tracks);
        str_work = theContext.getString(R.string.str_work);
        str_works = theContext.getString(R.string.str_works);
        str_folders_uc_dativ = theContext.getString(R.string.str_folders_uc_dativ);
        str_unknown_composer = AppGlobals.getGenderedUnknownComposer(theContext, genderismMode);
        str_unknown_genre = theContext.getString(R.string.str_unknown_genre);
        str_unknown_performer = theContext.getString(R.string.str_unknown_performer);
        str_various_performers = theContext.getString(R.string.str_various_performers);

        //sShowAlbumAndWorkDuration = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_AND_OPUS_DURATION, false);
    }

    // public accessor
    public static void setShowAlbumDuration(boolean bShow)
    {
        sShowAlbumAndWorkDuration = bShow;
    }

    public static void setComposerDecompMode(boolean bDecomposeComposerName, boolean bShowComposerLastNameFirst)
    {
        sDecomposeComposerName = bDecomposeComposerName;
        sShowComposerLastNameFirst = sDecomposeComposerName && bShowComposerLastNameFirst;
    }

    /*
     * helper, returning "n tracks"
     */
    public String getTrackNoInfo()
    {
        String trackText = (no_of_tracks == 1) ? str_track : str_tracks;
        return no_of_tracks + " " + trackText;
    }

    /*
     * helper
     */
    @SuppressWarnings("WeakerAccess")
    public String getWorkNoInfo()
    {
        String workText = (no_of_matching_works == 1) ? str_work : str_works;
        String infoText;
        if (no_of_works == no_of_matching_works)
            infoText = no_of_matching_works + " " + workText;
        else
            infoText = no_of_matching_works + "/" + no_of_works + " " + workText;
        return infoText;
    }

    // for sorting
    /*
    @Override   POSSIBLE, BUT THE OTHER ONE IS PREFERRED
    public int compareTo(Object another)
    {
        return name.compareTo(((AudioBaseObject) another).name);
    }
    */

    @Override
    public int compareTo(@NonNull AudioBaseObject another)
    {
        return name.toUpperCase().compareTo(another.name.toUpperCase());
    }

    // to be overridden
    public long getDuration() { return duration; }

    // to be overridden
    public String getSection() { return name; }

    public String getDurationAsString()
    {
        long duration = getDuration();
        if (duration == 0)  // todo: find a better solution
            return "";      // maybe the background initialisation is still running
        else
            return "[" + Utilities.convertMsToHMmSs(getDuration()) + "]";
    }

    public String getName()
    {
        return name;
    }

    // for display in a list, normal text
    public String getTitle()
    {
        return name;
    }

    // for display in a list, small text
    public String getInfo()
    {
        return getTrackNoInfo();
    }

    // to be overridden
    public AudioComposer getComposer()
    {
        return null;
    }

    // to be overridden
    // TODO: remove hack
    public String getRawPath()
    {
        return null;
    }

    // to be overridden
    public String getPath()
    {
        return null;
    }

    // convert media Uris like "content://..." to a real path
    // to be overridden
    public String getRealPath(Context context)
    {
        return null;
    }

    // get composer name or "unknown composer"
    public final String getComposerName(boolean bReplaceEmptyWithUnknown)
    {
        AudioComposer theComposer = getComposer();
        final String name;
        if (theComposer == null)
        {
            name = "";
        }
        else
        {
            name = theComposer.getName();
        }
        return (bReplaceEmptyWithUnknown && name.isEmpty()) ? str_unknown_composer : name;
    }

    // get composer name or "unknown composer"
    public final String getPerformerName(boolean bReplaceEmptyWithUnknown)
    {
        AudioPerformer thePerformer = getPerformer();
        final String name;
        if (thePerformer == null)
        {
            name = "";
        }
        else
        {
            name = thePerformer.getName();
        }
        return (bReplaceEmptyWithUnknown && name.isEmpty()) ? str_unknown_performer : name;
    }

    // to be overridden
    public AudioPerformer getPerformer()
    {
        return null;
    }

    // to be overridden
    public ArrayList<AudioComposer>getComposerList()
    {
        return null;
    }

    // to be overridden
    public ArrayList<AudioPerformer>getPerformerList()
    {
        return null;
    }

    // to be overridden
    public AudioGenre getGenre()
    {
        return null;
    }

    // to be overridden
    public String getAlbumPicturePath(Context context)
    {
        return null;
    }

    // to be overridden
    protected void initPicturePath(Context context)
    {
        picture_path = null;
    }


    /**************************************************************************
     *
     * calculate picture path and return it
     *
     * Note that the context is used in SAF mode to find pictures
     *
     *************************************************************************/
    public String getPicturePath(Context context)
    {
        if (!mbPictureInitialised)
        {
            initPicturePath(context);
            mbPictureInitialised = true;
        }
        return picture_path;
    }


    /**************************************************************************
     *
     * remember that there is no picture, do not try it again
     *
     *************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public void setPicturePathInvalid()
    {
        picture_path = null;
        mbPictureInitialised = true;
    }


    /**************************************************************************
     *
     * Check if the object matches the given filter
     *
     * @param theFilterComposer    composer or null
     * @return true, iff the filter matches
     *
     *************************************************************************/
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer
    )
    {
        if (theFilterComposer == null)
        {
            return true;    // no filter, no mismatch
        }

        if (theFilterComposer == getComposer())
        {
            return true;    // match
        }

        ArrayList<AudioComposer> composerList = getComposerList();
        //noinspection RedundantIfStatement
        if ((composerList != null) && composerList.contains(theFilterComposer))
        {
            return true;    // match
        }

        return false;    // mismatch
    }


    /**************************************************************************
     *
     * Check if the object matches the given filter
     *
     * @param theFilterPerformer    performer or null
     * @return true, iff the filter matches
     *
     *************************************************************************/
    public boolean matchesFilter
    (
        AudioPerformer theFilterPerformer
    )
    {
        if (theFilterPerformer == null)
        {
            return true;    // no filter, no mismatch
        }

        if (theFilterPerformer == getPerformer())
        {
            return true;    // match
        }

        ArrayList<AudioPerformer> performerList = getPerformerList();
        //noinspection RedundantIfStatement
        if ((performerList != null) && performerList.contains(theFilterPerformer))
        {
            return true;    // match
        }

        return false;    // mismatch
    }


    /**************************************************************************
     *
     * Check if the object matches the given filter
     *
     * @param theFilterGenre    genre or null
     * @return true, iff the filter matches
     *
     *************************************************************************/
    public boolean matchesFilter
    (
        AudioGenre theFilterGenre
    )
    {
        if (theFilterGenre == null)
        {
            return true;    // no filter, no mismatch
        }

        return (theFilterGenre == getGenre());
    }


    /**************************************************************************
     *
     * Check if the object matches the given filters
     *
     * @param theFilterComposer     composer or null
     * @param theFilterPerformer    performer or null
     * @param theFilterGenre        genre or null
     * @return true, iff the filters match
     *
     * to be overridden
     *************************************************************************/
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        // we could make the class abstract, but it's easier to define a
        // pessimistic function here
        return false;
    }


    // helper function to check if theString matches a text search
    public static boolean matchString(final String theString, final String searchString, boolean bCaseSensitive)
    {
        if (theString == null)
            return false;

        if (bCaseSensitive)
            return theString.contains(searchString);
        else
            return theString.toUpperCase().contains(searchString);
    }


    /**************************************************************************
     *
     * Check, if the object matches the given search text
     *
     * @param searchString      the search text, in upper case if flag is "true"
     * @param bCaseSensitive    compare case sensitive or not
     * @return true, iff the search text matches
     *
     * to be overridden
     *************************************************************************/
    public boolean matchesText(final String searchString, boolean bCaseSensitive)
    {
        return matchString(name, searchString, bCaseSensitive);
    }


    /**************************************************************************
     *
     * Check, if the object symbolises a group of persons (composers or performers)
     * to be overridden
     *************************************************************************/
    public boolean isPersonGroup()
    {
        return false;
    }


    /**************************************************************************
     *
     * Helper, used by Performer and Composer
     *
     *************************************************************************/
    static String beautifyName(String name)
    {
        return (name == null) ? null : name.replace("\n", "; ");
    }


    /**************************************************************************
     *
     * Helper
     *
     *************************************************************************/
    private boolean checkName(String path, String theName)
    {
        File f = new File(path, theName + ".jpg");
        if (!f.isFile() || !f.canRead())
        {
            f = new File(path, theName + ".png");
        }
        if (!f.isFile() || !f.canRead())
        {
            picture_path = null;
        } else
        {
            picture_path = f.getPath();
        }
        return (picture_path != null);
    }


    /**************************************************************************
     *
     * Helper
     *
     *************************************************************************/
    private boolean checkPath(String path, String theComposerName)
    {
        if (!checkName(path, theComposerName))
        {
            // maybe it's a unicode normalisation problem?
            if (!Normalizer.isNormalized(theComposerName, Normalizer.Form.NFD))
            {
                // string is not decomposed, i.e. accented characters are NOT combinations of character and accent.
                // retry with decomposed string
                String decomposedName = Normalizer.normalize(theComposerName, Normalizer.Form.NFD);
                //Log.v(LOG_TAG, "addAudioComposer() -- retry name search with decomposed string: " + decomposedName);
                checkName(path, decomposedName);
            }
        }
        return (picture_path != null);
    }


    /**************************************************************************
     *
     * Helper, used by Performer and Composer
     *
     *************************************************************************/
    void initPicturePathForPersons()
    {
        String theComposerOrPerformerName = name;

        // first check aliases
        String aliasName = AppGlobals.sComposerAliasMap.get(name);
        if (aliasName != null)
        {
            theComposerOrPerformerName = aliasName;
        }

        if (sOwnPersonsPicturePath != null)
        {
            String[] pathArray = sOwnPersonsPicturePath.split("\n");
            for (String path : pathArray)
            {
                if (checkPath(path, theComposerOrPerformerName))
                {
                    break;
                }
            }
        } else
        {
            boolean bIsComposer = (this instanceof AudioComposer);
            picture_path = AppGlobals.getComposerOrPerformerPicture(theComposerOrPerformerName, bIsComposer);
            if (picture_path == null)
            {
                // maybe it's a unicode normalisation problem?
                if (!Normalizer.isNormalized(theComposerOrPerformerName, Normalizer.Form.NFD))
                {
                    // string is not decomposed, i.e. accented characters are NOT combinations of character and accent.
                    // retry with decomposed string
                    String decomposedName = Normalizer.normalize(theComposerOrPerformerName, Normalizer.Form.NFD);
                    //Log.v(LOG_TAG, "addAudioComposer() -- retry name search with decomposed string: " + decomposedName);
                    picture_path = AppGlobals.getComposerOrPerformerPicture(decomposedName, bIsComposer);
                }
            }
        }
    }
}
