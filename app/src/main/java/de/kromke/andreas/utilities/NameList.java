/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import java.util.ArrayList;

/**
 * Auxiliary class to handle composer and performer name lists, separated by "," or ";"
 */
@SuppressWarnings("JavadocBlankLines")
class NameList
{
    static boolean mbAllowCommaSeparator = false;
    static boolean mbAllowSemicolonSeparator = false;


    /**************************************************************************
     *
     * Accepts a string, denoting a list of persons (composers or performers), and returns
     * an array, if there is more than one, otherwise an empty list.
     *
     *************************************************************************/
    static ArrayList<String> splitToList(String theName)
    {
        ArrayList<String> nameList = new ArrayList<>();

        if (theName == null)
        {
            return nameList;
        }

        int first_index = 0;
        int paren_depth = 0;        // parenthesis depth
        int len = theName.length();
        for (int i = 0; i < len; i++)
        {
            char c = theName.charAt(i);
            if (c == '(')
            {
                paren_depth++;
            } else if (c == ')')
            {
                if (paren_depth > 0)
                {
                    paren_depth--;
                }
            } else if (isSeparatorChar(c) && (paren_depth == 0))
            {
                // delimiter found
                nameList.add(theName.substring(first_index, i));
                i++;    // skip delimiter
                first_index = i;
            }
        }

        // Add last part
        if (first_index > 0)
        {
            nameList.add(theName.substring(first_index, len));
        }

        return nameList;
    }


    /**************************************************************************
     *
     * check if a character is a list separator
     *
     *************************************************************************/
    static private boolean isSeparatorChar(char c)
    {
        return ((c == '\n') || (mbAllowSemicolonSeparator && (c == ';')) || (mbAllowCommaSeparator && (c == ',')));
    }
}
