/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.provider.BaseColumns;

/**
 * SQLite handling for all data (cached)
 *
 * Note: the database is stored in /data/data/de.kromke..../databases
 * Note: Android db is resided in  /data/data/com.android.providers.media/databases
 */
@SuppressWarnings("ALL")
public class MyMusicDatabase
{
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public MyMusicDatabase() {}

    /* Inner class that defines the table contents */
    @SuppressWarnings("WeakerAccess")
    public static abstract class MusicEntry implements BaseColumns
    {
        public static final String TABLE_NAME = "musicfiles";
        public static final String COLUMN_NAME_ENTRY_ID        = "entryid";             // track ID from Android music database
        public static final String COLUMN_NAME_TRACK_NO        = "trackno";
        public static final String COLUMN_NAME_TITLE           = "title";
        public static final String COLUMN_NAME_ALBUM           = "album";
        public static final String COLUMN_NAME_ALBUM_ID        = "album_id";            // added with database version 2
        public static final String COLUMN_NAME_DURATION        = "duration";
        public static final String COLUMN_NAME_GROUPING        = "grouping";            // the name of the complete piece, i.e. "Symphony No. 5"
        public static final String COLUMN_NAME_SUBTITLE        = "subtitle";            //  maybe like "op. 42"?
        public static final String COLUMN_NAME_COMPOSER        = "composer";            // the composer of the complete piece, i.e. Wolfgang Mozart
        public static final String COLUMN_NAME_PERFORMER       = "performer";           // performer(s) (called "artist" in subculture), i.e. NDR Radiosinfonie
        public static final String COLUMN_NAME_ALBUM_PERFORMER = "album_performer";     // added with database version 3
        public static final String COLUMN_NAME_CONDUCTOR       = "conductor";
        public static final String COLUMN_NAME_GENRE           = "genre";
        public static final String COLUMN_NAME_YEAR            = "year";
        public static final String COLUMN_NAME_PATH            = "path";
        public static final String COLUMN_NAME_MEDIA_URI       = "uri";                 // added with database version 4
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";      // SQLite uses 1, 2, 3, 4, 6, or 8 bytes depending on the magnitude of the value.
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + MusicEntry.TABLE_NAME + " (" +
                    MusicEntry._ID + " INTEGER PRIMARY KEY," +
                    MusicEntry.COLUMN_NAME_ENTRY_ID        + INT_TYPE  + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_TRACK_NO        + INT_TYPE  + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_TITLE           + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_ALBUM           + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_ALBUM_ID        + INT_TYPE  + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_DURATION        + INT_TYPE  + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_GROUPING        + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_SUBTITLE        + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_COMPOSER        + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_PERFORMER       + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_ALBUM_PERFORMER + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_CONDUCTOR       + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_GENRE           + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_YEAR            + INT_TYPE  + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_PATH            + TEXT_TYPE + COMMA_SEP +
                    MusicEntry.COLUMN_NAME_MEDIA_URI       + TEXT_TYPE +
            " )";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + MusicEntry.TABLE_NAME;
}
