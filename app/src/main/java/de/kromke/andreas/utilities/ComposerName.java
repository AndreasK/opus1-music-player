/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

/**
 * Auxiliary class to handle composer names
 */
@SuppressWarnings("JavadocBlankLines")
public class ComposerName
{
    private char c;
    private int pos;    // for pattern matching
    private boolean eos;
    String name;
    String birthAndDeathDates;

    /**************************************************************************
     *
     * accepts the whole string and splits it up to
     * name and birthAndDeathDates
     *
     *************************************************************************/
    public ComposerName(String theName, boolean bDecomposeFirstAndLastName)
    {
        if (theName == null)
        {
            theName = "";
        }

        // eliminate leading and trailing spaces
        name = theName.trim();

        // separate the life time substring

        int i = getDatePos();
        if (i >= 0)
        {
            // move "(lived from bla to blah)" to different string
            birthAndDeathDates = name.substring(i);
            name = name.substring(0, i);
            name = name.trim();
        }
        else
        {
            birthAndDeathDates = null;
        }

        // handle "Meier, Karl" or "Beethoven, Ludwig van"

        if (bDecomposeFirstAndLastName)
        {
            i = name.indexOf(",");
            int j = name.lastIndexOf(",");  // if there is more than one comma, i and j will differ
            if ((i >= 0) && (i == j))
            {
                String last = name.substring(0, i).trim();    // surname, without comma, without leading or trailing spaces
                String first = name.substring(i + 1).trim();    // first name, without comma, ...
                if (first.isEmpty() || last.isEmpty())
                {
                    name = first + last;  // one of these is empty, do not add a space here
                } else
                {
                    name = first + " " + last;  // recombine and make sure there is a single space between first name and surname
                }
            }
        }
    }

    private void nextChar()
    {
        if (!eos)
        {
            c = name.charAt(pos++);
        }
        else
        {
            c = 0;
        }
        eos = (pos >= name.length());
    }

    private boolean isNum(char c)
    {
        return (c >= '0') && (c <= '9');
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isQuestionMarkOrNumber()
    {
        // skip spaces
        skipSpaces();

        if (c == '?')
        {
            // skip '?'
            nextChar();
        } else if (isNum(c))
        {
            // get a sequence of numbers as birth date
            do
            {
                nextChar();
            } while (isNum(c));
        } else
        {
            // no match
            return false;
        }
        return true;
    }

    private void skipSpaces()
    {
        while(c == ' ')
        {
            nextChar();
        }
    }

    // Look for a birth and death date in brackets, like (1770-1827) or (*1830)
    // and return its position, if any
    private int getDatePos()
    {
        int bracketpos = name.lastIndexOf('(');
        if (bracketpos < 0)
        {
            // no opening character found, return -1
            return bracketpos;
        }

        eos = false;
        pos = bracketpos;

        // skip '('
        nextChar();
        nextChar();
        // skip spaces
        skipSpaces();
        if (eos)
        {
            // no match
            return -1;
        }

        // now there must be a sequence of numbers or an asterisk * or a question mark

        // skip '*', if exists
        if (c == '*')
        {
            // skip '*'
            nextChar();
            skipSpaces();
        }

        if (!isQuestionMarkOrNumber())
        {
            // no match
            return -1;
        }

        // we must find a '-', followed by a sequence of number or a question mark
        skipSpaces();
        if (c == '-')
        {
            // skip '-'
            nextChar();

            if (!isQuestionMarkOrNumber())
            {
                // no match
                return -1;
            }
        }

        skipSpaces();
        if (c != ')')
        {
            // no match
            return -1;
        }

        return bracketpos;
    }

    @SuppressWarnings({"unused", "RedundantSuppression"})
    public boolean isSame(final ComposerName another)
    {
        //noinspection SimplifiableIfStatement
        if ((name.isEmpty()) && (another.name.isEmpty()))
        {
            return true;
        }

        return (name.equals(another.name));
    }

}
