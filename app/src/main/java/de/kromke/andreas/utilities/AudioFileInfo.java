/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.util.Log;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.Tag;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * wrapper for jaudiotagger
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
public class AudioFileInfo
{
    static private final String LOG_TAG = "O1M : AudioFileInfo";
    private AudioFile mf = null;
    public static int noOfFileNotFoundErrors = 0;
    public static int noOfCannotReadErrors = 0;
    public static int noOfSuccess = 0;

    public static class AudioTags
    {
        public String tagTitle;         // is necessary for aac files where Android cannot read ID3 tags
        public int tagYear;
        public String tagComposer;
        public String tagConductor;
        public String tagGrouping;         // mp4: may hold ©wrk or ©grp, mp3: TIT2 or GRP1
        public String tagMovementName;     // mp4: ©mvi and ©mvn, mp3: MVNM and MVIN (up to "/")
        public String tagSubtitle;
        public String tagAlbum;
        public String tagAlbumArtist;
        public String tagDiscNo;
        public String tagDiscTotal;
        public String tagTrack;
        public String tagTrackTotal;
        public String tagGenre;             // English
    }

    public static class Info
    {
        public String absPath;
        public String type;
        public String format;
        public long bitRateInKbitPerSecond;
        public int bitsPerSample;
        public int channels;                    // number of channels (1 = mono, 2 = stereo)
        public String strChannels;              // number of channels as text, e.g. "Joint Stereo"
        public boolean lossless;
        public int durationInSeconds;
    }


    // constructor
    public AudioFileInfo(final String thisPath)
    {
        File theAudioFile = new File(thisPath);
        if (!theAudioFile.exists())
        {
            noOfFileNotFoundErrors++;
            Log.e(LOG_TAG, "does not exist: " + thisPath);
            return;
        }
        if (!theAudioFile.canRead())
        {
            noOfCannotReadErrors++;
            Log.e(LOG_TAG, "cannot read: " + thisPath);
            return;
        }
        open(theAudioFile/*, null*/);
    }


    // Try to open an jaudiotagger object with a file. In case of failure it may
    // be repeated with mp3 mode.
    private void open(File theFile/*, String openAs*/)
    {
        try
        {
//            mf = (openAs != null) ? AudioFileIO.readAs(theFile, openAs) : AudioFileIO.read(theFile);
            mf = AudioFileIO.read(theFile);
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "cannot find file " + theFile.getPath() + " because of: " + e.getMessage());
            mf = null;
        } catch (org.jaudiotagger.audio.exceptions.CannotReadException e)
        {
            Log.e(LOG_TAG, "cannot read audio file " + theFile.getPath() + " because of: " + e.getMessage());
            mf = null;
        } catch (org.jaudiotagger.audio.exceptions.ReadOnlyFileException e)
        {
            Log.e(LOG_TAG, "cannot open read-only audio file " + theFile.getPath() + " because of: " + e.getMessage());
            mf = null;
        } catch (org.jaudiotagger.tag.TagException e)
        {
            Log.e(LOG_TAG, "cannot read tag from audio file " + theFile.getPath() + " because of: " + e.getMessage());
            mf = null;
        } catch (org.jaudiotagger.audio.exceptions.InvalidAudioFrameException e)
        {
            Log.e(LOG_TAG, "invalid audio frame in " + theFile.getPath() + " because of: " + e.getMessage());
            mf = null;
        } catch (org.jaudiotagger.audio.exceptions.UnknownFilenameExtensionException e)
        {
            Log.e(LOG_TAG, "unknown file name extension of " + theFile.getPath() + " because of: " + e.getMessage());
            /*
            if (openAs != null)
            {
                // should not happen here
                mf = null;
            }
            */
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "cannot read file " + theFile.getPath() + " because of: " + e.getMessage());
            mf = null;
        }

        if (mf != null)
        {
            noOfSuccess++;
        }
    }

    /*
    * method to convert a movement number to a string with a roman numeral
    */
    @SuppressWarnings("StringConcatenationInLoop")
    static private String romanNumeral(int n)
    {
        String roman = "";

        if (n >= 40)
        {
            // fallback for large numbers: return arabic number representation
            return roman + n;
        }

        while (n >= 10)
        {
            roman += "X";
            n -= 10;
        }
        while (n >= 9)
        {
            roman += "IX";
            n -= 9;
        }
        while (n >= 5)
        {
            roman += "V";
            n -= 5;
        }
        while (n >= 4)
        {
            roman += "IV";
            n -= 4;
        }
        while (n >= 1)
        {
            roman += "I";
            n -= 1;
        }

        return roman;
    }

    /*
    * method to combine movement number and name
    */
    static private String movementNumberAndName(String movementName, String arabicNumber)
    {
        int n;

        if ((arabicNumber == null) || (arabicNumber.isEmpty()))
        {
            n = 0;
        }
        else
        {
            n = Utilities.convertToInt(arabicNumber, 0);
        }

        if (n > 0)
        {
            return romanNumeral(n) + ". " + movementName;
        }
        else
        {
            return movementName;
        }
    }

    /*
    * get tag safely, i.e. without crash
    * never return null, instead return ""
    */
    static private String getTagStringSafely(Tag tag, FieldKey id)
    {
        String theTagValue;
        try
        {
            theTagValue = tag.getFirst(id);
            if (theTagValue == null)
            {
                theTagValue = "";
            }
            else
            {
                theTagValue = theTagValue.trim();
            }
        } catch (KeyNotFoundException | UnsupportedOperationException e)
        {
            theTagValue = "";
        }
        return theTagValue;
    }

    /*
    * call tagger library, only needed when using Android db
    */
    public AudioTags getTags()
    {
        if (mf == null)
        {
            return null;    // file is not open
        }

        AudioTags ret = new AudioTags();
        // audio file information that cannot be written to:
        //AudioHeader ah = f.getAudioHeader();
        // audio file information that could be changed:
        Tag tag = mf.getTag();

        if (tag == null)
        {
            Log.w(LOG_TAG, "no audio tags for " + mf.getFile().getAbsolutePath());
            ret.tagTitle = null;
            ret.tagYear = 0;
            ret.tagComposer = null;
            ret.tagConductor = null;
            ret.tagGrouping = null;
            ret.tagMovementName = null;
            ret.tagSubtitle = null;
            ret.tagAlbum = null;
            ret.tagAlbumArtist = null;
            ret.tagDiscNo = null;
            ret.tagDiscTotal = null;
            ret.tagTrack = null;
            ret.tagTrackTotal = null;
            ret.tagGenre = null;
        }
        else
        {
            // mp4:shwm is currently not supported
            // mp4:©mvc is MOVEMENT_TOTAL and currently not needed
            // for mp3 files MOVEMENT_NO is "n/m" while for mp4 it is "n"

            String tagGroup = "";

            String tagMovementName = getTagStringSafely(tag, FieldKey.MOVEMENT);      // mp4: "©mvn", mp3: MVNM
            if (!tagMovementName.isEmpty())
            {
                String tagMovementNo = getTagStringSafely(tag, FieldKey.MOVEMENT_NO);     // mp4: "©mvi", mp3: MVIN
                /* CURRENTLY NOT NEEDED
                String tagMovementTotal = tag.getFirst(FieldKey.MOVEMENT_TOTAL);     // mp4: "©mvc", mp3: MVIN
                Log.v(LOG_TAG, "tagMovementTotal = " + tagMovementTotal);
                */
                //Log.v(LOG_TAG, "tagMovementName  = " + tagMovementName);
                //Log.v(LOG_TAG, "tagMovementNo    = " + tagMovementNo);
                tagMovementName = movementNumberAndName(tagMovementName, tagMovementNo);
                tagGroup = getTagStringSafely(tag, FieldKey.WORK);               // mp4: "©wrk"
                //Log.v(LOG_TAG, "WORK    = " + tagGroup);
            }

            if (tagGroup.isEmpty())
            {
                tagGroup = getTagStringSafely(tag, FieldKey.GROUPING);          // mp4: "©grp", mp3: TIT1
                //Log.v(LOG_TAG, "GROUPING    = " + tagGroup);
            }

            if (tagGroup.isEmpty())
            {
                tagGroup = getTagStringSafely(tag, FieldKey.ITUNES_GROUPING); // mp3: GRP1, mp4: does not exist
                //Log.v(LOG_TAG, "mp3:GRP1    = " + tagGroup);
            }
            // note: use trim() to remove leading and trailing spaces
            ret.tagTitle        = getTagStringSafely(tag, FieldKey.TITLE);
            ret.tagComposer     = getTagStringSafely(tag, FieldKey.COMPOSER);
            ret.tagConductor    = getTagStringSafely(tag, FieldKey.CONDUCTOR);
            ret.tagGrouping     = tagGroup;
            ret.tagMovementName = tagMovementName;
            ret.tagSubtitle     = getTagStringSafely(tag, FieldKey.SUBTITLE);
            ret.tagAlbum        = getTagStringSafely(tag, FieldKey.ALBUM);
            ret.tagAlbumArtist  = getTagStringSafely(tag, FieldKey.ALBUM_ARTIST);
            ret.tagDiscNo       = getTagStringSafely(tag, FieldKey.DISC_NO);
            ret.tagDiscTotal    = getTagStringSafely(tag, FieldKey.DISC_TOTAL);
            ret.tagTrack        = getTagStringSafely(tag, FieldKey.TRACK);
            ret.tagTrackTotal   = getTagStringSafely(tag, FieldKey.TRACK_TOTAL);
            ret.tagGenre        = getTagStringSafely(tag, FieldKey.GENRE);
            ret.tagYear         = Utilities.convertToInt(getTagStringSafely(tag, FieldKey.YEAR), 0);

            //Log.v(LOG_TAG, "path           = " + thisPath);
            //Log.v(LOG_TAG, "tagGenre       = " + tag.getFirst(FieldKey.GENRE));
        }

        // iTunes hack
        if ((ret.tagGenre != null) && (ret.tagGenre.equals("(32)")))
        {
            ret.tagGenre = "Classical";
        }
/*
        Log.v(LOG_TAG, "tagComposer    = " + ret.tagComposer);
        Log.v(LOG_TAG, "tagConductor   = " + ret.tagConductor);
        Log.v(LOG_TAG, "tagGrouping    = " + ret.tagGrouping);
        Log.v(LOG_TAG, "tagSubtitle    = " + ret.tagSubtitle);
        Log.v(LOG_TAG, "tagAlbum       = " + ret.tagAlbum);
        Log.v(LOG_TAG, "tagAlbumArtist = " + ret.tagAlbumArtist);
        Log.v(LOG_TAG, "tagDiscNo      = " + ret.tagDiscNo);
        Log.v(LOG_TAG, "tagDiscNoTotal = " + ret.tagDiscTotal);
        Log.v(LOG_TAG, "tagTrack       = " + ret.tagTrack);
        Log.v(LOG_TAG, "tagTrackTotal  = " + ret.tagTrackTotal);
        Log.v(LOG_TAG, "tagGenre       = " + ret.tagGenre);
        Log.v(LOG_TAG, "=====================================\n\n");
*/

        // on Nexus S with Dalvik: try to force garbage collection here.
        //            (android:largeHeap="true" is not sufficient)
        // This is not necessary with Nexus 5

        System.gc();

        return ret;
    }


    /**************************************************************************
     *
     * get detailed audio file info
     *
     *************************************************************************/
    public Info getInfo()
    {
        if (mf == null)
        {
            return null;
        }

        AudioHeader header = mf.getAudioHeader();
        if (header == null)
        {
            return null;
        }
        Log.v(LOG_TAG, "channels = " + header.getChannels());

        Info ret = new Info();
        ret.absPath = mf.getFile().getAbsolutePath();
        ret.type = header.getEncodingType();
        ret.format = header.getFormat();
        ret.bitRateInKbitPerSecond = header.getBitRateAsNumber();
        ret.bitsPerSample = header.getBitsPerSample();
        ret.strChannels = header.getChannels();
        switch(ret.strChannels)
        {
            case "Mono":
                ret.channels = 1;
                break;

            case "2":
                ret.strChannels = "Stereo";
            case "Stereo":
            case "Joint Stereo":
                ret.channels = 2;
                break;

            default:
                ret.channels = Utilities.convertToInt(ret.strChannels, 1);
                break;
        }
        ret.lossless = header.isLossless();
        ret.durationInSeconds = header.getTrackLength();

        return ret;
    }
}
