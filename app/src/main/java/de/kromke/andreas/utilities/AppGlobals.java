/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import androidx.annotation.NonNull;
import de.kromke.andreas.opus1musicplayer.PlayListHandler;
import de.kromke.andreas.opus1musicplayer.R;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * global variables, shared by all activities
 * @noinspection RedundantSuppression
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines", "JavadocLinkAsPlainText"})
public class AppGlobals
{
    static private final String LOG_TAG = "O1M : AppGlobals";
    // global settings
    //static final boolean ignore_notifications_and_ringtones = true; // otherwise they appear as music...
    private static final int limit_number_of_tracks = 999999;                  // for debugging

    public static final long bookmarkRepeatMs = 5000;      // repeat the last seconds when continuing the track
    public static final long bookmarkMinimumMs = 0; //10000;    // do not store bookmarks for the first seconds

    // all tracks on device
    private static ArrayList<AudioTrack> audioTrackList = null;

    @SuppressWarnings("rawtypes")
    public static AsyncTask theBackgroundInitialisation = null;       // this reads the track table, while running
    public static boolean bBackgroundInitialisationDone = false;
    public static ArrayList<AudioBaseObject> sVirtualAlbumsList = null;  // albums added during background initialisation
    //public static int sNumOfFilteredFiles = 0;                           // files skipped due to path filter
    public static int sNumOfTracksTotal = -1;                           // written by worker thread
    public static int sNumOfTracksProcessed = -1;

    private static boolean mMergeAlbumsOfSameName;
    private static int mHandlePersonsLists;
    private static ContentResolver mResolver;
    private static DatabaseManager mDatabaseManager = null;
    //static private long mLatestAndroidDbImagesFileAdded = 0;
    public static String sOwnDatabasePath = "";             // TODO: repair this hack for downloading composer pictures
    public static boolean bOwnDatabaseIsActive = false;   // failure state
    public static boolean bOwnSafDatabaseIsActive = false;

    public static Map<String, String> sComposerAliasMap = new HashMap<>();

    // localised strings
    private static String m_str_composer_uc         = "";
    private static String m_str_genre_uc            = "";
    private static String m_str_performer_uc        = "";
    private static String m_str_genre_classical     = "";
    private static String m_str_genre_baroque       = "";
    private static String m_str_genre_soundtrack    = "";
    private static String m_str_genre_symphony      = "";
    private static String m_str_genre_sonata        = "";
    private static String m_str_genre_chamber_music = "";
    private static String m_str_genre_chorus        = "";
    private static String m_str_genre_opera         = "";
    private static String m_str_genre_speech        = "";
    private static String m_str_genre_neoclassical  = "";
    private static String m_str_genre_audiobook     = "";
    private static String m_str_genre_other         = "";


    /**************************************************************************
     *
     * initialisation
     *
     * usually reads the internal db, but in case bCreateNewDb == true it
     * will recreate it.
     *
     *************************************************************************/
    static public void initAppGlobals
    (
        Context context,
        ContentResolver musicResolver,
        boolean bCreateNewDb,
        String filterPaths,             // black/whitelist for Android db
        boolean bMergeAlbumsOfSameName,         // only for Android db
        int handlePersonsLists,
        boolean bUseOwnDatabase,
        boolean bOwnDatabaseIsSaf,
        //boolean bUseJaudioTaglib, /* jaudiotagger removed */
        boolean bUsePaths,              // for Android DB: always use paths, not Uri
        final String ownDatabasePath
    )
    {
        mHandlePersonsLists = handlePersonsLists;
        NameList.mbAllowSemicolonSeparator = (handlePersonsLists > 1);
        NameList.mbAllowCommaSeparator = (handlePersonsLists > 2);
        mResolver = musicResolver;

        // get localised strings for later use
        m_str_composer_uc         = context.getString(R.string.str_composer_uc);
        m_str_genre_uc            = context.getString(R.string.str_genre_uc);
        m_str_performer_uc        = context.getString(R.string.str_performer_uc);
        m_str_genre_classical     = context.getString(R.string.str_genre_classical);
        m_str_genre_baroque       = context.getString(R.string.str_genre_baroque);
        m_str_genre_soundtrack    = context.getString(R.string.str_genre_soundtrack);
        m_str_genre_symphony      = context.getString(R.string.str_genre_symphony);
        m_str_genre_sonata        = context.getString(R.string.str_genre_sonata);
        m_str_genre_chamber_music = context.getString(R.string.str_genre_chamber_music);
        m_str_genre_chorus        = context.getString(R.string.str_genre_chorus);
        m_str_genre_opera         = context.getString(R.string.str_genre_opera);
        m_str_genre_speech        = context.getString(R.string.str_genre_speech);
        m_str_genre_neoclassical  = context.getString(R.string.str_genre_neoclassical);
        m_str_genre_audiobook     = context.getString(R.string.str_genre_audiobook);
        m_str_genre_other         = context.getString(R.string.str_genre_other);

        if ((bUseOwnDatabase) && (mDatabaseManager == null))
        {
            mMergeAlbumsOfSameName = false;
            mDatabaseManager = new DatabaseManagerOwn(ownDatabasePath, limit_number_of_tracks);
            if (!mDatabaseManager.exists())
            {
                Log.e(LOG_TAG, "initAppGlobals() : cannot open own database, falling back to Android db.");
                mDatabaseManager = null;
            }
        }

        if (mDatabaseManager != null)
        {
            bOwnDatabaseIsActive = true;
            bOwnSafDatabaseIsActive = bOwnDatabaseIsSaf;
        }
        else
        {
            bOwnDatabaseIsActive = false;
            mMergeAlbumsOfSameName = bMergeAlbumsOfSameName;
            mDatabaseManager = new DatabaseManagerAndroid(
                                        mResolver,
                                        filterPaths,
                                        mMergeAlbumsOfSameName,
                                        limit_number_of_tracks,
                                        // bUseJaudioTaglib,    /* jaudiotagger removed */
                                        bUsePaths);
        }

        if (bCreateNewDb)
        {
            audioTrackList = null;
            theBackgroundInitialisation = null;
        }

        // open private database and get modification date
        MyMusicDatabaseHandler.open(context, bCreateNewDb, bOwnDatabaseIsActive);
        // open playlist and bookmark database
        PrivateDbHandler.open(context, false);

        /*
        mLatestAndroidDbImagesFileAdded = getLastDateAdded(MediaStore.Images.Media.EXTERNAL_CONTENT_URI) * 1000;
        Log.v(LOG_TAG, "Android images db modification date = " + mLatestAndroidDbImagesFileAdded + " (" + convertMsToIso(mLatestAndroidDbImagesFileAdded) + ")");
        */
    }


    /**************************************************************************
     *
     * application exits or is going to restart. Avoid some warnings here.
     *
     *************************************************************************/
    static public void exit()
    {
        if (mDatabaseManager != null)
        {
            mDatabaseManager.close();
            mDatabaseManager = null;
        }
    }

    /**************************************************************************
     *
     * player must know if db is Android or own
     *
     *************************************************************************/
    public static boolean isAndroidDb()
    {
        return (mDatabaseManager instanceof DatabaseManagerAndroid);
    }


    /**************************************************************************
     *
     * get background task progress information in percent
     *
     *************************************************************************/
    public static int getBackGroundInitialisationProgressPercent()
    {
        if (mDatabaseManager == null)
        {
            return 0;
        }

        int total = mDatabaseManager.getTracksTotal();
        int done = mDatabaseManager.getTracksDone();
        if (total == 0)
        {
            return 0;
        }
        //Log.d(LOG_TAG, "progress: done = " + done + ", total = " + total);

        // heuristics: assume 50 % for the database read phase and 50 % for building internal structures
        int dbReadPercent = (done * 100 + total/2) / total;
        if ((done < total) || (sNumOfTracksProcessed <= 0))
        {
            return (dbReadPercent + 1) / 2;
        }
        int buildStructsPercent = (sNumOfTracksProcessed * 100 + sNumOfTracksTotal/2) / sNumOfTracksTotal;
        return 50 + (buildStructsPercent + 1) / 2;
    }


    /* *************************************************************************
     *
     * get background task progress information
     *
     *************************************************************************/
    /*
    public static int getBackGroundInitialisationNumberOfTracksTotal()
    {
        if (mDatabaseManager == null)
        {
            return 0;
        }
        else
        {
            return mDatabaseManager.getTracksTotal();
        }
    }
     */


    /* *************************************************************************
     *
     * get background task progress information
     *
     *************************************************************************/
    /*
    public static int getBackGroundInitialisationNumberOfTracksDone()
    {
        if (mDatabaseManager == null)
        {
            return 0;
        }
        else
        {
            return mDatabaseManager.getTracksDone();
        }
    }
     */


    /**************************************************************************
     *
     * get background task progress information
     *
     *************************************************************************/
    public static boolean getBackgroundInitialisationFailedDueToAndroidBug()
    {
        if (mDatabaseManager == null)
        {
            return false;
        }
        else
        {
            return mDatabaseManager.getBackgroundInitialisationFailedDueToAndroidBug();
        }
    }


    /**************************************************************************
     *
     * get total number of tracks and -1, if not initialised
     *
     *************************************************************************/
    static public int getAudioTrackListSize()
    {
        return (audioTrackList == null) ? -1 : audioTrackList.size();
    }


    /**************************************************************************
     *
     * get track by path. Used when evaluating an intent or building a play list
     *
     *************************************************************************/
    static public AudioTrack getAudioTrack(final String path)
    {
        int size = getAudioTrackListSize();
        for (int i = 0; i < size; i++)
        {
            AudioTrack theTrack = audioTrackList.get(i);
            if (path.equals(theTrack.mMediaUri) || (path.equals(theTrack.getRawPath())))
            {
                return theTrack;
            }
        }

        return null;    // not found
    }


    /**************************************************************************
     *
     * get all albums (from either Android db or own one)
     *
     * Contrary to function retrieving all tracks this does not run early in
     * the background, but directly from the fragment when it's needed.
     * Further, it does not use the private db.
     *
     *************************************************************************/
    static public void initAudioAlbumList()
    {
        if (!AudioFilters.audioAlbumList.isInitialised())
        {
            ArrayList<AudioBaseObject> tempList;
            if (mDatabaseManager != null)
            {
                tempList = mDatabaseManager.queryAlbums();
            }
            else
            {
                tempList = null;
            }

            // replace null with empty list to avoid null pointer exceptions
            if (tempList == null)
            {
                Log.w(LOG_TAG, "no album found, album list is empty");
                tempList = new ArrayList<>();
            }

            AudioFilters.setAlbumList(tempList);      // assign in one step to be thread safe
            Log.d(LOG_TAG, "found " + AudioFilters.audioAlbumList.getTotalSize() + " albums");
        }
    }


    /**************************************************************************
     *
     * Check if object is in playing list.
     *
     *************************************************************************/
    static public boolean isInPlayingList(final AudioBaseObject theObject)
    {
        int index = AudioFilters.audioPlayingList.findInRawList(theObject);
        return (index >= 0);
    }


    /**************************************************************************
     *
     * get list of corresponding tracks for object
     *
     *************************************************************************/
    static public @NonNull ArrayList<AudioBaseObject> getTracksOfObject(final AudioBaseObject theObject)
    {
        ArrayList<AudioBaseObject> tempList = new ArrayList<>();

        switch(theObject.objectType)
        {
            case AUDIO_ALBUM_OBJECT:
                AudioAlbum theAlbum = (AudioAlbum) theObject;
                // then add the tracks, i.e. all tracks of each work
                for (int i = 0; i < theAlbum.mWorkList.size(); i++)
                {
                    AudioWork theWork = theAlbum.mWorkList.get(i);
                    tempList.addAll(theWork.mTrackList);
                }
                break;

            case AUDIO_TRACK_OBJECT:
                AudioTrack theTrack = (AudioTrack) theObject;
                tempList.add(theTrack);
                break;

            case AUDIO_WORK_OBJECT:
                AudioWork theWork = (AudioWork) theObject;
                // then add the tracks belonging to work
                tempList.addAll(theWork.mTrackList);
                break;

            case AUDIO_FOLDER_OBJECT:
                AudioFolder theFolder = (AudioFolder) theObject;
                // then add the tracks belonging to folder
                tempList.addAll(theFolder.mTrackList);
                break;

            case AUDIO_PLAYLIST_OBJECT:
                tempList = Playlists.readAudioPlaylist(theObject.id);
                break;

            default:
                break;
        }

        return tempList;
    }


    /**************************************************************************
     *
     * copy given object list to the "tracks" list
     *
     * The "tracks" list contains one complete album or work, its
     * objects can then be copied to the playing list.
     *
     *************************************************************************/
    static public void copyObjectToTracks(final ArrayList<AudioBaseObject> theObjectList)
    {
        if (audioTrackList == null)
        {
            // there is something basically wrong
            return;
        }

        ArrayList<AudioBaseObject> addList = new ArrayList<>();
        for (AudioBaseObject theObject : theObjectList)
        {
            addList.addAll(getTracksOfObject(theObject));
        }
        AudioFilters.setTrackList(addList);
        PlayListHandler.normaliseTracksList();        // (re-)arrange album and work rows
    }


    /**************************************************************************
     *
     * copy given object to the "tracks" list
     *
     * The "tracks" list contains one complete album or work, its
     * objects can then be copied to the playing list.
     *
     *************************************************************************/
    static public void copyObjectToTracks(final AudioBaseObject theObject)
    {
        if (audioTrackList == null)
        {
            // there is something basically wrong
            return;
        }

        ArrayList<AudioBaseObject> tempList = getTracksOfObject(theObject);
        AudioFilters.setTrackList(tempList);
        PlayListHandler.normaliseTracksList();        // (re-)arrange album and work rows
    }


    /**************************************************************************
     *
     * put or append the given object(s) to the playing list
     *
     *************************************************************************/
    static public void playObjects(final AudioBaseObject[] theObjects, boolean bAppend)
    {
        if (audioTrackList == null)
        {
            // there is something basically wrong
            return;
        }

        for (AudioBaseObject theObject : theObjects)
        {
            ArrayList<AudioBaseObject> tempList = getTracksOfObject(theObject);
            AudioFilters.setPlayingList(tempList, bAppend);
            // if there is more than one in the list, append the others:
            bAppend = true;
        }
        PlayListHandler.normalisePlayingList();        // (re-)arrange album and work rows
        Playlists.writePlaylistZero();    // save permanently
    }


    /* *************************************************************************
     *
     * put or append the given object to the playing list
     *
     *************************************************************************/
    /*
    static public void playObject(final AudioBaseObject theObject, boolean bAppend)
    {
        if (audioTrackList == null)
        {
            // there is something basically wrong
            return;
        }

        ArrayList<AudioBaseObject> tempList = getTracksOfObject(theObject);
        AudioFilters.SetPlayingList(tempList, bAppend);
        PlayListHandler.normalisePlayingList();        // (re-)arrange album and work rows
        Playlists.writePlaylistZero();    // save permanently
    }
     */


    /**************************************************************************
     *
     * initialise group_no and group_last for each track in a list
     * and return the duration sum
     *
     *************************************************************************/
    @SuppressWarnings("UnusedReturnValue")
    static private long setGroupAttributes(ArrayList<AudioTrack> theList)
    {
        AudioTrack prevTrack = null;
        int noInGroup = 0;
        long duration = 0;
        for (AudioTrack theTrack: theList)
        {
            if (!theTrack.isSameGroup(prevTrack))
            {
                // this is the first group or a new one in the sorted list or there is no group
                noInGroup = 0;
                if (prevTrack != null)
                {
                    prevTrack.group_last = true;
                }
            }

            theTrack.group_no = noInGroup++;
            theTrack.group_last = false;
            duration += theTrack.duration;
            prevTrack = theTrack;
        }

        if (prevTrack != null)
        {
            prevTrack.group_last = true;
        }

        return duration;
    }


    /**************************************************************************
     *
     * long running command to be started in background, if possible
     *
     *  Warning: This runs in a background task!!!
     *
     *************************************************************************/
    static public void loadAllTracksTable()
    {
        if (audioTrackList == null)
        {
            // this runs in the background task
            ArrayList<AudioTrack> tempList;
            if (mDatabaseManager != null)
            {
                //sNumOfFilteredFiles = 0;
                tempList = mDatabaseManager.queryTracks();
            }
            else
            {
                tempList = null;
            }
            // replace null with empty list to avoid null pointer exceptions
            if (tempList == null)
            {
                Log.w(LOG_TAG, "no track found, track list is empty");
                tempList = new ArrayList<>();
            }

            audioTrackList = tempList;  // assign in one step to be thread safe
            setGroupAttributes(audioTrackList);
        }
    }


    /**************************************************************************
     *
     * wait for background task
     *
     * return value: 0 (OK), 1 (timeout),
     *
     * Called when user tries to open an album  while
     * database is being initialised in background (asynchronously).
     *
     *************************************************************************/
    static public int waitForBackgroundTaskWithTimeout()
    {
        int ret = 0;

        try
        {
            theBackgroundInitialisation.get(1000, MILLISECONDS);
        }
        catch (TimeoutException e)
        {
            Log.w(LOG_TAG, "background task is still busy:" + e.getMessage());
            ret = 1;
        }
        catch (CancellationException | InterruptedException | ExecutionException e)
        {
            Log.e(LOG_TAG, "error while waiting for background task:" + e.getMessage());
            ret = -1;
        }

        return ret;
    }


    /**************************************************************************
     *
     * wait for background task
     *
     * Called when user tries to switch to Genre or Folder fragment while
     * database is being initialised in background (asynchronously).
     *
     *************************************************************************/
    static public void waitForBackgroundTask()
    {
        try
        {
            theBackgroundInitialisation.get();
        } catch (InterruptedException | ExecutionException e)
        {
            Log.e(LOG_TAG, "error while waiting for background task:" + e.getMessage());
        }
    }


    /**************************************************************************
     * helper function to add a composer and avoid double entries
     * <p/>
     * Note that this is called once for each work found, so we can
     * count the works here.
     *************************************************************************/
    static private AudioComposer addAudioComposer
    (
        ArrayList<AudioBaseObject> tempComposerList,
        String theName,
        boolean bAllowEmpty,
        boolean bIsMoreThanOnePerson
    )
    {
        AudioComposer theComposer = null;

        if (bAllowEmpty || !theName.isEmpty())
        {
            // split name and life period etc.
            ComposerName theComposerName = new ComposerName(theName, AudioBaseObject.sDecomposeComposerName);

            for (AudioBaseObject object : tempComposerList)
            {
                AudioComposer p = (AudioComposer) object;
                if (p.name.equals(theComposerName.name))
                {
                    // already in list
                    theComposer = p;
                    theComposer.no_of_works++;   // count number of works TODO: unnecessary
                    break;
                }
            }

            if (theComposer == null)
            {
                // composer not yet in list
                String picturePath = null;
                /*
                if (AudioBaseObject.sOwnPersonsPicturePath == null)
                {
                    picturePath = getComposerOrPerformerPicture(theComposerName.name, true);
                    if (picturePath == null)
                    {
                        // maybe it's a unicode normalisation problem?
                        if (!Normalizer.isNormalized(theComposerName.name, Normalizer.Form.NFD))
                        {
                            // string is not decomposed, i.e. accented characters are NOT combinations of character and accent.
                            // retry with decomposed string
                            String decomposedName = Normalizer.normalize(theComposerName.name, Normalizer.Form.NFD);
                            //Log.v(LOG_TAG, "addAudioComposer() -- retry name search with decomposed string: " + decomposedName);
                            picturePath = getComposerOrPerformerPicture(decomposedName, true);
                        }
                    }
                }
                 */

                // create new object
                //noinspection ConstantConditions
                theComposer = new AudioComposer(theComposerName.name, theComposerName.birthAndDeathDates, picturePath, 1, bIsMoreThanOnePerson);
                tempComposerList.add(theComposer);
            }
        }

        return theComposer;
    }


    /**************************************************************************
     * helper function to add a composer and avoid double entries
     * <p/>
     * Note that this is called once for each work found, so we can
     * count the works here.
     *************************************************************************/
    static private void addAudioComposer
    (
        ArrayList<AudioBaseObject> tempComposerList,
        AudioTrack theTrack,
        AudioWork theWork
    )
    {
        boolean bIsMoreThanOnePerson = false;

        if (mHandlePersonsLists > 0)
        {
            ArrayList<String> composerNames = NameList.splitToList(theTrack.composer);
            for (String theName : composerNames)
            {
                // add a single composer in the list
                AudioComposer theComposer = addAudioComposer(tempComposerList, theName, false, false);
                if (theComposer != null)
                {
                    theWork.mComposerList.add(theComposer);
                    theComposer.mTrackList.add(theTrack);
                    bIsMoreThanOnePerson = true;
                }
            }
        }

        String theName = theTrack.composer;
        if (theName == null)
        {
            theName = "";
        }

        // add the composer group
        AudioComposer theComposer = addAudioComposer(tempComposerList, theName, true, bIsMoreThanOnePerson);

        // link composer to track
        theTrack.composer = null;                  // This string is no longer needed. Free it.
        theComposer.mTrackList.add(theTrack);

        theWork.mComposer = theComposer;
    }


    /**************************************************************************
     * helper function to add a performer and avoid double entries
     *
     * Note that this is called once for each work found, so we can
     * count the works here.
     *************************************************************************/
    static private AudioPerformer addAudioPerformer
    (
        ArrayList<AudioBaseObject> tempPerformerList,
        String theName,
        boolean bAllowEmpty,
        boolean bIsMoreThanOnePerson
    )
    {
        theName = theName.trim();
        AudioPerformer thePerformer = null;
        if (bAllowEmpty || !theName.isEmpty())
        {
            for (AudioBaseObject object: tempPerformerList)
            {
                AudioPerformer p = (AudioPerformer) object;
                if (p.name.equals(theName))
                {
                    // already in list
                    thePerformer = p;
                    thePerformer.no_of_works++;   // count number of works TODO: unnecessary
                    break;
                }
            }

            if (thePerformer == null)
            {
                // performer not yet in list, create new performer object
                thePerformer = new AudioPerformer(theName, 1, bIsMoreThanOnePerson);
                tempPerformerList.add(thePerformer);
            }
        }

        return thePerformer;
    }


    /**************************************************************************
     * helper function to add a performer and avoid double entries
     *
     * Note that this is called once for each work found, so we can
     * count the works here.
     *************************************************************************/
    static private void addAudioPerformer(ArrayList<AudioBaseObject> tempPerformerList, AudioTrack theTrack)
    {
        boolean bIsMoreThanOnePerson = false;

        if (mHandlePersonsLists > 0)
        {
            ArrayList<String> performerNames = NameList.splitToList(theTrack.artist);
            for (String theName : performerNames)
            {
                AudioPerformer thePerformer = addAudioPerformer(tempPerformerList, theName, false, false);
                if (thePerformer != null)
                {
                    theTrack.mPerformerList.add(thePerformer);
                    thePerformer.mTrackList.add(theTrack);
                    bIsMoreThanOnePerson = true;
                }
            }
        }

        // add complete string as performer name
        String theName = (theTrack.artist != null) ? theTrack.artist : "";
        AudioPerformer thePerformer = addAudioPerformer(tempPerformerList, theName, true, bIsMoreThanOnePerson);

        // link performer to track and vice versa
        theTrack.mPerformer = thePerformer;
        theTrack.artist = null;                  // This string is no longer needed. Free it.
        if (thePerformer != null)
        {
            thePerformer.mTrackList.add(theTrack);
        }
    }


    /**************************************************************************
     * helper function to add a music genre and avoid double entries
     * <p/>
     * Note that this is called once for each work found, so we can
     * count the works here.
     *************************************************************************/
    static private void addAudioGenre
    (
        ArrayList<AudioBaseObject> tempGenreList,
        AudioTrack theTrack
    )
    {
        String theName = theTrack.genre;
        if (theName == null)
        {
            theName = "";
        }

        AudioGenre theGenre = null;
        for (AudioBaseObject object: tempGenreList)
        {
            AudioGenre p = (AudioGenre) object;
            if (p.name.equals(theName))
            {
                // already in list
                theGenre = p;
                theGenre.no_of_works++;   // count number of works TODO: unnecessary
                break;
            }
        }

        if (theGenre == null)
        {
            // none found. create a new one

            // localise
            String theLocalisedName;
            switch(theName)
            {
                case "Classical":     theLocalisedName = m_str_genre_classical; break;
                case "Baroque":       theLocalisedName = m_str_genre_baroque; break;
                case "Soundtrack":    theLocalisedName = m_str_genre_soundtrack; break;
                case "Symphony":      theLocalisedName = m_str_genre_symphony; break;
                case "Sonata":        theLocalisedName = m_str_genre_sonata; break;
                case "Chamber Music": theLocalisedName = m_str_genre_chamber_music; break;
                case "Chorus":        theLocalisedName = m_str_genre_chorus; break;
                case "Opera":         theLocalisedName = m_str_genre_opera; break;
                case "Speech":        theLocalisedName = m_str_genre_speech; break;
                case "Neoclassical":  theLocalisedName = m_str_genre_neoclassical; break;
                case "Audiobook":     theLocalisedName = m_str_genre_audiobook; break;
                case "Other":         theLocalisedName = m_str_genre_other; break;
                default: theLocalisedName = theName;    // do not translate that stuff...
            }

            // create new genre object
            theGenre = new AudioGenre(theName, theLocalisedName, 1);
            tempGenreList.add(theGenre);
        }

        // link genre to track and vice versa
        theTrack.mGenre = theGenre;
        theTrack.genre = null;                  // This string is no longer needed. Free it.
        theGenre.mTrackList.add(theTrack);
    }


    /**************************************************************************
     *
     * helper function to get the album artist and year
     *
     * If not explicitly specified, we try to find a common artist by
     * checking all tracks.
     *
     * Not used for own db
     *
     *************************************************************************/
    private static void addTrackInfoToAlbum
    (
        AudioAlbum theAlbum,
        String album_artist, String artist, int year
    )
    {
        assert(theAlbum != null);
        if (album_artist != null)
        {
            if (theAlbum.album_artist == null)
            {
                // take first non-null value
                //TODO: consistency check
                theAlbum.album_artist = album_artist;
                //Log.e(LOG_TAG, "addTrackInfoToAlbum(): Set album artist " + album_artist);
            }
        }

        theAlbum.setCommonArtist(artist);
        theAlbum.setCommonYear(year);
        //Log.d(LOG_TAG, "addTrackInfoToAlbum(): Set year for Album " + theAlbum.name + ": " + year);
    }


    /**************************************************************************
     * creates a new album for orphan tracks
     *
     * Note that we may not just extend AudioFilters.audioAlbumList.raw.list,
     * because we are running in a background thread and this list is being
     * used in the main (UI) thread.
     *
     *************************************************************************/
    private static AudioAlbum addVirtualAlbum(long id, String name)
    {
        AudioAlbum theAlbum = new AudioAlbum(
                id,
                name,
                "unknown artist",
                1,  //        int no_of_tracks,
                0, //int first_year,
                0, // int last_year,
                null // String the_picture_path)
                );

        if (sVirtualAlbumsList == null)
        {
            sVirtualAlbumsList = new ArrayList<>();
        }
        sVirtualAlbumsList.add(theAlbum);
        return theAlbum;
    }


    /**************************************************************************
     * helper helper function to add a work to the album object
     *************************************************************************/
    private static void addWorkToAlbum(AudioAlbum theAlbum, AudioWork theWork)
    {
        theAlbum.mWorkList.add(theWork);
        theWork.mAlbum = theAlbum;
    }


    /**************************************************************************
     *
     * Find a matching album and add a work to the album object
     *
     *************************************************************************/
    private static void addWorkToAlbum
    (
        final String album,
        AudioWork theWork,
        long theAlbumId
    )
    {
        AudioAlbum foundAlbum = null;

        int size = AudioFilters.audioAlbumList.getTotalSize();
        int size2 = (sVirtualAlbumsList == null) ? 0 : sVirtualAlbumsList.size();

        //
        // find album by name (if configured)
        //

        if (mMergeAlbumsOfSameName)
        {
            for (int i = 0; i < size; i++)
            {
                AudioAlbum theAlbum = (AudioAlbum) AudioFilters.audioAlbumList.raw.list.get(i);
                if (theAlbum.name.equals(album))
                {
                    foundAlbum = theAlbum;
                    break; // there should be only one match
                }
            }

            if (foundAlbum == null)
            {
                for (int i = 0; i < size2; i++)
                {
                    AudioAlbum theAlbum = (AudioAlbum) sVirtualAlbumsList.get(i);
                    if (theAlbum.name.equals(album))
                    {
                        foundAlbum = theAlbum;
                        break; // there should be only one match
                    }
                }
            }

            if (foundAlbum == null)
            {
                Log.e(LOG_TAG, "addWorkToAlbum(): No album name match for \" + album + \". Trying id instead.");
            }
        } // if bmergeAlbumsOfSameName

        //
        // find album by id
        //

        if (foundAlbum == null)
        {
            for (int i = 0; i < size; i++)
            {
                AudioAlbum theAlbum = (AudioAlbum) AudioFilters.audioAlbumList.raw.list.get(i);
                if (theAlbum.id == theAlbumId)
                {
                    foundAlbum = theAlbum;
                    break; // there should be only one match
                }
            }
        }

        if (foundAlbum == null)
        {
            for (int i = 0; i < size2; i++)
            {
                AudioAlbum theAlbum = (AudioAlbum) sVirtualAlbumsList.get(i);
                if (theAlbum.id == theAlbumId)
                {
                    foundAlbum = theAlbum;
                    break; // there should be only one match
                }
            }
        }

        if (foundAlbum == null)
        {
            //
            // add virtual album for orphaned files
            //

            Log.w(LOG_TAG, "addWorkToAlbum(): No album match for \"" + album + "\" (" + theAlbumId + "). Create virtual album.");
            foundAlbum = addVirtualAlbum(theAlbumId, album);
        }

        addWorkToAlbum(foundAlbum, theWork);
    }


    /**************************************************************************
     * helper function to add a folder and avoid double entries
     *
     * Note that any track belonging to the folder is added to the folder's
     * track list.
     *************************************************************************/
    static private void addAudioFolder(ArrayList<AudioBaseObject> tempList, final AudioBaseObject theTrack)
    {
        String thePath = theTrack.getRawPath();     // should be either path or SAF Uri
        String uriStr = null;
        if (SafUtilities.isSafPath(thePath))
        {
            uriStr = thePath;
            Uri theUri = Uri.parse(uriStr);
            thePath = theUri.getPath();
            if (thePath == null)
            {
                thePath = "/tree/INVALID";
            }
        }

        // go from document to its containing folder
        thePath = SafUtilities.getDirectoryPath(thePath);
        uriStr = SafUtilities.getDirectoryUri(uriStr);

        for (AudioBaseObject object: tempList)
        {
            AudioFolder theObject = (AudioFolder) object;
            if (theObject.getPath().equals(thePath))
            {
                // already in list
                theObject.no_of_tracks++;   // count number of files TODO: redundant
                theObject.mTrackList.add(theTrack);    // add audio track to existing folder
                return;
            }
        }
        // folder not yet in list
        Log.d(LOG_TAG, "addAudioFolder(): path \"" + thePath + "\".");
        AudioFolder theObject = new AudioFolder(thePath, uriStr, 1);
        theObject.mTrackList.add(theTrack);    // add audio track to new folder
        tempList.add(theObject);
    }


    /**************************************************************************
     *
     * get all composers, performers and genres
     *
     *  Warning: This runs in a background task!!!
     *
     *************************************************************************/
    static public void initAudioComposersPerformersGenreWorkFolderList()
    {
        if ((AudioFilters.audioComposerList.getTotalSize() == -1) &&
            (AudioFilters.audioPerformerList.getTotalSize() == -1) &&
            (AudioFilters.audioGenreList.getTotalSize() == -1) &&
            (AudioFilters.audioWorkList.getTotalSize() == -1) &&
            (AudioFilters.audioFolderList.getTotalSize() == -1))
        {
            ArrayList<AudioBaseObject> tempComposerList = new ArrayList<>();
            ArrayList<AudioBaseObject> tempPerformerList = new ArrayList<>();
            ArrayList<AudioBaseObject> tempGenreList = new ArrayList<>();
            ArrayList<AudioBaseObject> tempWorkList = new ArrayList<>();
            ArrayList<AudioBaseObject> tempFolderList = new ArrayList<>();

            AudioTrack theFirstTrack = null;
            AudioWork theWork = null;
            AudioAlbum theAlbum = null;

            sNumOfTracksTotal = getAudioTrackListSize();        // read by main thread
            sNumOfTracksProcessed = 0;
            sVirtualAlbumsList = null;

            for (AudioTrack theTrack: audioTrackList)
            {
                // ++ DEBUG CODE
                /*
                if (theTrack.album.startsWith("Conan "))
                {
                    Log.d(LOG_TAG, "initAudioComposersPerformersGenreList(): trackname = " + theTrack.name);
                }
                */
                // -- DEBUG CODE
                addAudioFolder(tempFolderList, theTrack);
                if (theTrack.group_no == 0)
                {
                    // Here we only handle tracks with group number 0, i.e. the first
                    // track of each work. Thus we count works, not tracks.
                    // We also ignore the possibility that a performer differs among
                    // tracks of a work.

                    //Log.w(LOG_TAG, "initAudioComposersPerformersGenreList(): trackname = " + theTrack.name);

                    theFirstTrack = theTrack;

                    //
                    // create a new work
                    //

                    theWork = new AudioWork(
                            theTrack.id,
                            theTrack.duration,
                            (theTrack.grouping == null) ? theTrack.name : theTrack.grouping,
                            1);

                    //
                    // sort tracks into filter lists and create new entries if necessary
                    //

                    addAudioComposer(tempComposerList, theFirstTrack, theWork);
                    addAudioPerformer(tempPerformerList, theFirstTrack);
                    addAudioGenre(tempGenreList, theFirstTrack);

                    //
                    // put the new work into album list
                    //

                    // link work and track and composer in all directions
                    theWork.mTrackList.add(theFirstTrack);
                    theTrack.mWork = theWork;

                    tempWorkList.add(theWork);

                    addWorkToAlbum(theTrack.album, theWork, theTrack.album_id);
                    theAlbum = theTrack.mWork.mAlbum;
                    if (theAlbum != null)
                    {
                        theAlbum.setCommonComposer(theWork.mComposer);

                        if (isAndroidDb())
                        {
                            // this information is part of own db, therefore we do not have to add it here
                            addTrackInfoToAlbum(theAlbum, theTrack.album_artist, theTrack.mPerformer.name, theTrack.year);
                        }
                    }
                }
                else
                {
                    // the other tracks of the same group share work, composer, performer and genre
                    if (theFirstTrack != null)
                    {
                        theTrack.mWork = theWork;
                        //theTrack.grouping = null;         // no longer needed
                        theTrack.composer    = null;    // no longer needed, save memory
                        theTrack.mPerformer  = theFirstTrack.mPerformer;
                        theTrack.mGenre      = theFirstTrack.mGenre;
                        theTrack.genre       = null;    // no longer needed, save memory
                    }
                    else
                    {
                        Log.e(LOG_TAG, "initAudioComposersPerformersGenreList(): theFirstTrack shall not be null");
                    }

                    if (theWork != null)
                    {
                        theWork.mTrackList.add(theTrack);
                        theWork.duration += theTrack.duration;
                        theWork.no_of_tracks++;     // TODO: redundant
                    }
                    else
                    {
                        Log.e(LOG_TAG, "initAudioComposersPerformersGenreList(): theWork shall not be null");
                    }

                    theTrack.artist = null;                 // no longer needed, save memory
                }

                if ((theAlbum != null) && (isAndroidDb()))
                {
                    // This information is part of own db, therefore we do not have to add it here.
                    // In fact we need it for orphaned audio files, i.e. those with virtual album.
                    theAlbum.setNoOfTracks();       // count works and tracks, necessary for orphan albums
                }

                theTrack.album_artist = null;           // no longer needed!
                sNumOfTracksProcessed++;                // read by main thread
            }

            // sort by name
            Collections.sort(tempComposerList);
            AudioFilters.setComposerList(tempComposerList);
            Collections.sort(tempPerformerList);
            AudioFilters.setPerformerList(tempPerformerList);
            Collections.sort(tempGenreList);
            AudioFilters.setGenreList(tempGenreList);
            Collections.sort(tempWorkList);
            AudioFilters.setWorkList(tempWorkList);
            AudioFilters.setFolderList(tempFolderList);
        }
    }


    /**************************************************************************
     *
     * build composer aliases map
     *
     *  Warning: This runs in a background task!!!
     *
     *************************************************************************/
    static public void buildComposerAliasesMap()
    {
        if (AudioBaseObject.sOwnPersonsPicturePath != null)
        {
            String[] pathArray = AudioBaseObject.sOwnPersonsPicturePath.split("\n");
            for (String path : pathArray)
            {
                File aliasesFileDir = new File(path);
                if (aliasesFileDir.exists() && aliasesFileDir.canRead())
                {
                    File aliasesFile = new File(aliasesFileDir, "aliases.txt");
                    if (aliasesFile.exists() && aliasesFile.canRead())
                    {
                        try
                        {
                            FileInputStream is = new FileInputStream(aliasesFile);
                            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                            String line;
                            String originalName = null;      // format is: one original name, then aliases, finally an empty line
                            while((line = reader.readLine()) != null)
                            {
                                //Log.d(LOG_TAG, "buildComposerAliasesMap() : read line: " + line);
                                if (line.isEmpty())
                                {
                                    originalName = null;
                                }
                                else
                                if (originalName == null)
                                {
                                    originalName = line;
                                }
                                else
                                {
                                    Log.d(LOG_TAG, "buildComposerAliasesMap() : create alias: " + line + " -> " + originalName);
                                    sComposerAliasMap.put(line, originalName);
                                }
                            }
                            is.close();
                        } catch (FileNotFoundException e)
                        {
                            Log.d(LOG_TAG, "buildComposerAliasesMap() : file not found");
                        } catch (IOException e)
                        {
                            Log.d(LOG_TAG, "buildComposerAliasesMap() : cannot read");
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Log.w(LOG_TAG, "buildComposerAliasesMap() : no alias file \"" + aliasesFile.getPath() + "\"");
                    }
                }
            }
        }
    //    sComposerAliasMap.put("Antonin Dvorak", "Antonín Dvořák");
    //   sComposerAliasMap.put("Anton Dvorak", "Antonín Dvořák");
    //    sComposerAliasMap.put("Felix Mendelssohn", "Felix Mendelssohn Bartholdy");
    //    sComposerAliasMap.put("Wolfgang Mozart", "Wolfgang Amadeus Mozart");
    //    sComposerAliasMap.put("Modest Mussorgsky", "Modest Petrowitsch Mussorgski");
    }


    /* *************************************************************************
     * debug helper to query all images
     *************************************************************************/
    /*
    static public void getAllPictures()
    {
        // query external audio:
        //  Media       or
        //  Genres      or
        //  Playlists   or
        //  Artists     or
        //  Albums      or
        //  Radio
        final String[] columns =
        {
            MediaStore.Images.Media._ID,
            // from base class MediaColumns:
            MediaStore.Images.Media.DATA,  // here: the path
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        };

        // this is kind of SQL query:
        Cursor theCursor = mResolver.query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                columns,    // columns to return, null returns all rows
                null,      // rows to return, as "WHERE ...", null returns all rows
                null, //whereVal,   // selection arguments, replacing question marks in previous argument
                null        // sort order, null: unordered
        );
        // handle error cases
        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return;
        }
        if (!theCursor.moveToFirst())
        {
            theCursor.close();
            return;
        }

        // from class MediaColumns:
        int idColumn = theCursor.getColumnIndex(MediaStore.Images.Media._ID);
        int dataColumn = theCursor.getColumnIndex(MediaStore.Images.Media.DATA);  // data stream
        int displaynameColumn = theCursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
        int sizeColumn = theCursor.getColumnIndex(MediaStore.Images.Media.SIZE);

        do
        {
            long thisId = theCursor.getLong(idColumn);
            String thisPath = theCursor.getString(dataColumn);
            String thisDisplayname = theCursor.getString(displaynameColumn);
            long thisSize = theCursor.getInt(sizeColumn);

            Log.v(LOG_TAG, "thisId          = " + thisId);
            Log.v(LOG_TAG, "thisPath        = " + thisPath);
            Log.v(LOG_TAG, "thisDisplayname = " + thisDisplayname);
            Log.v(LOG_TAG, "thisSize        = " + thisSize);
            Log.v(LOG_TAG, "==============================================\n");

            showStringAsBytes(thisPath);
            showStringAsBytes(thisDisplayname);
        }
        while (theCursor.moveToNext());
        theCursor.close();
    }
*/


    /**************************************************************************
     * get composer picture
     *************************************************************************/
    public static String getComposerOrPerformerPicture(final String name, boolean bIsComposer)
    {
        if ((name == null) || (name.isEmpty()))
        {
            return null;
        }

        // query external audio:
        //  Media       or
        //  Genres      or
        //  Playlists   or
        //  Artists     or
        //  Albums      or
        //  Radio
        final String[] columns =
        {
            MediaStore.Images.Media._ID,
            // from base class MediaColumns:
            MediaStore.Images.Media.DATA,  // here: the path
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        };
        // make sure name does not contain '
        String _name = name.replace("'", "''");
//        final String where = MediaStore.Images.Media.DATA + " LIKE '%" + _name + ".___'";
        final String where = MediaStore.Images.Media.DISPLAY_NAME + " LIKE '" + _name + ".%'";
        //final String whereVal[] = {_name};

        // this is kind of SQL query:
        Cursor theCursor;
        try
        {
            theCursor = mResolver.query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    columns,    // columns to return, null returns all rows
                    where,      // rows to return, as "WHERE ...", null returns all rows
                    null, //whereVal,   // selection arguments, replacing question marks in previous argument
                    null        // sort order, null: unordered
            );
        }
        catch (android.database.sqlite.SQLiteException e)
        {
            Log.e(LOG_TAG, "SQL exception for " + (bIsComposer ? "composer" : "performer") + " \"" + name + "\" found");
            theCursor = null;
        }
        // handle error cases
        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }

        if (!theCursor.moveToFirst())
        {
            //Log.v(LOG_TAG, "no image for " + (bIsComposer ? "composer" : "performer") + " \"" + name + "\" found");
            /*
            if (bIsComposer)
            {
                String decomposedName = Normalizer.normalize(name, Normalizer.Form.NFD);
                showStringAsBytes(name);
                showStringAsBytes(decomposedName);

            ///// DEBUG STUFF
            // -> http://www.fileformat.info/info/charset/UTF-16/list.htm
            // -> http://www.fileformat.info/info/unicode/char/0301/index.htm
            // -> http://stackoverflow.com/questions/7710448/anyone-can-explain-the-same-thing-issue-of-utf-8?rq=1
                showStringAsBytes(name);
                // c3 ad in UTF-8 is 00ed in UTF-16
                Log.v(LOG_TAG, "replace " + "\u00ed" + " with " + "i\u0301" + ".");

                String newString = name.replace("\u00ed", "i\u0301");
                showStringAsBytes(newString);
                getAllPictures();
            }
            */
            ///// END DEBUG STUFF
            theCursor.close();
            return null;
        }

        // from class MediaColumns:
        int dataColumn = theCursor.getColumnIndex(MediaStore.Images.Media.DATA);  // data stream

        String thisPath = theCursor.getString(dataColumn);

        /*
        int idColumn = theCursor.getColumnIndex(MediaStore.Images.Media._ID);
        int displaynameColumn = theCursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
        int sizeColumn = theCursor.getColumnIndex(MediaStore.Images.Media.SIZE);

        long thisId = theCursor.getLong(idColumn);
        String thisDisplayname = theCursor.getString(displaynameColumn);
        long thisSize = theCursor.getInt(sizeColumn);

        Log.v(LOG_TAG, "thisId          = " + thisId);
        Log.v(LOG_TAG, "thisPath        = " + thisPath);
        Log.v(LOG_TAG, "thisDisplayname = " + thisDisplayname);
        Log.v(LOG_TAG, "thisSize        = " + thisSize);
        Log.v(LOG_TAG, "==============================================\n");
        */

        theCursor.close();

        return thisPath;
    }


    /**************************************************************************
     * helper functions
     *************************************************************************/
    public static String getComposerFilterText(AudioComposer theComposer)
    {
        return m_str_composer_uc + ": " + theComposer.getTitle();
    }
    public static String getGenreFilterText(AudioGenre theGenre)
    {
        return m_str_genre_uc + ": " + theGenre.getTitle();
    }
    public static String getPerformerFilterText(AudioPerformer thePerformer)
    {
        return m_str_performer_uc + ": " + thePerformer.getTitle();
    }


    /**************************************************************************
     *
     * get gendered version of "conductor" according to genderism mode
     *
     *************************************************************************/
    static String getGenderedConductor(Context context, int genderismMode)
    {
        final int[] resTable =
        {
            R.string.str_conductor_gen,         // 0
            R.string.str_conductor_abbrev,
            R.string.str_conductor_ess,
            R.string.str_conductor_fm,
            R.string.str_conductor_mf,          // 4
            R.string.str_conductor_capI,
            R.string.str_conductor_underscore,
            R.string.str_conductor_x,           // 7
            R.string.str_conductor_f,
            R.string.str_conductor_star         // 9
        };

        if (genderismMode >= resTable.length)
        {
            genderismMode = 0;
        }
        return context.getString(resTable[genderismMode]);
    }


    /**************************************************************************
     *
     * get gendered version of "unknown composer" according to genderism mode
     *
     *************************************************************************/
    static String getGenderedUnknownComposer(Context context, int genderismMode)
    {
        final int[] resTable =
        {
            R.string.str_unknown_composer_gen,         // 0
            R.string.str_unknown_composer_abbrev,
            R.string.str_unknown_composer_ess,
            R.string.str_unknown_composer_fm,
            R.string.str_unknown_composer_mf,          // 4
            R.string.str_unknown_composer_capI,
            R.string.str_unknown_composer_underscore,
            R.string.str_unknown_composer_x,           // 7
            R.string.str_unknown_composer_f,
            R.string.str_unknown_composer_star         // 9
        };

        if (genderismMode >= resTable.length)
        {
            genderismMode = 0;
        }
        return context.getString(resTable[genderismMode]);
    }


    /**************************************************************************
     *
     * get gendered version of "Composers" according to genderism mode
     *
     *************************************************************************/
    public static String getGenderedComposers(Context context, int genderismMode)
    {
        final int[] resTable =
        {
            R.string.str_composers_uc_gen,         // 0
            R.string.str_composers_uc_abbrev,
            R.string.str_composers_uc_ess,
            R.string.str_composers_uc_fm,
            R.string.str_composers_uc_mf,          // 4
            R.string.str_composers_uc_capI,
            R.string.str_composers_uc_underscore,
            R.string.str_composers_uc_x,           // 7
            R.string.str_composers_uc_f,
            R.string.str_composers_uc_star         // 9
        };

        if (genderismMode >= resTable.length)
        {
            genderismMode = 0;
        }
        return context.getString(resTable[genderismMode]);
    }


    /**************************************************************************
     *
     * get gendered version of "Performers" according to genderism mode
     *
     *************************************************************************/
    public static String getGenderedPerformers(Context context, int genderismMode)
    {
        final int[] resTable =
        {
            R.string.str_performers_uc_gen,         // 0
            R.string.str_performers_uc_abbrev,
            R.string.str_performers_uc_ess,
            R.string.str_performers_uc_fm,
            R.string.str_performers_uc_mf,          // 4
            R.string.str_performers_uc_capI,
            R.string.str_performers_uc_underscore,
            R.string.str_performers_uc_x,           // 7
            R.string.str_performers_uc_f,
            R.string.str_performers_uc_star         // 9
        };

        if (genderismMode >= resTable.length)
        {
            genderismMode = 0;
        }
        return context.getString(resTable[genderismMode]);
    }

    /* *************************************************************************
     *
     * Create composer name aliases
     *
     *************************************************************************/
    /*
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void createAliases(File destDir)
    {
        createAlias(destDir, "Antonín Dvořák.jpg", "Antonin Dvorak.jpg");
        createAlias(destDir, "Antonín Dvořák.jpg", "Anton Dvorak.jpg");
    }
    */
}
