/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;



/**
 * all necessary information about a playlist
 */
@SuppressWarnings("WeakerAccess")
public class AudioPlaylist extends AudioBaseObject
{
//    ArrayList<AudioTrack> mTrackList = new ArrayList<>();  // objects of that playlist

    public AudioPlaylist(
            long id,
            String name,
            int no_of_tracks,
            long duration)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_PLAYLIST_OBJECT;
        this.id = id;
        this.name = name;
        this.no_of_tracks = no_of_tracks;
        this.duration = duration;
    }


    // for display in a list, small text
    @Override
    public String getInfo()
    {
        String infoText = "";
        infoText += "(" + getTrackNoInfo();
        long duration = getDuration();
        if (duration > 0)
        {
            infoText += ", " + Utilities.convertMsToHMmSs(getDuration());
        }
        infoText += ")";
        return infoText;
    }

/*
    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        if ((theFilterComposer == null) && (theFilterPerformer == null) && (theFilterGenre == null))
        {
            return true;
        }

        int size = mTrackList.size();
        for (int i = 0; i < size; i++)
        {
            AudioTrack theObject = mTrackList.get(i);
            if (theObject.matchesFilter(theFilterComposer, theFilterPerformer, theFilterGenre))
            {
                // one matching work is sufficient
                return true;
            }
        }

        return false;    // no matches
    }
*/
}
