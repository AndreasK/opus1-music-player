/*
 * Copyright (C) 2016-2023 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Build;
import android.util.Log;

import static android.media.AudioDeviceInfo.*;
import static android.media.AudioManager.*;

// MediaSessionCompat, PlaybackstateCompat and MediaMetadata not necessary for API 21
@SuppressLint("ObsoleteSdkInt")
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
@TargetApi(21)
public class MediaPlayBtInfo
{
    private static final String LOG_TAG = "O1M : MediaPlayBtInfo";
    private static final String AVRCP_PLAYSTATE_CHANGED = "com.android.music.playstatechanged";
    private static final String AVRCP_META_CHANGED = "com.android.music.metachanged";

    private long mAudioId;
    private boolean mAudioIsPlaying;
    private String mAudioArtist;
    private String mAudioAlbum;
    private String mComposerAndWork;
    private String mAudioTrack;
    private String mAudioComposer;
    private Drawable mAudioAlbumCover;   // may be null
    private long mAudioDuration;
    private long mAudioPosition;
    private int mAudioListPosition;
    private int mAudioListSize;
    private int hack;

    private final MediaSession mMediaSession;   // MediaSession available with API 21 (5.0)
    // media session flags:
    //  transport controls: callback functions like onPlay(), onPause()
    //  media buttons: if not set, car radio gets confused
    static final int sFlags = MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS |
                              MediaSession.FLAG_HANDLES_MEDIA_BUTTONS;
    @SuppressWarnings({"FieldCanBeLocal", "unused", "RedundantSuppression"})
    private boolean mBtConnected;       // debug helper
    // all our capabilities for the remote connection, PlaybackState needs API 21
    static final long sActions = PlaybackState.ACTION_PLAY |
                                PlaybackState.ACTION_PAUSE |
                                PlaybackState.ACTION_PLAY_PAUSE |
                                PlaybackState.ACTION_SKIP_TO_NEXT |
                                PlaybackState.ACTION_SKIP_TO_PREVIOUS |
                                PlaybackState.ACTION_SEEK_TO |
                                PlaybackState.ACTION_STOP;

    // interface (in fact kind a set of callback functions)
    MediaPlayBtRemoteController mMediaPlayBtRemoteController;
    public interface MediaPlayBtRemoteController
    {
        void onMediaControlPlayState(boolean isplay);
        void onMediaControlSkip(int direction);
        void onMediaControlSeek(long position);
        void onMediaControlStop();
    }


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    public MediaPlayBtInfo
    (
        Context context,
        MediaPlayBtRemoteController controller
    )
    {
        mMediaPlayBtRemoteController = controller;
        mAudioIsPlaying = false;

        // WARNING: For Android 4.4 one must register android.support.v4.media.session.MediaButtonReceiver
        //          in manifest. See https://stackoverflow.com/questions/44408617/android-crash-on-boot-mediabuttonreceiver-may-not-be-null
        mMediaSession = new MediaSession(context, "Opus1MediaSession");

        mMediaSession.setCallback(new MediaSession.Callback()
        {
            @Override
            public void onPlay()
            {
                Log.d(LOG_TAG, "media session play");
                if (mMediaPlayBtRemoteController != null)
                {
                    mMediaPlayBtRemoteController.onMediaControlPlayState(true);
                }
            }
            @Override
            public void onPause()
            {
                Log.d(LOG_TAG, "media session pause");
                if (mMediaPlayBtRemoteController != null)
                {
                    mMediaPlayBtRemoteController.onMediaControlPlayState(false);
                }
            }
            @Override
            public void onSkipToNext()
            {
                Log.d(LOG_TAG, "media session next");
                if (mMediaPlayBtRemoteController != null)
                {
                    mMediaPlayBtRemoteController.onMediaControlSkip(1);
                }
            }
            @Override
            public void onSkipToPrevious()
            {
                Log.d(LOG_TAG, "media session previous");
                if (mMediaPlayBtRemoteController != null)
                {
                    mMediaPlayBtRemoteController.onMediaControlSkip(-1);
                }
            }
            @Override
            public void onSeekTo(long pos)
            {
                Log.d(LOG_TAG, "media session seek to " + pos);
                if (mMediaPlayBtRemoteController != null)
                {
                    mMediaPlayBtRemoteController.onMediaControlSeek(pos);
                }
            }
            @Override
            public void onStop()
            {
                Log.d(LOG_TAG, "media session stop");
                if (mMediaPlayBtRemoteController != null)
                {
                    mMediaPlayBtRemoteController.onMediaControlStop();
                }
            }
        });

        // These lines seem to be unnecessary for remote control via car radio
        /*
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(context.getPackageName(), MediaPlayBtInfo.class.getName()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        mMediaSession.setMediaButtonReceiver(pendingIntent);
        */

        mMediaSession.setFlags(sFlags);
        mMediaSession.setActive(true);
    }


    /**************************************************************************
     *
     * Set object parameters
     *
     *************************************************************************/
    public void Init
    (
        Context context,
        long AudioId,
        String AudioArtist,
        String AudioAlbum,
        String ComposerAndWork,
        String AudioTrack,
        String AudioComposer,
        Drawable AudioAlbumCover,
        long duration,
        int AudioListPosition,      // 0..n-1
        int AudioListSize           // n
    )
    {
        mAudioId           = AudioId;
        mAudioArtist       = AudioArtist;
        mAudioAlbum        = AudioAlbum;
        mComposerAndWork   = ComposerAndWork;
        mAudioTrack        = AudioTrack;
        mAudioComposer     = AudioComposer;
        mAudioAlbumCover   = AudioAlbumCover;
        mAudioDuration     = duration;
        mAudioPosition     = 0;
        mAudioListPosition = AudioListPosition;
        mAudioListSize     = AudioListSize;

        mAudioIsPlaying = false;
        hack = 3;

        // debug the alternative Bluetooth metadata styles
        Log.d(LOG_TAG, "album     = " + mAudioAlbum);
        Log.d(LOG_TAG, "title     = " + mAudioTrack);
        Log.d(LOG_TAG, "performer = " + mAudioArtist);
        Log.d(LOG_TAG, "track " + mAudioListPosition + " / " + mAudioListSize);

        //noinspection SimplifiableIfStatement
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            mBtConnected = isBtConnected(context);
        }
        else
        {
            mBtConnected = true;
        }

        sendMetaData(context);
    }


    /**************************************************************************
     *
     * debug helper, in fact we do not care if connected or not
     *
     *************************************************************************/
    @TargetApi(23)
    static private boolean isBtConnected
    (
        Context context
    )
    {
        boolean btConnected = false;
        AudioManager mAudioManager = context.getSystemService(AudioManager.class);
        if (mAudioManager != null)
        {
            AudioDeviceInfo[] deviceInfo = mAudioManager.getDevices(GET_DEVICES_OUTPUTS);
            int i;
            for (i = 0; i < deviceInfo.length; i++)
            {
                int type = deviceInfo[i].getType();
                String stype;
                switch (type)
                {
                    case TYPE_BLUETOOTH_A2DP:
                        stype = "BT A2DP";
                        btConnected = true;
                        break;
                    case TYPE_BLUETOOTH_SCO:
                        stype = "BT SCO";
                        break;
                    case TYPE_BUILTIN_EARPIECE:
                        stype = "builtin earpiece";
                        break;
                    case TYPE_BUILTIN_SPEAKER:
                        stype = "builtin speaker";
                        break;
                    case TYPE_WIRED_HEADPHONES:
                        stype = "wired headphones";
                        break;
                    case TYPE_TELEPHONY:
                        stype = "telephony";
                        break;
                    default:
                        stype = String.valueOf(type);
                        break;
                }
                Log.d(LOG_TAG, "found device " + deviceInfo[i].getProductName() + " of type " + stype);
            }
        }

        return btConnected;
    }


    /**************************************************************************
     *
     * send these metadata once at initialisation and later on any update
     *
     * does neither use playState nor position
     *
     *************************************************************************/
    private void sendMediaMetaData()
    {
        // MediaMetadata available with API 21 (5.0)
        final MediaMetadata.Builder metadata = new MediaMetadata.Builder();
        metadata.putString(MediaMetadata.METADATA_KEY_TITLE, mAudioTrack);
        if (mComposerAndWork != null)
        {
            // Media Style Notification prefers DISPLAY_TITLE to TITLE, if set.
            metadata.putString(MediaMetadata.METADATA_KEY_DISPLAY_TITLE, mComposerAndWork);
        }
        metadata.putString(MediaMetadata.METADATA_KEY_ARTIST, mAudioArtist);
        // Android 13 Media Style Notification ignores the album?!?
        metadata.putString(MediaMetadata.METADATA_KEY_ALBUM, mAudioAlbum);
        // This seems NEVER to be displayed anywhere
        metadata.putString(MediaMetadata.METADATA_KEY_COMPOSER, mAudioComposer);
        metadata.putLong(MediaMetadata.METADATA_KEY_TRACK_NUMBER, mAudioListPosition + 1);    // range 1..n
        metadata.putLong(MediaMetadata.METADATA_KEY_NUM_TRACKS, mAudioListSize);
        metadata.putLong(MediaMetadata.METADATA_KEY_DURATION, mAudioDuration);
        if (mAudioAlbumCover != null)
        {
            metadata.putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, ((BitmapDrawable) mAudioAlbumCover).getBitmap());
        }
//            metadata.putString(MediaMetadata.METADATA_KEY_ART_URI, radioImageUri);
//            metadata.putString(MediaMetadata.METADATA_KEY_DISPLAY_ICON_URI, radioImageUri);
//            metadata.putString(MediaMetadata.METADATA_KEY_ALBUM_ART_URI, songImageUri);
        mMediaSession.setMetadata(metadata.build());
    }


    /**************************************************************************
     *
     * send meta data once, e.g. on initialisation
     *
     *************************************************************************/
    private void sendMetaData(Context context)
    {
        // This block tells other apps on the same device which music is currently
        // being played ... if they listen with a BroadcastReceiver.
        // -> https://stackoverflow.com/questions/22247026/get-info-about-audio-playing-in-another-application
        Intent intent = new Intent(AVRCP_META_CHANGED);
        intent.putExtra("id", mAudioId);
        intent.putExtra("artist", mAudioArtist);
        intent.putExtra("album", mAudioAlbum);
        intent.putExtra("track", mAudioTrack);
        intent.putExtra("playing", mAudioIsPlaying);
        intent.putExtra("ListSize", mAudioListSize);
        intent.putExtra("duration", mAudioDuration);
        intent.putExtra("position", mAudioPosition);
        context.sendBroadcast(intent);
        //Log.d(LOG_TAG, "sendMetaData() : sent intent of type " + AVRCP_META_CHANGED);

        // This is for the Bluetooth session and tells the car radio about the music,
        // but also sets the lockscreen background
        sendMediaMetaData();
    }


    /**************************************************************************
     *
     * play state is updated by the Music Service
     *
     *************************************************************************/
    public void sendPlayState(Context context, boolean isPlay, int duration, int position)
    {
        boolean bDurationUpdate = false;
        boolean bPlayStateUpdate = false;
        boolean bPositionUpdate = false;
        if (mAudioIsPlaying != isPlay)
        {
            mAudioIsPlaying = isPlay;
            bPlayStateUpdate = true;
        }
        if (mAudioDuration != duration)
        {
            mAudioDuration = duration;
            bDurationUpdate = true;
        }
        if (mAudioPosition != position)
        {
            mAudioPosition = position;
            bPositionUpdate = true;
        }

        if (bPlayStateUpdate || bDurationUpdate || bPositionUpdate)
        {
            // this is for other apps on same device
            Intent intent = new Intent(AVRCP_PLAYSTATE_CHANGED);
            intent.putExtra("id", mAudioId);
            intent.putExtra("artist", mAudioArtist);
            intent.putExtra("album", mAudioAlbum);
            intent.putExtra("track", mAudioTrack);
            intent.putExtra("playing", mAudioIsPlaying);
            intent.putExtra("ListSize", mAudioListSize);
            intent.putExtra("duration", mAudioDuration);
            intent.putExtra("position", mAudioPosition);
            context.sendBroadcast(intent);
            //Log.d(LOG_TAG, "sendPlayState() : sent intent of type " + AVRCP_PLAYSTATE_CHANGED);
        }

        if (bDurationUpdate || (hack > 0))
        {
            hack--;
            // this is for car radio
            sendMediaMetaData();
        }

        if (bPlayStateUpdate || bDurationUpdate || bPositionUpdate)
        {
            int playState;
            if (mAudioIsPlaying)
            {
                //Log.d(LOG_TAG, "sendPlayState() : state PLAYING");
                playState = PlaybackState.STATE_PLAYING;
            } else if (duration == 0)
            {
                Log.d(LOG_TAG, "sendPlayState() : state STOPPED");
                playState = PlaybackState.STATE_STOPPED;
            } else
            {
                Log.d(LOG_TAG, "sendPlayState() : state PAUSED");
                playState = PlaybackState.STATE_PAUSED;
            }
            PlaybackState state = new PlaybackState.Builder()
                    .setActions(sActions)
                    .setState(playState, position, 1.0f/*, SystemClock.elapsedRealtime()*/)
                    .build();

            mMediaSession.setPlaybackState(state);
        }
    }


    /**************************************************************************
     *
     * Music Service tells us that music has been stopped
     *
     * Instead of a real "stop" condition we send empty strings to the device and hope the best
     *
     *************************************************************************/
    public void stop(Context context)
    {
        mAudioId = -1;
        mAudioArtist = "";
        mAudioAlbum = "";
        mAudioTrack = "";
        mAudioAlbumCover = null;
        sendPlayState(context, false, 0, 0);
        //mMediaSession.release();      this leads to STOP command not reaching car radio
    }


    /**************************************************************************
     *
     * Music Service must pass media session to notifications
     *
     *************************************************************************/
    public MediaSession getMediaSession()
    {
        return mMediaSession;
    }
}
