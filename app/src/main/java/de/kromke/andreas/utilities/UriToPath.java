/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;

import androidx.annotation.NonNull;

/**
 * From https://invent.kde.org/multimedia/kid3/-/blob/master/android/Kid3Activity.java
 */

@SuppressWarnings("JavadocLinkAsPlainText")
public class UriToPath
{
    private static final String LOG_TAG = "O1M : UTP"; //for debugging purposes.

    public static String getPathFromIntentUri(Context context, @NonNull Uri intentUri)
    {
        // content or file
        String intentScheme = intentUri.getScheme();
        if ("file".equals(intentScheme))
        {
            Log.d(LOG_TAG, "getPathFromIntentUri(): file scheme");
            String filePath = intentUri.getPath();
            if (filePath != null)
            {
                Log.d(LOG_TAG, "getPathFromIntentUri(): got path from Uri");
                return filePath;
            }
            else
            {
                Log.w(LOG_TAG, "getPathFromIntentUri(): no path from Uri");
                return intentUri.toString();
            }
        }
        else if ("content".equals(intentScheme))
        {
            Log.d(LOG_TAG, "getPathFromIntentUri(): content scheme");
            return getRealPathFromURI(context, intentUri);
        }

        return null;
    }

    /*
     * Uri from Android media database:
     *  content://media/external/audio/media/<number>
     *      scheme: "content"
     *      authority = "media"
     *      isDocumentUri() = false
     */
    private static String getRealPathFromURI(final Context context, final Uri uri)
    {
        final boolean isKitKat = true; // Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        final String authority = uri.getAuthority();

        //noinspection ConstantConditions
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri))
        {
            Log.d(LOG_TAG, "getRealPathFromURI(): DocumentUri");

            // DocumentProvider
            if ("com.android.externalstorage.documents".equals(authority))
            {
                // ExternalStorageProvider
                Log.d(LOG_TAG, "getRealPathFromURI(): external storage provider");

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type))
                {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            else if ("com.android.providers.downloads.documents".equals(authority))
            {
                Log.d(LOG_TAG, "getRealPathFromURI(): downloads provider");

                // DownloadsProvider
                final String id = DocumentsContract.getDocumentId(uri);
                long longId;
                try
                {
                    longId = Long.parseLong(id);
                } catch (NumberFormatException nfe)
                {
                    return getDataColumn(context, uri, null, null);
                }

                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), longId);
                return getDataColumn(context, contentUri, null, null);
            }
            else if ("com.android.providers.media.documents".equals(authority))
            {
                Log.d(LOG_TAG, "getRealPathFromURI(): media provider");

                // MediaProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                //noinspection ExtractMethodRecommender
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type))
                {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type))
                {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type))
                {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }

        if ("content".equalsIgnoreCase(uri.getScheme()))
        {
            if ("com.google.android.apps.photos.content".equals(authority))
            {
                Log.d(LOG_TAG, "getRealPathFromURI(): Google Photos");

                return uri.getLastPathSegment();
            }
            String path = getDataColumn(context, uri, null, null);
            if (path == null) {
                int colonPos;
                path = uri.getPath();
                if (path != null && path.startsWith("/external_storage_root/"))
                {
                    path = Environment.getExternalStorageDirectory() + path.substring(22);
                } else if (path != null && path.startsWith("/document/") &&
                        (colonPos = path.indexOf(':')) != -1)
                {
                    String storagePath = "/storage/" + path.substring(10, colonPos) +
                            "/" + path.substring(colonPos + 1);
                    if ((new File(storagePath)).exists())
                    {
                        path = storagePath;
                    }
                }
            }
            return path;
        } else if ("file".equalsIgnoreCase(uri.getScheme()))
        {
            return uri.getPath();
        }
        return null;
    }


    // Get the value of the data column for this URI.
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs)
    {
        Log.d(LOG_TAG, "getDataColumn()");

        String result = null;

        final String column = "_data";
        final String[] projection = {column};
        Cursor cursor;
        try
        {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,null);
        } catch (IllegalArgumentException e)
        {
            cursor = null;
        }

        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                final int index = cursor.getColumnIndex(column);
                if (index != -1)
                {
                    result = cursor.getString(index);
                }
            }
            cursor.close();
        }
        return result;
    }
}
