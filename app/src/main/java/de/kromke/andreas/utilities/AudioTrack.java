/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import static de.kromke.andreas.utilities.UriToPath.getPathFromIntentUri;

import android.content.Context;
import android.net.Uri;

import java.util.ArrayList;

/**
 * all data for a single audio file
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
public class AudioTrack extends AudioBaseObject
{
    private final String mPath;     // Android 11: volume and relative path, otherwise: absolute path or SAF path
    public final String mMediaUri;  // Android 11: media content Uri, only used with Android database, otherwise null
    public int track_no;            // including the CD number as 1000, 2000 et cetera
    public String album;
    public long album_id;           // For Android 11 the data type "int" is not sufficient
    public String grouping;         // the name of the complete piece, i.e. "Symphony No. 5"
    public String subtitle;         //  maybe like "op. 42"?
    public String composer;         // the composer of the complete piece, i.e. Wolfgang Mozart
    public String artist;           // only temporarily used during list creation, later it is null, use mPerformer
    public String album_artist;     // only temporarily used during list creation, later it is null, use mWork->mAlbum->mPerformer
    public String conductor;
    public String genre;
    public AudioWork mWork = null;              // work as object once the work list has been created
    public AudioPerformer mPerformer = null;   // performer as object once the performer list has been created
    public ArrayList<AudioPerformer> mPerformerList = new ArrayList<>();  // list of performers
    public AudioGenre mGenre = null;       // genre as object once the genre list has been created
    public int year;
    public int group_no;
    public boolean group_last;              // last in group
    public long play_pos_ms = -1;           // bookmarked play position (0 = none, -1 = unknown)
    public long play_pos_ms_original = -1;  // original value to be checked for changes


    /*
     * partial constructor
     */
    public AudioTrack(
            long theId,
            int theTrackNo,
            String theTitle,
            String theAlbum,
            long theAlbumId,
            String theUri,
            String thePath,
            long theDuration,
            String theArtist,
            int theYear)
    {
        objectType   = AudioObjectType.AUDIO_TRACK_OBJECT;
        id           = theId;
        track_no     = theTrackNo;        // disc and track number (disc number 1000, 2000, ...)
        name         = (theTitle != null) ? theTitle : "";
        album        = theAlbum;
        album_id     = theAlbumId;
        mMediaUri = theUri;
        mPath        = thePath;
        duration     = theDuration;
        artist       = theArtist;
        year         = theYear;

        grouping     = null;
        subtitle     = null;
        conductor    = null;
        group_no     = 0;
        group_last   = true;
        composer     = null;
        genre        = null;
        album_artist = null;
    }

    /*
     * (nearly) complete constructor
     */
    public AudioTrack(
            long   theId,
            int    theTrackNo,
            String theTitle,
            String theAlbum,
            long   theAlbumId,
            long   theDuration,
            String theGrouping,
            String theSubtitle,
            String theComposer,
            String thePerformer,
            String theAlbumPerformer,
            String theConductor,
            String theGenre,
            int    theYear,
            String theUri,
            String thePath)
    {
        objectType   = AudioObjectType.AUDIO_TRACK_OBJECT;
        id           = theId;
        track_no     = theTrackNo;
        album        = theAlbum;
        album_id     = theAlbumId;
        name         = theTitle;
        duration     = theDuration;
        grouping     = theGrouping;
        subtitle     = theSubtitle;
        composer     = theComposer;
        artist       = thePerformer;
        album_artist = theAlbumPerformer;
        conductor    = theConductor;
        genre        = theGenre;
        year         = theYear;        // value from Android Db is nonsense for FLAC files
        mMediaUri = theUri;
        mPath        = thePath;
        if ((grouping != null) && (grouping.isEmpty()))
        {
            grouping = null;
        }

        // to be initialised later
        group_no     = 0;
        group_last   = true;
    }

    public void changeTitle(String audioTrackTitle)
    {
        name = (audioTrackTitle != null) ? audioTrackTitle : "";
    }

    public void addTagInfo(
        String audioTrackMovementName,  // may be null, but not empty
        String audioTrackGrouping,
        String audioTrackSubtitle,
        String audioTrackComposer,
        String audioTrackConductor,
        String audioTrackGenre,
        String audioAlbumArtist,
        int audioYear)
    {
        if (audioTrackMovementName != null)
        {
            // if there is a movement name, replace title with this one
            name = audioTrackMovementName;
        }
        grouping     = audioTrackGrouping;
        subtitle     = audioTrackSubtitle;
        composer     = audioTrackComposer;
        conductor    = audioTrackConductor;
        genre        = audioTrackGenre;
        album_artist = audioAlbumArtist;
        year         = audioYear;               // update value from Android Db
    }

    @Override
    public AudioComposer getComposer()
    {
        if (mWork == null)
        {
            return null;
        }
        else
        {
            return mWork.getComposer();
        }
    }

    @Override
    public AudioPerformer getPerformer()
    {
        return mPerformer;
    }

    @Override
    public ArrayList<AudioPerformer>getPerformerList()
    {
        return mPerformerList;
    }

    @Override
    public final String getPath()
    {
        return (mMediaUri != null) ? mMediaUri : mPath;
    }

    // TODO: remove hack
    @Override
    public String getRawPath()
    {
        return mPath;
    }

    @Override
    public String getRealPath(Context context)
    {
        if ((mPath != null) && SafUtilities.isSafPath(mPath))
        {
            Uri theUri = Uri.parse(mPath);
            if (theUri != null)
            {
                String thePath = getPathFromIntentUri(context, theUri);
                if (thePath != null)
                {
                    return thePath;
                }
            }
        }
        return mPath;
    }

    public String getPerformerName()
    {
        if (mPerformer != null)
            return mPerformer.name;
        else
            return "";
    }

    public String getConductorName()
    {
        if (conductor != null)
            return conductor;
        else
            return "";
    }

    public AudioAlbum getAlbum()
    {
        return (mWork != null) ? mWork.mAlbum : null;
    }

    public int getWorkNoOfTracks()
    {
        return (mWork != null) ? mWork.no_of_tracks : 1;
    }

    @Override
    public AudioGenre getGenre()
    {
        return mGenre;
    }

    // The composer matches if both are not set or if they are identical.
    static private boolean composerMatch(final String composer1, final String composer2)
    {
        if (composer1 == null)
        {
            // only match, if both are null
            return (composer2 == null);
        }

        //noinspection SimplifiableIfStatement
        if (composer2 == null)
        {
            // only match, if both are null
            return false;
        }

        return (composer1.equals(composer2));
    }

    // used for display
    public boolean isSameWork(final AudioTrack anotherTrack)
    {
        if ((mWork == null) || (anotherTrack.mWork == null))
        {
            return false;
        }
        return mWork == anotherTrack.mWork;
    }

    // Two tracks belong to the same group if grouping is valid, and
    // both grouping and album composer match.
    // Warning: This may only be used for initialisation, because later
    //          the composer string will be null-ed to save memory
    public boolean isSameGroup(final AudioTrack anotherTrack)
    {
        //noinspection SimplifiableIfStatement
        if ((anotherTrack != null) &&
            (anotherTrack.grouping != null) &&
            (grouping != null) &&
            (grouping.equals(anotherTrack.grouping)) &&
            (album_id == anotherTrack.album_id))
        {
            return composerMatch(composer, anotherTrack.composer);
        }
        else
        {
            // no grouping or different group
            return false;
        }
    }


    /**************************************************************************
     *
     * Check, if the object matches the given search text
     *
     * @param searchString      the search text, in upper case if flag is "true"
     * @param bCaseSensitive    compare case sensitive or not
     * @return true, iff the search text matches
     *
     *************************************************************************/
    @Override
    public boolean matchesText(final String searchString, boolean bCaseSensitive)
    {
        return (matchString(name, searchString, bCaseSensitive)) ||
                (matchString(grouping, searchString, bCaseSensitive)) ||
                (matchString(getComposer().name, searchString, bCaseSensitive)) ||
                (matchString(getPerformer().name, searchString, bCaseSensitive));
    }

    @Override
    public String getAlbumPicturePath(Context context)
    {
        if (mWork != null)
            return mWork.getAlbumPicturePath(context);
        else
            return null;
    }
}
