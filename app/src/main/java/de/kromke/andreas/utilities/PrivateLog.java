package de.kromke.andreas.utilities;

import static android.os.Environment.getExternalStorageState;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.kromke.andreas.opus1musicplayer.BuildConfig;

public class PrivateLog
{
    private static final String logPrefix = "O1M : ";
    private static final String LOG_TAG = logPrefix + "Log";
    private static final String logfile = "/o1m.log";
    private static boolean sbAccessError = false;
    private static int level = 0;
    private static String indent = "";

    public static void start()
    {
        level = 0;
        indent = "";
        appendLog("\n");
    }

    public static void f_enter(final String tag, final String msg)
    {
        d(tag, msg);
        level++;
        if (level < 10)
        {
            indent = " ".repeat(level);
        }
    }

    public static void f_leave(final String tag, final String msg)
    {
        if (level > 0)
        {
            level--;
            if (level < 10)
            {
                indent = " ".repeat(level);
            }
        }
        else
        {
            Log.e(logPrefix + LOG_TAG, "log level error");
        }
        d(tag, msg + " -- leave");
    }

    public static void v(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('V', tag, msg);
        }
        Log.v(logPrefix + tag, msg);
    }

    public static void i(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('I', tag, msg);
        }
        Log.i(logPrefix + tag, msg);
    }

    public static void d(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('D', tag, msg);
        }
        Log.d(logPrefix + tag, msg);
    }

    public static void w(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('W', tag, msg);
        }
        Log.w(logPrefix + tag, msg);
    }

    public static void e(final String tag, final String msg)
    {
        if (!sbAccessError)
        {
            appendLogWithDate('E', tag, msg);
        }
        Log.e(logPrefix + tag, msg);
    }

    private static void appendLogWithDate(char channel, final String tag, final String msg)
    {
        if (BuildConfig.DEBUG)
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMANY);
            String date = df.format(Calendar.getInstance().getTime());
            appendLog(date + " " + channel + indent + " " + tag + " " + msg);
        }
    }

    private static void appendLog(String text)
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStorageDirectory();
        } else
        {
            Log.e(LOG_TAG, "appendLog() -- media not mounted, no private log path");
            return;
        }

        File logFile = new File(basePath + logfile);
        if (!logFile.exists())
        {
            try
            {
                if (!logFile.createNewFile())
                {
                    Log.e(LOG_TAG, "appendLog() -- cannot create private log file");
                    sbAccessError = true;
                    return;
                }
            }
            catch (IOException e)
            {
                Log.e(LOG_TAG, "appendLog() -- cannot create private log file");
                sbAccessError = true;
                return;
            }
        }

        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            Log.e(LOG_TAG, "appendLog() -- cannot write to private log file");
            sbAccessError = true;
        }
    }
}
