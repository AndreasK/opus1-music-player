/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

//import android.util.Log;

import android.content.Context;

import java.util.ArrayList;

/**
 * all necessary information about an album
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioAlbum extends AudioBaseObject
{
    private AudioComposer mComposer;
    private boolean mbComposerSet;
    ArrayList<AudioWork> mWorkList = new ArrayList<>();  // works on that album
    private final String album_artist_pseudo;         // nonsense from Android db
    public String album_artist;                 // explicit value from tagger
    private String common_artist;               // compare artists of all tracks, set to "≠" if they differ
    // public int album_no_of_discs;            // for future use
    // public String album_genre;               // for future use
    public int album_first_year;
    public int album_last_year;

    // clean, complete constructor, filled with information from own db
    public AudioAlbum(
            long the_id,
            String the_album_name,
            String the_album_artist,                  // derived from tags "album artist"
            @SuppressWarnings({"unused", "RedundantSuppression"}) String the_common_composer,
            String the_common_artist,       // derived from tracks
            int no_of_tracks,
            long the_duration,
            int first_year,
            int last_year,
            String the_picture_path)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_ALBUM_OBJECT;
        id                  = the_id;
        name                = the_album_name;
        album_artist_pseudo = null;
        album_artist        = beautifyName(the_album_artist);
        common_artist       = beautifyName(the_common_artist);
        this.no_of_tracks   = no_of_tracks;
        duration            = the_duration;
        album_first_year    = first_year;
        album_last_year     = last_year;
        picture_path        = the_picture_path;
        mComposer           = null;
        mbComposerSet       = false;
    }

    // constructor, filled with information from Android db
    public AudioAlbum(
            long id,
            String name,
            String artist,
            int no_of_tracks,
            int first_year,
            int last_year,
            String the_picture_path)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_ALBUM_OBJECT;
        this.id = id;
        this.name = name;
        album_artist_pseudo = beautifyName(artist);
        this.no_of_tracks = no_of_tracks;
        album_first_year  = first_year;
        album_last_year   = last_year;
        picture_path      = the_picture_path;

        // to be set later
        album_artist = null;
        common_artist = null;
    }

    @Override
    public AudioComposer getComposer()
    {
        return mComposer;
    }

    @Override
    protected void initPicturePath(Context context)
    {
        // do nothing, picture_path is already set
    }

    // find out if there is a common composer for this album
    // also used for own db, despite it would not be necessary
    @SuppressWarnings("WeakerAccess")
    public void setCommonComposer(AudioComposer composer)
    {
        if (composer != null)
        {
            if (!mbComposerSet)
            {
                // first composer entry: just remember
                mComposer = composer;
                mbComposerSet = true;
            } else if (mComposer != composer)
            {
                // there is no common composer
                mComposer = null;
            }
        }
    }


    @Override
    public final String getRealPath(Context context)
    {
        String commonPath = null;

        for (AudioWork theWork: mWorkList)
        {
            String thePath = theWork.getRealPath(context);
            if (thePath != null)
            {
                if (commonPath == null)
                {
                    commonPath = thePath;
                } else if (!commonPath.equals(thePath))
                {
                    commonPath = "≠";
                }
            }
        }

        return commonPath;
    }


    // find out if there is a common performer for this album
    // not used for own db
    @SuppressWarnings("WeakerAccess")
    public void setCommonArtist(String artist)
    {
        if (artist != null)
        {
            if (common_artist == null)
            {
                common_artist = beautifyName(artist);
                //Log.e(LOG_TAG, "AddAlbumArtistToAlbum(): Set common artist " + artist);
            } else if (!common_artist.equals(beautifyName(artist)))
            {
                //Log.e(LOG_TAG, "AddAlbumArtistToAlbum(): Set common artist " + "≠");
                common_artist = "≠";
            }
        }
    }

    // find out if there is a common year for this album
    // not used for own db
    @SuppressWarnings("WeakerAccess")
    public void setCommonYear(int year)
    {
        if (year != 0)
        {
            if (album_first_year == 0)
            {
                album_first_year = album_last_year = year;
            }
            else if (year < album_first_year)
            {
                album_first_year = year;
            }
            else if (year > album_last_year)
            {
                album_last_year = year;
            }
        }
    }

    // needed for orphan albums
    public void setNoOfTracks()
    {
        no_of_tracks = 0;
        no_of_works = 0;

        for (AudioWork theWork: mWorkList)
        {
            no_of_works++;
            no_of_tracks += theWork.no_of_tracks;
        }
    }

    @Override
    public long getDuration()
    {
        if (duration == 0)
        {
            // This is only necessary for Android db. The mWorkList must already be initialised
            if (AppGlobals.bBackgroundInitialisationDone)   //HACK
            {
                // accumulate duration of all works
                int size = mWorkList.size();
                for (int i = 0; i < size; i++)
                {
                    AudioWork theWork = mWorkList.get(i);
                    duration += theWork.getDuration();
                }
            }
        }
        // else already calculated

        return duration;
    }

    // helper for getInfo()
    private String getAlbumYearAppendString()
    {
        if ((sShowAlbumYear) && (album_first_year > 0) && (album_last_year > 0))
        {
            if (album_first_year < album_last_year)
                return " (" + album_first_year + "-" + album_last_year + ")";
            else
                return " (" + album_first_year + ")";
        }
        else
        {
            return "";
        }
    }

    public String getAlbumArtist()
    {
        if ((album_artist != null) && !album_artist.isEmpty())
        {
            if (album_artist.equals("various") || album_artist.equals("Various"))
                return str_various_performers;
            else
                return album_artist;
        }

        if ((common_artist != null) && !common_artist.isEmpty())
        {
            if (common_artist.equals("≠"))
                return str_various_performers;
            else
                return common_artist;
        }

        if ((album_artist_pseudo != null) && !album_artist_pseudo.isEmpty())
        {
            return album_artist_pseudo;
        }

        return "";
    }

    // for display in a list, small text
    @Override
    public String getInfo()
    {
        String infoText = getAlbumArtist();
        if (!infoText.isEmpty())
        {
            infoText += getAlbumYearAppendString() + "\n";
        }

        infoText += "(" + getTrackNoInfo();
        if (sShowAlbumAndWorkDuration)
        {
            long duration = getDuration();
            if (duration > 0)
            {
                infoText += ", " + Utilities.convertMsToHMmSs(getDuration());
            }
        }
        infoText += ")";
        return infoText;
    }


    /**************************************************************************
     *
     * Check, if the object matches the given filter objects
     *
     *************************************************************************/
    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        if ((theFilterComposer == null) && (theFilterPerformer == null) && (theFilterGenre == null))
        {
            return true;
        }

        for (AudioWork theWork: mWorkList)
        {
            if (theWork.matchesFilter(theFilterComposer, theFilterPerformer, theFilterGenre))
            {
                // one matching work is sufficient
                return true;
            }
        }

        return false;    // no matches
    }


    /**************************************************************************
     *
     * Check, if the object matches the given search text
     *
     * @param searchString      the search text, in upper case if flag is "true"
     * @param bCaseSensitive    compare case sensitive or not
     * @return true, iff the search text matches
     *
     *************************************************************************/
    @Override
    public boolean matchesText(final String searchString, boolean bCaseSensitive)
    {
        return (matchString(name, searchString, bCaseSensitive)) ||
                (matchString(album_artist_pseudo, searchString, bCaseSensitive));
    }

    @Override
    public String getAlbumPicturePath(Context context)
    {
        return getPicturePath(context);
    }

    public boolean isEmpty() { return (mWorkList.isEmpty()); }
}
