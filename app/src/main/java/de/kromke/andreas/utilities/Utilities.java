/*
 * Copyright (C) 2016-2023 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import static android.os.Environment.getExternalStorageState;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


/**
 * global variables, shared by all activities
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines", "JavadocLinkAsPlainText"})
public class Utilities
{
    static private final String LOG_TAG = "O1M : Utilities";
    public static final String sDbPath = "ClassicalMusicDb";
    public static final String sDbName = "musicmetadata.db";
    public static final String sDbSafName = "safmetadata.db";


    /************************************************************************************
     *
     * get base path of database file from environment
     *
     ***********************************************************************************/
    public static String getDefaultSharedDbFileBasePath(Context context)
    {
        File basePath;

        if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStorageDirectory();
        } else
        {
            basePath = context.getFilesDir();
        }
        return basePath.getPath() + "/" + sDbPath;
    }


    /************************************************************************************
     *
     * get default path of database file itself
     *
     ***********************************************************************************/
    public static String getDefaultSharedDbFilePath(Context context)
    {
        return getDefaultSharedDbFileBasePath(context) + "/" + sDbName;
    }


    /************************************************************************************
     *
     * get default base path of composers and interpreters
     *
     ***********************************************************************************/
    public static String getDefaultOwnPersonsPicturePath(Context context)
    {
        return getDefaultSharedDbFileBasePath(context) + "/" + "Komponisten_gpl";
    }


    /************************************************************************************
     *
     * get path of database file from music base path
     *
     ***********************************************************************************/
    public static String getDatabasePathFromMusicBasePath(String musicBasePath)
    {
        String databasePath;
        if (musicBasePath.endsWith(sDbPath))
        {
            databasePath = musicBasePath + "/" + sDbName;
        }
        else
        {
            databasePath = musicBasePath + "/"+ sDbPath + "/" + sDbName;
        }
        return databasePath;
    }


    @SuppressLint("DefaultLocale")
    public static String convertMsToHMmSs(long ms)
    {
        long seconds = (ms + 999) / 1000;       // round up
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        if (h > 0)
            return String.format("%d:%02d:%02d", h, m, s);
        else
            return String.format("%02d:%02d", m, s);
    }


    /* *************************************************************************
     * Debug helper to show a UTF-8 or -16 coded string as byte
     *************************************************************************/
    /*
    static public void showStringAsBytes(final String theString)
    {
        byte[] b = theString.getBytes();
        for (int i = 0; i < b.length; i++)
        {
            Log.d(LOG_TAG, "Byte " + i + " = " + String.format("%02X", b[i]));
        }
    }
    */

    /**************************************************************************
     * helper function
     *************************************************************************/
    public static String convertMsToIso(long ms)
    {
        // get ISO8601 date instead of dumb US format (Z = time zone) ...
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        @SuppressWarnings("UnnecessaryLocalVariable")
        String strTime = df.format(new java.util.Date(ms));
        return strTime;
    }


    /**************************************************************************
     * helper function
     *************************************************************************/
    public static int convertToInt(final String s, int defVal)
    {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return defVal;
        }
    }


    /**************************************************************************
     *
     * safe way to check if string differ
     *
     *************************************************************************/
    static public boolean stringsDiffer(final String s1, final String s2)
    {
        if (s1 == null)
        {
            return s2 != null;
        }
        // now s1 is not null
        return !s1.equals(s2);
    }


    /**************************************************************************
     *
     * helper to get path from URI
     *
     * /tree/primary:Music/bla
     * /tree/12F0-3703:Music/bla
     *
     *************************************************************************/
    static public String pathFromTreeUri(Uri uri)
    {
        String uriPath = uri.getPath();
        if (uriPath != null)
        {
            if (uriPath.startsWith("/tree/primary:"))
            {
                return "/storage/emulated/0/" + uriPath.substring(14);
            }

            if ((uriPath.length() >= 17) &&
                    (uriPath.substring(0, 16).matches("^/tree/....-....:$")))
            {
                return "/storage/" + uriPath.substring(6, 15) + "/" + uriPath.substring(16);
            }
        }

        return null;
    }


    /**************************************************************************
     *
     * -> https://stackoverflow.com/questions/9324103/download-and-extract-zip-file-in-android
     *
     * For whatever reason we either get an ominous URI from the Download
     * Manager which we cannot easily convert to a File or even a path, or we
     * get a File Descriptor which we can use to create a FileInputStream.
     *
     *************************************************************************/
    public static int unzip(InputStream zipFileInputStream, File destDir)
    {
        int numFiles = 0;
        try
        {
            ZipInputStream zin = new ZipInputStream(zipFileInputStream);

            byte[] b = new byte[1024];

            ZipEntry ze;
            while ((ze = zin.getNextEntry()) != null)
            {
                String name = ze.getName();
                Log.v(LOG_TAG, "unzip() : Unzipping " + name);
                File f = new File(destDir, name);

                // security check against Zip Path Traversal Vulnerability
                String canonicalPath = f.getCanonicalPath();
                if (!canonicalPath.startsWith(destDir.getAbsolutePath()))
                {
                    Log.e(LOG_TAG, "unzip() : illegal path " + canonicalPath);
                    return 0;
                }
                else
                if (ze.isDirectory())
                {
                    if (!f.isDirectory())
                    {
                        boolean res = f.mkdirs();
                        if (!res)
                        {
                            Log.e(LOG_TAG, "unzip() : Cannot create directory " + f.getPath());
                        }
                    }
                }
                else
                {
                    FileOutputStream fout = new FileOutputStream(f);

                    BufferedInputStream in = new BufferedInputStream(zin);
                    BufferedOutputStream out = new BufferedOutputStream(fout);

                    int n;
                    while ((n = in.read(b,0,1024)) >= 0)
                    {
                        out.write(b,0,n);
                    }

                    zin.closeEntry();
                    out.close();
                    numFiles++;
                }
            }
            zin.close();
        } catch(Exception e) {
            Log.e(LOG_TAG, "unzip() : error: ", e);
            numFiles = -1;
        }

        return numFiles;
    }


    /* *************************************************************************
     *
     * helper (does not work, as symlinks seem to be unsupported
     *
     *************************************************************************/
    /*
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void createAlias(final File destDir, final String oldname, final String newname)
    {
        File newFile = new File(destDir, newname);
        try
        {
            Os.symlink(oldname, newFile.getPath());
        } catch (ErrnoException e)
        {
            e.printStackTrace();
        }
    }
    */
}
