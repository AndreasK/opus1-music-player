/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import de.kromke.andreas.opus1musicplayer.BuildConfig;

/*
 * all database operations in case the Android db is used, including the private cache db handling
 */
@SuppressWarnings("JavadocBlankLines")
public class DatabaseManagerAndroid implements DatabaseManager
{
    private static final String LOG_TAG = "O1M : DbManagerAndroid";
    static private int backGroundInitialisationNumberOfTracksTotal = 0;
    static private int backGroundInitialisationNumberOfTracksDone = 0;
    static private boolean bBackgroundInitialisationFailedDueToAndroidBug = false;
    static private ContentResolver mResolver;
    static private int mMaxNoTracks = 0;
    static private String[] mFilterPaths = null;
    static private int mNoFilterSkipped;
    static private boolean mMergeAlbumsOfSameName;
    static private long mLatestAndroidDbAudioFileAdded = 0;
    // private final boolean mbUseJaudioTaglib; /* jaudiotagger removed */
    private final boolean mbForceReadingPaths;

    private static final String[] album_columns29 =
    {
        // pre-Android 11: get album art path
        android.provider.MediaStore.Audio.Albums._ID,
        android.provider.MediaStore.Audio.Albums.ALBUM,
        android.provider.MediaStore.Audio.Albums.ARTIST,
        android.provider.MediaStore.Audio.Albums.NUMBER_OF_SONGS,
        android.provider.MediaStore.Audio.Albums.FIRST_YEAR,
        android.provider.MediaStore.Audio.Albums.LAST_YEAR,
        android.provider.MediaStore.Audio.Albums.ALBUM_ART      // abandoned with Android 11
    };
    private static final String[] album_columns30 =
    {
        // Android 11: derive album art later, from _ID
        android.provider.MediaStore.Audio.Albums._ID,
        android.provider.MediaStore.Audio.Albums.ALBUM,
        android.provider.MediaStore.Audio.Albums.ARTIST,
        android.provider.MediaStore.Audio.Albums.NUMBER_OF_SONGS,
        android.provider.MediaStore.Audio.Albums.FIRST_YEAR,
        android.provider.MediaStore.Audio.Albums.LAST_YEAR
    };
    private static final String[] album_columns = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ? album_columns30 : album_columns29;
    private static final String album_orderBy = MediaStore.Audio.Albums.ALBUM + " COLLATE NOCASE ASC";
    private static final Uri album_content_uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;

    private static final String[] media_columns29 =
    {
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.ALBUM_ID,
        MediaStore.Audio.Media.ALBUM,
        MediaStore.Audio.Media.TRACK,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.COMPOSER,
        MediaStore.Audio.Media.ARTIST,
        /* MediaStore.Audio.Media.ALBUM_ARTIST, is hidden via @hide */
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.YEAR,
        // from base class MediaColumns:
        MediaStore.Audio.Media.DATA,  // here: the path, no longer recommended for Android 11
        MediaStore.Audio.Media.SIZE
    };
    private static final String[] media_columns30 =
    {
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.ALBUM_ID,
        MediaStore.Audio.Media.ALBUM,
        MediaStore.Audio.Media.TRACK,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.COMPOSER,
        MediaStore.Audio.Media.ARTIST,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.YEAR,
        // from base class MediaColumns:
        MediaStore.Audio.Media.DISPLAY_NAME,    // needed for file name extension
        MediaStore.Audio.Media.VOLUME_NAME,     // start of path, like "/storage/bla", needed for path view
        MediaStore.Audio.Media.RELATIVE_PATH,   // path without "/storage/bla", needed for path view
        MediaStore.Audio.Media.SIZE
    };
    private static final String[] media_columns = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) ? media_columns30 : media_columns29;
    private static final String media_orderBy = MediaStore.Audio.Media.ALBUM + "," + MediaStore.Audio.Media.TRACK;

    private static final Uri media_content_uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;


    /**************************************************************************
     *
     * the constructor needs the resolver to access the Android db
     *
     *************************************************************************/
    DatabaseManagerAndroid
    (
        ContentResolver musicResolver,
        String filterPaths,             // black/whitelist, LF separated
        boolean bMergeAlbumsOfSameName,
        int maxNoTracks,
        // boolean bUseJaudioTaglib,    /* jaudiotagger removed */
        boolean bForceReadingPaths
    )
    {
        Log.v(LOG_TAG, "open Android database");
        mResolver = musicResolver;
        setFilterTable(filterPaths);
        mMergeAlbumsOfSameName = bMergeAlbumsOfSameName;
        mMaxNoTracks = maxNoTracks;
        // mbUseJaudioTaglib = bUseJaudioTaglib;    /* jaudiotagger removed */
        mbForceReadingPaths = bForceReadingPaths;

        // get modification date of audio media table. Note that we convert seconds to milliseconds here
        mLatestAndroidDbAudioFileAdded = getLastDateAdded(media_content_uri) * 1000;
        Log.v(LOG_TAG, "Android audio db modification date = " + mLatestAndroidDbAudioFileAdded + " (" + Utilities.convertMsToIso(mLatestAndroidDbAudioFileAdded) + ")");
    }


    /**************************************************************************
     *
     * check if db exists
     *
     *************************************************************************/
    public boolean exists()
    {
        return true;
    }


    /**************************************************************************
     *
     * close database (nothing to do here)
     *
     *************************************************************************/
    public void close()
    {
    }

    /**************************************************************************
     *
     * get a list of albums
     *
     * Note that this function may return null
     *
     *************************************************************************/
    public ArrayList<AudioBaseObject> queryAlbums()
    {
        Cursor theCursor = mResolver.query(
                android.provider.MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                album_columns,    // columns to return, null returns all rows
                null,       // rows to return, as "WHERE ...", null returns all rows
                null,       // selection arguments, replacing question marks in previous argument
                album_orderBy     // sort order, null: unordered
        );
        // handle error cases
        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no album found");
            theCursor.close();
            return null;
        }

        // from class AlbumColumns:
        // this is an Android bug and leads to an exception:
        // -> http://stackoverflow.com/questions/34151038/why-is-androids-mediastore-audio-album-album-id-causing-an-illegalstateexceptio
        //int idColumn         = theCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ID);
        // In fact the "album artist" is nonsense due to an Android bug
        // -> https://stackoverflow.com/questions/17392035/android-getting-album-artist-from-cursor
        int idColumn         = theCursor.getColumnIndex(MediaStore.Audio.Albums._ID);
        int albumColumn      = theCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
        int artistColumn     = theCursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST);        // Android-Bug!
        int noOfTracksColumn = theCursor.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);
        int firstYearColumn  = theCursor.getColumnIndex(MediaStore.Audio.Albums.FIRST_YEAR);
        int lastYearColumn   = theCursor.getColumnIndex(MediaStore.Audio.Albums.LAST_YEAR);
        int albumArtColumn   = theCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);     // is -1 for Android 11 and newer

        //
        // loop to add all album data to the global album list
        //
        ArrayList<AudioBaseObject> audioAlbumList = new ArrayList<>();
        AudioAlbum theAlbum = null;
        do
        {
            long thisId = theCursor.getLong(idColumn);
            String thisAlbumName = theCursor.getString(albumColumn);
            String theAlbumArtist = theCursor.getString(artistColumn);        // Android-Bug!
            int thisNoOfTracks = theCursor.getInt(noOfTracksColumn);
            long thisFirstYear  = theCursor.getInt(firstYearColumn);
            long thisLastYear   = theCursor.getInt(lastYearColumn);
            String thisAlbumArt;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            {
                // Android 30 method to the get the album art Uri.
                // These look like ""content://media/external/audio/albums/8204931316969154785".
                // https://stackoverflow.com/questions/66077161/android-loadthumbnail-album-artwork-api-29
                Uri contentUri = ContentUris.withAppendedId(
                        album_content_uri,
                        thisId);
                thisAlbumArt = contentUri.toString();
                // Bitmap = resolver.loadThumbnail(contentUri, new Size(640, 480), null);
            }
            else
            {
                thisAlbumArt = theCursor.getString(albumArtColumn);
            }

            //noinspection ConstantConditions
            if (BuildConfig.BUILD_TYPE.startsWith("debug"))
            {
                Log.v(LOG_TAG, "Album Id (_ID)     = " + thisId);
                Log.v(LOG_TAG, "Album Name         = " + thisAlbumName);
                Log.v(LOG_TAG, "Album Interpreter  = " + theAlbumArtist);        // Android-Bug!
                Log.v(LOG_TAG, "Album No of Tracks = " + thisNoOfTracks);
                Log.v(LOG_TAG, "Album First Year   = " + thisFirstYear);
                Log.v(LOG_TAG, "Album Last Year    = " + thisLastYear);
                Log.v(LOG_TAG, "Album Art          = " + thisAlbumArt);
                Log.v(LOG_TAG, "==============================================\n");
            }

            if ((thisAlbumName != null) && !isAlbumIllegal(thisAlbumName))
            {
                if (theAlbumArtist.equals("<unknown>"))
                {
                    theAlbumArtist = null;
                }

                // count total number for later use
                backGroundInitialisationNumberOfTracksTotal += thisNoOfTracks;

                // merge albums of same name, if requested by user settings
                boolean bAddAlbum = true;
                if (mMergeAlbumsOfSameName && (theAlbum != null) && theAlbum.name.equals(thisAlbumName))
                {
                    // this is a duplicate
                    theAlbum.no_of_tracks += thisNoOfTracks;
                    bAddAlbum = false;
                }

                if (bAddAlbum)
                {
                    theAlbum = new AudioAlbum(
                            thisId,
                            thisAlbumName,
                            theAlbumArtist,     // in fact Android gives some artist of some track of album
                            thisNoOfTracks,
                            (int) thisFirstYear,
                            (int) thisLastYear,
                            thisAlbumArt);
                    audioAlbumList.add(theAlbum);
                }
                else
                {
                    Log.w(LOG_TAG, "Album merged: " + thisAlbumName);
                }
            }
        }
        while (theCursor.moveToNext());

        theCursor.close();
        Log.d(LOG_TAG, "found " + audioAlbumList.size() + " albums");
        return audioAlbumList;
    }


    /**************************************************************************
     *
     * get all tracks from Android media database and from the files
     *
     *  Warning: This runs in a background task!!!
     *
     *************************************************************************/
    private ArrayList<AudioTrack> queryTracksFromAndroidDb()
    {
        ArrayList<AudioTrack> tempList = new ArrayList<>();
        backGroundInitialisationNumberOfTracksDone = 0;
        final File f = Environment.getExternalStorageDirectory(); //VOLUME_EXTERNAL_PRIMARY
        final String externalPrimaryStoragePath = f.getAbsolutePath();

        //noinspection ExtractMethodRecommender
        final String where;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q)
        {
            // We want also files from folder Audiobooks in Android 10.
            // But note that these do not have
            // albums in Android's database. TODO: Create albums for audiobooks.
            where = MediaStore.Audio.Media.IS_MUSIC + " OR " + MediaStore.Audio.Media.IS_AUDIOBOOK;
        }
        else
        {
            where = MediaStore.Audio.Media.IS_MUSIC;
        }
        //final String whereVal[] = { null };

        // this is kind of SQL query:
        Cursor theCursor = mResolver.query(
                media_content_uri,
                (mbForceReadingPaths) ? media_columns29 : media_columns,    // columns to return, null returns all rows
                where,      // rows to return, as "WHERE ...", null returns all rows
                null, // whereVal,   // selection arguments, replacing question marks in previous argument
                media_orderBy     // sort order, null: unordered
        );
        // handle error cases
        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no track found in Android music db");
            theCursor.close();
            return null;
        }

        // from class AudioColumns:
        int idColumn = theCursor.getColumnIndex(MediaStore.Audio.Media._ID);
        int trackColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
        int titleColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
        //int composerColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.COMPOSER);       // fails for mp4 due to Android bug
        int artistColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int durationColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
        int yearColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.YEAR);
        // from base class MediaColumns:
        int dataColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.DATA);  // absolute file path
        int nameColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME);  // file name
        int volumeColumn      = theCursor.getColumnIndex(MediaStore.Audio.Media.VOLUME_NAME);   // API 29
        int relPathColumn     = theCursor.getColumnIndex(MediaStore.Audio.Media.RELATIVE_PATH);  // API 29
        int sizeColumn        = theCursor.getColumnIndex(MediaStore.Audio.Media.SIZE);
        int albumColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
        int albumIdColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
        //int albumArtistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ARTIST);
        //int albumKeyColumn    = theCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_KEY);
        // AudioTagReaderInterface theTagsFromFile = (mbUseJaudioTaglib) ? new JaudioTagReader() : new MyTagReader(); /* jaudiotagger removed */
        AudioTagReaderInterface theTagsFromFile = new MyTagReader(); /* jaudiotagger removed */
        mNoFilterSkipped = 0;

        //
        // loop to add all music files to the album music list
        //

        do
        {
            long thisId = theCursor.getLong(idColumn);
            int thisTrack = theCursor.getInt(trackColumn);
            String thisAlbum = theCursor.getString(albumColumn);
            long thisAlbumId = theCursor.getLong(albumIdColumn);
            String thisTitle = theCursor.getString(titleColumn);
            //String thisComposer = theCursor.getString(composerColumn);    // ignored due to Android bug
            String thisPerformer = theCursor.getString(artistColumn);     // performer/interpreter
            long thisDuration = theCursor.getInt(durationColumn);
            long thisYear = theCursor.getInt(yearColumn);

            String thisPath      = (dataColumn >= 0) ? theCursor.getString(dataColumn) : null;
            String thisName      = (nameColumn >= 0) ? theCursor.getString(nameColumn) : thisPath;
            String thisVolume    = (volumeColumn >= 0) ? theCursor.getString(volumeColumn) : null;
            String thisRelPath   = (relPathColumn >= 0) ? theCursor.getString(relPathColumn) : null;
            long thisSize        = theCursor.getInt(sizeColumn);
            String thisUri = null;

            if (thisVolume != null)
            {
                // for whatever reason the internal memory is passed as "external_primary"
                if (MediaStore.VOLUME_EXTERNAL_PRIMARY.equals(thisVolume))
                {
                    // usually "/storage/emulated/0"
                    thisVolume = externalPrimaryStoragePath;
                }
                else
                {
                    // for whatever reason SD card volume is converted to lower case
                    if (thisVolume.matches("....-...."))
                    {
                        thisVolume = thisVolume.toUpperCase();
                    }
                    thisVolume = "/storage/" + thisVolume;
                }
            }

            if (thisPath == null)
            {
                // Android 30 method to the get the audio file Uri.
                // These look like "content://media/external/audio/media/2718".

                Uri contentUri = ContentUris.withAppendedId(
                        media_content_uri,
                        thisId);
                thisUri = contentUri.toString();
                // This is the recommended method to open the media file:
                // ContentResolver#openFileDescriptor(Uri, String)
            }

            //noinspection ConstantConditions
            if (BuildConfig.BUILD_TYPE.startsWith("debug"))
            {
                Log.v(LOG_TAG, "thisId        = " + thisId);
                Log.v(LOG_TAG, "albumId       = " + thisAlbumId);
                Log.v(LOG_TAG, "thisTrack     = " + thisTrack);
                Log.v(LOG_TAG, "thisTitle     = " + thisTitle);
                //Log.v(LOG_TAG, "thisComposer  = " + thisComposer);
                Log.v(LOG_TAG, "thisPerformer = " + thisPerformer);
                Log.v(LOG_TAG, "thisDuration  = " + thisDuration);
                Log.v(LOG_TAG, "thisPath      = " + thisPath);
                Log.v(LOG_TAG, "thisName      = " + thisName);
                Log.v(LOG_TAG, "thisUri       = " + thisUri);
                //Log.v(LOG_TAG, "thisYear        = " + thisYear);
                Log.v(LOG_TAG, "thisSize      = " + thisSize);
                Log.v(LOG_TAG, "==============================================\n");
            }

            // Android 30 and newer: use Uri (though path also would be possible)
            // Android 29 and older: use path
            if ((thisPath == null) && (thisRelPath != null))
            {
                thisPath = thisVolume + "/" + thisRelPath + thisName;
            }

            if ((thisPerformer != null) && (thisPerformer.equals("<unknown>")))
            {
                thisPerformer = null;
            }

            if (thisTitle == null)
            {
                Log.w(LOG_TAG, "queryTracksFromAndroidDb(): corrupt entry skipped");
                continue;
            }

            if ((mFilterPaths != null) && applyFilterTable(thisPath))
            {
                Log.v(LOG_TAG, "queryTracksFromAndroidDb(): filtered entry skipped");
                mNoFilterSkipped++;
                continue;
            }

            AudioTrack theTrack = new AudioTrack(
                    thisId,
                    thisTrack,
                    thisTitle,
                    thisAlbum,
                    thisAlbumId,
                    thisUri,
                    thisPath,
                    thisDuration,
                    thisPerformer,
                    (int) thisYear);

            //
            // Get necessary, but missing tags not from Android db, but directly from tagger
            //

            theTagsFromFile.get(theTrack.getPath(), thisName, thisSize, mResolver);

            //AudioFileInfo audioInfo = new AudioFileInfo(thisPath);
            //AudioFileInfo.AudioTags theTagsFromFile = audioInfo.getTags();
            //if (theTagsFromFile != null)
            if (theTagsFromFile.isValid())
            {
                // For .aac files Android cannot get ID3 tags, on the other hand Android uses the file name as track title
                // in case there are no tags at all, which is better than nothing.

                String tagTitle = theTagsFromFile.tagTitle;
                if ((tagTitle != null) && (!tagTitle.isEmpty()) && !thisTitle.equals(tagTitle))
                {
                    Log.w(LOG_TAG, "InitAudioTracklist(): track title mismatch for file \"" + thisPath + "\"");
                    Log.w(LOG_TAG, "     tagger: \"" + tagTitle  + "\"");
                    Log.w(LOG_TAG, "    Android: \"" + thisTitle + "\"");
                    theTrack.changeTitle(tagTitle);
                    Log.w(LOG_TAG, "   choosing: \"" + theTrack.name + "\"");
                }
                /*
                Log.v(LOG_TAG, "theTagsFromFile.tagComposer = " + theTagsFromFile.tagComposer);
                */
                // NOTE: make sure unused tags are null, not empty strings
                theTrack.addTagInfo(
                        fixString(theTagsFromFile.tagMovementName),
                        fixString(theTagsFromFile.tagGrouping),
                        fixString(theTagsFromFile.tagSubtitle),
                        fixString(theTagsFromFile.tagComposer),
                        fixString(theTagsFromFile.tagConductor),
                        fixString(theTagsFromFile.tagGenre),
                        fixString(theTagsFromFile.tagAlbumArtist),
                        theTagsFromFile.tagYear);
            }

            tempList.add(theTrack);
            backGroundInitialisationNumberOfTracksDone++;
        }
        while (theCursor.moveToNext() && (tempList.size() < mMaxNoTracks));

        theCursor.close();
        Log.d(LOG_TAG, "found " + tempList.size() + " tracks");
        if (tempList.size() >= mMaxNoTracks)
        {
            Log.w(LOG_TAG, "NUMBER OF TRACKS LIMITED");
        }

        return tempList;
    }


    /**************************************************************************
     *
     * try private database before asking Android database
     *
     * Note that we waste some time with Read() in case the private
     * db is outdated, because we delete it afterwards.
     *
     *  Warning: This runs in a background task!!!
     *
     *************************************************************************/
    public ArrayList<AudioTrack> queryTracks()
    {
        ArrayList<AudioTrack> audioTrackList;
        int size = 0;

        // note that for a new private db the modification date will be zero
        if (MyMusicDatabaseHandler.mDbModificationDate == 0)
        {
            // private db does not exist, yet
            Log.v(LOG_TAG, "queryTracks() -- private db does not exist, yet");
        }
        else
        if (MyMusicDatabaseHandler.mDbModificationDate < mLatestAndroidDbAudioFileAdded)
        {
            // private db outdated, delete all rows
            Log.v(LOG_TAG, "queryTracks() -- cache outdated and must be recreated");
            Log.v(LOG_TAG, "queryTracks() -- delete all rows of cache database ...");
            MyMusicDatabaseHandler.deleteAllRows();
            Log.v(LOG_TAG, "queryTracks() -- ... done");
        }
        else
        {
            // private db valid, read all rows
            Log.v(LOG_TAG, "queryTracks() -- read private db ... ");
            audioTrackList = MyMusicDatabaseHandler.read();
            backGroundInitialisationNumberOfTracksDone = backGroundInitialisationNumberOfTracksTotal;
            Log.v(LOG_TAG, "queryTracks() -- ... done");
            if (audioTrackList != null)
            {
                Log.d(LOG_TAG, "found " + audioTrackList.size() + " tracks in private db");
                return audioTrackList;
            }
            Log.d(LOG_TAG, "found " + size + " tracks in private db");
        }

        // nothing stored, yet, thus read everything...
        AudioTagReaderInterface.noOfFileNotFoundErrors = 0;
        AudioTagReaderInterface.noOfCannotReadErrors = 0;
        AudioTagReaderInterface.noOfSuccess = 0;
        audioTrackList = queryTracksFromAndroidDb();
        if (audioTrackList != null) size = audioTrackList.size();
        Log.d(LOG_TAG, "found " + size + " tracks in system db");
        Log.d(LOG_TAG, AudioTagReaderInterface.noOfFileNotFoundErrors + " files not found");
        Log.d(LOG_TAG, AudioTagReaderInterface.noOfCannotReadErrors + " not accessible");
        Log.d(LOG_TAG, AudioTagReaderInterface.noOfSuccess + " files successfully read");

        // ...and store it to our database
        //AppGlobals.sNumOfFilteredFiles = mNoFilterSkipped;
        bBackgroundInitialisationFailedDueToAndroidBug = false;
        if (audioTrackList != null)
        {
            if ((AudioTagReaderInterface.noOfSuccess == 0) && (mNoFilterSkipped == 0))
            {
                Log.e(LOG_TAG, "GOT ERROR FOR ANY SINGLE FILE DUE TO ANDROID BUG. NO CACHE DB WRITTEN.");
                bBackgroundInitialisationFailedDueToAndroidBug = true;
            }
            else
            {
                Log.v(LOG_TAG, "queryTracks() -- write private db ... ");
                MyMusicDatabaseHandler.insert(audioTrackList);
                Log.v(LOG_TAG, "queryTracks() -- ... done");
            }
        } else
        {
            Log.w(LOG_TAG, "queryTracks(): no music found on device");
        }

        return audioTrackList;
    }


    /**************************************************************************
     * get date of last added file of media table
     *************************************************************************/
    static private long getLastDateAdded(@SuppressWarnings("SameParameterValue") final Uri tableName)
    {
        // tableName:
        //  MediaStore.Audio.Media.EXTERNAL_CONTENT_URI or
        //  MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        //  ...
        final String[] columns =
        {
            MediaStore.Audio.Media.DATE_ADDED
        };
        final String orderBy = MediaStore.Audio.Media.DATE_ADDED + " DESC";  // descending, i.e. get highest number, i.e. newest row

        // this is kind of SQL query:
        Cursor theCursor = mResolver.query(
                tableName,
                columns,    // columns to return, null returns all rows
                null,       // rows to return, as "WHERE ...", null returns all rows
                null,       // whereVal,   // selection arguments, replacing question marks in previous argument
                orderBy     // sort order, null: unordered
        );
        // handle error cases
        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return 0;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "nothing found");
            theCursor.close();
            return 0;
        }

        // from base class MediaColumns:
        int dateAddedColumn = theCursor.getColumnIndex(MediaStore.Audio.Media.DATE_ADDED);

        long thisDateAdded = theCursor.getLong(dateAddedColumn);
        theCursor.close();
        return thisDateAdded;
    }


    /**************************************************************************
     *
     * getter
     *
     *************************************************************************/
    public int getTracksTotal() { return backGroundInitialisationNumberOfTracksTotal; }
    public int getTracksDone() { return backGroundInitialisationNumberOfTracksDone; }
    public boolean getBackgroundInitialisationFailedDueToAndroidBug() { return bBackgroundInitialisationFailedDueToAndroidBug; }


    /**************************************************************************
     * helper function to ignore internal "Albums"
     *************************************************************************/
    private static boolean isAlbumIllegal(final String thisAlbumName)
    {
        return (thisAlbumName.equals("Notifications")) || (thisAlbumName.equals("Ringtones"));
    }


    /**************************************************************************
     * helper function to check path filter list
     *
     * Example:
     * -/storage/emulated/0/Music
     * +/storage/emulated/0/Music/Bach - Violin Concertos
     *
     * or relative:
     * -Music
     * +Music/Bach - Violin Concertos
     *
     * This should also work:
     * +/storage/emulated/0/Music
     * -Bach - Violin Concertos
     *************************************************************************/
    private static void setFilterTable(String filterPaths)
    {
        if (filterPaths != null)
        {
            // black/whitelist, separated by newline
            String[] temp = filterPaths.split(System.lineSeparator());
            int nValid = 0;
            String prevAbsPath = "/storage/emulated/0/";
            int index = 0;
            for (String line : temp)
            {
                if (line.startsWith("+") || line.startsWith("-"))
                {
                    if (line.substring(1).startsWith("/"))
                    {
                        prevAbsPath = line.substring(1);
                        if (!prevAbsPath.endsWith("/"))
                        {
                            prevAbsPath = prevAbsPath + "/";
                        }
                    }
                    else
                    {
                        // insert previous absolute path
                        line = line.charAt(0) + prevAbsPath + line.substring(1);
                        temp[index] = line;
                    }
                    nValid++;
                }
                else
                if (line.isEmpty())
                {
                    Log.w(LOG_TAG, "setFilterTable() : empty line skipped");
                }
                else
                {
                    Log.e(LOG_TAG, "setFilterTable() : line invalid: " + line);
                    return; // do not filter anything
                }
                index++;
            }

            if (nValid > 0)
            {
                Log.w(LOG_TAG, "setFilterTable() : " + nValid + " valid lines");
                mFilterPaths = temp;
            }
        }
    }

    /**************************************************************************
     * helper function to apply path filter list
     *************************************************************************/
    private static boolean applyFilterTable(String path)
    {
        boolean bRemove = false;
        boolean bIsFirst = true;
        for (String line : mFilterPaths)
        {
            char pm = line.charAt(0);
            if (bIsFirst)
            {
                if (pm == '+')
                {
                    // first entry is "+", thus remove anything else per default
                    bRemove = true;
                }
                bIsFirst = false;
            }
            String fPath = line.substring(1);
            if (path.startsWith(fPath))
            {
                bRemove = (pm == '-');
            }
        }
        return bRemove;
    }

    /**************************************************************************
     * helper function to make sure there is no empty string
     *************************************************************************/
    private static String fixString(String s)
    {
        if ((s == null) || (s.isEmpty()))
        {
            return null;
        }
        else
        {
            return s;
        }
    }
}
