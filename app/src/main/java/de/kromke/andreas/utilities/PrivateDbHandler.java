/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

/**
 * Handles playlist header table, playlist row table and bookmarks
 */
@SuppressWarnings("JavadocBlankLines")
public class PrivateDbHandler
{
    static private final String LOG_TAG = "O1M : PrivateDbHandler";
    static private final int MAX_NUM_BOOKMARKS = 100;
    private static PrivateDbHelper mDbHelper = null;
    private static SQLiteDatabase db = null;
    private static final boolean dbIsWritable = false;
    public  static long mDbModificationDate = 0;


    @SuppressWarnings("WeakerAccess")
    public static class playlistRow
    {
        int pos;        // 0..n
        String path;
    }


    @SuppressWarnings("WeakerAccess")
    public static class Bookmark
    {
        public String path;
        public long pos;
        public long creationDate;
    }


    /**************************************************************************
     *
     * open the database containing our tables
     *
     *************************************************************************/
    public static void open(Context context, boolean bCreateNew)
    {
        mDbHelper = new PrivateDbHelper(context);

        // get date of last change, needed for consistency check
        File dbPath = context.getDatabasePath(mDbHelper.getDbName());
        if (dbPath != null)
        {
            Log.v(LOG_TAG, "private db path = " + dbPath);
            if (bCreateNew)
            {
                deleteAllPlaylistHeaderRows();
                Log.v(LOG_TAG, "db to be recreated");
                mDbModificationDate = 0;
            }
            else
            {
                mDbModificationDate = dbPath.lastModified();
            }
            Log.v(LOG_TAG, "private db modification date = " + mDbModificationDate + " (" + Utilities.convertMsToIso(mDbModificationDate) + ")");
        }
        else
        {
            Log.v(LOG_TAG, "no private db found");
            mDbModificationDate = 0;
        }
    }


    /**************************************************************************
     *
     * Playlist Header table: delete all rows, but keep the table
     *
     *************************************************************************/
    private static void deleteAllPlaylistHeaderRows()
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(PrivateDatabase.PlaylistHeaderEntry.TABLE_NAME, null /* where */, null /* whereArgs */);
    }

/*
    public static void deletePlaylistHeaderEntry(final String name)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(PrivateDatabase.PlaylistHeaderEntry.TABLE_NAME,
                PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_NAME + "=" + "'" + name + "'", null);
    }
*/

    /**************************************************************************
     *
     * Playlist Header table: delete a single entry, specified by id
     *
     * playlistId = 0 is the current playing list
     *
     *************************************************************************/
    static void deletePlaylistHeaderEntry(long id)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(PrivateDatabase.PlaylistHeaderEntry.TABLE_NAME,
                PrivateDatabase.PlaylistHeaderEntry._ID + "=" + id, null);
    }


    /**************************************************************************
     *
     * Playlist Header table: insert a single entry
     *
     * Only used for playlists, not for the playing list, as the playing
     * list has id = 0
     *
     *************************************************************************/
    static long insertPlaylistHeaderEntry(final String name, int no_of_tracks, long duration)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();

        // use current time in [ms] as unique key
        long key = System.currentTimeMillis();
        values.put(PrivateDatabase.PlaylistHeaderEntry._ID, key);
        values.put(PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_NAME, name);
        values.put(PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_NO_TRACKS, no_of_tracks);
        values.put(PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_DURATION, duration);

        // Insert the new row, returning the primary key value of the new row
        /*long newRowId;
        newRowId = */db.insert(
            PrivateDatabase.PlaylistHeaderEntry.TABLE_NAME,
            null,
            values);
        return key;
    }


    /**************************************************************************
     *
     * Playlist Header table: read all entries
     *
     * Note that this table does not include playlist 0 which is the
     * stored Playing List.
     *
     *************************************************************************/
    static ArrayList<AudioBaseObject> readPlaylistHeaderTable()
    {
        // Gets the data repository in read mode
        if ((db == null) || (dbIsWritable))
        {
            db = mDbHelper.getReadableDatabase();
        }

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                PrivateDatabase.PlaylistHeaderEntry._ID;

        Cursor theCursor = db.query(
                PrivateDatabase.PlaylistHeaderEntry.TABLE_NAME,  // The table to query
                null,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no playlist found");
            theCursor.close();
            return null;
        }

        ArrayList<AudioBaseObject> theList = new ArrayList<>();

        do
        {
            @SuppressLint("Range") AudioPlaylist thePlaylist = new AudioPlaylist(
                    theCursor.getLong(theCursor.getColumnIndex(PrivateDatabase.PlaylistHeaderEntry._ID)),
                    theCursor.getString(theCursor.getColumnIndex(PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_NAME)),
                    theCursor.getInt(theCursor.getColumnIndex(PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_NO_TRACKS)),
                    theCursor.getLong(theCursor.getColumnIndex(PrivateDatabase.PlaylistHeaderEntry.COLUMN_NAME_DURATION)));

            theList.add(thePlaylist);
        }
        while (theCursor.moveToNext());
        theCursor.close();

        return theList;
    }


    /* *************************************************************************
     *
     * Playlist Row table: delete all rows, but keep the table
     *
     *************************************************************************/
    /*
    private static void deleteAllPlaylistRows()
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(PrivateDatabase.PlaylistRowEntry.TABLE_NAME, null / where /, null / whereArgs /);
    }
    */



    /**************************************************************************
     *
     * Playlist Row table: delete a single entry, specified by id
     *
     * playlistId = 0 is the current playing list
     *
     *************************************************************************/
    static void deletePlaylistRowEntry(long id)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(PrivateDatabase.PlaylistRowEntry.TABLE_NAME,
                PrivateDatabase.PlaylistRowEntry.COLUMN_PLAYLIST_ID + "=" + id, null);
    }


    /**************************************************************************
     *
     * Playlist Row table: insert a single entry
     *
     * playlistId = 0 is the current playing list
     *
     *************************************************************************/
    static void insertPlaylistRowEntry(long playlistId, ArrayList<AudioBaseObject> theList)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        int pos = 0;
        for (int i = 0; i < theList.size(); i++)
        {
            AudioBaseObject theObject = theList.get(i);
            if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
            {
                AudioTrack theTrack = (AudioTrack) theObject;

                // Create a new map of values, where column names are the keys
                ContentValues values = new ContentValues();

                values.put(PrivateDatabase.PlaylistRowEntry.COLUMN_PLAYLIST_ID, playlistId);
                values.put(PrivateDatabase.PlaylistRowEntry.COLUMN_PLAYLIST_POS, pos++);
                values.put(PrivateDatabase.PlaylistRowEntry.COLUMN_PATH, theTrack.getPath());

                // Insert the new row, returning the primary key value of the new row
                /*long newRowId;
                newRowId = */
                db.insert(
                        PrivateDatabase.PlaylistRowEntry.TABLE_NAME,
                        null,
                        values);
            }
        }
    }


    /**************************************************************************
     *
     * Playlist Row table: read all entries
     *
     * playlistId = 0 is the current playing list
     *
     *************************************************************************/
    @SuppressLint("Range")
    static ArrayList<playlistRow> readPlaylistRowTable(long playlistId)
    {
        // Gets the data repository in read mode
        if ((db == null) || (dbIsWritable))
        {
            db = mDbHelper.getReadableDatabase();
        }

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                PrivateDatabase.PlaylistRowEntry.COLUMN_PLAYLIST_POS;

        final String where = PrivateDatabase.PlaylistRowEntry.COLUMN_PLAYLIST_ID + " = " + playlistId;
        Cursor theCursor = db.query(
                PrivateDatabase.PlaylistRowEntry.TABLE_NAME,  // The table to query
                null,                     // The columns to return
                where,                             // The columns for the WHERE clause
                null,                  // The values for the WHERE clause
                null,                      // don't group the rows
                null,                       // don't filter by row groups
                sortOrder                          // The sort order
        );

        if (theCursor == null)
        {
            Log.v(LOG_TAG, "no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            Log.v(LOG_TAG, "no playlist row found");
            theCursor.close();
            return null;
        }

        ArrayList<playlistRow> theList = new ArrayList<>();

        do
        {
            playlistRow theObject = new playlistRow();
            theObject.pos  = theCursor.getInt(theCursor.getColumnIndex(PrivateDatabase.PlaylistRowEntry.COLUMN_PLAYLIST_POS));
            theObject.path = theCursor.getString(theCursor.getColumnIndex(PrivateDatabase.PlaylistRowEntry.COLUMN_PATH));
            theList.add(theObject);
        }
        while (theCursor.moveToNext());
        theCursor.close();

        return theList;
    }


    /**************************************************************************
     *
     * Bookmark table: insert a single entry
     *
     * Note: we use "replace" here to update existing values if any
     *
     *************************************************************************/
    private static void insertBookmark(final String path, long pos)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();

        // use current time in [ms] for LRU
        long creation_time = System.currentTimeMillis();
        values.put(PrivateDatabase.Bookmarks._ID, path);
        values.put(PrivateDatabase.Bookmarks.COLUMN_CREATION_TIME, creation_time);
        values.put(PrivateDatabase.Bookmarks.COLUMN_PLAY_POS, pos);

        // Insert the new row, returning the primary key value of the new row
        /*long newRowId;
        newRowId = */db.replace(
            PrivateDatabase.Bookmarks.TABLE_NAME,
            null,
            values);
    }


    /**************************************************************************
     *
     * Bookmark table: delete a single entry, specified by path
     *
     *************************************************************************/
    public static void deleteBookmark(final String path)
    {
        // Gets the data repository in write mode
        if ((db == null) || (!dbIsWritable))
        {
            db = mDbHelper.getWritableDatabase();
        }

        db.delete(PrivateDatabase.Bookmarks.TABLE_NAME,
                PrivateDatabase.Bookmarks._ID + " = ?",
                            new String[] {path});
    }


    /**************************************************************************
     *
     * Bookmarks table: read entry for given key
     *
     *************************************************************************/
    @SuppressLint("Range")
    public static Bookmark readBookmark(final String path)
    {
        // Gets the data repository in read mode
        if ((db == null) || (dbIsWritable))
        {
            db = mDbHelper.getReadableDatabase();
        }

        final String where = PrivateDatabase.Bookmarks._ID + " = ?";
        Cursor theCursor = db.query(
                PrivateDatabase.Bookmarks.TABLE_NAME,  // The table to query
                null,                     // The columns to return: all
                where,                             // The columns for the WHERE clause
                new String[] {path},               // The values for the WHERE clause
                null,                      // don't group the rows
                null,                       // don't filter by row groups
                null                       // The sort order
        );

        if (theCursor == null)
        {
            Log.e(LOG_TAG, "readBookmark() : no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            //Log.v(LOG_TAG, "no bookmark found");
            theCursor.close();
            return null;
        }

        Bookmark theBoookmark;
        do
        {
            Bookmark theObject = new Bookmark();
            theObject.path          = theCursor.getString(theCursor.getColumnIndex(PrivateDatabase.Bookmarks._ID));
            theObject.pos           = theCursor.getLong(theCursor.getColumnIndex(PrivateDatabase.Bookmarks.COLUMN_PLAY_POS));
            theObject.creationDate  = theCursor.getLong(theCursor.getColumnIndex(PrivateDatabase.Bookmarks.COLUMN_CREATION_TIME));
            theBoookmark = theObject;
        }
        while (theCursor.moveToNext());
        theCursor.close();

        return theBoookmark;
    }


    /**************************************************************************
     *
     * Bookmarks table: get number of entries
     *
     *************************************************************************/
    private static int getTableLenBookmark()
    {
        // Gets the data repository in read mode
        if ((db == null) || (dbIsWritable))
        {
            db = mDbHelper.getReadableDatabase();
        }

        return (int) DatabaseUtils.queryNumEntries(db, PrivateDatabase.Bookmarks.TABLE_NAME);
    }


    /**************************************************************************
     *
     * Bookmarks table: get oldest entry
     *
     *************************************************************************/
    @SuppressLint("Range")
    private static Bookmark getOldestBookmark()
    {
        // Gets the data repository in read mode
        if ((db == null) || (dbIsWritable))
        {
            db = mDbHelper.getReadableDatabase();
        }

        // According to https://stackoverflow.com/questions/45061067/select-the-row-with-maximum-minimum-value-in-sqlite
        // we can retrieve the complete row here by adding the "*":
        final String[] columns = new String[]{"*", "MIN(" + PrivateDatabase.Bookmarks._ID + ")"};
        Cursor theCursor = db.query(
                PrivateDatabase.Bookmarks.TABLE_NAME,  // The table to query
                columns,                  // The columns to return
                null,                     // The columns for the WHERE clause
                null,                   // The values for the WHERE clause
                null,                      // don't group the rows
                null,                       // don't filter by row groups
                null                       // The sort order
        );

        if (theCursor == null)
        {
            Log.e(LOG_TAG, "getOldestBookmark() : no cursor");
            return null;
        }
        if (!theCursor.moveToFirst())
        {
            //Log.v(LOG_TAG, "no bookmark found");
            theCursor.close();
            return null;
        }

        Bookmark theBoookmark;
        do
        {
            Bookmark theObject = new Bookmark();
            theObject.path          = theCursor.getString(theCursor.getColumnIndex(PrivateDatabase.Bookmarks._ID));
            theObject.pos           = theCursor.getLong(theCursor.getColumnIndex(PrivateDatabase.Bookmarks.COLUMN_PLAY_POS));
            theObject.creationDate  = theCursor.getLong(theCursor.getColumnIndex(PrivateDatabase.Bookmarks.COLUMN_CREATION_TIME));
            theBoookmark = theObject;
        }
        while (theCursor.moveToNext());
        theCursor.close();

        return theBoookmark;
    }


    /**************************************************************************
     *
     * Bookmark table: update a single entry and limit the number of entries
     *
     *************************************************************************/
    public static void updateBookmark(final String path, long pos)
    {
        int numBookmarks = getTableLenBookmark();
        if (numBookmarks >= MAX_NUM_BOOKMARKS)
        {
            Bookmark oldestBookmark = getOldestBookmark();
            if (oldestBookmark == null)
            {
                Log.e(LOG_TAG, "no oldest bookmark");
            }
            else
            {
                Log.d(LOG_TAG, "updateBookmark() : delete oldest bookmark, created " + oldestBookmark.creationDate);
                deleteBookmark(oldestBookmark.path);
            }
        }

        insertBookmark(path, pos);
    }
}
