/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * all data for a single audio composer
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioComposer extends AudioBaseObject
{
    ArrayList<AudioTrack> mTrackList = new ArrayList<>();  // tracks with that composer, only group number 0 tracks
    private final String birthAndDeathDates;
    private String firstName;
    private String lastName;
    private final String nameLastFirst;       // "lastName, firstName"
    private final boolean bIsGroupOfPersons;


    /**************************************************************************
     *
     * constructor
     *
     * Note that names like "Ludwig van Beethoven (1770-1827)" are passed as
     *      theName = "Ludwig van Beethoven"
     *  and theLifePeriod = "(1770-1827)".
     *
     *************************************************************************/
    public AudioComposer(String theName, String theLifePeriod, String thePicture, int theNoOfWorks, boolean bIsMoreThanOnePerson)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_COMPOSER_OBJECT;
        name = theName;
        birthAndDeathDates = theLifePeriod;
        no_of_works = no_of_matching_works = theNoOfWorks;
        picture_path = thePicture;
        bIsGroupOfPersons = bIsMoreThanOnePerson;

        // get first and last name for sorting etc.

        if (bIsGroupOfPersons)
        {
            name = beautifyName(name);
            firstName = "";
            lastName = name;        // do not split to first and last name
        }
        else
        if (name.isEmpty() || !sDecomposeComposerName)
        {
            firstName = "";
            lastName = name;        // do not split to first and last name
        }
        else
        {
            // get the last space character in the name. Note that we get -1 for "not found" which is no problem as we add 1 later
            int pos = getLastNamePos();
            lastName = name.substring(pos + 1);
            firstName = (pos < 0) ? "" : name.substring(0, pos);

            // Hack for Felix Mendelssohn Bartholdy who should be written Mendelssohn-Bartholdy, but he is not...
            if (lastName.equals("Bartholdy"))
            {
                pos = firstName.lastIndexOf(' ');   // "Felix Mendelssohn", <pos> points to the space
                if (pos > 0)
                {
                    String lastName1 = firstName.substring(pos + 1);
                    if (lastName1.equals("Mendelssohn"))
                    {
                        lastName = lastName1 + " " + lastName;    // precede last name #2 with last name #1
                        firstName = firstName.substring(0, pos);    // remove last name #1 from first name
                    }
                }
            }
        }

        // compose alternative name scheme with last name shown first

        if (firstName.isEmpty() || lastName.isEmpty())
        {
            // cannot split up
            nameLastFirst = name;
        }
        else
        {
            nameLastFirst = lastName + ", " + firstName;
        }
    }

    // Get the position in the name where the last name begins.
    // This is for sorting, so "van Beethoven" shall be treated as "Beethoven".
    // But further: "Strauß (Sohn)" shall be detected as to belong together.
    private int getLastNamePos()
    {
        int pos = name.lastIndexOf(' ');
        if (pos < 0)
        {
            // no space character found, return -1
            return pos;
        }
        // note that name cannot end with space character due to trimming
        // look for something like "(Sohn")
        if (name.charAt(pos + 1) == '(')
        {
            // opening bracket found. Look for an earlier space character
            if (pos > 0)
            {
                int pos2 = name.lastIndexOf(' ', pos - 1);
                pos = (pos2 >= 0) ? pos2 : pos;
            }
        }

        return pos;
    }

    // get first name including "von" or "van" or "de" or "de la" etc.
    private String getFirstName()
    {
        return firstName;
    }

    // get last name without "von" or "van" or "de" or "de la" etc.
    private String getLastName()
    {
        return lastName;
    }

    // for fast scroll sections. Use composer.
    @Override
    public String getSection()
    {
        return getLastName();
    }

    // for sorting (< 0: this is less than another, > 0: this is greater than another
    @Override
    public int compareTo(@NonNull AudioBaseObject another)
    {
        AudioComposer anotherComposer = (AudioComposer) another;
        // compare last name. If that differs, this is the first key to search for
        int result = getLastName().compareTo(anotherComposer.getLastName());
        // only compare first name in case the last name is similar
        return (result == 0) ? getFirstName().compareTo(((AudioComposer) another).getFirstName()) : result;
    }

    /*
    public boolean isSame(final ComposerName another)
    {
        //noinspection SimplifiableIfStatement
        if ((name.isEmpty()) && (another.name.isEmpty()))
        {
            return true;
        }
        return (name.equals(another.name));
    }
    */

    /*
    public static boolean isSame(final String composer1, final String composer2)
    {
        if ((composer1 == null) || (composer1.isEmpty()))
        {
            // only match, if both are null
            return ((composer2 == null) || (composer2.isEmpty()));
        }

        if ((composer2 == null) || (composer2.isEmpty()))
        {
            // only match, if both are null
            return false;
        }

        // both are non-null
        return (composer1.equals(composer2));
    }
    */

    // get name as "first last" or "last, first"
    public String getName()
    {
        if (sShowComposerLastNameFirst)
            return nameLastFirst;
        else
            return name;
    }

    // for display in a list, normal text
    public String getTitle()
    {
        if (name.isEmpty())
            return str_unknown_composer;
        else
        if (birthAndDeathDates != null)
            return getName() + " " + birthAndDeathDates;
        else
            return getName();
    }

    // for display in a list, small text
    @Override
    public String getInfo()
    {
        return "(" + getWorkNoInfo() + ")";
    }


    @Override
    public void initPicturePath(Context context)
    {
        initPicturePathForPersons();
    }


    /**************************************************************************
     *
     * Check, if the object matches the given filter objects
     *
     *************************************************************************/
    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        // theFilterComposer is ignored, because we already have a matching track list
        if ((theFilterPerformer == null) && (theFilterGenre == null))
        {
            // no filter, so always matches
            return true;
        }

        for (AudioTrack theTrack: mTrackList)
        {
            if (theTrack.matchesFilter(theFilterPerformer) && theTrack.matchesFilter(theFilterGenre))
            {
                // one match is sufficient
                return true;
            }
        }

        return false;    // no matches found
    }


    /**************************************************************************
     *
     * Check, if the object symbolises a group of persons (composers or performers)
     *
     *************************************************************************/
    @Override
    public boolean isPersonGroup()
    {
        return bIsGroupOfPersons;
    }
}
