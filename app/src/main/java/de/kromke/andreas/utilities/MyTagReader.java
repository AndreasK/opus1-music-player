/*
 * Copyright (C) 2016-17 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import de.kromke.andreas.audiotags.TagsBase;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Reads tags from audio file
 */

@SuppressWarnings("WeakerAccess")
public class MyTagReader extends AudioTagReaderInterface
{
    private static final String LOG_TAG = "O1M : MyTagReader";

    /*
     * get data from input stream
     */
    private void _get(String name, InputStream is, long l)
    {
        TagsBase tags = TagsBase.fromFileName(name, is, l);
        noOfSuccess++;
        if ((tags != null) && tags.read())
        {
            tags.combineValues();
            if (TagsBase.logLevel > 0)
            {
                tags.dump();
            }

            // replace numerical genres with text
            tags.replaceNumericalGenres();

            tagTitle = tags.values[TagsBase.idTitle];
            tagComposer = tags.values[TagsBase.idComposer];
            tagConductor = tags.values[TagsBase.idConductor];
            tagGrouping = tags.values[TagsBase.idGrouping];
            tagMovementName = tags.values[TagsBase.idCombinedMovement];    // automatically falls back to title, if necessary;
            tagSubtitle = tags.values[TagsBase.idSubtitle];
            tagAlbum = tags.values[TagsBase.idAlbum];
            tagAlbumArtist = tags.values[TagsBase.idAlbumArtist];
            tagDiscNo = tags.values[TagsBase.idDiscNo];
            tagDiscTotal = tags.values[TagsBase.idDiscTotal];
            tagTrack = tags.values[TagsBase.idTrackNo];
            tagTrackTotal = tags.values[TagsBase.idTrackTotal];
            tagGenre = tags.values[TagsBase.idGenre];
            // information from Android DB is unreliable, so get it here
            tagYear = tags.getNumericValue(TagsBase.idYear, 0);

            // fallback for missing title which must not be be null
            if (tagTitle == null)
            {
                tagTitle = name;
                int index = tagTitle.lastIndexOf('.');
                if (index > 0)
                {
                    tagTitle = tagTitle.substring(0, index);        // remove file name extension
                }
                Log.w(LOG_TAG, "_get() : derived title \"" + tagTitle + "\" from file name");
            }
        }

        if (TagsBase.logLevel > 0)
        {
            Log.v(LOG_TAG, "tagComposer    = " + tagComposer);
            Log.v(LOG_TAG, "tagConductor   = " + tagConductor);
            Log.v(LOG_TAG, "tagGrouping    = " + tagGrouping);
            Log.v(LOG_TAG, "tagSubtitle    = " + tagSubtitle);
            Log.v(LOG_TAG, "tagAlbum       = " + tagAlbum);
            Log.v(LOG_TAG, "tagAlbumArtist = " + tagAlbumArtist);
            Log.v(LOG_TAG, "tagDiscNo      = " + tagDiscNo);
            Log.v(LOG_TAG, "tagDiscNoTotal = " + tagDiscTotal);
            Log.v(LOG_TAG, "tagTrack       = " + tagTrack);
            Log.v(LOG_TAG, "tagTrackTotal  = " + tagTrackTotal);
            Log.v(LOG_TAG, "tagGenre       = " + tagGenre);
            Log.v(LOG_TAG, "=====================================\n\n");
        }
    }

    /*
     * get data from file or Uri, size may be -1 if unknown (from "share..." or "open with...")
    */
    public void _get(String thisPathOrUri, String name, long size, ContentResolver resolver)
    {
        TagsBase.doUseMovementTotal = false;    // do not show "1/4 Allegro", but "1. Allegro"

        InputStream is;
        long l;
        if (thisPathOrUri.startsWith("content://"))
//            if (thisPathOrUri.startsWith("content://media/"))
        {
            Uri uri = Uri.parse(thisPathOrUri);
            try
            {
                is = resolver.openInputStream(uri);
                l = size;
                _get(name, is, l);
            } catch (Exception e)
            {
                Log.v(LOG_TAG, "get() : cannot open Uri \"" + thisPathOrUri + "\": " + e.getMessage());
                //noinspection UnnecessaryReturnStatement
                return;
            }
        }
        else
        {
            File f = new File(thisPathOrUri);
            // WARN: try (bla) automatically closes bla outside this block!
            try (FileInputStream fis = new FileInputStream(f))
            {
                l = f.length();
                name = f.getName();
                is = fis;
                _get(name, is, l);
            } catch (Exception e)
            {
                Log.v(LOG_TAG, "get() : cannot open file \"" + thisPathOrUri + "\": " + e.getMessage());
                //noinspection UnnecessaryReturnStatement
                return;
            }
        }
    }

}
