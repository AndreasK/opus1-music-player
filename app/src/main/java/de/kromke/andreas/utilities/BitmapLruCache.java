/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * LRU cache for bitmaps
 */
@SuppressWarnings("WeakerAccess")
public class BitmapLruCache
{
    private static final String LOG_TAG = "O1M : BitmapLruCache";

    private final Map<Integer, BitmapCacheEntry> mCacheMap;
    private final int mCacheLen;
    private long mAccessTime;

    private static class BitmapCacheEntry
    {
        public long last_access;
        public Bitmap bitmap;
    }

    // constructor
    public BitmapLruCache(int nEntries)
    {
        Log.d(LOG_TAG, "LruCacheCreate()");
        mCacheMap = new HashMap<>();
        mCacheLen = nEntries;
        mAccessTime = 0;
    }

    public boolean containsKey(int key)
    {
        return mCacheMap.containsKey(key);
    }

    public Bitmap get(int key)
    {
        BitmapCacheEntry entry = mCacheMap.get(key);
        if (entry != null)
        {
            // update access time
            mAccessTime++;
            entry.last_access = mAccessTime;
            // return stored bitmap
            return entry.bitmap;
        }

        return null;
    }

    public void put(int key, Bitmap bitmap)
    {
        BitmapCacheEntry entry = new BitmapCacheEntry();
        entry.last_access = mAccessTime;
        entry.bitmap = bitmap;
        if (mCacheMap.size() >= mCacheLen)
        {
            // have to remove oldest element, i.e. that with lowest access time
            long oldestEntryAccessTime = Long.MAX_VALUE;
            int oldestEntryKey = -1;
            for (Map.Entry<Integer, BitmapCacheEntry> e : mCacheMap.entrySet())
            {
                int theKey = e.getKey();
                long theAccess = e.getValue().last_access;
                if ((oldestEntryKey < 0) || (theAccess < oldestEntryAccessTime))
                {
                    oldestEntryKey = theKey;
                    oldestEntryAccessTime = theAccess;
                }
            }
            mCacheMap.remove(oldestEntryKey);
        }
        mCacheMap.put(key, entry);
    }

}
