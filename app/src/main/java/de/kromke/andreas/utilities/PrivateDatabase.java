/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

/**
 * SQLite handling for playlist headers, playlist tracks and bookmarks.
 *
 * Note that we should not deal with audio track or album IDs here, because these
 *  do not survive a database rebuild.
 *
 * Note: the database is stored in /data/data/de.kromke..../databases
 * Note: Android db is resided in  /data/data/com.android.providers.media/databases
 */
@SuppressWarnings("ALL")
public class PrivateDatabase
{
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public PrivateDatabase() {}

    /* Inner class that defines the table contents */
    @SuppressWarnings("WeakerAccess")
    public static abstract class PlaylistHeaderEntry
    {
        public static final String TABLE_NAME = "playlistheaders";
        public static final String _ID = "_id";                          // (long) the creation date is used als unique ID
        public static final String COLUMN_NAME_NAME = "name";            // playlist name
        public static final String COLUMN_NAME_NO_TRACKS = "no_tracks";
        public static final String COLUMN_NAME_DURATION = "duration";

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + INT_TYPE + " PRIMARY KEY," +
                        COLUMN_NAME_NAME      + TEXT_TYPE + COMMA_SEP +
                        COLUMN_NAME_NO_TRACKS + INT_TYPE + COMMA_SEP +
                        COLUMN_NAME_DURATION  + INT_TYPE +
                        " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    /* Inner class that defines the table contents */
    @SuppressWarnings("WeakerAccess")
    public static abstract class PlaylistRowEntry
    {
        public static final String TABLE_NAME = "playlistrows";
        public static final String _ID = "_id";                             // (long) the creation date is used als unique ID
        public static final String COLUMN_PLAYLIST_ID  = "playlist_id";     // the playlist the row belongs to
        public static final String COLUMN_PLAYLIST_POS = "playlist_pos";    // position in list (0..)
        public static final String COLUMN_PATH = "path";                    // path of audio file

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + INT_TYPE + " PRIMARY KEY," +
                        COLUMN_PLAYLIST_ID  + INT_TYPE + COMMA_SEP +
                        COLUMN_PLAYLIST_POS + INT_TYPE + COMMA_SEP +
                        COLUMN_PATH         + TEXT_TYPE +
                        " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    /* Inner class that defines the table contents */
    @SuppressWarnings("WeakerAccess")
    public static abstract class Bookmarks
    {
        public static final String TABLE_NAME = "bookmarks";
        public static final String _ID = "path";                            // path of audio file (primary key)
        public static final String COLUMN_CREATION_TIME = "creation_time";  // (long) creation date and time for LRU mechanism
        public static final String COLUMN_PLAY_POS = "play_pos";            // remembered play position in [ms]

        public static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + TEXT_TYPE + " PRIMARY KEY," +
                        COLUMN_CREATION_TIME  + INT_TYPE + COMMA_SEP +
                        COLUMN_PLAY_POS + INT_TYPE +
                        " )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
