/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PrivateDbHelper extends SQLiteOpenHelper
{
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "PrivateData.db";

    @SuppressWarnings("WeakerAccess")
    public PrivateDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @SuppressWarnings("WeakerAccess")
    public final String getDbName()
    {
        return DATABASE_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(PrivateDatabase.PlaylistHeaderEntry.SQL_CREATE_ENTRIES);
        db.execSQL(PrivateDatabase.PlaylistRowEntry.SQL_CREATE_ENTRIES);
        db.execSQL(PrivateDatabase.Bookmarks.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // Just discard the data and start over
        db.execSQL(PrivateDatabase.PlaylistHeaderEntry.SQL_DELETE_ENTRIES);
        db.execSQL(PrivateDatabase.PlaylistRowEntry.SQL_DELETE_ENTRIES);
        db.execSQL(PrivateDatabase.Bookmarks.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        onUpgrade(db, oldVersion, newVersion);
    }
}
