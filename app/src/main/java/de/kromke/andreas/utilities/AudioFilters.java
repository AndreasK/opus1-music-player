/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.util.Log;

import java.util.ArrayList;

/**
 * Handles audio filters and filtered lists
 */
@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
public class AudioFilters
{
    // current filters
    static private AudioBaseObject filterComposer = null;
    static private AudioBaseObject filterGenre = null;
    static private AudioBaseObject filterPerformer = null;

    // all lists
    static public FilteredAudioObjectList audioAlbumList = new FilteredAudioObjectList();           // all albums
    static public FilteredAudioObjectList audioComposerList = new FilteredAudioObjectList();
    static public FilteredAudioObjectList audioGenreList = new FilteredAudioObjectList();
    static public FilteredAudioObjectList audioPerformerList = new FilteredAudioObjectList();
    static public FilteredAudioObjectList audioWorkList = new FilteredAudioObjectList();
    static public FilteredAudioObjectList audioTracksList = new FilteredAudioObjectList();          // the opened album or work
    static public FilteredAudioObjectList audioFolderList = new FilteredAudioObjectList();
    static public FilteredAudioObjectList audioPlayingList = new FilteredAudioObjectList();         // the current playing list
    static public FilteredAudioObjectList audioPlaylistList = new FilteredAudioObjectList();        // playlists

    static public void setAlbumList(ArrayList<AudioBaseObject> theList)
    {
        audioAlbumList.setRawList(theList);
    }

    static public void setTrackList(ArrayList<AudioBaseObject> theList)
    {
        // Note that in case the list contains more than one element,
        // the first object in list is a header, i.e. a work or album
        audioTracksList.setRawList(theList);
    }

    static public void setPlayingList(ArrayList<AudioBaseObject> theList, boolean bAppend)
    {
        // Note that in case the list contains more than one element,
        // the first object in list is a header, i.e. a work or album
        if (bAppend)
            audioPlayingList.appendRawList(theList);
        else
            audioPlayingList.setRawList(theList);
    }

    static public boolean removeObjectsFromPlayingList(ArrayList<AudioBaseObject> theList)
    {
        boolean bDone;
        //noinspection SimplifiableIfStatement
        if (audioPlayingList != null)
        {
            bDone = audioPlayingList.removeObjects(theList);
        }
        else
        {
            bDone = false;
        }
        return bDone;
    }

    static public void setComposerList(ArrayList<AudioBaseObject> theList)
    {
        audioComposerList.setRawList(theList);
    }

    static public void setPerformerList(ArrayList<AudioBaseObject> theList)
    {
        audioPerformerList.setRawList(theList);
    }

    static public void setWorkList(ArrayList<AudioBaseObject> theList)
    {
        audioWorkList.setRawList(theList);
    }

    static public void setGenreList(ArrayList<AudioBaseObject> theList)
    {
        audioGenreList.setRawList(theList);
    }

    static public void setFolderList(ArrayList<AudioBaseObject> theList)
    {
       audioFolderList.setRawList(theList);
    }

    static public void setPlaylistList(ArrayList<AudioBaseObject> theList)
    {
        audioPlaylistList.setRawList(theList);
    }

    /* *************************************************************************
     *
     * Find composer
     *
     *************************************************************************/
    /*
    static public AudioComposer findComposer(final String name)
    {
        AudioComposer theComposer = null;
        if ((name != null) && (audioComposerList != null))
        {
            for (int i = 0; i < audioComposerList.raw.getSize(); i++)
            {
                AudioComposer theObject = (AudioComposer) audioComposerList.raw.list.get(i);
                if (name.equals(theObject.name))
                {
                    theComposer = theObject;
                    break;
                }
            }
        }

        return theComposer;
    }
    */


    /**************************************************************************
     *
     * update list
     *
     *************************************************************************/

    private static void updateFilter()
    {
        long startTime = System.nanoTime();
        audioAlbumList.setFilters((AudioComposer) filterComposer, (AudioGenre) filterGenre, (AudioPerformer) filterPerformer);
        startTime = (System.nanoTime() - startTime) / 1000000;     // milliseconds
        Log.d("AudioFilters", "updateFilter() : elapsed for album filter = " + startTime);

        startTime = System.nanoTime();
        audioComposerList.setFilters((AudioComposer) filterComposer, (AudioGenre) filterGenre, (AudioPerformer) filterPerformer);
        startTime = (System.nanoTime() - startTime) / 1000000;     // milliseconds
        Log.d("AudioFilters", "updateFilter() : elapsed for composer filter = " + startTime);

        startTime = System.nanoTime();
        audioGenreList.setFilters((AudioComposer) filterComposer, (AudioGenre) filterGenre, (AudioPerformer) filterPerformer);
        startTime = (System.nanoTime() - startTime) / 1000000;     // milliseconds
        Log.d("AudioFilters", "updateFilter() : elapsed for genre filter = " + startTime);

        startTime = System.nanoTime();
        audioPerformerList.setFilters((AudioComposer) filterComposer, (AudioGenre) filterGenre, (AudioPerformer) filterPerformer);
        startTime = (System.nanoTime() - startTime) / 1000000;     // milliseconds
        Log.d("AudioFilters", "updateFilter() : elapsed for performer filter = " + startTime);

        startTime = System.nanoTime();
        audioWorkList.setFilters((AudioComposer) filterComposer, (AudioGenre) filterGenre, (AudioPerformer) filterPerformer);
        startTime = (System.nanoTime() - startTime) / 1000000;     // milliseconds
        Log.d("AudioFilters", "updateFilter() : elapsed for work filter = " + startTime);

        startTime = System.nanoTime();
        updateMatchingWorks();
        startTime = (System.nanoTime() - startTime) / 1000000;     // milliseconds
        Log.d("AudioFilters", "updateFilter() : elapsed for count works = " + startTime);
    }


    /**************************************************************************
     *
     * change filter
     *
     *************************************************************************/

    public static void setFilterComposer(AudioBaseObject theNewFilter)
    {
        if (theNewFilter != filterComposer)
        {
            filterComposer = theNewFilter;
            if (theNewFilter == null)
            {
                // remove selection
                audioComposerList.setRawSelectedIndex(-1);
            }
            updateFilter();
        }
    }

    public static void setFilterPerformer(AudioBaseObject theNewFilter)
    {
        if (theNewFilter != filterPerformer)
        {
            filterPerformer = theNewFilter;
            if (theNewFilter == null)
            {
                // remove selection
                audioPerformerList.setRawSelectedIndex(-1);
            }
            updateFilter();
        }
    }

    public static void setFilterGenre(AudioBaseObject theNewFilter)
    {
        if (theNewFilter != filterGenre)
        {
            filterGenre = theNewFilter;
            if (theNewFilter == null)
            {
                // remove selection
                audioGenreList.setRawSelectedIndex(-1);
            }
            updateFilter();
        }
    }


    /**************************************************************************
     *
     * query
     *
     *************************************************************************/

    public static boolean isComposerFilter()
    {
        return (filterComposer != null);
    }

    public static boolean isGenreFilter()
    {
        return (filterGenre != null);
    }

    public static boolean isPerformerFilter()
    {
        return (filterPerformer != null);
    }

    public static boolean isFilter()
    {
        return isComposerFilter() || isGenreFilter() || isPerformerFilter();
    }


    /**************************************************************************
     *
     * Compose the text shown at the lower edge of the screen
     * describing the active filters
     *
     * Note that composer filter will not be shown in composer list etc.
     *
     *************************************************************************/

    public static String getFilterText(FilteredAudioObjectList theAudioObjectList)
    {
        String filterText = "";
        if ((filterComposer != null) && (theAudioObjectList != audioComposerList))
        {
            filterText += AppGlobals.getComposerFilterText((AudioComposer) filterComposer);
        }
        if ((filterGenre != null) && (theAudioObjectList != audioGenreList))
        {
            if (!filterText.isEmpty())
            {
                filterText += "\n";
            }
            filterText += AppGlobals.getGenreFilterText((AudioGenre) filterGenre);
        }
        if ((filterPerformer != null) && (theAudioObjectList != audioPerformerList))
        {
            if (!filterText.isEmpty())
            {
                filterText += "\n";
            }
            filterText += AppGlobals.getPerformerFilterText((AudioPerformer) filterPerformer);
        }
        return filterText;
    }


    /**************************************************************************
     *
     * helper function to reset the number of matching works to be the
     * total number of works
     *
     *************************************************************************/
    private static void resetMatchingWorks(FilteredAudioObjectList theList)
    {
        for (int i = 0; i < theList.raw.getSize(); i++)
        {
            AudioBaseObject theObject = theList.raw.list.get(i);
            theObject.no_of_matching_works = theObject.no_of_works;
        }
    }


    /**************************************************************************
     *
     * Count the number of matching works, as shown in the genre or
     * composer or performer list.
     *
     * Note that due to performance reasons it's sufficient to update only
     * the items that match the current filter criteria, because the others
     * are invisible.
     *
     *************************************************************************/
    public static void updateMatchingWorks()
    {
        // it has been advised to get the list count only once to save procedure calls,
        // thus "size" is initialised only once
        ArrayList<AudioBaseObject> objectList;
        int size;

        // composer entries match genre and performer
        if ((filterGenre == null) && (filterPerformer == null))
        {
            resetMatchingWorks(audioComposerList);
        }
        else
        {
            objectList = audioComposerList.getListFiltered().list;
            size = objectList.size();
            for (int i = 0; i < size; i++)
            {
                // get a composer
                AudioComposer theObject = (AudioComposer) objectList.get(i);
                // update the number of matching works for that composer
                theObject.no_of_matching_works = audioWorkList.countMatchingObjects(
                        theObject,
                        (AudioGenre) filterGenre,
                        (AudioPerformer) filterPerformer);
            }
        }

        // genre entries match composer and performer
        if ((filterComposer == null) && (filterPerformer == null))
        {
            resetMatchingWorks(audioGenreList);
        }
        else
        {
            objectList = audioGenreList.getListFiltered().list;
            size = objectList.size();
            for (int i = 0; i < size; i++)
            {
                // get a genre
                AudioGenre theObject = (AudioGenre) objectList.get(i);
                // update the number of matching works for that genre
                theObject.no_of_matching_works = audioWorkList.countMatchingObjects(
                        (AudioComposer) filterComposer,
                        theObject,
                        (AudioPerformer) filterPerformer);
            }
        }

        // performer entries match composer and genre
        if ((filterComposer == null) && (filterGenre == null))
        {
            resetMatchingWorks(audioPerformerList);
        }
        else
        {
            objectList = audioPerformerList.getListFiltered().list;
            size = objectList.size();
            for (int i = 0; i < size; i++)
            {
                // get a performer
                AudioPerformer theObject = (AudioPerformer) objectList.get(i);
                // update the number of matching works for that performer
                theObject.no_of_matching_works = audioWorkList.countMatchingObjects(
                        (AudioComposer) filterComposer,
                        (AudioGenre) filterGenre,
                        theObject);
            }
        }
    }

    /* UNOPTIMISED VERSION
    public static void updateMatchingWorks()
    {
        // composer entries match genre and performer
        if ((filterGenre == null) && (filterPerformer == null))
        {
            resetMatchingWorks(audioComposerList);
        }
        else
        {
            for (int i = 0; i < audioComposerList.raw.list.size(); i++)
            {
                // get a composer
                AudioComposer theObject = (AudioComposer) audioComposerList.raw.list.get(i);
                // update the number of matching works for that composer
                theObject.no_of_matching_works = audioWorkList.countMatchingObjects(
                        theObject,
                        (AudioGenre) filterGenre,
                        (AudioPerformer) filterPerformer);
            }
        }

        // genre entries match composer and performer
        if ((filterComposer == null) && (filterPerformer == null))
        {
            resetMatchingWorks(audioGenreList);
        }
        else
        {
            for (int i = 0; i < audioGenreList.getTotalSize(); i++)
            {
                // get a genre
                AudioGenre theObject = (AudioGenre) audioGenreList.raw.list.get(i);
                // update the number of matching works for that genre
                theObject.no_of_matching_works = audioWorkList.countMatchingObjects(
                        (AudioComposer) filterComposer,
                        theObject,
                        (AudioPerformer) filterPerformer);
            }
        }

        // performer entries match composer and genre
        if ((filterComposer == null) && (filterGenre == null))
        {
            resetMatchingWorks(audioPerformerList);
        }
        else
        {
            for (int i = 0; i < audioPerformerList.getTotalSize(); i++)
            {
                // get a performer
                AudioPerformer theObject = (AudioPerformer) audioPerformerList.raw.list.get(i);
                // update the number of matching works for that performer
                theObject.no_of_matching_works = audioWorkList.countMatchingObjects(
                        (AudioComposer) filterComposer,
                        (AudioGenre) filterGenre,
                        theObject);
            }
        }
    } */

}
