/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;

import java.io.File;
import java.util.ArrayList;

/**
 * all necessary information about a folder containing music files
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioFolder extends AudioBaseObject
{
    private final String mPath;
    private final String mUriStr;
    public int no_of_folders;
    public ArrayList<AudioBaseObject> mTrackList;
    private static final String[] pictureFilenames =
    {
        "albumart.jpg",     // will not work in Android 11 without full file access
        "AlbumArt.jpg",     // ditto
        "folder.jpg",       // ditto
        "ordner.jpg"        // hack against Android 11+
    };

    public AudioFolder(
            String thePath,         // is pseudo path in SAF mode
            String theUriStr,       // is null in normal (non-SAF) mode
            int theNoOfTracks)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_FOLDER_OBJECT;
        mPath = thePath;
        mUriStr = theUriStr;
        no_of_tracks = theNoOfTracks;
        no_of_folders = 1;
        mTrackList = new ArrayList<>();

        if (mPath != null)
        {
            // create name from path
            int pos = thePath.lastIndexOf('/');
            if (pos >= 0)
            {
                pos++;
            } else
            {
                pos = 0;
            }
            name = thePath.substring(pos);
        }
    }

    @Override
    public final String getPath()
    {
        return mPath;
    }

    // for display in a list, small text
    @Override
    public String getInfo()
    {
        String infoText = "(" + getTrackNoInfo();
        if (no_of_folders > 1)
        {
            infoText += " in " + no_of_folders + " " + str_folders_uc_dativ;
        }
        infoText += ")";
        return infoText;
    }

    /**************************************************************************
     *
     * The path can either be a content URI or a traditional one.
     * This function tries to find a picture in that directory.
     *
     *************************************************************************/
    @Override
    public void initPicturePath(Context context)
    {
        if (mPath == null)
        {
            picture_path = null;
        }
        else
        if (mUriStr != null)
        {
            // SAF mode
            for (String name : pictureFilenames)
            {
                picture_path = SafUtilities.canRead(context, mUriStr, name);
                if (picture_path != null)
                {
                    break;
                }
            }
        }
        else
        {
            // non-SAF mode

            for (String name : pictureFilenames)
            {
                File picFile = new File(mPath, name);
                if (picFile.exists() && picFile.canRead())
                {
                    picture_path = picFile.getPath();
                    break;
                }
            }
        }
    }

    /**************************************************************************
     * base class function
     *************************************************************************/
    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        // folders are unfiltered
        return true;
    }
}
