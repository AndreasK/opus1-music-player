/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import androidx.annotation.LayoutRes;
import androidx.core.content.ContextCompat;

import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import de.kromke.andreas.opus1musicplayer.ImageViewDeferred;
import de.kromke.andreas.opus1musicplayer.R;
import de.kromke.andreas.opus1musicplayer.UserSettings;

//import static java.lang.System.identityHashCode;

/**
 * Fills the list view with data of different types
 **/
@SuppressWarnings({"JavadocBlankLines", "JavadocLinkAsPlainText"})
public class AudioBaseListAdapter extends BaseAdapter implements SectionIndexer
{
    static private final String LOG_TAG = "O1M : AudioBaseListAdpt";
    //public String mUniqueHash;   // for debugging purposes, to check if there are multiple instances
    private final Context mContext;
    private FilteredAudioObjectList.AudioObjectList mObjects;
    private BitmapLruCache mBitmapCache;
    // section handling for fast scroll
    private String[] mSections;
    private int[] mSectionForPosition;
    private int[] mPositionForSection;

    private final LayoutInflater mTheInflater;
    private final Drawable mDefaultBitmapNoAlbumArt;
    private final Drawable mDefaultBitmapUnknownPerson;
    private final Drawable mDefaultBitmapUnknownPersonGroup;
    private final Drawable mDefaultBitmapUnknownGenre;
    private final Drawable mDefaultBitmapFolder;
    private final Drawable mDefaultBitmapFolderMusic;
    private final Drawable mDefaultBitmapPlaylist;
    private int mImageSize;
    private static final int mLruSize = 20;
    private String mComposerColourString = "";
    private final boolean mUseGridView;

    // global settings
    public static boolean sbShowTrackNumbers;
    public static int sShowConductorMode;
    public static boolean sbShowSubtitle;
    public static int sGenderismMode;

    private Bitmap bmFolderMask = null;        // only needed in case there is a folder in the list
    private int mNumOfDifferentViews = 1;
    private final boolean mPlaylistMode;
    private final boolean mFileViewInsteadOfTrackView;
    private int mTitleColour, mInfoColour;


    /**************************************************************************
     *
     * Section Indexer function
     *
     *************************************************************************/
    @Override
    public Object[] getSections()
    {
        return mSections;
    }


    /**************************************************************************
     *
     * Section Indexer function
     *
     *************************************************************************/
    @Override
    public int getPositionForSection(int sectionIndex)
    {
        return mPositionForSection[sectionIndex];
    }


    /**************************************************************************
     *
     * Section Indexer function
     *
     *************************************************************************/
    @Override
    public int getSectionForPosition(int position)
    {
        return mSectionForPosition[position];
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void loadFolderMaskBitmap()
    {
        if (bmFolderMask == null)
        {
            bmFolderMask = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.folder_2_mask);
            if (bmFolderMask != null)
            {
                bmFolderMask = BitmapUtils.getResizedBitmap(bmFolderMask, mImageSize, mImageSize);
            }
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void createSections()
    {
        int size = mObjects.getSize();
        if ((size <= 0) || (mFileViewInsteadOfTrackView)|| (mPlaylistMode))
        {
            mSectionForPosition = new int[0];
            mPositionForSection = new int[0];
            mSections = new String[0];
            return;
        }

        mSectionForPosition = new int[size];
        int[] tempPositionForSection = new int[size];   // to be truncated later
        String[] tempSections = new String[size];   // to be truncated later

        //boolean bIsSorted = true;

        int num_sections = 0;
        char cp = ' ';
        int i_pos = 0;

        for (AudioBaseObject theObject: mObjects.list)
        {
            final String theSectionName = theObject.getSection();
            char c0;

            if ((theSectionName != null) && !theSectionName.isEmpty())
            {
                c0 = Character.toUpperCase(theSectionName.charAt(0));
                if (c0 < '0')
                {
                    c0 = ' ';   // treat all control characters like space
                }
                else
                if (Character.isDigit(c0))
                {
                    c0 = '0';   // treat all digits as same section
                }
            }
            else
            {
                c0 = cp;
            }

            if ((i_pos == 0) || (c0 != cp))
            {
                tempPositionForSection[num_sections] = i_pos;
                switch(c0)
                {
                    case ' ': tempSections[num_sections] = "#"; break;
                    case '0': tempSections[num_sections] = "0-9"; break;
                    default:  tempSections[num_sections] = Character.toString(c0); break;
                }
                num_sections++;
            }

            //Log.d(LOG_TAG, "createSections() : album \"" + theSectionName + "\" is section " + (num_sections - 1));
            mSectionForPosition[i_pos++] = num_sections - 1;
            cp = c0;
        }

        // truncate arrays

        mPositionForSection = new int[num_sections];
        mSections = new String[num_sections];
        // truncate array by copying
        System.arraycopy(tempPositionForSection, 0, mPositionForSection, 0, num_sections);
        System.arraycopy(tempSections, 0, mSections, 0, num_sections);
    }


    /**************************************************************************
     *
     * called by constructor or in case the list has changed
     *
     *************************************************************************/
    private void scanObjectList()
    {
        // create bitmap cache
        mBitmapCache = new BitmapLruCache(mLruSize);

        // only tracks use a Linear Layout, and only in case they are not shown as files
        mNumOfDifferentViews = 1;
        if (mObjects != null)
        {
            int size = mObjects.getSize();

            if (mFileViewInsteadOfTrackView)
            {
                // This is the folder list. Each object is shown as Relative Layout, so
                // mNumOfDifferentViews remains 1.
                // The objects are coloured alternatingly
                for (int i = 0; i < size; i++)
                {
                    AudioBaseObject theObject = mObjects.list.get(i);
                    theObject.alternating_colour = i % 2;
                    if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_FOLDER_OBJECT)
                    {
                        loadFolderMaskBitmap();
                    }
                }
            } else
            {
                AudioTrack thePreviousTrack = null;
                int theColour = 1;

                for (int i = 0; i < size; i++)
                {
                    int increment = 1;
                    AudioBaseObject theObject = mObjects.list.get(i);
                    if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
                    {
                        AudioTrack theTrack = (AudioTrack) theObject;
                        // tracks are shown in linear view
                        mNumOfDifferentViews = 2;
                        if ((thePreviousTrack != null) && theTrack.isSameWork(thePreviousTrack))
                        {
                            // use same colour as previous object
                            increment = 0;
                        }

                        // maybe the next one is a (also) a track
                        thePreviousTrack = (AudioTrack) theObject;
                    } else
                    {
                        // objects other than tracks are shown with alternating colour
                        // the object was not a track
                        thePreviousTrack = null;
                        if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_FOLDER_OBJECT)
                        {
                            loadFolderMaskBitmap();
                        }
                    }

                    theColour = (theColour + increment) % 2;
                    theObject.alternating_colour = theColour;
                }
            }
        }
    }


    /**************************************************************************
     *
     * constructor
     *
     * bFileViewInsteadOfTrackView is true only in folder fragment where all
     * tracks are shown as files, i.e. with icon.
     *
     * BUG in Android, see: http://stackoverflow.com/questions/13685275/outofmemory-error-in-custom-listview-adapter-mono-android
     *
     *************************************************************************/
    public AudioBaseListAdapter
    (
        Context c,
        FilteredAudioObjectList.AudioObjectList theObjects,
        int theImageSize,
        boolean bFileViewInsteadOfTrackView,
        boolean bPlaylistMode,
        boolean useGridView
    )
    {
        //mUniqueHash = String.format("0x%08x", identityHashCode(this));
        //Log.d(LOG_TAG, "AudioBaseListAdapter(" + mUniqueHash + ")");
        mContext = c;
        mObjects = theObjects;
        mTheInflater = LayoutInflater.from(c);
        mUseGridView = useGridView;
        mImageSize = theImageSize;

        if (mImageSize < 8)
        {
            Log.e(LOG_TAG, "AudioBaseListAdapter() : illegal image size " + theImageSize);
            mImageSize = 8;
        }
        mFileViewInsteadOfTrackView = bFileViewInsteadOfTrackView;  // show tracks as files with file name, used for folder view
        mPlaylistMode = bPlaylistMode;

        // compose composer colour string, make sure to remove alpha value
//        int composerColour = UserSettings.getThemedColourFromResId(mContext, R.attr.colourTextLine2AsHeader);
//        mComposerColourString = "<font color='#" + String.format("%X", composerColour).substring(2) + "'>";

        // default bitmap
        //        mDefaultBitmap = c.getResources().getDrawable(R.drawable.no_album_art);  // deprecated
        mDefaultBitmapNoAlbumArt = ContextCompat.getDrawable(c, R.drawable.no_album_art);
        mDefaultBitmapUnknownPerson = ContextCompat.getDrawable(c, R.drawable.no_composer_art);
        mDefaultBitmapUnknownPersonGroup = ContextCompat.getDrawable(c, R.drawable.faces_512px);
        mDefaultBitmapUnknownGenre = ContextCompat.getDrawable(mContext, R.drawable.genre_unknown);
        mDefaultBitmapFolder = ContextCompat.getDrawable(mContext, R.drawable.folder_2);
        mDefaultBitmapFolderMusic = ContextCompat.getDrawable(mContext, R.drawable.folder_music);
        mDefaultBitmapPlaylist = ContextCompat.getDrawable(mContext, R.drawable.playlist);

        // updates number of different views and row colours
        scanObjectList();
        createSections();
    }


    /**************************************************************************
     *
     * exchange the object list, used when filters change
     *
     *************************************************************************/
    public void setObjectList(FilteredAudioObjectList.AudioObjectList theObjects)
    {
        Log.d(LOG_TAG, "setObjectList()");
        mObjects = theObjects;
        notifyDataSetChanged();     // omitting this will crash the SectionIndexer!!!
        scanObjectList();
        createSections();
    }


    /**************************************************************************
     *
     * the title text of the header shall be different, used for playing fragment
     *
     *************************************************************************/
    public void setHeaderColoursForPlaylistMode(int colourLine1, int colourLine2, int colourComposer)
    {
        mTitleColour = colourLine1;
        mInfoColour = colourLine2;
        // compose composer colour string, make sure to remove alpha value
        mComposerColourString = "<font color='#" + String.format("%X", colourComposer).substring(2) + "'>";
    }


    /**************************************************************************
     *
     * tell number of objects in list
     *
     *************************************************************************/
    @Override
    public int getCount()
    {
        int result;

        if (mObjects == null)
            result = 0;
        else
            result = mObjects.getSize();    // may return -1 in case the list does not even exist
        if (result < 0)
            result = 0;
        return result;
    }

    @Override
    public Object getItem(int arg0)
    {
        return null;
    }

    @Override
    public long getItemId(int arg0)
    {
        return 0;
    }


    /**************************************************************************
     *
     * get title text line (bold text) for object
     *
     *************************************************************************/
    private String getTitleForObject(final AudioBaseObject currObject)
    {
        return currObject.getTitle();
    }


    /**************************************************************************
     *
     * get info text line (small text) for object
     *
     *************************************************************************/
    private String getInfoForObject(final AudioBaseObject currObject, final AudioBaseObject nextObject)
    {
        if (mFileViewInsteadOfTrackView && currObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
        {
            String thePath = currObject.getRawPath();
            if (SafUtilities.isSafPath(thePath))
            {
                Uri theUri = Uri.parse(thePath);
                thePath = theUri.getPath();
                if (thePath == null)
                {
                    Log.e(LOG_TAG, "getInfoForObject() : invalid Uri: " + currObject.getPath());
                    thePath = "/uri/invalid";
                }
            }
            int pos = thePath.lastIndexOf('/');
            if (pos < 0)
                pos = 0;    // should not happen
            return thePath.substring(pos + 1);
        } else
        {
            if ((currObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_WORK_OBJECT) &&
                    (nextObject != null) &&
                    (nextObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT))
            {
                return ((AudioWork) currObject).getInfoWithoutPerformer();
            } else
            {
                return currObject.getInfo();
            }
        }
    }

    // helper
    private Drawable getDrawableForGenreWithoutPicturePath(final AudioBaseObject currObject)
    {
        Drawable drawable;
        switch (currObject.name)
        {
            case "Classical":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_classical);
                break;
            case "Chamber Music":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_chamber_music);
                break;
            case "Sonata":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_sonata);
                break;
            case "Opera":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_opera);
                break;
            case "Symphony":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_symphony);
                break;
            case "Baroque":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_baroque);
                break;
            case "Soundtrack":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_soundtrack);
                break;
            case "Audiobook":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_audiobook);
                break;
            case "Speech":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_speech);
                break;
            case "Rock":
            case "Pop":
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_pop);
                break;
            case "":
                drawable = mDefaultBitmapUnknownGenre;
                break;
            default:
                drawable = ContextCompat.getDrawable(mContext, R.drawable.genre_others);
                break;
        }
        return drawable;
    }


    /*
     * comment missing
     */
    private Drawable getDrawableForObjectWithoutPicturePath(final AudioBaseObject currObject)
    {
        switch (currObject.objectType)
        {
            case AUDIO_ALBUM_OBJECT:
                return mDefaultBitmapNoAlbumArt;
            case AUDIO_PERFORMER_OBJECT:
            case AUDIO_WORK_OBJECT:
            case AUDIO_COMPOSER_OBJECT:
                return (currObject.isPersonGroup()) ?  mDefaultBitmapUnknownPersonGroup : mDefaultBitmapUnknownPerson;
            case AUDIO_TRACK_OBJECT:
                return mDefaultBitmapFolderMusic;
            case AUDIO_FOLDER_OBJECT:
                return mDefaultBitmapFolder;
            case AUDIO_GENRE_OBJECT:
                return getDrawableForGenreWithoutPicturePath(currObject);
            case AUDIO_PLAYLIST_OBJECT:
                return mDefaultBitmapPlaylist;
        }
        return null;
    }


    /**************************************************************************
     *
     * number of different values returned by getItemViewType()
     *
     *************************************************************************/
    @Override
    public int getViewTypeCount()
    {
        // two types of views: 0 == album header / 1 = track
        return mNumOfDifferentViews;
    }


    /**************************************************************************
     *
     * If the list has a header with different view type, this function
     * must return different values
     *
     *************************************************************************/
    @Override
    public int getItemViewType(int position)
    {
        if (mNumOfDifferentViews < 2)
        {
            // there is only one view type
            return 0;
        }

        AudioBaseObject theObject = mObjects.list.get(position);
        if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
        {
            return 1;   // Linear Layout
        } else
        {
            return 0;   // Relative Layout
        }
    }


    /**************************************************************************
     *
     * check if both objects are single-track-works and share
     * album, composer, performer and year
     *
     *************************************************************************/
    private boolean matchObjects
    (
        final AudioTrack track1,
        final AudioBaseObject object2
    )
    {
        // other object must exist and must be track
        if ((object2 == null) || (object2.objectType != AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT))
        {
            return false;
        }

        AudioTrack track2 = (AudioTrack) object2;

        // both objects must belong to one-track-works
        if ((track1.getWorkNoOfTracks() != 1) || (track2.getWorkNoOfTracks() != 1) ||
            (track1.mWork == null) || (track2.mWork == null))
        {
            return false;
        }

        //noinspection RedundantIfStatement
        if ((track1.getPerformer() != track2.getPerformer()) ||
            (track1.getComposer() != track2.getComposer()) ||
            (track1.getAlbum() != track2.getAlbum()) ||
            (track1.year != track2.year) ||
            (!track1.getConductorName().equals(track2.getConductorName())))
        {
            return false;
        }

        return true;
    }


    /**************************************************************************
     *
     * Helper function to generate the track title
     *
     *************************************************************************/
    public static String getTrackTitle(AudioTrack currObject)
    {
        String trackTitle = currObject.name;

        // signalise bookmark
        if (currObject.play_pos_ms > 0)
        {
            trackTitle = "* " + trackTitle;
        }

        // precede track number, if set in preferences
        if (sbShowTrackNumbers)
        {
            if (currObject.track_no > 0)
            {
                // divide by 1000 to get CD number from track number
                int cd_no = currObject.track_no / 1000;
                if (cd_no > 0)
                {
                    int tr_no = currObject.track_no % 1000;
                    trackTitle = "[" + cd_no + "/" + tr_no + "] " + trackTitle;
                } else
                {
                    trackTitle = "[" + currObject.track_no + "] " + trackTitle;
                }
            }
        }

        // add subtitle, if set in preferences
        if ((sbShowSubtitle) && (currObject.subtitle != null))
        {
            trackTitle += ", " + currObject.subtitle;
        }

        return trackTitle;
    }


    /**************************************************************************
     *
     * get view for tracks
     *
     *************************************************************************/
    private View getTrackView
    (
        int position,
        AudioTrack currObject,
        AudioBaseObject prevAudioObject,
        AudioBaseObject nextAudioObject,
        View convertView,
        ViewGroup parent
    )
    {
        LinearLayout audioTrackLay;

        // check if previous and next objects match
        boolean bMatchesPrevious = matchObjects(currObject, prevAudioObject);
        boolean bMatchesNext = matchObjects(currObject, nextAudioObject);
        //Log.d(LOG_TAG, "getTrackView() -- matchesPrevious = " + bMatchesPrevious);
        //Log.d(LOG_TAG, "getTrackView() -- matchesNext = " + bMatchesNext);

        //map to audio track layout
        if (convertView != null)
        {
            // just recycle the View. Can be sure it's the correct type
            audioTrackLay = (LinearLayout) convertView;
        } else
        {
            audioTrackLay = (LinearLayout) mTheInflater.inflate(R.layout.row_track, parent, false);
        }

        // apply colour theme
        //audioTrackLay.setBackgroundColor(UserSettings.getThemedColourFromResId(R.color.track_list_header_background));
        //get title and artist views
        TextView composerAndWorkView = audioTrackLay.findViewById(R.id.track_composer_and_work);
        TextView titleView = audioTrackLay.findViewById(R.id.track_title);
        TextView artistView = audioTrackLay.findViewById(R.id.track_performer);

        //get track information
        String trackComposer = currObject.getComposerName(false /* unknown composer remains "" */);
        String trackGroup = currObject.grouping;
        String trackTitle = getTrackTitle(currObject);
        String trackPerformer = currObject.getPerformerName();
        long trackYear = currObject.year;

        // check previous and next item to decide which information to show
        boolean bPrecededWithWork = false;
        boolean bIsFirstOfGroup = true;
        boolean bIsLastOfGroup = true;
        if (prevAudioObject != null)
        {
            if ((prevAudioObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_WORK_OBJECT))
            {
                bPrecededWithWork = true;
            }

            if ((prevAudioObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT))
            {
                if (currObject.isSameWork((AudioTrack) prevAudioObject))
                {
                    bIsFirstOfGroup = false;
                }
            }
        }

        if (nextAudioObject != null)
        {
            if ((nextAudioObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT))
            {
                if (currObject.isSameWork((AudioTrack) nextAudioObject))
                {
                    bIsLastOfGroup = false;
                }
            }
        }

        // omit composer if the track is preceded with a work entry or if previous tracks "matches"
        if (bPrecededWithWork || bMatchesPrevious)
        {
            trackComposer = "";
        }

        // omit work if the track is preceded with a work entry
        if (trackGroup == null || bPrecededWithWork)
        {
            trackGroup = "";
        }

        // add conductor, if set in preferences
        if ((sShowConductorMode != 0) && (currObject.conductor != null))
        {
            if (!trackPerformer.isEmpty())
            {
                if (sShowConductorMode == 2)
                {
                    trackPerformer += "\n";
                } else
                {
                    trackPerformer += ", ";
                }
            }

            String role = AppGlobals.getGenderedConductor(mContext, sGenderismMode);
            trackPerformer += role + ": " + currObject.conductor;
        }

        // if there is no grouping, use title instead
        if (trackGroup.isEmpty() && !bPrecededWithWork)
        {
            trackGroup = trackTitle;
            trackTitle = "";
        }

        // if it's not the first one of the group, omit composer, work and performer
        if (!bIsFirstOfGroup /* currObject.group_no != 0 */)
        {
            trackComposer = "";
            trackGroup = "";
        }

        // For a group, add the performer only to the last element
        // Also omit, if next element "matches"
        if (!bIsLastOfGroup /* !currObject.group_last */ || bMatchesNext)
        {
            trackPerformer = "";
            trackYear = 0;
        }

        // composer and work resp. title
        if (!trackGroup.isEmpty())
        {
            String textShown;
            boolean bIsComposer = !trackComposer.isEmpty();
            if (bIsComposer)
            {
                // "<font color='#e0e0e0'><i>" + ...
                textShown = mComposerColourString + "<i>" + trackComposer + ":</i></font><br>" + trackGroup;
            } else
            {
                textShown = trackGroup;
            }
            // work without movements/pieces: add duration here
            if (trackTitle.isEmpty())
            {
                textShown = textShown + " " + currObject.getDurationAsString();
            }
            //textViewHeightAuto(composerAndWorkView);
            if (bIsComposer)
                composerAndWorkView.setText(Html.fromHtml(textShown));  // slower, avoid if not necessary
            else
                composerAndWorkView.setText(textShown);     // faster
            //textViewHeightAuto(composerAndWorkView);
            composerAndWorkView.setVisibility(View.VISIBLE);
        } else
        {
            // neither composer nor work
            composerAndWorkView.setText("");
            //composerAndWorkView.setHeight(0);
            composerAndWorkView.setVisibility(View.GONE);
        }

        // title and duration
        if (!trackTitle.isEmpty())
        {
            String titleShown = trackTitle + " " + currObject.getDurationAsString();
            //textViewHeightAuto(titleView);
            titleView.setText(titleShown);
            //textViewHeightAuto(titleView);
            titleView.setVisibility(View.VISIBLE);
        } else
        {
            // work without movements/pieces
            titleView.setText("");
            //titleView.setHeight(0);
            titleView.setVisibility(View.GONE);
        }

        // performer and year
        if ((!trackPerformer.isEmpty()) || (trackYear != 0))
        {
            String textShown = trackPerformer;
            if ((!trackPerformer.isEmpty()) && (trackYear != 0))
            {
                textShown = textShown + " ";
            }
            if (trackYear != 0)
            {
                textShown = textShown + "(" + trackYear + ")";
            }
            //textViewHeightAuto(artistView);
            artistView.setText(textShown);
            //textViewHeightAuto(artistView);
            artistView.setVisibility(View.VISIBLE);
        } else
        {
            // not first movement of the work, or there is no performer
            artistView.setText("");
            //artistView.setHeight(0);
            artistView.setVisibility(View.GONE);
        }
/*
        if (position == mObjectList.selectedIndex)
        {
            audioTrackLay.setBackgroundColor(UserSettings.getThemedColourFromResId(R.attr.colourRowSelected));
        } else
        {
            audioTrackLay.setBackgroundColor(UserSettings.getThemedColourFromResId(R.attr.colourRowNormal));
        }
*/
        //set position as tag
        audioTrackLay.setTag(position);
        return audioTrackLay;
    }


    /**************************************************************************
     *
     * get view for any object except tracks:
     *
     * Works, Albums, Playlists, Composers, Performers, Genres
     *
     *************************************************************************/
    private View getOtherView
    (
        int position,
        AudioBaseObject currObject,
        AudioBaseObject nextObject,
        View convertView,
        ViewGroup parent
    )
    {
        RelativeLayout theRelativeLayout;

        if (convertView != null)
        {
            // just recycle the View. Can be sure it's the correct type
            //Log.d(LOG_TAG, "getView(" + position + ") : recycle");
            theRelativeLayout = (RelativeLayout) convertView;
        }
        else
        {
            // inflate a new hierarchy from the xml layout, containing all text and image objects
            //Log.d(LOG_TAG, "getView(" + position + ") : create");
            @LayoutRes int resource = (mUseGridView) ? R.layout.row_grid : R.layout.row_with_image;
            theRelativeLayout = (RelativeLayout) mTheInflater.inflate(resource, parent, false);
        }

        if (theRelativeLayout != null)
        {
            //get title and artist sub-views from the just created hierarchy
            TextView theTitleView  = theRelativeLayout.findViewById(R.id.album_title);
            TextView theInfoView   = theRelativeLayout.findViewById(R.id.album_info);
            ImageViewDeferred theImageView = theRelativeLayout.findViewById(R.id.album_cover_image);
            //noinspection StatementWithEmptyBody
            if (mUseGridView)
            {
                //ViewGroup.LayoutParams params;
                //params = albumCoverImageView.getLayoutParams();
                //mAlbumImageSize = params.width;
                //mAlbumImageSize = 512;
            }
            else
            {
                // set image view size as determined by Preferences
                ViewGroup.LayoutParams params;
                params = theImageView.getLayoutParams();
                params.width = mImageSize;
                params.height = mImageSize;
                theImageView.setLayoutParams(params);
            }

            // set title and info strings
            theTitleView.setText(getTitleForObject(currObject));
            theInfoView.setText(getInfoForObject(currObject, nextObject));

            // set title colour
            if (mPlaylistMode)
            {
                theTitleView.setTextColor(mTitleColour);
                theInfoView.setTextColor(mInfoColour);
            }

            // get picture

            Drawable defaultDrawable = getDrawableForObjectWithoutPicturePath(currObject);
            boolean isFolder = currObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_FOLDER_OBJECT;

            theImageView.setPicturePath(
                                    currObject,
                                    position,
                                    currObject.getPicturePath(mContext),
                                    mImageSize,
                                    isFolder ? bmFolderMask : null,
                                    defaultDrawable,
                                    mBitmapCache);

/*
            if (position == mObjectList.selectedIndex)
            {
                theRelativeLayout.setBackgroundColor(UserSettings.getThemedColourFromResId(R.attr.colourRowSelected));
            }
            else
            {
                theRelativeLayout.setBackgroundColor(UserSettings.getThemedColourFromResId(R.attr.colourRowNormal));
            }
*/
            //set position as tag
            theRelativeLayout.setTag(position);
        }
        return theRelativeLayout;
    }


    /**************************************************************************
     *
     * provide view for list element
     *
     * convertView can be reused, if not null and has correct type, see Adapter.java
     *
     *************************************************************************/
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        //Log.d(LOG_TAG, "getView(" + mUniqueHash + ")");
        int listSize = mObjects.list.size();
        if (position >= listSize)
        {
            Log.e(LOG_TAG, "getView(" + position + ", ..) : invalid position");
            return null;    // From Google's documentation it's unclear if we may return null here?!?
        }
        AudioBaseObject currAudioObject = mObjects.list.get(position);
        View theView;

        AudioBaseObject nextAudioObject = (position < listSize - 1) ? mObjects.list.get(position + 1) : null;
        if ((currAudioObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT) && (!mFileViewInsteadOfTrackView))
        {
            //
            // tracks (using a Linear Layout)
            //

            AudioBaseObject prevAudioObject = (position > 0) ? mObjects.list.get(position - 1) : null;
            // the tracks use a different view
            theView = getTrackView(position, (AudioTrack) currAudioObject, prevAudioObject, nextAudioObject, convertView, parent);
        }
        else
        {
            //
            // other objects (using a Relative Layout)
            //

            theView = getOtherView(position, currAudioObject, nextAudioObject, convertView, parent);
        }

        int theBackgroundColourResId;
        if (position == mObjects.selectedIndex)
        {
            theBackgroundColourResId = R.attr.colourRowSelected;
        }
        else
        {
            theBackgroundColourResId = (currAudioObject.alternating_colour == 0) ? (R.attr.colourRowNormal) : (R.attr.colourRowNormal2);
        }

        theView.setBackgroundColor(UserSettings.getThemedColourFromResId(mContext, theBackgroundColourResId));

        return theView;
    }
}
