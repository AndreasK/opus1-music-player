/*
 * Copyright (C) 2016-17 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import androidx.annotation.NonNull;

import android.content.ContentResolver;
import android.util.Log;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.KeyNotFoundException;
import org.jaudiotagger.tag.Tag;

import java.io.File;
import java.io.IOException;

/**
 * Reads tags from audio file
 */

@SuppressWarnings("WeakerAccess")
public class JaudioTagReader extends AudioTagReaderInterface
{
    private static final String LOG_TAG = "O1M : JaudioTagReader";
    private Tag mTag = null;


    /**************************************************************************
     * helper function
     *************************************************************************/
    private static int convertToInt(final String s)
    {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /*
    * method to convert a movement number to a string with a roman numeral
    */
    @SuppressWarnings("StringConcatenationInLoop")
    static private String romanNumeral(int n)
    {
        String roman = "";

        if (n >= 40)
        {
            // fallback for large numbers: return arabic number representation
            return roman + n;
        }

        while (n >= 10)
        {
            roman += "X";
            n -= 10;
        }
        while (n >= 9)
        {
            roman += "IX";
            n -= 9;
        }
        while (n >= 5)
        {
            roman += "V";
            n -= 5;
        }
        while (n >= 4)
        {
            roman += "IV";
            n -= 4;
        }
        while (n >= 1)
        {
            roman += "I";
            n -= 1;
        }

        return roman;
    }

    /*
    * method to combine movement number and name
    */
    static private String movementNumberAndName(String movementName, String arabicNumber)
    {
        int n;

        if ((arabicNumber == null) || (arabicNumber.isEmpty()))
        {
            n = 0;
        }
        else
            try
            {
                n = Integer.parseInt(arabicNumber);
            } catch (NumberFormatException e)
            {
                n = 0;
            }

        if (n > 0)
        {
            return romanNumeral(n) + ". " + movementName;
        }
        else
        {
            return movementName;
        }
    }

    /*
    * get tag safely, i.e. without crash
    */
    private @NonNull
    String getTagStringSafely(FieldKey id)
    {
        String theTagValue;
        try
        {
            theTagValue = mTag.getFirst(id);
            if (theTagValue == null)
            {
                theTagValue = "";
            }
            else
            {
                theTagValue = theTagValue.trim();
            }
        } catch (KeyNotFoundException | UnsupportedOperationException e)
        {
            theTagValue = "";
        }
        return theTagValue;
    }

    /*
    * helper to open the file
    */
    private void OpenFile(final String thisPath)
    {
        File theAudioFile = new File(thisPath);
        AudioFile f = null;

        try
        {
            f = AudioFileIO.read(theAudioFile);
            noOfSuccess++;
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "cannot read file " + thisPath + "because of:" + e.getMessage());
        } catch (org.jaudiotagger.audio.exceptions.CannotReadException e)
        {
            Log.e(LOG_TAG, "cannot read audio file " + thisPath + "because of:" + e.getMessage());
        } catch (org.jaudiotagger.audio.exceptions.ReadOnlyFileException e)
        {
            Log.e(LOG_TAG, "cannot open read-only audio file " + thisPath + "because of:" + e.getMessage());
        } catch (org.jaudiotagger.tag.TagException e)
        {
            Log.e(LOG_TAG, "cannot read tag from audio file " + thisPath + "because of:" + e.getMessage());
        } catch (org.jaudiotagger.audio.exceptions.InvalidAudioFrameException e)
        {
            Log.e(LOG_TAG, "invalid audio frame in " + thisPath + "because of:" + e.getMessage());
        } catch (org.jaudiotagger.audio.exceptions.UnknownFilenameExtensionException e)
        {
            Log.e(LOG_TAG, "unknown file name extension of " + thisPath + " because of: " + e.getMessage());
        }

        if (f != null)
        {
            // audio file information that cannot be written to:
            //AudioHeader ah = f.getAudioHeader();
            // audio file information that could be changed:
            mTag = f.getTag();
        }
        else
        {
            mTag = null;
            Log.w(LOG_TAG, "no audio tags for " + thisPath);
        }
    }

    /*
    * get
    */
    public void _get(String thisPathOrUri, String name, long size, ContentResolver resolver)
    {
        OpenFile(thisPathOrUri);

        if (mTag != null)
        {
            // mp4:shwm is currently not supported
            // mp4:©mvc is MOVEMENT_TOTAL and currently not needed
            // for mp3 files MOVEMENT_NO is "n/m" while for mp4 it is "n"

            String tGroup = "";

            String tMovementName = getTagStringSafely(FieldKey.MOVEMENT);      // mp4: "©mvn", mp3: MVNM
            if (!tMovementName.isEmpty())
            {
                String tMovementNo = getTagStringSafely(FieldKey.MOVEMENT_NO);     // mp4: "©mvi", mp3: MVIN
                /* CURRENTLY NOT NEEDED
                String tagMovementTotal = tag.getFirst(FieldKey.MOVEMENT_TOTAL);     // mp4: "©mvc", mp3: MVIN
                Log.v("AppGlobals", "tagMovementTotal = " + tagMovementTotal);
                */
                Log.v(LOG_TAG, "tagMovementName  = " + tMovementName);
                Log.v(LOG_TAG, "tagMovementNo    = " + tMovementNo);
                tMovementName = movementNumberAndName(tMovementName, tMovementNo);
                tGroup = getTagStringSafely(FieldKey.WORK);               // mp4: "©wrk"
                Log.v(LOG_TAG, "WORK    = " + tGroup);
            }

            if (tGroup.isEmpty())
            {
                tGroup = getTagStringSafely(FieldKey.GROUPING);          // mp4: "©grp", mp3: TIT1
                Log.v(LOG_TAG, "GROUPING    = " + tGroup);
            }

            if (tGroup.isEmpty())
            {
                try
                {
                    tGroup = getTagStringSafely(FieldKey.ITUNES_GROUPING); // mp3: GRP1, mp4: does not exist
                    Log.v(LOG_TAG, "mp3:GRP1    = " + tGroup);
                } catch (KeyNotFoundException e)
                {
                    tGroup = "";
                }
            }

            tagTitle        = getTagStringSafely(FieldKey.TITLE);
            tagComposer     = getTagStringSafely(FieldKey.COMPOSER);
            tagConductor    = getTagStringSafely(FieldKey.CONDUCTOR);
            tagGrouping     = tGroup;
            tagMovementName = tMovementName;
            tagSubtitle     = getTagStringSafely(FieldKey.SUBTITLE);
            tagAlbum        = getTagStringSafely(FieldKey.ALBUM);
            tagAlbumArtist  = getTagStringSafely(FieldKey.ALBUM_ARTIST);
            tagDiscNo       = getTagStringSafely(FieldKey.DISC_NO);
            tagDiscTotal    = getTagStringSafely(FieldKey.DISC_TOTAL);
            tagTrack        = getTagStringSafely(FieldKey.TRACK);
            tagTrackTotal   = getTagStringSafely(FieldKey.TRACK_TOTAL);
            tagGenre        = getTagStringSafely(FieldKey.GENRE);
            tagYear         = convertToInt(getTagStringSafely(FieldKey.YEAR));

            mTag = null;    // is no longer needed, allow garbage collection
        }

        Log.v(LOG_TAG, "tagComposer    = " + tagComposer);
        Log.v(LOG_TAG, "tagConductor   = " + tagConductor);
        Log.v(LOG_TAG, "tagGrouping    = " + tagGrouping);
        Log.v(LOG_TAG, "tagSubtitle    = " + tagSubtitle);
        Log.v(LOG_TAG, "tagAlbum       = " + tagAlbum);
        Log.v(LOG_TAG, "tagAlbumArtist = " + tagAlbumArtist);
        Log.v(LOG_TAG, "tagDiscNo      = " + tagDiscNo);
        Log.v(LOG_TAG, "tagDiscNoTotal = " + tagDiscTotal);
        Log.v(LOG_TAG, "tagTrack       = " + tagTrack);
        Log.v(LOG_TAG, "tagTrackTotal  = " + tagTrackTotal);
        Log.v(LOG_TAG, "tagGenre       = " + tagGenre);
        Log.v(LOG_TAG, "=====================================\n\n");
    }

}
