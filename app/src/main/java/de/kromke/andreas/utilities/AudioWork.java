/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.ArrayList;

/**
 * all data for a single audio work, may containing movements
 */
@SuppressWarnings("JavadocBlankLines")
public class AudioWork extends AudioBaseObject
{
    ArrayList<AudioTrack> mTrackList = new ArrayList<>();  // tracks belonging to that work
    public AudioAlbum mAlbum = null;
    AudioComposer mComposer = null;
    ArrayList<AudioComposer> mComposerList = new ArrayList<>();  // list of composers

    /* THIS IS ALL REDUNDANT AND CAN BE TAKEN FROM FIRST TRACK
    public String album;
    public int track_no;           // track number of first track of this group (including disc number as 1000)
    public String composer;         // the composer of the complete piece, i.e. Wolfgang Mozart
    public String performer;      // performer(s) (called "artist" in subculture), i.e. NDR Radiosinfonie
    public String album_performer;      // aka "album artist"
    public String conductor;
    public String genre;
    public long year;
    */


    /*
     * complete constructor
     */
    @SuppressWarnings("WeakerAccess")
    public AudioWork(
            long    the_id,
            long    the_duration,
            String  the_grouping,
            int     the_no_of_parts)
    {
        super();            // call base class constructor
        objectType = AudioObjectType.AUDIO_WORK_OBJECT;
        id           = the_id;
        duration     = the_duration;
        name         = the_grouping;
        no_of_tracks = the_no_of_parts;
    }

    // for fast scroll sections. Use composer.
    @Override
    public String getSection()
    {
        AudioComposer thisComposer = mTrackList.get(0).getComposer();
        return thisComposer.getSection();
    }

    // for sorting. Compare for composer first, then for work name
    @Override
    public int compareTo(@NonNull AudioBaseObject another)
    {
        // get composer from first track of that work
        AudioComposer thisComposer = mTrackList.get(0).getComposer();
        AudioWork anotherWork = (AudioWork) another;
        AudioComposer anotherComposer = anotherWork.mTrackList.get(0).getComposer();
        // compare composer. If that differs, this is the first key to search for
        int result = thisComposer.compareTo(anotherComposer);
        // only compare work name in case it's the same composer
        return (result == 0) ? name.compareTo(anotherWork.name) : result;
    }

    @Override
    public AudioComposer getComposer()
    {
        return mComposer;
    }

    @Override
    public AudioPerformer getPerformer()
    {
        return mTrackList.get(0).mPerformer;
    }


    /**************************************************************************
     *
     * return attribute
     *
     *************************************************************************/
    @Override
    public ArrayList<AudioComposer>getComposerList()
    {
        return mComposerList;
    }


    /**************************************************************************
     *
     * collect all the performers from all tracks
     *
     *************************************************************************/
    @Override
    public ArrayList<AudioPerformer>getPerformerList()
    {
        ArrayList<AudioPerformer> tempList = new ArrayList<>();

        for (AudioTrack theTrack: mTrackList)
        {
            ArrayList<AudioPerformer> performerList = theTrack.getPerformerList();
            if (performerList != null)
            {
                // add list to list, but avoid double entries
                for (AudioPerformer performer: performerList)
                {
                    if (!tempList.contains(performer))
                    {
                        tempList.add(performer);
                    }
                }
            }
        }
        return mTrackList.get(0).getPerformerList();
    }

    @Override
    public AudioGenre getGenre()
    {
        return mTrackList.get(0).mGenre;
    }

    @Override
    public final String getRealPath(Context context)
    {
        String commonPath = null;

        for (AudioTrack theTrack: mTrackList)
        {
            String thePath = theTrack.getRealPath(context);
            if (thePath != null)
            {
                int pos = thePath.lastIndexOf('/');
                thePath = thePath.substring(0, pos);
                if (commonPath == null)
                {
                    commonPath = thePath;
                } else if (!commonPath.equals(thePath))
                {
                    commonPath = "≠";
                }
            }
        }

        return commonPath;
    }

    // for display in a list
    @Override
    public final String getTitle()
    {
        //get title and composer strings
        String workText = name;
        String thisComposerName = getComposer().getName();
        if (!thisComposerName.isEmpty())
        {
            // precede with composer, if exists
            workText = thisComposerName + ": " + workText;
        }
        return workText;
    }


    /**************************************************************************
     *
     * string helper
     *
     *************************************************************************/
    private String addLfString(final String inString, final String addString)
    {
        if (addString.isEmpty())
        {
            // nothing to add
            return inString;
        }

        // line feed only necessary if both strings not empty
        String lfString = (inString.isEmpty()) ? "" : "\n";

        return inString + lfString + addString;
    }

    // helper
    private String _getInfo(String infoText)
    {
        if (no_of_tracks > 1)
        {
            infoText = addLfString(infoText, "(" + no_of_tracks + " " + str_parts);
            if (sShowAlbumAndWorkDuration)
            {
                infoText += ", " + Utilities.convertMsToHMmSs(getDuration());
            }
            infoText += ")";
        }
        else
        if (sShowAlbumAndWorkDuration)
        {
            infoText = addLfString(infoText, "(" + Utilities.convertMsToHMmSs(getDuration()) + ")");
        }
        return infoText;
    }

    // for display in a list, small text, without performer, for playing list
    @SuppressWarnings("WeakerAccess")
    public String getInfoWithoutPerformer()
    {
        // add interpreter, album and number of titles, if appropriate
        String infoText = str_album_uc + ": " + getAlbumName();
        return _getInfo(infoText);
    }

    // for display in a list, small text
    @Override
    public String getInfo()
    {
        // add interpreter, album and number of titles, if appropriate
        String infoText = addLfString(getPerformerName(), str_album_uc + ": " + getAlbumName());
        return _getInfo(infoText);
    }


    /**************************************************************************
     *
     * Check, if the object matches the given filter objects
     *
     *************************************************************************/
    @Override
    public boolean matchesFilter
    (
        AudioComposer theFilterComposer,
        AudioPerformer theFilterPerformer,
        AudioGenre theFilterGenre
    )
    {
        return (matchesFilter(theFilterComposer) && matchesFilter(theFilterPerformer) && matchesFilter(theFilterGenre));
    }


    /**************************************************************************
     *
     * Check, if the object matches the given search text
     *
     * @param searchString      the search text, in upper case if flag is "true"
     * @param bCaseSensitive    compare case sensitive or not
     * @return true, iff the search text matches
     *
     *************************************************************************/
    @Override
    public boolean matchesText(final String searchString, boolean bCaseSensitive)
    {
        return (matchString(name, searchString, bCaseSensitive)) ||
               (matchString(getComposer().name, searchString, bCaseSensitive)) ||
               (matchString(getPerformer().name, searchString, bCaseSensitive));
    }

    public String getAlbumName()
    {
        if (mAlbum != null)
            return mAlbum.name;
        else
            return "";
    }

    private String getPerformerName()
    {
        AudioTrack theFirstTrack = mTrackList.get(0);
        return theFirstTrack.getPerformerName();
    }

    @Override
    public String getAlbumPicturePath(Context context)
    {
        if (mAlbum != null)
            return mAlbum.getAlbumPicturePath(context);
        else
            return null;
    }

    private String getPerformerPicturePath(Context context)
    {
        AudioTrack theFirstTrack = mTrackList.get(0);
        if (theFirstTrack != null)
        {
            AudioPerformer performer = theFirstTrack.getPerformer();
            if (performer != null)
            {
                return performer.getPicturePath(context);
            }
        }
        return null;
    }

    private String getComposerPicturePath(Context context)
    {
        if (mComposer != null)
            return mComposer.getPicturePath(context);
        return null;
    }

    @Override
    public String getPicturePath(Context context)
    {
        switch (sWorkPictureMode)
        {
            case 0:
                return getComposerPicturePath(context);

            case 1:
                return getPerformerPicturePath(context);

            case 2:
                return getAlbumPicturePath(context);
        }

        return null;
    }
}
