/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.util.Log;

import java.util.ArrayList;

/**
 * Various folder helper functions
 */
@SuppressWarnings("JavadocBlankLines")
public class FolderHelper
{
    static private final String LOG_TAG = "FolderHelper";
    static public FilteredAudioObjectList mCurrFolderList = null;
    static public AudioFolder mCurrentFolder = null;     // is null in case current path contains no music
    static private String mCurrentPath = null;
    static private String mRootPath;
    static private boolean mIsInit = false;


    /**************************************************************************
     *
     * set common folder prefix, i.e. the base path all music is below
     *
     * Note that in SAF mode, the paths look like this:
     *  /tree/5213-1B10:Music/document/5213-1B10:Music/CDs/Alexander Borodin - 3 Symphonies etc
     * or
     *  /tree/primary:Music/Klassik/document/primary:Music/Klassik/Beethoven - Sonatas
     * or
     *  /tree/primary:Music/document/primary:Music/Folk/Wurzelsepp - 42 String Quartets
     *
     *************************************************************************/
    public static void initCommonFolderPrefix()
    {
        if (mIsInit)
        {
            // already called. Do not run again, otherwise current path
            // is reset whenever spinner is changed from folder list and back.
            return;
        }

        mRootPath = null;

        for (int i = 0; i < AudioFilters.audioFolderList.raw.getSize(); i++)
        {
            AudioFolder theFolder = (AudioFolder) AudioFilters.audioFolderList.raw.list.get(i);
            if (mRootPath == null)
            {
                mRootPath = theFolder.getPath();
            }
            else
            {
                int pos = getCommonPrefix(mRootPath, theFolder.getPath());
                if (pos < mRootPath.length())
                {
                    if (pos == 0)
                    {
                        // no common prefix
                        mRootPath = "";
                    }
                    mRootPath = mRootPath.substring(0, pos);
                }
            }
        }

        if (mRootPath == null)
        {
            Log.w(LOG_TAG, "initCommonFolderPrefix() : no root path found");
            mRootPath = "/";
        }

        // path must end with "/"
        //if (!rootPath.endsWith("/"))
        int pos = mRootPath.lastIndexOf('/');
        if ((pos >= 0) && (pos != mRootPath.length() - 1))
        {
            mRootPath = mRootPath.substring(0, pos + 1);
        }

        mCurrentPath = mRootPath;

        mIsInit = true;
        Log.v(LOG_TAG, "initCommonFolderPrefix() : rootPath = " + mRootPath);
    }


    /**************************************************************************
     *
     * helper function to add a path and avoid double entries
     *
     *************************************************************************/
    static private void addFolder(final AudioFolder theNewFolder)
    {
        for (int i = 0; i < mCurrFolderList.raw.getSize(); i++)
        {
            AudioFolder theFolder = (AudioFolder) mCurrFolderList.raw.list.get(i);
            if (theFolder.name.equals(theNewFolder.name))
            {
                // already in list
                theFolder.no_of_tracks += theNewFolder.no_of_tracks;
                theFolder.no_of_folders++;
                return;
            }
        }
        // folder not yet in list
        Log.v(LOG_TAG, "addFolder() -- add folder to list: " + theNewFolder.getPath());
        mCurrFolderList.raw.list.add(theNewFolder);
    }


    /**************************************************************************
     *
     * Given a path as string, return an AudioFolder object. Note that
     * no object is found in case there are not audio objects in that path,
     * in this case return NULL.
     *
     *************************************************************************/
    private static AudioFolder audioFolderFromPath(String path)
    {
        if (path.endsWith("/"))
        {
            path = path.substring(0, path.length() - 1);
        }
        for (int i = 0; i < AudioFilters.audioFolderList.raw.getSize(); i++)
        {
            AudioFolder theFolder = (AudioFolder) AudioFilters.audioFolderList.raw.list.get(i);
            Log.v(LOG_TAG, "audioFolderFromPath() : " + path + " =?= " + theFolder.getPath());
            if (theFolder.getPath().equals(path))
            {
                return theFolder;
            }
        }

        return null;    // not found
    }


    /**************************************************************************
     *
     * filter all folders according to current path
     *
     *************************************************************************/
    public static void enterCurrentPath()
    {
        AudioFolder theFolder;

        Log.v(LOG_TAG, "enterCurrentPath() : " + mCurrentPath);
        mCurrFolderList = new FilteredAudioObjectList();
        mCurrFolderList.setRawList(new ArrayList<>());
        int cl = mCurrentPath.length();

        /*
        // create object for current path, if it contains music files
        theFolder = audioFolderFromPath(currentPath);
        if (theFolder != null)
        {
            AudioFolder theNewFolder = new AudioFolder(theFolder.path, theFolder.no_of_tracks);
            theNewFolder.name = ".";
            AddFolder(theNewFolder);
        }
        */

        // put matching folders to currFolderList
        int size = AudioFilters.audioFolderList.raw.getSize();
        for (int i = 0; i < size; i++)
        {
            theFolder = (AudioFolder) AudioFilters.audioFolderList.raw.list.get(i);
            String thePath = theFolder.getPath();
            if (getCommonPrefix(thePath, mCurrentPath) == cl)
            {
                // The folder is somewhere, not necessarily directly, in the current path,
                // e.g.: curr = "/a/b" and thePath = "/a/b/c/d".
                Log.v(LOG_TAG, "enterCurrentPath() : check path " + thePath);

                // Extract the first path element, which is "c" in the example above
                int pos = thePath.indexOf('/', cl);
                if (pos < 0)
                {
                    // This is a folder that we already know, because it contains music files
                    Log.v(LOG_TAG, "enterCurrentPath() : direct match");
                    addFolder(theFolder);
                }
                else
                {
                    // This is an in-between folder, it contains folders that contain music files.
                    String theName = thePath.substring(cl, pos);
                    Log.v(LOG_TAG, "enterCurrentPath() : collect folder with name " + theName);
                    AudioFolder theNewFolder = new AudioFolder(mCurrentPath + theName, null, theFolder.no_of_tracks);
                    addFolder(theNewFolder);
                }
            }
        }

        // try to create folder object for current path, which succeeds in
        // case we have music files, and add all those music files to the list
        mCurrentFolder = audioFolderFromPath(mCurrentPath);
        if (mCurrentFolder != null)
        {
            mCurrFolderList.raw.list.addAll(mCurrentFolder.mTrackList);
        }
    }


    /**************************************************************************
     * helper function: get common prefix string
     *************************************************************************/
    private static int getCommonPrefix(String s1, String s2)
    {
        int l = s1.length();
        if (s2.length() < l)
        {
            // get minimum length
            l = s2.length();
        }

        for (int i = 0; i < l; i++)
        {
            if (s1.charAt(i) != s2.charAt(i))
                return i;
        }

        return l;
    }


    /**************************************************************************
     * In case of SAF mode, get a beautified path and avoid redundancy,
     * used for the footer line in the Folder GUI
     *************************************************************************/
    public static String getCurrentPath()
    {
        if ((mCurrentPath == null) || !mCurrentPath.startsWith("/tree/"))
        {
            return mCurrentPath;
        }

        // SAF mode: remove second segment of path, for better readability
        String[] segments = mCurrentPath.split(":");
        if (segments.length == 2)
        {
            return segments[0];
        }
        if (segments.length == 3)
        {
            return segments[0] + "/" + segments[2];
        }

        Log.w(LOG_TAG, "getCurrentPath() : unexpected SAF path: " + mCurrentPath);
        return mCurrentPath;
    }


    /**************************************************************************
     *
     * go one level down
     *
     *************************************************************************/
    public static void gotoSubdir(final String name)
    {
        mCurrentPath += name + "/";
        enterCurrentPath();
    }


    /**************************************************************************
     * Go one level up, if possible, and then return "true", otherwise
     * return false
     *************************************************************************/
    public static boolean gotoParent()
    {
        if (!mCurrentPath.equals(mRootPath))
        {
            int pos = mCurrentPath.lastIndexOf('/');
            if (pos > 1)
            {
                String newPath = mCurrentPath.substring(0, pos - 1);
                pos = newPath.lastIndexOf('/');
                if (pos > 0)
                {
                    mCurrentPath = newPath.substring(0, pos + 1);
                    enterCurrentPath();
                    return true;
                }
            }
        }
        return false;
    }
}
