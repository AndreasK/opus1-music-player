/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/QTFFPreface/qtffPreface.html
// for 'ilst':
// -> https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/Metadata/Metadata.html#//apple_ref/doc/uid/TP40000939-CH1-SW1
// -> c068960_ISO_IEC_14496-12_2015.pdf
// -> https://developer.android.com/reference/java/io/InputStream
// in fact we need sub-boxes inside "moov.udta.meta.ilst"



import android.util.Log;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SpellCheckingInspection", "JavadocBlankLines"})
public class TagsMp4 extends TagsBase
{
    private static final String LOG_TAG = "TMP4";
    private static final int maxLevel = 10;
    private String atomPath = "";       // for debugging purposes

    // helper
    private char getCharFromByte(byte b)
    {
        if ((b & 0xff) == 0xa9)
        {
            return '©';
        }
        return (char) b;
    }

    // helper
    private String getStringFromByteArray(byte[] data, int index, int len)
    {
        StringBuilder res = new StringBuilder();
        while(len > 0)
        {
            res.append(getCharFromByte(data[index++]));
            len--;
        }
        return res.toString();
    }

    // assign Mp4 tags to universal tags
    private int getMp4StringTagId(final String mp4Key)
    {
        int id = -1;
        switch (mp4Key)
        {
            case "©alb": id = idAlbum; break;
            case "©ART": id = idArtist; break;
            case "aART": id = idAlbumArtist; break;
            case "©con": id = idConductor; break;
            case "©grp": id = idGrouping; break;            // according to mp3tag application
            case "©nam": id = idTitle; break;
            //case "com.apple.iTunes.SUBTITLE": id = idSubtitle; break;
            case "©wrt": id = idComposer; break;
            //case "disk": id = idDiscNo; break;
            //case "disk": id = idDiscTotal; break;
            //case "trkn": id = idTrack; break;
            //case "trkn": id = idTrackTotal; break;
            case "©gen": id = idGenre; break;
            case "©day": id = idYear; break;
            case "©cmt": id = idComment; break;
            // iTunes:
            case "©mvn": id = idAppleMovement; break;
            case "©mvi": id = idAppleMovementNo; break;
            case "©mvc": id = idAppleMovementTotal; break;
            //case "<-->": id = idAppleMp3Work; break;
            case "©wrk": id = idAppleMp4Work; break;
        }

        return id;
    }

    @SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SpellCheckingInspection"})
    private class Mp4Box
    {
        Mp4Box parent;
        long boxSize;               // brutto size
        long boxPayloadSize = 0;        // netto size, without header
        String boxType = null;
        String userType = null;
        int version = 0;            // "full boxes" have version ..
        int flags = 0;              // .. and flags

        private final byte[] data = new byte[8];
        private byte[] payload = null;
        private long bytesRead = 0;
        private long posAt = 0;         // for debugging purposes

        public Mp4Box(Mp4Box theParent)
        {
            parent = theParent;
        }

        // used to identify atoms of type  "moov.udta.meta.ilst.****.data"
        public String getParentName()
        {
            if (parent == null)
            {
                return "";
            }
            else
            {
                return parent.boxType;
            }
        }


        // used to identify atoms of type  "moov.udta.meta.ilst.****.data"
        public String getGrandParentName()
        {
            if ((parent == null) || (parent.parent == null))
            {
                return "";
            }
            else
            {
                return parent.parent.boxType;
            }
        }

        public boolean read()
        {
            posAt = filePos;
            bytesRead += data.length;
            return readBytes(data);
        }

        public boolean check()
        {
            //Log.d(LOG_TAG, "Mp4Box::check() : level " + level);
            boxSize = getBigEndianInt4(data, 0);
            boxType = getStringFromByteArray(data, 4, 4);

            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() :" + levelStr + " Box type \"" + atomPath + boxType + "\", size = " + boxSize + " at file pos " + posAt);

            if (boxSize == 1)
            {
                byte[] data2 = new byte[8];
                bytesRead += data2.length;
                if (!readBytes(data2))
                {
                    return false;
                }
                boxSize = getBigEndianInt8(data, 0);
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "     extended size = " + boxSize);
            }
            else
            if (boxSize == 0)
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "     extends to end of file");
                boxSize = Long.MAX_VALUE;
            }

            if (boxType.equals("uuid"))
            {
                byte[] data3 = new byte[16];
                bytesRead += data3.length;
                if (!readBytes(data3))
                {
                    return false;
                }
                userType = getStringFromByteArray(data3, 0, 16);
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    user type = " + userType);
            }

            boxPayloadSize = boxSize - bytesRead;
            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    payload size = " + boxPayloadSize);

            return true;
        }

        boolean skipPayload()
        {
            return safeSkip(boxPayloadSize);
        }

        // some boxes, called "full boxes", have version and flags
        boolean readVersionAndFlags()
        {
            byte[] v = new byte[4];
            bytesRead += v.length;
            if (!readBytes(v))
            {
                return false;
            }
            boxPayloadSize = boxSize - bytesRead;
            version = v[0] & 0xff;
            flags = getBigEndianInt3(v, 1);
            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    version = " + version + ", flags = " + flags);
            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    remaining payload size = " + boxPayloadSize);
            return true;
        }

        // iTunes tag payload start with four zero bytes, after version and flags
        boolean read4Zeroes()
        {
            if (boxPayloadSize < 4)
            {
                return false;
            }
            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    read four bytes that should be zero");
            byte[] v = new byte[4];
            bytesRead += v.length;
            if (!readBytes(v))
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    payload to small for iTunes tag");
                return false;
            }
            boxPayloadSize = boxSize - bytesRead;
            int zero = getBigEndianInt4(v, 0);
            if (zero != 0)
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    payload for iTunes tag must start with four zero bytes");
                return false;
            }
            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    remaining payload size = " + boxPayloadSize);
            return true;
        }

        boolean isContainer()
        {
            return (boxType.equals("trak") ||
                    boxType.equals("moov") ||
                    boxType.equals("mdia") ||
                    boxType.equals("udta") ||
                    boxType.equals("ilst") ||
                    boxType.equals("minf"));
        }

        boolean readPayload()
        {
            payload = new byte[(int) boxPayloadSize];
            return readBytes(payload);
        }

        boolean isItunesTag()
        {
            if (getGrandParentName().equals("ilst"))
            {
                switch (boxType)
                {
                    case "data":
                        return readVersionAndFlags() && read4Zeroes();
                    case "mean":
                    case "name":
                        return read4Zeroes();
                }
            }

            return false;
        }

        boolean readItunesPayload()
        {
            if (!readPayload())
            {
                return false;
            }
            // finally get data
            String parentName = getParentName();
            if (!boxType.equals("data") || (flags == 1))
            {
                //
                // value is a string
                //
                String v = new String(payload, 0, payload.length, StandardCharsets.UTF_8);
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::readItunesPayload() : " + levelStr + "    value = \"" + v + "\"");
                if (!v.isEmpty())
                {
                    // just skip empty strings, the values will remain null
                    int id = getMp4StringTagId(parentName);
                    if (id >= 0)
                    {
                        values[id] = trimValue(v);
                    }
                }
            }
            else
            if ((flags == 0) || ((flags == 21 /* ©mvi and ©mvc */) && (payload.length == 2)))
            {
                //
                // value is numeric
                //
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::readItunesPayload() : " + levelStr + "    value is numeric");
                if ((payload.length >= 2) && (payload.length %2 == 0))
                {
                    int n = payload.length / 2;     // number of numerical values
                    int[] dv = new int[n];
                    for (int i = 0; i < n; i++)
                    {
                        dv[i] = getBigEndianInt2(payload, i * 2);
                        if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::readItunesPayload() : " + levelStr + "    value[" + i +"] is " + dv[i]);
                    }
                    if (parentName.equals("trkn") && (n >= 3))
                    {
                        values[idTrackNo] = "" + dv[1];
                        values[idTrackTotal] = "" + dv[2];
                    }
                    else
                    if (parentName.equals("disk") && (n >= 3))
                    {
                        values[idDiscNo] = "" + dv[1];
                        values[idDiscTotal] = "" + dv[2];
                    }
                    else
                    if (parentName.equals("©mvi"))
                    {
                        values[idAppleMovementNo] = "" + dv[0];
                    }
                    else
                    if (parentName.equals("©mvc"))
                    {
                        values[idAppleMovementTotal] = "" + dv[0];
                    }
                }
                else
                {
                    Log.w(LOG_TAG, "Mp4Box::check() : " + levelStr + "    payload size for numeric values should be even and > 1");
                }
            }
            else
            if ((flags == 13) || (flags == 14))
            {
                // 13: jpg
                // 14: png
                if (parentName.equals("covr"))
                {
                    int picSize = payload.length;
                    int picType = 3;       // this is the cover picture for ID3 tags
                    final String mimeType = (flags == 13) ? "image/jpeg" : "image/png";
                    if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::readItunesPayload() : cover picture size = " + picSize);

                    rememberCoverPic(picType, picSize, mimeType);
                    if (mHandlePictureCb != null)
                    {
                        int ret = mHandlePictureCb.handlePicture(payload, 0, picSize, picType, mimeType);
                        //noinspection RedundantIfStatement
                        if (ret < 0)
                        {
                            return false;       // fatal error
                        }
                    }
                }
            }
            else
            if ((flags == 21) && (payload.length == 1))
            {
                // boolean value, consisting of one uint8 (pgap)
                if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::check() : " + levelStr + "    value " + atomPath + boxType + " with flags = " + flags + " and payload length " + payload.length + " ignored (" + fileName + ")");
            }
            else
            {
                Log.w(LOG_TAG, "Mp4Box::check() : " + levelStr + "    value " + atomPath + boxType + " with flags = " + flags + " and payload length " + payload.length + " has unknown type (" + fileName + ")");
            }
            return true;
        }
    }


    /**************************************************************************
     *
     * main loop, recursive
     *
     *  loop
     *      read box
     *      recursion, if appropriate
     *  end loop
     *
     *************************************************************************/
    private boolean read_container(Mp4Box parentBox)
    {
        String atomPathSaved = atomPath;

        long payloadSize;
        if (parentBox != null)
        {
            level++;
            levelStr += "    ";
            payloadSize = parentBox.boxPayloadSize;
            atomPath += parentBox.boxType + ".";
        }
        else
        {
            payloadSize = Long.MAX_VALUE;
            atomPath = "";
        }

        if (level >= maxLevel)
        {
            Log.w(LOG_TAG, "Mp4Box::read_container() : level exceeds maximum of " + maxLevel + ", container is ignored");
            safeSkip(payloadSize);
            return true;
        }

        if (bytesLeft < payloadSize)
        {
            if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::read_container() : bytes left = " + bytesLeft + ", but payload size is " + payloadSize);
            return false;
        }

        long bytesLeftSaved = bytesLeft - payloadSize;
        bytesLeft = payloadSize;
        boolean res = true;

        do
        {
            //if (logLevel > 0) Log.d(LOG_TAG, "Mp4Box::read_container() : bytes left = " + bytesLeft);
            Mp4Box box = new Mp4Box(parentBox);
            if (!box.read())
            {
                //noinspection SimplifiableConditionalExpression
                res = (bEof) ? true : false;
                break;
            }
            if (!box.check())
            {
                res = false;
                break;
            }
            if (box.boxPayloadSize > maxFrameSize)
            {
                // Box too large

                if (!box.skipPayload())
                {
                    res = false;
                    break;
                }
            }
            else
            if (box.boxType.equals("meta"))
            {
                if (!box.readVersionAndFlags())
                {
                    return false;
                }
                if (!read_container(box))
                {
                    return false;
                }
            }
            else
            if (box.isContainer())
            {
                // container
                // Box we are interested in. Read payload.

                if (!read_container(box))
                {
                    return false;
                }
            }
            else
            if ((parentBox != null) && (parentBox.boxType.equals("ilst")))
            {
                if (!read_container(box))
                {
                    return false;
                }
            }
            else
            if (box.isItunesTag())
            {
                // finally found what we are interested in
                if (!box.readItunesPayload())
                {
                    return false;
                }
            }
            else
            {
                // box we are not interested in. Skip it.

                if (!box.skipPayload())
                {
                    res = false;
                    break;
                }
            }
        }
        while (bytesLeft > 0);

        bytesLeft = bytesLeftSaved;
        atomPath = atomPathSaved;

        if (parentBox != null)
        {
            level--;
            levelStr = levelStr.substring(4);
        }
        return res;
    }


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    public TagsMp4(InputStream is)
    {
        mInputStream = is;
        tagType = 10;            // MP4
    }


    /**************************************************************************
     *
     * main API function. Starts recursion.
     *
     *************************************************************************/
    public boolean read()
    {
        return read_container(null);
    }
}
