/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://xiph.org/vorbis/doc/v-comment.html
// -> https://xiph.org/flac/format.html#metadata_block_picture

// Abstract class used for both Ogg and Flac files, as both contain Vorbis comments


import android.util.Base64;
import android.util.Log;

import java.nio.charset.StandardCharsets;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SameParameterValue", "SpellCheckingInspection"})
public abstract class TagsVorbis extends TagsBase
{
    private static final String LOG_TAG = "TVOR";
    private int tmp;    // hack


    // assign Vorbis tags to universal tags
    private static int getVorbisStringTagId(final String key)
    {
        int id = -1;
        switch (key)
        {
            case "ALBUM": id = idAlbum; break;
            case "ARTIST": id = idArtist; break;
            case "ALBUMARTIST": id = idAlbumArtist; break;
            case "CONDUCTOR": id = idConductor; break;          // might be unofficial
            case "GROUPING": id = idGrouping; break;
            case "TITLE": id = idTitle; break;
            case "SUBTITLE": id = idSubtitle; break;            // might be unofficial
            case "COMPOSER": id = idComposer; break;
            //case "DISCNUMBER": id = idDiscNo; break;
            //case "DISCNUMBER": id = idDiscTotal; break;
            //case "TRACKNUMBER": id = idTrack; break;
            //case "TRACKNUMBER": id = idTrackTotal; break;
            case "GENRE": id = idGenre; break;
            case "DATE": id = idYear; break;
            case "COMMENT": id = idComment; break;
            // iTunes:
            case "MOVEMENTNAME": id = idAppleMovement; break;
            case "MOVEMENT": id = idAppleMovementNo; break;
            case "MOVEMENTTOTAL": id = idAppleMovementTotal; break;
            case "WORK": id = idAppleMp4Work; break;        // handle like mp4
        }

        return id;
    }

    // same as in flac, but header and picture data are already present in a byte[] array
    boolean handleFlacPictureBlock(final byte[] pictureBlockData)
    {
        int payloadSize = pictureBlockData.length;
        if (payloadSize < 28)
        {
            Log.e(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : payload size too small for picture " + payloadSize);
            return true;        // non-fatal error
        }

        int index = 0;
        int picType = getBigEndianInt4(pictureBlockData, index);   // picture type
        index += 4;
        int strLen = getBigEndianInt4(pictureBlockData, index);    // MIME string length
        index += 4;
        if (strLen > maxFrameSize)
        {
            Log.e(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : MIME string length overflow " + strLen);
            return true;        // non-fatal error
        }

        // read MIME string and the following length of the description string
        final String mimeType = new String(pictureBlockData, index, strLen, StandardCharsets.ISO_8859_1);
        index += strLen;
        if (logLevel > 0) Log.d(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : MIME type = " + mimeType);

        strLen = getBigEndianInt4(pictureBlockData, index);    // description string length
        index += 4;
        if (strLen > maxFrameSize)
        {
            Log.e(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : description string length overflow " + strLen);
            return true;        // non-fatal error
        }

        final String pictureDescription = new String(pictureBlockData, index, strLen, StandardCharsets.UTF_8);
        index += strLen;
        if (logLevel > 0) Log.d(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : description = " + pictureDescription);

        int picWidth = getBigEndianInt4(pictureBlockData, index);
        int picHeight = getBigEndianInt4(pictureBlockData, index + 4);
        int colourDepth = getBigEndianInt4(pictureBlockData, index + 8);
        int numColours = getBigEndianInt4(pictureBlockData, index + 12);
        int picSize = getBigEndianInt4(pictureBlockData, index + 16);
        index += 20;

        if (logLevel > 0) Log.d(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : w = " + picWidth + ", h = " + picHeight + ", colour depth = " + colourDepth);
        if (logLevel > 0) Log.d(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : num of colours = " + numColours);
        if (logLevel > 0) Log.d(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : picture size = " + picSize);
        if (logLevel > 0) Log.d(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : remaining payload = " + payloadSize);

        if (picSize > payloadSize - index)
        {
            Log.e(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : picture size > payload size");
            return true;        // non-fatal error
        }

        if (picSize > maxFrameSize)
        {
            Log.e(LOG_TAG, "TagsVorbis::handleFlacPictureBlock() : picture size overflow");
            return true;        // non-fatal error
        }

        rememberCoverPic(picType, picSize, mimeType);
        if (mHandlePictureCb != null)
        {
            int ret = mHandlePictureCb.handlePicture(pictureBlockData, index, picSize, picType, mimeType);
            //noinspection RedundantIfStatement
            if (ret < 0)
            {
                return false;       // user-initiated, fatal error
            }
        }
        return true;
    }

    private boolean handleVorbisTagValue(String s)
    {
        int i = s.indexOf('=');
        String key;
        String val;
        if (i > 0)
        {
            key = s.substring(0, i);
            val = s.substring(i + 1);
        }
        else
        {
            key = s;
            val = "";
        }

        switch (key)
        {
            case "METADATA_BLOCK_PICTURE":
                try
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "handleVorbisTagValue() : base64 encoded string has length " + val.length());
                    byte[] pictureBlockData = Base64.decode(val, Base64.DEFAULT);
                    if (logLevel > 0) Log.d(LOG_TAG, "handleVorbisTagValue() : decoded metadata block has length " + pictureBlockData.length);
                    if (!handleFlacPictureBlock(pictureBlockData))
                    {
                        return false;       // user-initiated fatal error
                    }
                } catch (Exception e)
                {
                    Log.e(LOG_TAG, "handleVorbisTagValue() : cannot decode base64, exception: " + e);
                }
                break;

            case "TRACKNUMBER":
                splitNdivM(val, idTrackNo, idTrackTotal);
                break;

            case "DISCNUMBER":
                splitNdivM(val, idDiscNo, idDiscTotal);
                break;

            default:
                int id = getVorbisStringTagId(key);
                if (id >= 0)
                {
                    values[id] = trimValue(val);
                }
                break;
        }

        return true;
    }

    // helper to read from buffer or InputStream
    private boolean readLittleEndianInt4(InputBuffer buf)
    {
        if (buf != null)
        {
            tmp = getLittleEndianInt4(buf.indata, buf.readIndex);
            buf.readIndex += 4;
        }
        else
        {
            byte[] data = new byte[4];
            if (!readBytes(data))
            {
                return false;
            }
            tmp = getLittleEndianInt4(data, 0);
        }
        return true;
    }

    // helper, reading either from InputBuffer or InputStream
    private String readLengthedString(InputBuffer buf)
    {
        if (!readLittleEndianInt4(buf))
        {
            return null;
        }
        int len = tmp;

        if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readLengthedString() : length = " + len);
        if (len > 1024 * 1024)      // note that this could be an embedded image, base64 encoded
        {
            Log.e(LOG_TAG, "Vorbis::readLengthedString() : length too high");
            return null;
        }

        byte[] data = new byte[len];
        if (buf != null)
        {
            if (!buf.readBytes(data))
            {
                return null;
            }
        }
        else
        {
            if (!readBytes(data))
            {
                return null;
            }
        }

        return new String(data, 0, data.length, StandardCharsets.UTF_8);
    }

    // read either from InputStream or InputBuffer
    protected boolean readVorbisComment(InputBuffer buf)
    {
        String vendor = readLengthedString(buf);
        if (vendor == null)
        {
            return false;
        }

        if (!readLittleEndianInt4(buf))
        {
            return false;
        }
        int userCommentListLength = tmp;

        for (int i = 0; i < userCommentListLength; i++)
        {
            String userComment = readLengthedString(buf);
            if (userComment == null)
            {
                return false;
            }
            if (userComment.length() < 256)
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readCommentHeader() : user comment = \"" + userComment + "\"");
            }
            else
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readCommentHeader() : user comment = \"" + userComment.substring(0, 256) + "\"" + "...");
            }
            if (!handleVorbisTagValue(userComment))
            {
                return false;
            }
        }

        return true;
    }
}
