/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://xiph.org/vorbis/doc/framing.html
// -> https://xiph.org/vorbis/doc/v-comment.html
// -> https://www.ietf.org/rfc/rfc3533.txt

// Note that Ogg is a container format encapsulating e.g. Vorbis-encoded audio and Vorbis comments.


import android.util.Log;
import java.io.InputStream;

import static java.lang.System.arraycopy;

@SuppressWarnings({"BooleanMethodIsAlwaysInverted", "SameParameterValue"})
abstract class TagsOgg extends TagsVorbis
{
    private static final String LOG_TAG = "TOGG";
    int expectedPageNo = -1;            // pre-increment
    protected byte[] data = null;         // packet data, combined from pages


    private class OggPage
    {
        private int noOfSegments = 0;
        int pageSize = 0;           // sum of all segment sizes
        boolean lastPage = false;           // a segment < 255 marks the last page
        int pageSeqNo;

        private final byte[] data = new byte[27];
        @SuppressWarnings("FieldCanBeLocal")
        private byte[] lacing = null;

        public boolean read()
        {
            expectedPageNo++;
            return readBytes(data);
        }

        public boolean readLacingTable()
        {
            pageSize = 0;
            if (noOfSegments > 0)
            {
                lacing = new byte[noOfSegments];
                if (!readBytes(lacing))
                {
                    return false;
                }
                for (byte b : lacing)
                {
                    int segmentSize = (b & 0xff);
                    pageSize += segmentSize;
                    if (segmentSize < 255)
                    {
                        if (logLevel > 0) Log.d(LOG_TAG, "OggPage::readLacingTable() : segmentSize = " + segmentSize + ", this is the last page");
                        lastPage = true;
                    }
                }
            }
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::readLacingTable() : page size = " + pageSize);
            return true;
        }

        public boolean check()
        {
            if ((data[0] != 'O') || (data[1] != 'g') || (data[2] != 'g') || (data[3] != 'S'))
            {
                Log.e(LOG_TAG, "OggPage::check() : missing OggS signature");
                return false;
            }

            if (data[4] != 0)
            {
                Log.e(LOG_TAG, "OggPage::check() : structure revision must be 0");
                return false;
            }

            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : packet is " + (((data[5] & 1) != 0) ? "continued" : "fresh"));
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : packet is " + (((data[5] & 2) != 0) ? "first" : "not first"));
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : packet is " + (((data[5] & 4) != 0) ? "last" : "not last"));
            if ((data[5] & 0xf8) != 0)
            {
                Log.w(LOG_TAG, "OggPage::check() : additional flags found.");
            }

            // this has something to do with the timestamp, so we do not care
            long absGranPos = getLittleEndianInt8(data, 6);
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : absolute granule position = " + absGranPos);

            int streamSerialNo = getLittleEndianInt4(data, 14);
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : stream serial number = " + streamSerialNo);

            pageSeqNo = getLittleEndianInt4(data, 18);
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : page sequence number = " + pageSeqNo);

            int pageCheckSum = getLittleEndianInt4(data, 22);
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : page checksum = " + pageCheckSum);

            noOfSegments = data[26] & 0xff;     // make sure this is unsigned!
            if (logLevel > 0) Log.d(LOG_TAG, "OggPage::check() : number of segments = " + noOfSegments);

            return true;
        }

        public boolean isContinued()
        {
            return ((data[5] & 1) != 0);
        }
    }


    public TagsOgg(InputStream is)
    {
        mInputStream = is;
        tagType = 11;            // Vorbis
    }


    // combine Ogg pages to a packet. Each page is about 64 k maximum.
    protected boolean readPacket()
    {
        int i;
        final int maxPages = 1024;      // about 64 Megabytes
        byte[][] pageData = new byte[maxPages][];
        int packetSize = 0;

        for (i = 0; i < maxPages; i++)
        {
            bytesLeft = Long.MAX_VALUE;
            OggPage page = new OggPage();
            if (!page.read())
            {
                return false;
            }
            if (!page.check())
            {
                return false;
            }
            if (!page.readLacingTable())
            {
                return false;
            }
            if (expectedPageNo != page.pageSeqNo)
            {
                Log.e(LOG_TAG, "TagsVorbis::readPacket() : wrong page sequence number " + page.pageSeqNo);
                return false;
            }
            if ((i == 0) && page.isContinued())
            {
                Log.e(LOG_TAG, "TagsVorbis::readPacket() : first page cannot be continued");
                return false;
            }
            else
            if ((i > 0) && !page.isContinued())
            {
                Log.e(LOG_TAG, "TagsVorbis::readPacket() : first page cannot be continued");
                return false;
            }

            // each page gets its buffer
            pageData[i] = new byte[page.pageSize];
            if (!readBytes(pageData[i]))
            {
                return false;
            }
            packetSize += page.pageSize;

            if (page.lastPage)
            {
                break;
            }
        }

        if (i < maxPages)
        {
            // copy all pages to common buffer
            data = new byte[packetSize];
            int destPos = 0;
            for (int j = 0; j <= i; j++)
            {
                int pageLength = pageData[j].length;
                arraycopy(pageData[j], 0, data, destPos, pageLength);
                destPos += pageLength;
            }
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readPacket() : packet has " + packetSize + " bytes in " + (i + 1) + " pages.");
        }
        else
        {
            Log.e(LOG_TAG, "Vorbis::readPacket() : more than " + i + " pages.");
            return false;
        }

        return true;
    }
}
