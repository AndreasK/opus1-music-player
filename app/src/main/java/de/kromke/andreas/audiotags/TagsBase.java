/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;


// Tags are managed in a table to ease loop processing.
// All tags are text strings, numerical values from mp4 are converted.
// Multiple values are concatenated with a LineFeed character.

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import androidx.documentfile.provider.DocumentFile;

import static java.lang.System.arraycopy;

@SuppressWarnings({"unused", "RedundantSuppression", "SameParameterValue", "SpellCheckingInspection", "JavadocBlankLines"})
public abstract class TagsBase
{
    //
    // API
    //

    public interface TagCallbacks
    {
        int handlePicture(byte[] data, int index, int length, int pictureType, final String mimeType);
    }

    // define tag indices be able to process tags in a loop
    public static final int idAlbum = 0;                // mp3: TALB    mp4: ©alb       flac: ALBUM
    public static final int idArtist = 1;               // mp3: TPE1    mp4: ©ART       flac: ARTIST
    public static final int idAlbumArtist = 2;          // mp3: TPE2    mp4: aART       flac: ALBUMARTIST
    public static final int idConductor = 3;            // mp3: TPE3    mp4: com.apple.iTunes.CONDUCTOR     (should be ©con?)
    public static final int idGrouping = 4;             // mp3: TIT1    mp4: ©grp       flac: GROUPING
    public static final int idTitle = 5;                // mp3: TIT2    mp4: ©nam       flac: TITLE
    public static final int idSubtitle = 6;             // mp3: TIT3    mp4: com.apple.iTunes.SUBTITLE      (should be desc)
    public static final int idComposer = 7;             // mp3: TCOM    mp4: ©wrt       flac: COMPOSER
    public static final int idDiscNo = 8;               // mp3: TPOS    mp4: disk       flac: DISCNUMBER
    public static final int idDiscTotal = 9;            // mp3: TPOS    mp4: disk
    public static final int idTrackNo = 10;             // mp3: TRCK    mp4: trkn       flac: TRACKNUMBER
    public static final int idTrackTotal = 11;          // mp3: TRCK    mp4: trkn
    public static final int idGenre = 12;               // mp3: TCON    mp4: ©gen       flac: GENRE
    public static final int idYear = 13;                // mp3: TYER    mp4: ©day       flac: DATE
    public static final int idComment = 14;             // mp3: COMM    mp4: ©cmt       flac: COMMENT
    // iTunes:
    public static final int idAppleMovement = 15;       // mp3: MVNM   mp4: ©mvn   flac: MOVEMENT
    public static final int idAppleMovementNo = 16;     // mp3: MVIN   mp4: ©mvi   flac: MOVEMENT_NO
    public static final int idAppleMovementTotal = 17;  // mp3: MVIN   mp4: ©mvc   flac: MOVEMENT_TOTAL
    public static final int idAppleMp3Work = 18;        // mp3: GRP1   mp4: <-->
    public static final int idAppleMp4Work = 19;        // mp3: <-->   mp4: ©wrk   flac: WORK
    // derived fields
    public static final int idAppleWork = 20;           // mp3: GRP1   mp4: ©wrk   flac: WORK
    public static final int idCombinedMovement = 21;    // combine movement or title information
    // number of fields
    public static final int idNum = 22;

    // cover picture information. For further pictures use the callback mechanism!
    public int coverPictureSize = 0;                    // 0: none
    public int coverPictureType = 0;                    // 0: none or unknown, 1: jpeg, 2: png
    public int tagType = 0;                             // 0: unknown, 1: ID3v1, 2: ID3v2, 10: MP4, 11: Vorbis

    // configuration
    public static final int logLevel = 0;               // 0: no Log.d() and no Log.v()  1: no Log.v(), but Log.d() 2: all
    public static final boolean doTrim = true;          // remove leading and trailing spaces
    public static final boolean doEmptyAsNull = true;   // set empty strings as null pointer
    public static final boolean doCheckID3v1 = true;    // of not set, only allow v2
    public static final boolean doForceD3v1 = false;    // for debugging purposes
    public static boolean doUseMovementTotal = true;    // used number of movements for combined movement name
    public static boolean doUseMovementNumberRoman = true;  // use roman numbers or arabic digits for movement numbers

    // field names for debugging purposes
    public final static String[] names = {
            "Album Name",
            "Artist",
            "Album Artist",
            "Conductor",
            "Grouping",
            "Title",
            "Subtitle",
            "Composer",
            "Disc Number",
            "Discs Total",
            "Track Number",
            "Tracks Total",
            "Genre",
            "Year",
            "Comment",
            // iTunes:
            "Apple Movement",
            "Apple Movement Number",
            "Apple Movements Total",
            "Apple Mp3 Work",
            "Apple Mp4 Work",
            // derived fields
            "Apple Work",
            "Combined Movement"
    };

    // tags
    public String[] values = new String[idNum];


    /**************************************************************************
     *
     * flexible API for pictures: Register a callback handler
     *
     *************************************************************************/
    public void setPictureHandler(TagCallbacks cb)
    {
        mHandlePictureCb = cb;
    }


    /**************************************************************************
     *
     * Main functionality: read tag and fill values table
     *
     *************************************************************************/
    abstract public boolean read();


    /**************************************************************************
     *
     * Create an object from MIME type and input stream
     * Parameter fileLength maybe -1 if unknown.
     *
     *************************************************************************/
    public static TagsBase fromMimeType(final String mimeType, InputStream inputStream, long fileLength)
    {
        TagsBase ret = null;

        switch (mimeType)
        {
            case "audio/mp3":
            {
                ret = new TagsId3v2(inputStream);
                break;
            }
            // Google Drive uses "audio/x-m4a" for m4a files
            case "audio/x-m4a":
            {
                ret = new TagsMp4(inputStream);
                break;
            }
            case "audio/ogg":
            {
                // TODO: It's unclear if Ogg/Opus is also declared as "audio/ogg".
                ret = new TagsOggVorbis(inputStream);
                break;
            }
            case "audio/opus":
            {
                ret = new TagsOggOpus(inputStream);
                break;
            }
            case "audio/flac":
            {
                ret = new TagsFlac(inputStream);
                break;
            }
            // Google Drive uses "audio/mpeg" for mp3 files
            case "audio/mpeg":
                Log.w(LOG_TAG, "readTagsFromUri() : ambiguous MIME type " + mimeType);
                break;

            default:
                Log.e(LOG_TAG, "readTagsFromUri() : unknown MIME type " + mimeType);
                break;
        }

        if (ret != null)
        {
            ret.mFileLength = fileLength;
        }
        return ret;
    }


    /**************************************************************************
     *
     * Create an object from file name and input stream.
     * Parameter fileLength maybe -1 if unknown.
     *
     *************************************************************************/
    public static TagsBase fromFileName(final String fileName, InputStream inputStream, long fileLength)
    {
        TagsBase tags = null;
        int i = fileName.lastIndexOf('.');
        if (i >= 0)
        {
            switch (fileName.substring(i).toLowerCase())
            {
                case ".mp3":
                case ".aac":            // unusual and outdated, but may happen
                {
                    tags = new TagsId3v2(inputStream);
                    break;
                }
                case ".mp4":
                case ".m4a":
                {
                    tags = new TagsMp4(inputStream);
                    break;
                }
                case ".ogg":
                {
                    tags = new TagsOggVorbis(inputStream);
                    break;
                }
                case ".opus":
                {
                    tags = new TagsOggOpus(inputStream);
                    break;
                }
                case ".flac":
                {
                    tags = new TagsFlac(inputStream);
                    break;
                }
                default:
                {
                    Log.e(LOG_TAG, "readTagsFromUri() : unknown file name extension " + fileName);
                    break;
                }
            }
        }

        if (tags != null)
        {
            tags.mFileLength = fileLength;  // maybe -1, if unknown
            tags.fileName = fileName;       // debug helper
        }

        return tags;
    }


    /**************************************************************************
     *
     * Create an object from Uri
     *
     *************************************************************************/
    public static TagsBase fromUri(final Uri uri, Context context)
    {
        InputStream inputStream;
        try
        {
            inputStream = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "fromUri() : cannot open input stream, exception: " + e);
            return null;
        }
        if (inputStream == null)
        {
            return null;
        }

        String mimeType = context.getContentResolver().getType(uri);
        Log.d(LOG_TAG, "fromUri() : MIME type = " + mimeType);
        if (mimeType == null)
        {
            mimeType = "audio/*";
        }
        boolean res;
        TagsBase tags = null;
        // "audio/mpeg" and "audio/ogg" (Vorbis or Opus) are ambiguous
        if (!mimeType.equals("audio/mpeg") && !mimeType.equals("audio/ogg"))
        {
            tags = fromMimeType(mimeType, inputStream, -1);
        }
        if (tags == null)
        {
            final String fileName = filenameFromUri(uri, context);
            Log.d(LOG_TAG, "fromUri() : file name = " + fileName);
            if (fileName != null)
            {
                tags = fromFileName(fileName, inputStream, -1);
            }
        }
        return tags;
    }


    /**************************************************************************
     *
     * Create an object from DocumentFile (Google's Storage Access Framework)
     *
     *************************************************************************/
    public static TagsBase fromDocumentFile(final DocumentFile df, ContentResolver resolver)
    {
        InputStream inputStream;
        try
        {
            inputStream = resolver.openInputStream(df.getUri());
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "fromDocumentFile() : cannot open input stream, exception: " + e);
            return null;
        }
        if (inputStream == null)
        {
            return null;
        }

        String mimeType = df.getType();
        if (mimeType == null)
        {
            mimeType = "audio/*";
        }

        // get file type from mime type, but if ambiguous, then get file name extension
        boolean res;
        TagsBase tags = null;
        // "audio/mpeg" and "audio/ogg" (Vorbis or Opus) are ambiguous
        if (!mimeType.equals("audio/mpeg") && !mimeType.equals("audio/ogg"))
        {
            tags = fromMimeType(mimeType, inputStream, df.length());
        }

        if (tags != null)
        {
            tags.fileName = df.getName();       // debug helper
        }
        else
        {
            final String fileName = df.getName();
            if (fileName != null)
            {
                tags = fromFileName(fileName, inputStream, df.length());
            }
        }

        return tags;
    }


    /**************************************************************************
     *
     * close input stream
     *
     *************************************************************************/
    public void close()
    {
        if (mInputStream != null)
        {
            try
            {
                mInputStream.close();
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "close() : cannot close input stream, exception: " + e);
            }
            mInputStream = null;
        }
    }


    /************************************************************************************
     *
     * combine movement name of form:
     *
     *  "movement" or
     *  "n movement" or
     *  "n/m movement"
     *
     ***********************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public void combineValues()
    {
        // work

        if (values[idAppleMp4Work] != null)
        {
            values[idAppleWork] = values[idAppleMp4Work];
        }
        else
        {
            values[idAppleWork] = values[idAppleMp3Work];
        }

        if (values[idGrouping] == null)
        {
            values[idGrouping] = values[idAppleWork];
        }

        // movement

        String result;

        int n = getNumericValue(values[idAppleMovementNo], -1);
        final String movementNo = (doUseMovementNumberRoman) ? getRomanNumeral(n) : "" + n;
        int total = doUseMovementTotal ? getNumericValue(values[idAppleMovementTotal], -1) : -1;
        String name = values[idAppleMovement];
        if (name == null)
        {
            name = values[idTitle];
        }
        if (n <= 0)
        {
            result = name;
        }
        else
        if (total <= 0)
        {
            result = "" + movementNo + ". " + name;
        }
        else
        {
            result = "" + n + "/" + total + " " + name;
        }

        values[idCombinedMovement] = result;
    }


    /************************************************************************************
     *
     * Replace numerical genres as form "(n)" or "n" with text values
     *
     ***********************************************************************************/
    @SuppressWarnings({"WeakerAccess", "UnusedReturnValue"})
    public boolean replaceNumericalGenres()
    {
        final String[] genres =
        {
            "Blues",
            "Classic Rock",
            "Country",
            "Dance",
            "Disco",
            "Funk",
            "Grunge",
            "Hip-Hop",
            "Jazz",
            "Metal",
            "New Age",
            "Oldies",
            "Other",
            "Pop",
            "R&B",
            "Rap",
            "Reggae",
            "Rock",
            "Techno",
            "Industrial",
            "Alternative",
            "Ska",
            "Death Metal",
            "Pranks",
            "Soundtrack",
            "Euro-Techno",
            "Ambient",
            "Trip-Hop",
            "Vocal",
            "Jazz+Funk",
            "Fusion",
            "Trance",
            "Classical",
            "Instrumental",
            "Acid",
            "House",
            "Game",
            "Sound Clip",
            "Gospel",
            "Noise",
            "Alternative Rock",
            "Bass",
            "Soul",
            "Punk",
            "Space",
            "Meditative",
            "Instrumental Pop",
            "Instrumental Rock",
            "Ethnic",
            "Gothic",
            "Darkwave",
            "Techno-Industrial",
            "Electronic",
            "Pop-Folk",
            "Eurodance",
            "Dream",
            "Southern Rock",
            "Comedy",
            "Cult",
            "Gangsta",
            "Top 40",
            "Christian Rap",
            "Pop/Funk",
            "Jungle",
            "Native American",
            "Cabaret",
            "New Wave",
            "Psychedelic",
            "Rave",
            "Showtunes",
            "Trailer",
            "Lo-Fi",
            "Tribal",
            "Acid Punk",
            "Acid Jazz",
            "Polka",
            "Retro",
            "Musical",
            "Rock & Roll",
            "Hard Rock",
            "Folk",
            "Folk/Rock",
            "National Folk",
            "Swing",
            "Fusion",
            "Bebob",
            "Latin",
            "Revival",
            "Celtic",
            "Bluegrass",
            "Avantgarde",
            "Gothic Rock",
            "Progressive Rock",
            "Psychedelic Rock",
            "Symphonic Rock",
            "Slow Rock",
            "Big Band",
            "Chorus",
            "Easy Listening",
            "Acoustic",
            "Humour",
            "Speech",
            "Chanson",
            "Opera",
            "Chamber Music",
            "Sonata",
            "Symphony",
            "Booty Bass",
            "Primus",
            "Porn Groove",
            "Satire",
            "Slow Jam",
            "Club",
            "Tango",
            "Samba",
            "Folklore",
            "Ballad",
            "Power Ballad",
            "Rhythmic Soul",
            "Freestyle",
            "Duet",
            "Punk Rock",
            "Drum Solo",
            "A Cappella",
            "Euro-House",
            "Dance Hall",
            "Goa",
            "Drum & Bass",
            "Club-House",
            "Hardcore",
            "Terror",
            "Indie",
            "BritPop",
            "Negerpunk",
            "Polsk Punk",
            "Beat",
            "Christian Gangsta Rap",
            "Heavy Metal",
            "Black Metal",
            "Crossover",
            "Contemporary Christian",
            "Christian Rock",
            "Merengue",
            "Salsa",
            "Thrash Metal",
            "Anime",
            "Jpop",
            "Synthpop",
            "Abstract",
            "Art Rock",
            "Baroque",
            "Bhangra",
            "Big Beat",
            "Breakbeat",
            "Chillout",
            "Downtempo",
            "Dub",
            "EBM",
            "Eclectic",
            "Electro",
            "Electroclash",
            "Emo",
            "Experimental",
            "Garage",
            "Global",
            "IDM",
            "Illbient",
            "Industro-Goth",
            "Jam Band",
            "Krautrock",
            "Leftfield",
            "Lounge",
            "Math Rock",
            "New Romantic",
            "Nu-Breakz",
            "Post-Punk",
            "Post-Rock",
            "Psytrance",
            "Shoegaze",
            "Space Rock",
            "Trop Rock",
            "World Music",
            "Neoclassical",
            "Audiobook",
            "Audio Theatre",
            "Neue Deutsche Welle",
            "Podcast",
            "Indie Rock",
            "G-Funk",
            "Dubstep",
            "Garage Rock",
            "Psybient"
        };

        String genre = values[idGenre];
        if ((genre == null) || genre.isEmpty())
        {
            return false;       // nothing done
        }
        if (genre.startsWith("(") && genre.endsWith(")"))
        {
            genre = genre.substring(1,  genre.length() - 1);    // strip "(" and ")"
            if (genre.isEmpty())
            {
                return false;
            }
        }
        if (!Character.isDigit(genre.charAt(0)))
        {
            return false;
        }
        int val = getNumericValue(genre, -1);
        if ((val < 0) || (val >= genres.length))
        {
            return false;
        }

        Log.d(LOG_TAG, "replaceNumericalGenres() -- genre " + values[idGenre] + " = replaced by " + genres[val]);
        values[idGenre] = genres[val];
        return true;
    }


    /**************************************************************************
     *
     * Debug helper: Dump all results
     *
     *************************************************************************/
    public void dump()
    {
        for (int i = 0; i < idNum; i++)
        {
            Log.d(LOG_TAG, "dump() -- " + names[i] + " = " + values[i]);
        }
        if (coverPictureSize > 0)
        {
            Log.d(LOG_TAG, "dump() -- cover picture found: size = " + coverPictureSize + ", type = " + coverPictureType);
        }
        if (tagType > 0)
        {
            Log.d(LOG_TAG, "dump() -- tag type = " + tagType);
        }
    }


    /**************************************************************************
     *
     * Debug helper: Get dump as text string
     *
     *************************************************************************/
    public String getDump()
    {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < idNum; i++)
        {
            res.append(names[i]).append(" = ").append(values[i]).append("\n");
        }
        if (coverPictureSize > 0)
        {
            res.append("cover picture found: size = ").append(coverPictureSize).append(", type = ").append(coverPictureType);
        }
        if (tagType > 0)
        {
            res.append("tag type = ").append(tagType);
        }

        return res.toString();
    }


    /**************************************************************************
     *
     * convert text to number (for track/disc number/total and year)
     *
     *************************************************************************/
    public int getNumericValue(int id, int defVal)
    {
        if (id < idNum)
        {
            final String s = values[id];
            if (s != null)
            {
                return getNumericValue(s, defVal);
            }
        }
        return defVal;
    }


    /**************************************************************************
     *
     * convert text to roman number (for movement number)
     *
     *************************************************************************/
    @SuppressWarnings("StringConcatenationInLoop")
    static public String getRomanNumeral(int n)
    {
        String roman = "";

        if (n >= 40)
        {
            // fallback for large numbers: return arabic number representation
            return roman + n;
        }

        while (n >= 10)
        {
            roman += "X";
            n -= 10;
        }
        while (n >= 9)
        {
            roman += "IX";
            n -= 9;
        }
        while (n >= 5)
        {
            roman += "V";
            n -= 5;
        }
        while (n >= 4)
        {
            roman += "IV";
            n -= 4;
        }
        while (n >= 1)
        {
            roman += "I";
            n -= 1;
        }

        return roman;
    }

    //
    // Implementation constants
    //

    protected static final int maxFrameSize = 10 * 1024 * 1024;
    private static final int skipBufferSize = 512;
    private static final String LOG_TAG = "TBAS";

    //
    // common utility members and functions
    //

    protected InputStream mInputStream;         // audio file
    protected long mFileLength = 0;             // 0:unknown
    protected TagCallbacks mHandlePictureCb = null;
    protected int mCanSkip = -1;          // -1: unknown, 1: yes, 0: no
    private byte[] skipBuffer = null;
    protected boolean bEof = false;
    protected long bytesLeft = Long.MAX_VALUE;

    // debugging helpers
    protected long filePos = 0;
    protected int level = 0;                // 0: file level, others: for containers of containers etc.
    protected String levelStr = "";
    protected String fileName = null;

    // helper class to read from buffer
    protected static class InputBuffer
    {
        byte[] indata;
        int readIndex;

        protected InputBuffer(byte[] data, int index)
        {
            indata = data;
            readIndex = index;
        }

        protected boolean readBytes(byte[] data)
        {
            int bytesLeft = indata.length - readIndex;
            int bytesToRead = data.length;
            if (bytesLeft < bytesToRead)
            {
                Log.e(LOG_TAG, "InputBuffer::readBytes() -- not enough bytes left : " + bytesLeft + ", need " + data.length);
                return false;
            }

            arraycopy(indata, readIndex, data, 0, bytesToRead);
            readIndex += bytesToRead;
            return true;
        }
    }

    // helper
    protected boolean readBytes(byte[] data)
    {
        if (bytesLeft < data.length)
        {
            Log.e(LOG_TAG, "readBytes() -- not enough bytes left : " + bytesLeft + ", need " + data.length);
            return false;
        }

        int res;
        try
        {
            res = mInputStream.read(data);
            if (res > 0)
            {
                filePos += res;
            }
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "readBytes() -- read error : " + e.getMessage());
            return false;
        }

        if (res == -1)
        {
            bEof = true;
            Log.d(LOG_TAG, "readBytes() -- " + levelStr + " end-of-file");
            //Log.d(LOG_TAG, "readBytes() -- " + levelStr + " bytes left = " + bytesLeft);
            return false;
        }
        else
        if (res != data.length)
        {
            Log.e(LOG_TAG, "readBytes() -- read error, read " + res + " instead of " + data.length);
            return false;
        }

        bytesLeft -= data.length;
        return true;
    }


    // helper
    protected boolean safeSkip(long len)
    {
        if (mCanSkip != 0)
        {
            //
            // real skip (fast)
            //

            try
            {
                long res = mInputStream.skip(len);
                mCanSkip = 1;
                bytesLeft -= len;
                filePos += len;
                return (res == len);
            } catch (IOException e)
            {
                Log.w(LOG_TAG, "safeSkip() -- skipping not possible, going to dummy-read");
                mCanSkip = 0;
            }
        }

        //
        // dummy skip (slow)
        //

        if (skipBuffer == null)
        {
            skipBuffer = new byte[skipBufferSize];
        }
        while(len >= skipBuffer.length)
        {
            if (readBytes(skipBuffer))
            {
                return false;
            }
            len -= skipBuffer.length;
        }
        if (len > 0)
        {
            byte[] remainingBuffer = new byte[(int) len];
            //noinspection RedundantIfStatement
            if (readBytes(remainingBuffer))
            {
                return false;
            }
        }

        return true;
    }

    // helper-helper
    private int getIntFromByte(byte b, int shift)
    {
        int ret = (b & 0xff);      // this should lead to an unsigned value 0..255
        return ret << shift;
    }

    // helper-helper
    private long getLongFromByte(byte b, int shift)
    {
        long ret = (b & 0xff);      // this should lead to an unsigned value 0..255
        return ret << shift;
    }

    // helper
    protected int getBigEndianInt2(byte[] data, @SuppressWarnings("SameParameterValue") int index)
    {
        return getIntFromByte(data[index + 1], 0) +
                getIntFromByte(data[index    ], 8);
    }

    // helper
    protected int getBigEndianInt3(byte[] data, @SuppressWarnings("SameParameterValue") int index)
    {
        return getIntFromByte(data[index + 2], 0) +
                getIntFromByte(data[index + 1], 8) +
                getIntFromByte(data[index    ], 16);
    }

    // read 4-bytes integer value in big endian format from byte array
    protected int getBigEndianInt4(byte[] data, int index)
    {
        return (data[index + 3] & 0xff) + ((data[index + 2] & 0xff) << 8) + ((data[index + 1] & 0xff) << 16) + ((data[index] & 0xff) << 24);
    }

    // helper
    protected long getBigEndianInt8(byte[] data, @SuppressWarnings("SameParameterValue") int index)
    {
        return getLongFromByte(data[index + 7], 0) +
                getLongFromByte(data[index + 6], 8) +
                getLongFromByte(data[index + 5], 16) +
                getLongFromByte(data[index + 4], 24) +
                getLongFromByte(data[index + 3], 32) +
                getLongFromByte(data[index + 2], 40) +
                getLongFromByte(data[index + 1], 48) +
                getLongFromByte(data[index    ], 56);
    }

    // helper
    protected int getLittleEndianInt2(byte[] data, int index)
    {
        return (data[index] & 0xff) + ((data[index + 1] & 0xff) << 8);
    }

    // helper
    protected int getLittleEndianInt4(byte[] data, int index)
    {
        return (data[index] & 0xff) + ((data[index + 1] & 0xff) << 8) + ((data[index + 2] & 0xff) << 16) + ((data[index + 3] & 0xff) << 24);
    }

    // helper
    protected long getLittleEndianInt8(byte[] data, int index)
    {
        return getLongFromByte(data[index    ], 0) +
                getLongFromByte(data[index + 1], 8) +
                getLongFromByte(data[index + 2], 16) +
                getLongFromByte(data[index + 3], 24) +
                getLongFromByte(data[index + 4], 32) +
                getLongFromByte(data[index + 5], 40) +
                getLongFromByte(data[index + 6], 48) +
                getLongFromByte(data[index + 7], 56);
    }

    // convert text to number
    public static int getNumericValue(final String s, int defVal)
    {
        try
        {
            return Integer.parseInt(s);
        } catch (NumberFormatException e)
        {
            Log.e(LOG_TAG, "getNumericValue() : malformed number \"" + s + "\"");
            return defVal;
        }
    }

    // trimming of value string
    protected String trimValue(String v)
    {
        if (v != null)
        {
            if (v.startsWith(" ") || v.endsWith(" "))
            {
                Log.w(LOG_TAG, "trimValue() : leading or trailing spaces in \"" + v + "\" (" + fileName + ")");
                if (doTrim)
                {
                    v = v.trim();
                }
            }
            if (doEmptyAsNull && v.isEmpty())
            {
                Log.w(LOG_TAG, "trimValue() : empty value ignored (" + fileName + ")");
                v = null;
            }
        }
        return v;
    }

    // split string of type n/m for track or disc into separate numbers
    protected void splitNdivM(final String s, int id1, int id2)
    {
        if (s == null)
        {
            return;
        }

        int n1;
        int n2 = -1;
        int i = s.indexOf('/');
        if (i == -1)
        {
            n1 = getNumericValue(s, -1);
        }
        else
        {
            String s1 = s.substring(0, i);
            n1 = getNumericValue(s1, -1);
            String s2 = s.substring(i + 1);
            n2 = getNumericValue(s2, -1);
        }

        if (n1 != -1)
        {
            values[id1] = "" + n1;      // convert to string
        }

        if (n2 != -1)
        {
            values[id2] = "" + n2;      // convert to string
        }
    }

    // remember cover picture information
    protected void rememberCoverPic(int picType, int picSize, final String mimeType)
    {
        if (picType == 3)
        {
            coverPictureSize = picSize;
            if (mimeType.equals("image/jpeg"))
            {
                coverPictureType = 1;
            } else if (mimeType.equals("image/png"))
            {
                coverPictureType = 2;
            }
        }
        else
        {
            Log.d(LOG_TAG, "TagsBase::rememberCoverPic() : picture is not cover picture -> ignored");
        }
    }

    static String filenameFromUri(Uri uri, Context context)
    {
        DocumentFile df = DocumentFile.fromSingleUri(context, uri);
        if (df == null)
        {
            Log.e(LOG_TAG, "onActivityResult() : cannot get DocumentFile");
            return null;
        }
        else
        {
            return df.getName();
        }
    }
}
