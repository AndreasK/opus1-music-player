/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://xiph.org/vorbis/doc/v-comment.html
// -> https://www.rfc-editor.org/rfc/rfc7845.txt  (Opus)
// -> https://www.ietf.org/rfc/rfc3533.txt  (Ogg)

// Note that Ogg is a container format encapsulating e.g. Vorbis-encoded audio and Vorbis comments.


import android.util.Log;
import java.io.InputStream;

@SuppressWarnings({"SameParameterValue"})
public class TagsOggOpus extends TagsOgg
{
    private static final String LOG_TAG = "TOGO";


    private class Opus
    {
        @SuppressWarnings("FieldCanBeLocal")

        public boolean check()
        {
            if (data.length < 4)
            {
                return false;
            }
            if ((data[0] != 'O') || (data[1] != 'p') || (data[2] != 'u') || (data[3] != 's'))
            {
                Log.e(LOG_TAG, "Opus::check() : missing Opus signature");
                return false;
            }

            return true;
        }

        boolean getIdentificationHeader()
        {
            if (data.length < 18)
            {
                Log.e(LOG_TAG, "Opus::check() : packet too short for Opus id header");
                return false;
            }
            if ((data[4] != 'H') || (data[5] != 'e') || (data[6] != 'a') || (data[7] != 'd'))
            {
                Log.e(LOG_TAG, "Opus::check() : no Opus id header");
                return false;
            }

            int version = data[8];
            if (version != 1)
            {
                Log.e(LOG_TAG, "Opus::check() : wrong Opus id header version: " + version);
                return false;
            }

            int audioChannels = data[9];
            if (logLevel > 0) Log.d(LOG_TAG, "Opus::readIdentificationHeader() : number of audio channels = " + audioChannels);

            int preSkip = getLittleEndianInt2(data, 10);
            if (logLevel > 0) Log.d(LOG_TAG, "Opus::readIdentificationHeader() : pre-skip = " + preSkip);

            int audioSampleRate = getLittleEndianInt4(data, 12);
            if (logLevel > 0) Log.d(LOG_TAG, "Opus::readIdentificationHeader() : audio sample rate = " + audioSampleRate);

            int outputGain = getLittleEndianInt2(data, 16);
            if (logLevel > 0) Log.d(LOG_TAG, "Opus::readIdentificationHeader() : output gain = " + outputGain);

            return true;
        }

        boolean getComment()
        {
            if (data.length < 12)
            {
                Log.e(LOG_TAG, "Opus::getComment() : packet too short for Opus tag header");
                return false;
            }
            if ((data[4] != 'T') || (data[5] != 'a') || (data[6] != 'g') || (data[7] != 's'))
            {
                Log.e(LOG_TAG, "Opus::getComment() : no Opus tags header");
                return false;
            }

            /*
            int vendorStringLen = getLittleEndianInt4(data, 8);
            if (data.length < 12 + vendorStringLen)
            {
                Log.e(LOG_TAG, "Opus::getComment() : malformed Opus tags header");
                return false;
            }
            String vendorString = new String(data, 12, vendorStringLen, StandardCharsets.UTF_8);
            if (logLevel > 0) Log.d(LOG_TAG, "Opus::getComment() : vendor string = " + vendorString);
            */

            InputBuffer buf = new InputBuffer(data, 8);
            //noinspection RedundantIfStatement
            if (!readVorbisComment(buf))
            {
                return false;
            }

            return true;
        }
    }

    public TagsOggOpus(InputStream is)
    {
        super(is);              // Ogg
        tagType = 11;           // Vorbis
    }


    public boolean read()
    {
        // read Ogg packet, consisting of pages, to memory

        if (!readPacket())
        {
            return false;
        }

        //
        // read first Opus packet inside the Ogg packet, must be id header
        //

        Opus opus = new Opus();
        if (!opus.check())
        {
            return false;
        }
        if (!opus.getIdentificationHeader())
        {
            return false;
        }

        //
        // read second Ogg packet
        //

        if (!readPacket())
        {
            return false;
        }

        //
        // read second Opus packet inside the Ogg packet, must be tag header
        //

        opus = new Opus();
        if (!opus.check())
        {
            return false;
        }
        //noinspection RedundantIfStatement
        if (!opus.getComment())
        {
            return false;
        }

        return true;
    }
}
