/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://xiph.org/vorbis/doc/framing.html
// -> https://xiph.org/vorbis/doc/v-comment.html
// -> https://www.ietf.org/rfc/rfc3533.txt

// Note that Ogg is a container format encapsulating e.g. Vorbis-encoded audio and Vorbis comments.


import android.util.Log;
import java.io.InputStream;

@SuppressWarnings({"SameParameterValue"})
public class TagsOggVorbis extends TagsOgg
{
    private static final String LOG_TAG = "TOGV";


    private class Vorbis
    {
        @SuppressWarnings("FieldCanBeLocal")
        int packetType;
        int offset = 0;         // offset in packet data

        public boolean check()
        {
            packetType = data[0] & 0xff;
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::check() : packet type is " + packetType);

            if ((data[1] != 'v') || (data[2] != 'o') || (data[3] != 'r') || (data[4] != 'b') || (data[5] != 'i') || (data[6] != 's'))
            {
                Log.e(LOG_TAG, "Vorbis::check() : missing vorbis signature");
                return false;
            }

            offset = 7;
            return true;
        }

        boolean getIdentificationHeader()
        {
            int version = getLittleEndianInt4(data, offset);
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : version = " + version);

            int audioChannels = data[offset + 4];
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : number of audio channels = " + audioChannels);

            int audioSampleRate = getLittleEndianInt4(data, offset + 5);
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : audio sample rate = " + audioSampleRate);

            int bitrateMaximum = getLittleEndianInt4(data, offset + 9);
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : maximum bitrate = " + bitrateMaximum);
            int bitrateNominal = getLittleEndianInt4(data, offset + 13);
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : audio sample rate = " + bitrateNominal);
            int bitrateMinimum = getLittleEndianInt4(data, offset + 17);
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : audio sample rate = " + bitrateMinimum);

            int blocksize = data[offset + 21] & 0xff;
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : blocksize0 = " + (blocksize & 0x0f));
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : blocksize1 = " + (blocksize >> 4));

            int framing = data[offset + 22];
            if (logLevel > 0) Log.d(LOG_TAG, "Vorbis::readIdentificationHeader() : framing = " + framing);
            offset += 23;

            return true;
        }

        boolean getComment()
        {
            InputBuffer buf = new InputBuffer(data, offset);
            //noinspection RedundantIfStatement
            if (!readVorbisComment(buf))
            {
                return false;
            }

            return true;
        }
    }

    public TagsOggVorbis(InputStream is)
    {
        super(is);
        tagType = 11;            // Vorbis
    }


    public boolean read()
    {
        // read Ogg packet, consisting of pages, to memory

        if (!readPacket())
        {
            return false;
        }

        //
        // read first Vorbis packet inside the Ogg packet, must have type 1
        //

        Vorbis vorbis = new Vorbis();
        if (!vorbis.check())
        {
            return false;
        }
        if (vorbis.packetType == 1)
        {
            if (!vorbis.getIdentificationHeader())
            {
                return false;
            }
        }
        else
        {
            Log.e(LOG_TAG, "TagsVorbis::read() : unhandled packet type " + vorbis.packetType);
            return false;
        }

        //
        // read second Ogg packet
        //

        if (!readPacket())
        {
            return false;
        }

        //
        // read second Vorbis packet inside the Ogg packet, must have type 3
        //

        vorbis = new Vorbis();
        if (!vorbis.check())
        {
            return false;
        }
        if (vorbis.packetType == 3)
        {
            //noinspection RedundantIfStatement
            if (!vorbis.getComment())
            {
                return false;
            }
        }
        else
        {
            Log.e(LOG_TAG, "TagsVorbis::read() : unhandled packet type " + vorbis.packetType);
            return false;
        }

        return true;
    }
}
