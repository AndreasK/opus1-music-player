/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://id3.org/ID3v1
// -> https://developer.android.com/reference/java/io/InputStream


// In fact this is only a helper class for TagsId3v2.
// Supported: v1 and v1.1

import android.util.Log;

import java.nio.charset.StandardCharsets;

public class TagsId3v1
{
    private static final String LOG_TAG = "TID1";
    public final static int bufSize = 128;
    public byte[] data = new byte[bufSize];
    // values:
    public String song = null;
    public String artist = null;
    public String album = null;
    public String year = null;
    public String comment = null;
    public String genre = null;
    public String track = null;

    // decode string according to encoding mode
    private String decodeString(int index, int len)
    {
        // get actual length
        int i;
        for (i = 0; i < len; i++)
        {
            if (data[index + i] == '\0')
            {
                break;
            }
        }
        if (i > 0)
        {
            return new String(data, index, i, StandardCharsets.ISO_8859_1);
        }
        else
        {
            return null;
        }
    }

    public boolean check()
    {
        if ((data[0] != 'T') || (data[1] != 'A') || (data[2] != 'G'))
        {
            Log.e(LOG_TAG, "check() : missing ID3v1 signature");
            return false;
        }

        song = decodeString(3, 30);
        artist = decodeString(33, 30);
        album = decodeString(63, 30);
        year = decodeString(93, 4);
        comment = decodeString(97, 30);
        genre = "" + (data[127] & 0xff);

        if ((data[125] == '\0') && (data[126] != '\0'))
        {
            // ID3v1.1
            track = "" + (data[126] & 0xff);
        }
        return true;
    }

}
