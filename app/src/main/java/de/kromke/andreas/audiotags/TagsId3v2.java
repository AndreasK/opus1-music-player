/*
 * Copyright (C) 2020-21 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.audiotags;

// -> https://id3.org/Developer%20Information
// -> https://id3.org/id3v2.3.0
// -> https://developer.android.com/reference/java/io/InputStream


// Supported: v2.3 and v2.4 (and v1 and v1.1 via helper class)
// Not supported: unsynchronsation, compression

import android.util.Log;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@SuppressWarnings({"SpellCheckingInspection", "JavadocBlankLines"})
public class TagsId3v2 extends TagsBase
{
    private static final String LOG_TAG = "TID3";
    private final Id3v2Header header = new Id3v2Header();
    @SuppressWarnings("FieldCanBeLocal")
    private Id3v2ExtendedHeader extendedHeader = null;


    // get length of NUL terminated string, needed for conversion from bytes to string
    private int getStringLength(byte[] data, int index)
    {
        int i = index;
        while((i < data.length) && (data[i] != '\0'))
        {
            i++;
        }
        if (i >= data.length)
        {
            Log.e(LOG_TAG, "TagsId3v2::getStringLength() : string is not NUL terminated");
            return -1;
        }
        return i - index;
    }

    // decode string according to encoding mode
    private String decodeString(byte[] data, int index, int len, int mode)
    {
        Charset charset;
        switch(mode)
        {
            case 0:
                charset = StandardCharsets.ISO_8859_1;
                break;

            case 1:
                // Unicode 16 bit (with BOM, i.e. starts with ff,fe (little endian) or fe,ff (big endian))
                if (len < 2)
                {
                    Log.e(LOG_TAG, "Id3v2Frame::decodeString() : text too short for Unicode");
                    return null;
                }
                charset = StandardCharsets.UTF_16;
                break;

            case 2:
                // Unicode 16 bit, big endian (ID3v2.4)
                charset = StandardCharsets.UTF_16BE;
                break;

            case 3:
                // Unicode 8 bit (ID3v2.4)
                charset = StandardCharsets.UTF_8;
                break;

            default:
                Log.e(LOG_TAG, "Id3v2Frame::decodeString() : invalid text encoding: " + mode);
                return null;
        }

        return new String(data, index, len, charset);
    }

    // read 4-bytes integer value in big endian format from byte array, all bytes only with 7 bits
    private int getBigEndianInt4Synchsafe(byte[] data, int index)
    {
        if (((data[index + 3] & 0x80) != 0) ||((data[index + 2] & 0x80) != 0) || ((data[index + 1] & 0x80) != 0) || ((data[index] & 0x80) != 0))
        {
            Log.e(LOG_TAG, "TagsId3v2::getBigEndianInt4Synchsafe() : size bytes must be < 128");
            return -1;
        }
        else
        {
            return (data[index + 3] & 0x7f) + ((data[index + 2] & 0x7f) << 7) + ((data[index + 1] & 0x7f) << 14) + ((data[index] & 0x7f) << 21);
        }
    }

    private class Id3v2Header
    {
        int majorVersion;
        int minorVersion;
        int tagSize;

        private final byte[] data = new byte[10];

        public boolean read()
        {
            return readBytes(data);
        }

        public boolean checkSignature()
        {
            return (data[0] == 'I') && (data[1] == 'D') && (data[2] == '3');
        }

        public boolean check()
        {
            majorVersion = data[3];
            minorVersion = data[4];
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Header::check() : version = ID3v2." + majorVersion + "." + minorVersion);
            if ((majorVersion != 3) && (majorVersion != 4))
            {
                Log.e(LOG_TAG, "Id3v2Header::check() : only v2.3 and v2.4 are supported, not v2." + majorVersion + "." + minorVersion + "(" + fileName + ")");
                return false;
            }

            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Header::check() : flags = 0x" + String.format("%02X ", data[5]));
            tagSize = getBigEndianInt4Synchsafe(data, 6);
            if (tagSize < 0)
            {
                return false;
            }
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Header::check() : tag size = " + tagSize);

            bytesLeft = tagSize;
            return true;
        }

        public boolean isUnsynchronisation()
        {
            return ((data[5] & 0x80) != 0);
        }

        public boolean isExtendedHeader()
        {
            return ((data[5] & 0x40) != 0);
        }

        public boolean isFooter()
        {
            return ((data[5] & 0x20) != 0);
        }
    }


    private class Id3v2ExtendedHeader
    {
        private final byte[] data = new byte[6];
        private byte[] moreData = null;
        int extendedHeaderSize;
        int crc = 0;

        public boolean read()
        {
            return readBytes(data);
        }

        public boolean check()
        {
            extendedHeaderSize = getBigEndianInt4(data, 0);
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2ExtendedHeader::check() : extended header size = " + extendedHeaderSize);
            moreData = new byte[extendedHeaderSize];
            return readBytes(moreData);
        }

        public boolean isCrc()
        {
            return ((data[4] & 0x80) != 0);
        }

        public boolean readCrc()
        {
            byte[] crcdata = new byte[4];
            if (!readBytes(crcdata))
            {
                return false;
            }
            crc = getBigEndianInt4(crcdata, 0);
            return true;
        }
    }


    @SuppressWarnings("SpellCheckingInspection")
    private class Id3v2Frame
    {
        private final byte[] data = new byte[10];
        private final byte[] data2 = new byte[4];       // v2.4: optional data length indicator
        private byte[] moreData = null;
        String id = null;
        int payloadSize;

        public boolean read()
        {
            return readBytes(data);
        }

        public boolean check()
        {
            if ((data[0] == 0) && (data[1] == 0) && (data[2] == 0) && (data[3] == 0))
            {
                id = "";
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : frame seems to be empty");
            }
            else
            {
                id = "" + ((char) data[0]) + ((char) data[1]) + ((char) data[2]) + ((char) data[3]);
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : id = " + id);
            }
            if (header.majorVersion == 4)
            {
                payloadSize = getBigEndianInt4Synchsafe(data, 4);
            }
            else
            {
                payloadSize = getBigEndianInt4(data, 4);
            }

            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() :      size = " + payloadSize);
            if (payloadSize < 0)
            {
                return false;
            }
            if ((data[8] != 0) || (data[9] != 0))
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() :      flags = 0x" + String.format("%02X ", data[8]) + " 0x" + String.format("%02X ", data[9]));
            }

            if (header.majorVersion == 4)
            {
                if ((data[9] & 0x40) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : frame contains group information");
                }
                if ((data[9] & 0x08) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : is compressed");
                }
                if ((data[9] & 0x04) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : is encrypted");
                }
                if ((data[9] & 0x02) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : unsynchronisation flag is set");
                }
                if ((data[9] & 0x01) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : data length indicator is present");
                }
            }
            else
            {
                if ((data[9] & 0x80) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : is compressed");
                }
                if ((data[9] & 0x40) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : is encrypted");
                }
                if ((data[9] & 0x20) != 0)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : frame contains group information");
                }
            }

            if (payloadSize > maxFrameSize)
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::check() : frame is larger than limit (" + maxFrameSize + ")");
                return false;
            }

            return true;
        }

        public boolean isCompressed()
        {
            if (header.majorVersion == 4)
            {
                return  ((data[9] & 0x08) != 0);
            }
            else
            {
                return  ((data[9] & 0x80) != 0);
            }
        }

        public boolean isEncrypted()
        {
            if (header.majorVersion == 4)
            {
                return  ((data[9] & 0x04) != 0);
            }
            else
            {
                return  ((data[9] & 0x40) != 0);
            }
        }

        // v2.4 specific
        public boolean isUnsynchronisation()
        {
            return (header.majorVersion == 4) && ((data[9] & 0x02) != 0);
        }

        // v2.4 specific
        public boolean isDataLength()
        {
            return (header.majorVersion == 4) && ((data[9] & 0x01) != 0);
        }

        // v2.4 specific
        public boolean readDataLength()
        {
            if (readBytes(data2))
            {
                payloadSize -= 4;
                int dataLength = getBigEndianInt4Synchsafe(data2, 0);
                Log.w(LOG_TAG, "Id3v2Frame::readDataLength() : data length is " + dataLength);
                return (dataLength >= 0);
            }
            else
            {
                return false;
            }
        }

        public boolean isEmpty()
        {
            return id.isEmpty();
        }

        boolean readPayload()
        {
            if (payloadSize > 0)
            {
                moreData = new byte[payloadSize];
                return readBytes(moreData);
            }
            else
            if (!id.isEmpty())
            {
                // frame has valid ID, but is empty
                Log.w(LOG_TAG, "Id3v2Frame::check() : frame is empty");
            }
            return true;
        }

        boolean skipPayload()
        {
            return safeSkip(payloadSize);
        }

        public String toStringOrNull()
        {
            String ret;

            if ((moreData == null) || (moreData.length < 1))
            {
                ret = null;
            }
            else
            {
                ret = decodeString(moreData, 1, moreData.length - 1, moreData[0] & 0xff);
            }

            if (ret == null)
            {
                return null;
            }

            // remove NUL bytes
            while(ret.endsWith("\0"))
            {
                ret = ret.substring(0, ret.length() - 1);
            }

            return ret;
        }

        public boolean toPicture()
        {
            if ((moreData == null) || (moreData.length < 3))
            {
                Log.e(LOG_TAG, "Id3v2Frame::toPicture() : no payload or too small");
                return false;
            }

            int index = 0;
            int textEncoding = moreData[index++] & 0xff;
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::toPicture() : text encoding: " + textEncoding);

            int l = getStringLength(moreData, index);
            if (l < 0)
            {
                return false;
            }
            final String mimeType = new String(moreData, index, l, StandardCharsets.ISO_8859_1);
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::toPicture() : MIME type = " + mimeType);
            index += l + 1;     // skip string and NUL

            int picType = moreData[index++] & 0xff;
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::toPicture() : picture type: " + picType);

            l = getStringLength(moreData, index);
            if (l < 0)
            {
                return false;
            }
            final String description = decodeString(moreData, index, l, textEncoding);
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::toPicture() : description = " + description);
            index += l + 1;     // skip string and NUL
            int picSize = moreData.length - index;
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2Frame::toPicture() : picture size = " + picSize);

            rememberCoverPic(picType, picSize, mimeType);
            if (mHandlePictureCb != null)
            {
                int ret = mHandlePictureCb.handlePicture(moreData, index, picSize, picType, mimeType);
                //noinspection RedundantIfStatement
                if (ret < 0)
                {
                    return false;       // fatal error
                }
            }
            return true;
        }

    }

    private void evaluate(final Id3v2Frame frame)
    {
        int id = -1;
        int id2 = -1;
        switch(frame.id)
        {
            case "APIC":
                if (!frame.toPicture())
                {
                    return;
                }
                break;

            case "TALB":
                id = idAlbum;
                break;

            case "TPE1":
                id = idArtist;
                break;

            case "TPE2":
                id = idAlbumArtist;
                break;

            case "TPE3":
                id = idConductor;
                break;

            case "TIT1":
                id = idGrouping;
                break;

            case "TIT2":
                id = idTitle;
                break;

            case "TIT3":
                id = idSubtitle;
                break;

            case "TCOM":
                id = idComposer;
                break;

            case "TPOS":
                id = idDiscNo;
                id2 = idDiscTotal;
                break;

            case "TRCK":
                id = idTrackNo;
                id2 = idTrackTotal;
                break;

            case "TCON":
                id = idGenre;
                break;

            case "TDRC":            // v2.4
            case "TYER":            // v2.3
                id = idYear;
                break;

            case "COMM":
                id = idComment;
                break;

            case "MVNM":
                id = idAppleMovement;
                break;

            case "MVIN":
                id = idAppleMovementNo;
                id2 = idAppleMovementTotal;
                break;

            case "GRP1":
                id = idAppleMp3Work;
                break;
        }
        if (id >= 0)
        {
            if (id2 >= 0)
            {
                splitNdivM(trimValue(frame.toStringOrNull()), id, id2);
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2TagHeader::read() : " + names[id2] + " = " + values[id2]);
            }
            else
            {
                values[id] = trimValue(frame.toStringOrNull());
            }
            if (logLevel > 0) Log.d(LOG_TAG, "Id3v2TagHeader::read() : " + names[id] + " = " + values[id]);
        }
    }


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    public TagsId3v2(InputStream is)
    {
        mInputStream = is;
        tagType = 2;            // ID3v2
    }


    /**************************************************************************
     *
     * main loop:
     *
     *  read header
     *  loop
     *      read frame
     *  end loop
     *
     *************************************************************************/
    public boolean read()
    {
        //
        // read 8 byte header
        //

        if (!header.read())
        {
            return false;
        }

        //
        // check signature
        //

        if (doForceD3v1 || !header.checkSignature())
        {
            // file could be ID3v1
            if (doCheckID3v1)
            {
                if (mFileLength <= 0)
                {
                    Log.e(LOG_TAG, "read() : Cannot scan for ID3v1, because file length is unknown" + " (" + fileName + ")");
                    return false;
                }
                if (mFileLength < header.data.length + TagsId3v1.bufSize)
                {
                    Log.e(LOG_TAG, "read() : Cannot scan for ID3v1, because file is too small" + " (" + fileName + ")");
                    return false;
                }
                if (!safeSkip(mFileLength - header.data.length - TagsId3v1.bufSize))
                {
                    return false;
                }
                TagsId3v1 tagsV1 = new TagsId3v1();
                if (!readBytes(tagsV1.data))
                {
                    return false;
                }
                if (!tagsV1.check())
                {
                    Log.e(LOG_TAG, "read() : Both missing ID3v2 and ID3v1 signature" + " (" + fileName + ")");
                    return false;
                }

                Log.w(LOG_TAG, "read() : Got ID3v1 values, you should update the file" + " (" + fileName + ")");

                tagType = 1;        // ID3v1
                values[idTitle] = trimValue(tagsV1.song);
                values[idArtist] = trimValue(tagsV1.artist);
                values[idAlbum] = trimValue(tagsV1.album);
                values[idYear] = trimValue(tagsV1.year);
                values[idComment] = trimValue(tagsV1.comment);
                values[idGenre] = trimValue(tagsV1.genre);
                values[idTrackNo] = trimValue(tagsV1.track);
                return true;
            }
            else
            {
                Log.e(LOG_TAG, "read() : Missing ID3v2 signature" + " (" + fileName + ")");
                return false;
            }
        }

        //
        // check and evaluate header
        //

        if (!header.check())
        {
            return false;
        }

        if (header.isUnsynchronisation())
        {
            Log.e(LOG_TAG, "Id3v2TagHeader::read() : Header UNSYNCHRONISATION flag is not supported. Tag extraction might fail.");
        }

        if (header.isFooter())
        {
            Log.w(LOG_TAG, "Id3v2TagHeader::read() : FOOTER flag is not supported. Tag extraction might fail.");
        }

        //
        // read extended header, if exists
        //

        if (header.isExtendedHeader())
        {
            extendedHeader = new Id3v2ExtendedHeader();
            if (!extendedHeader.read())
            {
                return false;
            }
            if (!extendedHeader.check())
            {
                return false;
            }
            if (extendedHeader.moreData != null)
            {
                for (byte b : extendedHeader.moreData)
                {
                    if (logLevel > 0) Log.d(LOG_TAG, "Id3v2TagHeader::read() : extended header value = 0x" + String.format("%02X ", b));
                }
            }
            if (extendedHeader.isCrc())
            {
                if (!extendedHeader.readCrc())
                {
                    return false;
                }
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2TagHeader::read() : CRC = 0x" + String.format("%02X ", extendedHeader.crc));
            }
        }

        //
        // read frames
        //

        while (bytesLeft > 0)
        {
            Id3v2Frame frame = new Id3v2Frame();
            if (!frame.read())
            {
                break;
            }
            if (!frame.check())
            {
                Log.e(LOG_TAG, "Id3v2TagHeader::read() : frame is invalid, fatal error");
                break;
            }
            if (frame.isEmpty())
            {
                if (logLevel > 0) Log.d(LOG_TAG, "Id3v2TagHeader::read() : stopping at first empty frame");
                break;
            }
            if (frame.isUnsynchronisation())
            {
                Log.e(LOG_TAG, "Id3v2TagHeader::read() : frame UNSYNCHRONISATION flag is not supported. Tag extraction might fail.");
            }
            if (frame.isDataLength())
            {
                if (!frame.readDataLength())
                {
                    return false;
                }
            }
            if (frame.isCompressed() || frame.isEncrypted())
            {
                Log.w(LOG_TAG, "Id3v2TagHeader::read() : skipping compressed or encrypted frame.");
                if (!frame.skipPayload())
                {
                    return false;
                }
            }
            else
            {
                if (!frame.readPayload())
                {
                    return false;
                }
                evaluate(frame);
            }
        }

        return true;
    }
}
