/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import static java.lang.System.identityHashCode;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.Toast;

import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

import androidx.documentfile.provider.DocumentFile;
import de.kromke.andreas.utilities.AppGlobals;
import de.kromke.andreas.utilities.AudioTrack;
import de.kromke.andreas.utilities.MediaPlayBtInfo;
import de.kromke.andreas.utilities.SafUtilities;
import de.kromke.andreas.utilities.PrivateLog;


/*
 * A service keeps running while our application is
 * suspended. Handles MediaPlayer and Notifications.
 *
 * Note that activity may be recreated and that even
 * process may be restarted while notifications are
 * being displayed.
 */

@SuppressWarnings("JavadocBlankLines")
public class MediaPlayService extends Service implements
        MyMediaPlayer.OnPreparedListener, MyMediaPlayer.OnErrorListener,
        MyMediaPlayer.OnCompletionListener,
        MediaPlayBtInfo.MediaPlayBtRemoteController
{
    private final static String LOG_TAG = "MediaPlayService";
    public String mUniqueHash;   // for debugging purposes, to check if there are multiple instances

    private enum MediaPlayerState
    {
        STOPPED, PLAYING_PREPARED, PLAYING, PAUSED
    }
    MediaPlayerState mMediaPlayerState = MediaPlayerState.STOPPED;

    // interface (in fact kind a set of callback functions)
    MediaPlayServiceControl mMediaPlayServiceController = null;
    private MyMediaPlayer mMediaPlayer;         // Android media player, extended to catch exceptions
    private MyMediaPlayer mMediaPlayerNext;     // second player for "gapless" playback
    private boolean mbMediaPlayerNextPrepared;
    private int mCurrTrackArrayNumber = -1;   // index for PlayListHandler
    private int mNextTrackArrayNumber = -1;
    private boolean mStartPending;          // start of playback is pending, tell activity that it's playing
    private Drawable mAlbumImageDrawable;  // may be null
    //binder
    private final IBinder mMediaPlayerBind = new MediaPlayServiceBinder();
    //title of current audio file
    private String mAudioInformationFirstLine = "";
    private String mAudioInformationSecondLine = "";
    private String mAudioInformationThirdLine = "";
    // BT sink management and remote control, needs Android 6 and higher (API 23)
    MediaPlayBtInfo mBtInfo = null;
    MediaPlayNotification mNotification = null;

    //timer for play position updates
    private Timer mTimer = null;

    public final static String SERVICE_START_ACTION = "de.kromke.andreas.opus1musicplayer.action.start_service";

    /*
    * callback mechanism to activity
     */
    public interface MediaPlayServiceControl
    {
        void onNotificationPlayPauseChanged(int newTrackArrayNo);
        void onNotificationTrackChanged(int prevTrackArrayNo, long prevPosition, int newTrackArrayNo);    // prev, next and stop
    }


    /* #######################
     * ### service methods ###
     * #######################
     */

    /**************************************************************************
     *
     * Service function
     *
     * run by the system if someone calls Context.startService()
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    public void onCreate()
    {
        mUniqueHash = String.format("0x%08x", identityHashCode(this));
        PrivateLog.d(LOG_TAG, "onCreate(" + mUniqueHash + ")");

        //create the service
        super.onCreate();
        //create mMediaPlayer
        mMediaPlayer = createMediaPlayer();
        mMediaPlayerNext = createMediaPlayer();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            mBtInfo = new MediaPlayBtInfo(this, this /* context */);
        }

        mNotification = new MediaPlayNotification(this, MainActivity.class);

        PrivateLog.d(LOG_TAG, "onCreate() - done");
    }


    /**************************************************************************
     *
     * helper function for play/pause
     *
     *************************************************************************/
    private void onPlayPause(boolean isplay)
    {
        PrivateLog.f_enter(LOG_TAG, "onPlayPause(" + isplay + ")");
        if (isplay)
        {
            continuePlayer();
        }
        else
        {
            pausePlayer();
        }

        if (mMediaPlayServiceController != null)
        {
            // just tell activity, if listening
            mMediaPlayServiceController.onNotificationPlayPauseChanged(mCurrTrackArrayNumber);
        }
        else
        {
            PrivateLog.w(LOG_TAG, "onPlayPause() : service controller lost");
        }
        PrivateLog.f_leave(LOG_TAG, "onPlayPause()");
    }


    /**************************************************************************
     *
     * helper function for seek
     *
     *************************************************************************/
    private void onSeek(long position)
    {
        PrivateLog.d(LOG_TAG, "onSeek()");
        seek((int) position);
        // TODO: notification to activity?
    }


    /**************************************************************************
     *
     * helper function for stop
     *
     *************************************************************************/
    private void onStop()
    {
        PrivateLog.f_enter(LOG_TAG, "onStop()");

        // bookmark handling
        int prevTrackArrayNumber = PlayListHandler.getCurrEntryNo();
        int prevPosition = getPosition();

        stopPlayer();
        mCurrTrackArrayNumber = mNextTrackArrayNumber = -1;
        PlayListHandler.setCurrEntryNo(-1);

        if (mMediaPlayServiceController != null)
        {
            mMediaPlayServiceController.onNotificationTrackChanged(prevTrackArrayNumber, prevPosition, mCurrTrackArrayNumber);
        }
        else
        {
            PrivateLog.w(LOG_TAG, "onStop() : service controller lost");
        }
        PrivateLog.f_leave(LOG_TAG, "onStop()");
    }


    /**************************************************************************
     *
     * helper function for next/prev/advance
     *
     * delta = -1: prev
     * delta = 1: next and advance (automatic next)
     *
     *************************************************************************/
    private void onChgTrack(int delta)
    {
        PrivateLog.f_enter(LOG_TAG, "onChgTrack(" + delta + ")");

        // bookmark handling
        int prevTrackArrayNumber = PlayListHandler.getCurrEntryNo();

        PlayListHandler.gotoTrackRel(delta, false);
        int nextTrackNo = PlayListHandler.getCurrEntryNo();
        if ((mCurrTrackArrayNumber >= 0) && (nextTrackNo < 0))
        {
            stopPlayer();
        }
        if (nextTrackNo >= 0)
        {
            playAudioTrack(0);
        }

        if (mMediaPlayServiceController != null)
        {
            mMediaPlayServiceController.onNotificationTrackChanged(prevTrackArrayNumber, 0, mCurrTrackArrayNumber);
        }
        else
        {
            PrivateLog.w(LOG_TAG, "onChgTrack() : service controller lost");
        }
        PrivateLog.f_leave(LOG_TAG, "onChgTrack()");
    }


    /**************************************************************************
     *
     * helper function to react on an "action"
     *
     *************************************************************************/
    private void handleAction(final String theAction)
    {
        PrivateLog.f_enter(LOG_TAG, "handleAction(" + theAction + ")");

        switch (theAction)
        {
            case MediaPlayNotification.NOTIFICATION_PREV_ACTION:
                PrivateLog.i(LOG_TAG, "Clicked Previous");
                onChgTrack(-1);
                break;

            case MediaPlayNotification.NOTIFICATION_PLAY_ACTION:
                PrivateLog.i(LOG_TAG, "Clicked Play");
                onPlayPause(!isPlaying());
                break;

            case MediaPlayNotification.NOTIFICATION_NEXT_ACTION:
                PrivateLog.i(LOG_TAG, "Clicked Next");
                onChgTrack(1);
                break;

            case MediaPlayNotification.NOTIFICATION_STOP_ACTION:
                PrivateLog.i(LOG_TAG, "Clicked Stop");
                onStop();
                break;

            default:
                PrivateLog.i(LOG_TAG, "Unknown action");
                break;
        }
        PrivateLog.f_leave(LOG_TAG, "handleAction()");
    }


    /**************************************************************************
     *
     * Service function:
     *
     * Called by the system every time a client explicitly starts the service
     * by calling startService(Intent), providing the arguments it supplied
     * and a unique integer token representing the start request. Do not
     * call this method directly.
     *
     * This is also called whenever a button of a notification is activated,
     * e.g. the Play/Pause button.
     *
     *************************************************************************/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        PrivateLog.f_enter(LOG_TAG, "onStartCommand(startId = " + startId + ")");

        /*
         * handle error cases like null intent or illegal action
         */

        if (intent == null)
        {
            PrivateLog.w(LOG_TAG, "onStartCommand(startId = " + startId + ", intent is null");
            PrivateLog.f_leave(LOG_TAG, "onStartCommand()");
            return Service.START_STICKY;
        }

        PrivateLog.d(LOG_TAG, "onStartCommand(startId = " + startId + ": " + intent + ")");
        int ret = super.onStartCommand(intent, flags, startId);
        if (BuildConfig.DEBUG && (ret != Service.START_STICKY)) throw new AssertionError();

        // check if it is the start message
        final String theAction = intent.getAction();
        if (theAction == null)
        {
            PrivateLog.w(LOG_TAG, "onStartCommand() : action is null");
            PrivateLog.f_leave(LOG_TAG, "onStartCommand()");
            return ret;
        }

        PrivateLog.d(LOG_TAG, "onStartCommand() : action is " + theAction);
        if (theAction.equals(SERVICE_START_ACTION))
        {
            PrivateLog.i(LOG_TAG, "onStartCommand() : Start Service");
            PrivateLog.f_leave(LOG_TAG, "onStartCommand()");
            return ret;
        }

        /*
         * repair user settings, if our process had been restarted in the meantime
         */

        UserSettings.init(getApplicationContext());

        /*
         * get extra information to re-initialise the track list
         * if necessary
         */

        int currTrackNo = PlayListHandler.getCurrEntryNo();
        AudioTrack theTrack = PlayListHandler.getAudioTrack(currTrackNo);

        long currTrackId = -1;
        if (theTrack != null)
        {
            currTrackId = theTrack.id;
        }
        else
        {
            currTrackNo = -1;   // error
        }

        if (currTrackNo < 0)
        {
            PrivateLog.e(LOG_TAG, "onStartCommand() : curr track is invalid?!?");
        }

        if (intent.hasExtra(MediaPlayNotification.NOTIFICATION_TRACK_ARRAY_NO))
        {
            int trackArrayNo = intent.getIntExtra(MediaPlayNotification.NOTIFICATION_TRACK_ARRAY_NO, -1);
            PrivateLog.i(LOG_TAG, "onStartCommand() : MediaPlayNotification.NOTIFICATION_TRACK_ARRAY_NO is " + trackArrayNo);
            if (trackArrayNo != currTrackNo)
            {
                PrivateLog.e(LOG_TAG, "onStartCommand() : track array no changed from " + currTrackNo + " to " + trackArrayNo);
                currTrackNo = -1;   // stop due to error
            }
        }
        if (intent.hasExtra(MediaPlayNotification.NOTIFICATION_TRACK_ID))
        {
            long trackId = intent.getLongExtra(MediaPlayNotification.NOTIFICATION_TRACK_ID, -1);
            PrivateLog.i(LOG_TAG, "onStartCommand() : MediaPlayNotification.NOTIFICATION_TRACK_ID is " + trackId);
            if (trackId != currTrackId)
            {
                PrivateLog.e(LOG_TAG, "onStartCommand() : track id changed from " + currTrackId + " to " + trackId);
                currTrackNo = -1;   // stop due to error
            }
        }

        if (currTrackNo < 0)
        {
            PrivateLog.e(LOG_TAG, "onStartCommand() : stop due to error state");
            onStop();
        }
        else
        {
            handleAction(theAction);
        }

        PrivateLog.f_leave(LOG_TAG, "onStartCommand()");
        return ret;
    }


    /**************************************************************************
     *
     * Service function: Pendant to onCreate()
     *
     * Note that this function is not always called, and it is undocumented
     * why and when it's called.
     *
     *************************************************************************/
    @Override
    public void onDestroy()
    {
        PrivateLog.d(LOG_TAG, "onDestroy(" + mUniqueHash + ")");
        TimerStop();

        if (mMediaPlayer.isPlaying())
        {
            mMediaPlayer.stop();
        }
        mMediaPlayer.release();
        if (mMediaPlayerNext.isPlaying())
        {
            mMediaPlayerNext.stop();
        }
        mMediaPlayerNext.release();

        mBtInfo = null;
        mStartPending = false;

        removeNotificationAndStopBt();

        PrivateLog.d(LOG_TAG, "onDestroy() - done");
    }


    /**************************************************************************
     *
     * Service function: will bind to service
     *
     *************************************************************************/
    @Override
    public IBinder onBind(Intent intent)
    {
        PrivateLog.d(LOG_TAG, "onBind(" + mUniqueHash + ")");
        return mMediaPlayerBind;
    }


    /**************************************************************************
     *
     * Service function: release resources
     *
     *************************************************************************/
    @Override
    public boolean onUnbind(Intent intent)
    {
        PrivateLog.d(LOG_TAG, "onUnbind(" + mUniqueHash + ")");
        mMediaPlayServiceController = null;
        return false;
    }


    /**************************************************************************
     *
     * called once from onCreate()
     *
     *************************************************************************/
    public MyMediaPlayer createMediaPlayer()
    {
        MyMediaPlayer theMediaPlayer = new MyMediaPlayer();
        //set mMediaPlayer properties
        theMediaPlayer.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        theMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //set listeners
        theMediaPlayer.setOnPreparedListener(this);
        theMediaPlayer.setOnCompletionListener(this);
        theMediaPlayer.setOnErrorListener(this);
        if (theMediaPlayer.isPlaying())
        {
            mMediaPlayerState = MediaPlayerState.PLAYING;
        }

        mStartPending = false;

        return theMediaPlayer;
    }


    /* ###################################################
     * ### methods called from activity, i.e. commands ###
     * ###################################################
     */


    //binder
    public class MediaPlayServiceBinder extends Binder
    {
        MediaPlayService getService()
        {
            return MediaPlayService.this;
        }
    }


    /**************************************************************************
     *
     * set Bluetooth information (supported with Android 5 and later)
     *
     *************************************************************************/
    private void setBtInfo(AudioTrack currAudioTrack)
    {
        String btAlbum = currAudioTrack.album;
        String btTitle = currAudioTrack.name;
        String btInterpreter = currAudioTrack.getPerformerName();

        String composer = currAudioTrack.getComposerName(false);
        if (!composer.isEmpty())
        {
            composer += ": ";
        }
        String grouping = currAudioTrack.grouping;
        String composer_and_work = composer + ((grouping != null) ? grouping : btTitle);

        int btMetadataStyle = UserSettings.getVal(UserSettings.PREF_BLUETOOTH_METADATA_STYLE, 0);
        switch (btMetadataStyle)
        {
            case 1:
                // Berlingo 2014: no album, just title and interpreter

                if (grouping == null)
                {
                    // no movements: just precede title with composer

                    btTitle = composer + btTitle;
                }
                else
                {
                    // movements: show work instead of title and movement instead of interpreter

                    btInterpreter = btTitle;
                    btTitle = composer + grouping;
                }
                break;

            case 2:
                // use album for work and composer
                if (!composer.isEmpty() || (grouping != null))
                {
                    // composer/work -> album
                    if (grouping == null)
                    {
                        btAlbum = composer;
                    }
                    else
                    {
                        btAlbum = composer + grouping;
                    }
                }
                break;

            case 0:
            default:
                break;
        }

        PlayListHandler.TrackNumInfo trackNumInfo = PlayListHandler.getTrackNumInfo(PlayListHandler.getCurrEntryNo());
        mBtInfo.Init(
                this, // getApplicationContext(),
                currAudioTrack.id,
                btInterpreter,
                btAlbum,
                composer_and_work,
                btTitle,
                currAudioTrack.getComposerName(true),
                mAlbumImageDrawable,
                currAudioTrack.duration,
                trackNumInfo.trackno,          // 0..n-1
                trackNumInfo.numOfTracks);
    }


    /**************************************************************************
     *
     * data source can be URI for Android database, otherwise use the path
     *
     * Return 0: OK, 1: file not found, -1: other error
     *
     *************************************************************************/
    private int setDataSource(MyMediaPlayer player, AudioTrack currAudioTrack)
    {
        if (AppGlobals.isAndroidDb()&& (currAudioTrack.id >= 0))
        {
            //
            // 1. create URI from Android's track id
            //

            Uri trackUri = ContentUris.withAppendedId(
                    android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    currAudioTrack.id);
            //set the data source
            return player.setDataSourceExWithUri(getApplicationContext(), trackUri);
        }
        else
        if (SafUtilities.isSafPath(currAudioTrack.getPath()))
        {
            //
            // 2a. Not using Android DB, and this is no path, but an Uri
            //

            if (player.setDataSourceWithUriString(this, currAudioTrack.getPath()) != 0)
            {
                return -1;
            }
        }
        else
        {
            //
            // 2a. Not using Android DB, and this is a File path
            //

            return player.setDataSourceExWithPath(currAudioTrack.getPath());
        }

        return 0;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @SuppressWarnings("UnusedReturnValue")
    private boolean prepareNextTrack()
    {
        PrivateLog.f_enter(LOG_TAG, "prepareNextTrack()");
        boolean bAutoRepeat = UserSettings.getBool(UserSettings.PREF_AUTOREPEAT, false);
        mNextTrackArrayNumber = PlayListHandler.getNextTrack(bAutoRepeat);
        AudioTrack nextAudioTrack = (mNextTrackArrayNumber >= 0) ? PlayListHandler.getAudioTrack(mNextTrackArrayNumber) : null;
        mbMediaPlayerNextPrepared = false;
        boolean bResult = false;

        if (nextAudioTrack != null)
        {
            if (setDataSource(mMediaPlayerNext, nextAudioTrack) == 0)
            {
                PrivateLog.d(LOG_TAG, "prepareAsyncEx(mp = " + System.identityHashCode(mMediaPlayerNext) + ", trackarrno = " + mNextTrackArrayNumber + ")");
                bResult = mMediaPlayerNext.prepareAsyncEx(0);
            }
            else
            {
                PrivateLog.e(LOG_TAG, "prepareNextTrack() -- data source of next track is bad");
                mMediaPlayer.setNextMediaPlayerEx(null);
            }
        }
        else
        {
            mMediaPlayer.setNextMediaPlayerEx(null);
        }

        PrivateLog.f_leave(LOG_TAG, "prepareNextTrack()");
        return bResult;
    }


    /**************************************************************************
     *
     * play the current audio track
     *
     * Note that the media player starts asynchronously.
     * Also called from activity
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    public void playAudioTrack(long pos)
    {
        PrivateLog.d(LOG_TAG, "playAudioTrack(pos = " + pos + ")");

        mCurrTrackArrayNumber = PlayListHandler.getCurrEntryNo();
        mStartPending = false;
        TimerStop();

        if (mMediaPlayer == null)
        {
            PrivateLog.e(LOG_TAG, "playAudioTrack() - media player lost");
            mCurrTrackArrayNumber = mNextTrackArrayNumber = -1;
            PlayListHandler.setCurrEntryNo(-1);
            return;
        }

        mMediaPlayer.reset();
        mMediaPlayerNext.reset();
        mMediaPlayerState = MediaPlayerState.STOPPED;

        // only play tracks, no other entries
        if (mCurrTrackArrayNumber < 0)
        {
            PrivateLog.e(LOG_TAG, "playAudioTrack() - invalid track");
            return;
        }
        mCurrTrackArrayNumber = PlayListHandler.gotoTrack(mCurrTrackArrayNumber, 1);     // skip invalid entries
        if (mCurrTrackArrayNumber < 0)
        {
            PrivateLog.e(LOG_TAG, "playAudioTrack() - no valid track found");
            return;
        }

        //get audio track information
        AudioTrack currAudioTrack = PlayListHandler.getAudioTrack(mCurrTrackArrayNumber);
        if (currAudioTrack == null)
        {
            PrivateLog.e(LOG_TAG, "playAudioTrack() - invalid object?!?");
            return;
        }
        int dsRet = setDataSource(mMediaPlayer, currAudioTrack);
        if (dsRet != 0)
        {
            String errtxt = (dsRet > 0) ? getString(R.string.str_toast_cannot_find_file) : getString(R.string.str_toast_file_unknown_error);
            Toast theToast = Toast.makeText(getApplicationContext(), errtxt, Toast.LENGTH_LONG);
            theToast.show();
            return;     // error
        }

        // bookmark handling
        if ((pos == 0) && (currAudioTrack.play_pos_ms > 0))
        {
            // repeat the last seconds when continuing the track
            pos = Math.max(currAudioTrack.play_pos_ms - AppGlobals.bookmarkRepeatMs, 0);
        }

        // Bluetooth stuff
        setNotificationAndBtInfo(currAudioTrack);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            setBtInfo(currAudioTrack);
        }

        TimerStart();

        // start asynchronously, later onPrepared() will be called
        // mMediaPlayerNext will be prepared in callback

        PrivateLog.d(LOG_TAG, "prepare(mp = " + System.identityHashCode(mMediaPlayer) + ", trackArrNo = " + mCurrTrackArrayNumber + ")");
        mStartPending = mMediaPlayer.prepareAsyncEx(pos);
    }


    /**************************************************************************
     *
     * called from activity to register for callback notifications
     *
     *************************************************************************/
    public void setController(MediaPlayServiceControl controller)
    {
        PrivateLog.d(LOG_TAG, "setController()");
        if ((mMediaPlayServiceController != null) && (mMediaPlayServiceController != controller))
        {
            PrivateLog.w(LOG_TAG, "controller exchanged due to Android design flaw (activity replaced)");
        }
        mMediaPlayServiceController = controller;
    }


    /**************************************************************************
     *
     * playback methods, also called from activity
     *
     *************************************************************************/
    public int getPosition()
    {
        int pos = 0;
        if (mMediaPlayer != null)
        {
            pos = mMediaPlayer.getCurrentPosition();
        }
        return pos;
    }

    // also called from activity
    public int getDuration()
    {
        int pos = 0;
        if (mMediaPlayer != null)
        {
            pos = mMediaPlayer.getDuration();
        }
        return pos;
    }

    // return true if media player is running
    // also called from activity
    public boolean isReallyPlaying()
    {
        boolean is = false;
        if (mMediaPlayer != null)
        {
            is = mMediaPlayer.isPlayingEx();
        }
        return is;
    }

    // return true if media player is running or about to run, ie. being started asynchronously
    // also called from activity
    public boolean isPlaying()
    {
        boolean is = false;
        if (mMediaPlayer != null)
        {
            is = mMediaPlayer.isPlayingEx();
            if (!is && mStartPending)
            {
                PrivateLog.d(LOG_TAG, "Start is pending, treat as running");
                is = true;
            }
        }
        //Log.d(LOG_TAG, "isPlaying() -> " + is);
        return is;
    }

    // change from PLAYING state to PAUSE state
    // also called from activity
    public void pausePlayer()
    {
        PrivateLog.f_enter(LOG_TAG, "pausePlayer()");
        if ((mMediaPlayerState == MediaPlayerState.PLAYING) || (mMediaPlayerState == MediaPlayerState.PLAYING_PREPARED))
        {
            mMediaPlayerState = MediaPlayerState.PAUSED;
            TimerStop();
            mStartPending = false;

            // change notification to show PAUSE instead of PLAY icon
            sendNotificationAndBtInfo();

            if (mMediaPlayer != null)
            {
                mMediaPlayer.pause();
            } else
            {
                PrivateLog.e(LOG_TAG, "media player lost?!?");
            }
        }
        else
        {
            PrivateLog.w(LOG_TAG, "wrong player state for pause");
        }
        PrivateLog.f_leave(LOG_TAG, "pausePlayer()");
    }


    // change from PAUSE state to PLAYING state
    // also called from activity
    public void continuePlayer()
    {
        PrivateLog.f_enter(LOG_TAG, "continuePlayer()");
        if (mMediaPlayerState == MediaPlayerState.PAUSED)
        {
            TimerStart();
            boolean bStarted;

            if (mMediaPlayer != null)
            {
                bStarted = mMediaPlayer.startEx();
                if (bStarted)
                {
                    mMediaPlayerState = MediaPlayerState.PLAYING;
                }
            } else
            {
                PrivateLog.e(LOG_TAG, "media player lost?!?");
                bStarted = false;
            }

            if (bStarted)
            {
                // change notification to show PLAY instead of PAUSE icon
                sendNotificationAndBtInfo();
            }
        }
        else
        {
            PrivateLog.w(LOG_TAG, "wrong player state for continue");
        }
        PrivateLog.f_leave(LOG_TAG, "continuePlayer()");
    }


    private void removeNotificationAndStopBt()
    {
        if (mNotification != null)
        {
            mNotification.removeNotification();
        }

        if (mBtInfo != null)
        {
            mBtInfo.stop(getApplicationContext());  // send stop and release
            //mBtInfo = null;     // do not remove, because STOP will not reach car radio
        }
    }


    // stop is technically similar to pause, but we remove the notification and tell STOP to media session
    // also called from activity
    public void stopPlayer()
    {
        PrivateLog.f_enter(LOG_TAG, "stopPlayer()");

        if (mMediaPlayerState != MediaPlayerState.STOPPED)
        {
            mMediaPlayerState = MediaPlayerState.STOPPED;
            TimerStop();
            mStartPending = false;

            removeNotificationAndStopBt();
            if (mMediaPlayer != null)
            {
                mMediaPlayer.pause();
            } else
            {
                PrivateLog.e(LOG_TAG, "media player lost?!?");
            }
        }
        else
        {
            PrivateLog.w(LOG_TAG, "wrong player state for stop");
        }
        PrivateLog.f_leave(LOG_TAG, "stopPlayer()");
    }


    // also called from activity
    public void seek(int posn)
    {
        if ((mMediaPlayerState == MediaPlayerState.PLAYING) || (mMediaPlayerState == MediaPlayerState.PAUSED))
        {
            if (mMediaPlayer != null)
            {
                mMediaPlayer.seekTo(posn);
            }
            else
            {
                PrivateLog.e(LOG_TAG, "media player lost?!?");
            }
        }
        else
        {
            PrivateLog.w(LOG_TAG, "wrong player state for seek");
        }
    }


    /* ###########################################
     * ### callback functions from MediaPlayer ###
     * ###########################################
     */


    /**************************************************************************
     *
     * media player callback for asynchronous start
     *
     *************************************************************************/
    @Override
    public void onPrepared(MyMediaPlayer mp)
    {
        PrivateLog.f_enter(LOG_TAG, "onPrepared(mp = " + System.identityHashCode(mp) + ")");

        if (mp == mMediaPlayer)
        {
            PrivateLog.d(LOG_TAG, "onPrepared() -- this is the current player");
            prepareNextTrack();     // ignore return value here

            mStartPending = false;

            if (mp.startEx())
            {
                mMediaPlayerState = MediaPlayerState.PLAYING;
            }

            sendNotificationAndBtInfo();
        }
        else
        {
            mbMediaPlayerNextPrepared = true;
            mMediaPlayer.setNextMediaPlayerEx(mMediaPlayerNext);
            PrivateLog.d(LOG_TAG, "onPrepared() -- this is the next player");
        }
        PrivateLog.f_leave(LOG_TAG, "onPrepared()");
    }


    /**************************************************************************
     *
     * media player callback when playback has ended normally
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onCompletion(MyMediaPlayer mp)
    {
        PrivateLog.f_enter(LOG_TAG, "onCompletion(mp = " + System.identityHashCode(mp) + ")");
        PrivateLog.d(LOG_TAG, "onCompletion() -- curr track array number is " + mCurrTrackArrayNumber);
        PrivateLog.d(LOG_TAG, "onCompletion() -- next track array number is " + mNextTrackArrayNumber);
        if (mp == mMediaPlayer)
        {
            PrivateLog.d(LOG_TAG, "onCompletion() -- this is the current player");
            int prevTrackArrayNumber =  mCurrTrackArrayNumber;
            mp.reset();
            if (mbMediaPlayerNextPrepared)
            {
                // the next player is now the current one, i.e. we exchange the player objects
                PrivateLog.d(LOG_TAG, "onCompletion() -- exchange player objects");
                MyMediaPlayer temp = mMediaPlayer;
                mMediaPlayer = mMediaPlayerNext;
                mMediaPlayerNext = temp;

                boolean bAutoRepeat = UserSettings.getBool(UserSettings.PREF_AUTOREPEAT, false);
                PlayListHandler.advanceToNextTrack(bAutoRepeat);
                if (mNextTrackArrayNumber != PlayListHandler.getCurrEntryNo())
                {
                    PrivateLog.e(LOG_TAG, "onCompletion() -- real next track "+ mCurrTrackArrayNumber + " differs from planned next track " + mNextTrackArrayNumber);
                }
                mCurrTrackArrayNumber = mNextTrackArrayNumber;      // we already prepared it, so choose this one
                // mMediaPlayerState = MediaPlayerState.PLAYING;    unnecessary, because state should already be PLAYING
                prepareNextTrack();     // ignore return value here
            }
            else
            {
                PrivateLog.d(LOG_TAG, "onCompletion() -- no next player. Stop.");
                mCurrTrackArrayNumber = -1;
                PlayListHandler.setCurrEntryNo(-1);
                stopPlayer();
            }

            if (mMediaPlayServiceController != null)
            {
                mMediaPlayServiceController.onNotificationTrackChanged(prevTrackArrayNumber, 0, mCurrTrackArrayNumber);
            }

            AudioTrack currAudioTrack = (mCurrTrackArrayNumber >= 0) ? PlayListHandler.getAudioTrack(mCurrTrackArrayNumber) : null;
            if (currAudioTrack != null)
            {
                setNotificationAndBtInfo(currAudioTrack);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    setBtInfo(currAudioTrack);
                }
                sendNotificationAndBtInfo();
            }
        }
        else
        {
            PrivateLog.e(LOG_TAG, "onCompletion() -- this is the next player");
        }
        PrivateLog.f_leave(LOG_TAG, "onCompletion(mp = " + System.identityHashCode(mp) + ")");
    }


    /**************************************************************************
     *
     * media player callback when playback has been ended with error
     *
     *************************************************************************/
    @Override
    public boolean onError(MyMediaPlayer mp, int what, int extra)
    {
        PrivateLog.v(LOG_TAG, "onError(mp = " + System.identityHashCode(mp) + " what = " + what + ", extra = " + extra + ")");
        if (what == -38)
        {
            PrivateLog.v(LOG_TAG, "onError() -- wrong state is ignored (hack!)");
            return true;
        }
        else
        {
            mp.reset();
            mMediaPlayerState = MediaPlayerState.STOPPED;
            return false;
        }
    }


    /* ###############################################
     * ### callback functions from MediaPlayBtInfo ###
     * ###############################################
     */

    public void onMediaControlPlayState(boolean isplay)
    {
        PrivateLog.d(LOG_TAG, "media control play state " + isplay);
        if (isplay && (mMediaPlayerState == MediaPlayerState.STOPPED))
        {
            // hack to start from track #0
            PlayListHandler.setCurrEntryNo(0);
            playAudioTrack(0);
            int currTrackNo = PlayListHandler.getCurrEntryNo();
            if ((currTrackNo >= 0) && (mMediaPlayServiceController != null))
            {
                // TODO: what is the previous track number?
                mMediaPlayServiceController.onNotificationTrackChanged(-1, 0, currTrackNo);
            }
            else
            {
                PrivateLog.w(LOG_TAG, "onChgTrack() : service controller lost");
            }
        }
        else
        {
            onPlayPause(isplay);
        }
    }

    public void onMediaControlSkip(int direction)
    {
        PrivateLog.d(LOG_TAG, "media control skip " + direction);
        onChgTrack(direction);
    }

    public void onMediaControlSeek(long position)
    {
        PrivateLog.d(LOG_TAG, "media control seek " + position);
        onSeek(position);
    }

    public void onMediaControlStop()
    {
        PrivateLog.d(LOG_TAG, "media control stop");
        if (UserSettings.getBool(UserSettings.PREF_MEDIABUTTON_STOP_DOES_PAUSE, false))
        {
            onPlayPause(false);
        }
        else
        {
            onStop();
        }
    }

    /**************************************************************************
     *
     * timer for position updates
     *
     *************************************************************************/
    private class PositionTimerTask extends TimerTask
    {
        public void run()
        {
            if (mBtInfo != null)
            {
                if (mMediaPlayerState == MediaPlayerState.STOPPED)
                {
                    mBtInfo.sendPlayState(getApplicationContext(), false, 0, 0);
                }
                else
                {
                    //Log.v(LOG_TAG, "PositionTimerTask() : position = " + getPosition());
                    mBtInfo.sendPlayState(getApplicationContext(), isPlaying(), getDuration(), getPosition());
                }
            }
        }
    }

    private void TimerStart()
    {
        // delete old timer, if any
        TimerStop();
        //create new timer
        mTimer = new Timer();
        // start position timer
        mTimer.schedule(new PositionTimerTask(), 1000 /* 1s delay */, 1000 /* every 1s */);
        // was: mTimer.scheduleAtFixedRate(new PositionTimerTask(), 1000 /* 1s delay */, 1000 /* every 1s */);
    }

    private void TimerStop()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
        }
    }


    /**************************************************************************
     *
     * helper used in onPrepared() and onCompletion
     *
     *************************************************************************/
    private void sendNotificationAndBtInfo()
    {
        boolean bIsPlay = (mMediaPlayerState == MediaPlayerState.PLAYING);
        if (mBtInfo != null)
        {
            mBtInfo.sendPlayState(getApplicationContext(), bIsPlay /* play */, getDuration(), getPosition());
            setNotification(bIsPlay /* play */, mBtInfo.getMediaSession());
        } else
        {
            setNotification(bIsPlay /* play */, null);
        }
    }


    /************************************************************************************
     *
     * create BitmapDrawable from SAF Uri
     *
     ***********************************************************************************/
    private static Drawable createDrawableFromSafPath
    (
        Context context,
        final String path
    )
    {
        BitmapDrawable drawable = null;

        DocumentFile df = SafUtilities.getDocumentFileFromUriString(context, path);
        if (df != null)
        {
            InputStream is;
            try
            {
                is = context.getContentResolver().openInputStream(df.getUri());
                drawable = new BitmapDrawable(context.getResources(), is);
            } catch (Exception e)
            {
                PrivateLog.e(LOG_TAG, "createDrawableFromSafPath() : image file not found: " + path);
            }
        }

        return drawable;
    }


    /**************************************************************************
     *
     * set the information lines for the notification
     *  and the album art for both notification and bt info
     *
     *************************************************************************/
    private void setNotificationAndBtInfo(AudioTrack currAudioTrack)
    {
        // set information lines
        mAudioInformationFirstLine = currAudioTrack.getComposerName(true) + ":";

        if (currAudioTrack.grouping == null)
        {
            mAudioInformationSecondLine = currAudioTrack.name;
            mAudioInformationThirdLine = "";
        }
        else
        {
            mAudioInformationSecondLine = currAudioTrack.grouping;
            mAudioInformationThirdLine = currAudioTrack.name;
        }

        // get album art
        final String albumPicturePath = currAudioTrack.getAlbumPicturePath(this);
        if (albumPicturePath != null)
        {
            if (SafUtilities.isSafPath(albumPicturePath))
            {
                mAlbumImageDrawable = createDrawableFromSafPath(this, albumPicturePath);
            }
            else
            {
                mAlbumImageDrawable = Drawable.createFromPath(albumPicturePath);
            }
        }
        else
        {
            mAlbumImageDrawable = null;
        }
    }


    /**************************************************************************
     *
     * set notification or update current one, if any
     *
     * Old implementation used
     *    startForeground(NOTIFY_ID, theNotification, FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK);
     * and
     *    stopForeground()
     *
     * This does no longer work with Android 12 and newer when existing notifications must
     * be replaced due to track change when playing in background. Instead, we
     * now call
     *    mNotificationManager.notify(MY_NOTIFICATION_ID, theNotification);
     * and
     *    mNotificationManager.cancel(MY_NOTIFICATION_ID);
     * which basically seem to do the same.
     *
     *************************************************************************/
    private void setNotification(boolean isPlay, MediaSession mediaSession)
    {
        AudioTrack currAudioTrack = PlayListHandler.getAudioTrack(mCurrTrackArrayNumber);
        if (currAudioTrack != null)
        {
            mNotification.createAndShowNotification(
                    isPlay,
                    mAudioInformationFirstLine,
                    mAudioInformationSecondLine,
                    mAudioInformationThirdLine,
                    mCurrTrackArrayNumber,
                    currAudioTrack.id,
                    mAlbumImageDrawable,
                    mediaSession,
                    this);
        }
    }

}
