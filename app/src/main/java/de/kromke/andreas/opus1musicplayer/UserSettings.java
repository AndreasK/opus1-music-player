/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import androidx.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * helper class for user settings (preferences)
 */
@SuppressWarnings("WeakerAccess")
public class UserSettings
{
    private static final String LOG_TAG = "UserSettings";

    public static final String PREF_AUTOHIDE_CONTROLLER = "prefAutoHideController";
    public static final String PREF_AUTOPLAY_MODE = "prefAutoplayMode";
    public static final String PREF_CHOOSE_OTHER_TRACK = "prefChooseOtherTrack";
    public static final String PREF_SIZE_OF_ALBUM_HEADER = "prefSizeOfAlbumHeader";
    public static final String PREF_SIZE_OF_LIST_PICTURES = "prefSizeOfPicturesInLists";
    public static final String PREF_SIZE_OF_GRID_PICTURES = "prefSizeOfPicturesInGrid";
    public static final String PREF_SHOW_TRACK_NUMBER = "prefShowTracknumber";
    public static final String PREF_SHOW_ALBUM_AND_OPUS_DURATION = "prefShowAlbumAndWorkDuration";
    public static final String PREF_COMPOSER_SORT_BY_LAST_NAME = "prefComposerSortByLastName";
    public static final String PREF_SHOW_COMPOSER_LAST_NAME_FIRST = "prefShowComposerLastNameFirst";
    public static final String PREF_HANDLE_PERSON_LISTS = "prefHandlePersonLists";
    // public static final String PREF_USE_JAUDIOTAGGER_LIB = "prefUseJaudiotaggerLib"; /* jaudiotagger removed */
    public static final String PREF_SHOW_CONDUCTOR = "prefShowConductor";
    public static final String PREF_SHOW_SUBTITLE = "prefShowSubtitle";
    public static final String PREF_GENDERISM_NONSENSE = "prefGenderismNonsense";
    public static final String PREF_THEME = "prefTheme";
    public static final String PREF_ROTATION = "prefRotation";
    public static final String PREF_REBUILD_DB = "prefRebuildDb";
    public static final String PREF_BRANCH_MODE = "prefBranchMode";
    public static final String PREF_CASE_SENSITIVE_SEARCH = "prefCaseSensitiveSearch";
    public static final String PREF_MERGE_ALBUMS_OF_SAME_NAME = "prefMergeAlbumsOfSameName";
    public static final String PREF_NOTIFICATION_STYLE = "prefNotificationStyle";
    public static final String PREF_SHOW_GREETINGS = "prefShowGreetings";
    public static final String PREF_INSTALLED_APP_VERSION = "prefInstalledAppVersion";
    public static final String PREF_BLUETOOTH_METADATA_STYLE = "prefBluetoothMetadataStyle";
    public static final String PREF_MEDIABUTTON_STOP_DOES_PAUSE = "prefMediaButtonStopDoesPause";
    public static final String PREF_AUTOREPEAT = "prefAutoRepeat";
    public static final String PREF_GRIDVIEW = "prefGridView";
    public static final String PREF_USE_OWN_DATABASE = "prefUseOwnDatabase";
    public static final String PREF_OWN_DATABASE_PATH = "prefOwnDatabasePath";
    public static final String PREF_WHITE_BLACK_LIST_PATHS = "prefWhiteBlackListPaths";
    public static final String PREF_OWN_PERSONS_PATH = "prefOwnPersonsPath";
    public static final String PREF_WORK_PICTURE_MODE = "prefWorkPictureMode";
    public static final String PREF_LATEST_SAF_URI = "prefLatestSafUri";
    public static final String PREF_LATEST_SAF_DB_PATH = "prefLatestSafDb";
    public static final String PREF_REMEMBER_LATEST_SAF_DB_PATH = "prefRememberLatestSafDb";
    public static final String PREF_ALWAYS_GET_PATHS_FOR_ANDROID_DB = "prefAlwaysGetPathsFromAndroidDb";
    public static final String PREF_DEBUG_ALWAYS_RECREATE_DB = "prefDebugAlwaysRecreateDb";

    private static SharedPreferences mSharedPrefs;

    @SuppressWarnings("WeakerAccess")
    public static class AppVersionInfo
    {
        String versionName = "";
        int versionCode = 0;
        String strCreationTime = "";
        boolean isDebug;
    }

    @SuppressWarnings("WeakerAccess")
    public static AppVersionInfo getVersionInfo(Context context)
    {
        AppVersionInfo ret = new AppVersionInfo();
        PackageInfo packageinfo = null;

        try
        {
            packageinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.d(LOG_TAG, "exception: " + e.getMessage());
        }

        if (packageinfo != null)
        {
            ret.versionName = packageinfo.versionName;
            ret.versionCode = packageinfo.versionCode;
        }

        // get ISO8601 date instead of dumb US format (Z = time zone) ...
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        ret.strCreationTime = df.format(buildDate);
        ret.isDebug = BuildConfig.DEBUG;

        return ret;
    }


    /*
     * Initialises the module, if necessary. Note that re-initialisation may be necessary
     * if process was restarted.
     */
    public static void init(Context context)
    {
        if (mSharedPrefs == null)
        {
            mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    // more or less a hack
    public static void setContextIfNecessary(Context context)
    {
        if (mSharedPrefs == null)
        {
            Log.w(LOG_TAG, "setContextIfNecessary() : was null");
            mSharedPrefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context);
        }
    }

    // must be called before restarting the app, otherwise settings get lost
    @SuppressLint("ApplySharedPref")
    static void commit()
    {
        if (mSharedPrefs != null)
        {
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.commit();
        }
    }

    /*
     * remove, but not commit
     */
    public static void removeVal(final String key)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.remove(key);
        prefEditor.apply();
    }

    /*
    * updates a preference value and returns previous value or defVal, if none
     */
    @SuppressWarnings("SameParameterValue")
    static int updateVal(final String key, int newVal, int defVal)
    {
        int prevVal = defVal;

        if (mSharedPrefs.contains(key))
        {
            prevVal = getVal(key, defVal);
        }

        if (prevVal != newVal)
        {
            String vds = Integer.toString(newVal);
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.apply();
        }

        return prevVal;
    }

    /*
    * get a numerical value from the preferences and repair it, if necessary
     */
    public static int getVal(final String key, int defaultVal)
    {
        String vds = Integer.toString(defaultVal);
        String vs = mSharedPrefs.getString(key, vds);
        boolean bWriteBack = true;      // being pessimistic
        int v;

        try
        {
            v = Integer.parseInt(vs);
            bWriteBack = false;             // success
        } catch (NumberFormatException e)
        {
            v = defaultVal;
        }

        if (bWriteBack)
        {
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.apply();
        }

        return v;
    }

    /*
    * put and commit
     */
    public static void putVal(final String key, int theVal)
    {
        String vds = Integer.toString(theVal);
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putString(key, vds);
        prefEditor.apply();
        //prefEditor.commit();    // make sure datum is written to flash now
    }


    /*
    * put and commit
     */
    public static void putVal(final String key, boolean theVal)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putBoolean(key, theVal);
        prefEditor.apply();
        //prefEditor.commit();    // make sure datum is written to flash now
    }


    /*
    * same as getVal(), but save it in case it does not exist, yet
     */
    public static int getAndPutVal(final String key, int defaultVal)
    {
        String vds = Integer.toString(defaultVal);
        boolean bWriteBack = !mSharedPrefs.contains(key);   // not yet saved => writeIt = true
        String vs = mSharedPrefs.getString(key, vds);
        int v;

        try
        {
            v = Integer.parseInt(vs);
        } catch (NumberFormatException e)
        {
            v = defaultVal;
            bWriteBack = true;
        }

        // write it in case it has not been written or in case it's malformed
        if (bWriteBack)
        {
            SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
            prefEditor.putString(key, vds);
            prefEditor.apply();
        }

        return v;
    }

    /*
     * get a text value from the preferences
     */
    public static String getString(final String key)
    {
        return mSharedPrefs.getString(key, null);
    }

    /*
     * put a text value to the preferences
     */
    @SuppressWarnings("WeakerAccess")
    public static void putString(final String key, String value)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putString(key, value);
        prefEditor.apply();
    }

    /*
     * put and commit. Necessary before a restart(), otherwise values will not be written.
     */
    static void putStringAndCommit(@SuppressWarnings("SameParameterValue") final String key, String value)
    {
        SharedPreferences.Editor prefEditor = mSharedPrefs.edit();
        prefEditor.putString(key, value);
        prefEditor.apply();
        prefEditor.commit();    // make sure datum is written to flash now
    }

    /*
     * get a text value from the preferences and repair it, if necessary
     */
    @SuppressWarnings("WeakerAccess")
    public static String getAndPutString(final String key, String defaultVal)
    {
        if (!mSharedPrefs.contains(key))
        {
            // not set, yet: write it
            putString(key, defaultVal);
            return defaultVal;
        }
        return mSharedPrefs.getString(key, defaultVal);
    }

    /*
    * get a boolean value from the preferences and repair it, if necessary
     */
    public static boolean getBool(final String key, boolean defaultVal)
    {
        return mSharedPrefs.getBoolean(key, defaultVal);
    }

    /*
    * get a themed colour value
     */
    public static int getThemedColourFromResId(Context context, int id)
    {
        // strange stuff colour is a reference:
        // http://stackoverflow.com/questions/17277618/get-color-value-programmatically-when-its-a-reference-theme
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(id, typedValue, true);
        return typedValue.data;
    }

    /*
    * get number of pixels for given millimeters
     */
    private static int pixelsForMillimeters(Context context, int mm)
    {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mm,
                context.getResources().getDisplayMetrics());
        return (int) px;
    }

    // get number of pixels or millimeters
    public static int pixelsOrMillimeters(Context context, int pixels, int mm)
    {
        int mpixels = pixelsForMillimeters(context, mm);
        return Math.max(pixels, mpixels);
    }
}
