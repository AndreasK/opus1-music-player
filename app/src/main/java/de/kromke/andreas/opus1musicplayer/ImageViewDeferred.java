/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Size;

import java.io.IOException;
import java.io.InputStream;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.documentfile.provider.DocumentFile;
import de.kromke.andreas.utilities.AudioBaseObject;
import de.kromke.andreas.utilities.BitmapLruCache;
import de.kromke.andreas.utilities.BitmapUtils;
import de.kromke.andreas.utilities.SafUtilities;

/** @noinspection CommentedOutCode*/ // ImageView for deferred image loading in background
@SuppressWarnings("JavadocBlankLines")
public class ImageViewDeferred
        extends androidx.appcompat.widget.AppCompatImageView
{
    private final static String LOG_TAG = "O1M : ImageViewDeferred";

    //
    // class attributes
    //

    private static final boolean sbDeferred = true;     // debug helper

    private static final int MSG_CREATE_BITMAP = 11;    // UI thread -> worker thread
    private static final int MSG_DRAW_IMAGE = 12;       // worker thread -> UI thread

    private static final WorkerHandlerCallback theWorkerHandlerCallback = new WorkerHandlerCallback();
    private static final UiHandlerCallback theUiHandlerCallback = new UiHandlerCallback();
    private static Handler sUiHandler;                  // UI Thread handler
    private static Handler sWorkerThreadHandler;        // worker thread handler
    private static int sImageSize = 100;                // dummy, will be overwritten on first call

    //
    // object attributes
    //

    private final Context mContext;         // context of constructor
    private int mPosition;                  // corresponding database key

    private static class CreateBitmapMsg
    {
        public Context context;
        public int key;                     // LRU key
        public String path;                 // picture path or Uri
        public final Bitmap bmFolderMask;   // is null for non-folders
        public ImageViewDeferred view;      // associated view
        public final AudioBaseObject audioObject;
        public final BitmapLruCache bitmapCache;
        CreateBitmapMsg(
                Context context,
                int position,
                final String path,
                final Bitmap bmFolderMask,
                ImageViewDeferred view,
                AudioBaseObject theAudioObject,
                BitmapLruCache bitmapCache)
        {
            this.context = context;
            this.key = position;
            this.view = view;
            this.path = path;
            this.bmFolderMask = bmFolderMask;
            this.audioObject = theAudioObject;
            this.bitmapCache = bitmapCache;
        }
    }

    // callback for messages from UI thread to worker thread
    static private class WorkerHandlerCallback implements Handler.Callback
    {
        @Override
        public boolean handleMessage(Message message)
        {
            if (message.what == MSG_CREATE_BITMAP)
            {
                CreateBitmapMsg payload = (CreateBitmapMsg) message.obj;
                if (payload.view.mPosition != payload.key)
                {
                    Log.d(LOG_TAG, "handleMessage() : outdated message to BG discarded");
                    return true;
                }

                //Log.d(LOG_TAG, "handleMessage(MSG_CREATE_COVER) -- for position " + payload.key);
                Bitmap bitmap = null;
                BitmapLruCache bitmapCache = payload.bitmapCache;
                if (bitmapCache.containsKey(payload.key))
                {
                    // note that bitmap maybe null for invalid album art files
                    bitmap = bitmapCache.get(payload.key);
                    //Log.d(LOG_TAG, "handleMessage() : got bitmap from cache");
                }
                if (bitmap == null)
                {
                    bitmap = createBitmap(payload.context, payload.path, payload.bmFolderMask, sImageSize);
                }
                if (bitmap != null)
                {
                    bitmapCache.put(payload.key, bitmap);
                    // send message to draw image in UI thread, just pass the same payload
                    sUiHandler.sendMessage(sUiHandler.obtainMessage(MSG_DRAW_IMAGE, payload));
                }
                return true;
            }
            return false;
        }
    }

    // callback for messages from worker thread to UI thread
    static private class UiHandlerCallback implements Handler.Callback
    {
        @Override
        public boolean handleMessage(Message message)
        {
            if (message.what == MSG_DRAW_IMAGE)
            {
                CreateBitmapMsg payload = (CreateBitmapMsg) message.obj;
                if (payload.view.mPosition != payload.key)
                {
                    Log.d(LOG_TAG, "handleMessage() : outdated message to UI discarded");
                    return true;
                }

                //Log.d(LOG_TAG, "handleMessage(MSG_DRAW_IMAGE) -- for position " + payload.key);
                BitmapLruCache bitmapCache = payload.bitmapCache;
                Bitmap bitmap = bitmapCache.get(payload.key);
                if (bitmap != null)
                {
                    drawBitmap(payload.view, bitmap, payload.bmFolderMask, null);
                }
                else
                {
                    Log.w(LOG_TAG, "handleMessage(MSG_DRAW_IMAGE) -- invalid path: " + payload.path);
                    payload.audioObject.setPicturePathInvalid();
                }
                return true;
            }
            return false;
        }
    }


    /************************************************************************************
     *
     * constructor
     *
     ***********************************************************************************/
    public ImageViewDeferred(Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        mContext = context;
        mPosition = -1;         // no image key associated, yet

        // This call is supposed to register a callback run in the UI thread.
        if (sUiHandler == null)
        {
            sUiHandler = new Handler(theUiHandlerCallback);
        }

        // Note that the worker thread is static, i.e. exists only once for the class,
        // and so is the callback.
        if (sWorkerThreadHandler == null)
        {
            HandlerThread thread = new HandlerThread("DeferredDrawImageThread");
            thread.start();
            sWorkerThreadHandler = new Handler(thread.getLooper(), theWorkerHandlerCallback);
        }
    }


    /************************************************************************************
     *
     * pass picture file path (called in UI thread)
     *
     * Note that theAudioObject is needed to mark the image as invalid in case
     * of deferred failure.
     *
     ***********************************************************************************/
    public void setPicturePath
    (
        AudioBaseObject theAudioObject,
        int position,
        final String path,
        final int imageSize,
        Bitmap bmFolderMask,
        final Drawable defaultDrawable,
        BitmapLruCache bitmapCache
    )
    {
        sImageSize = imageSize;     // may be updated here

        // detect reuse
        /*
        //Log.w(LOG_TAG, "setPicturePath(" + position + ", \"" + path + "\")");
        if ((mPosition >= 0) && (mPosition != position))
        {
            Log.w(LOG_TAG, "setPicturePath() : object reused, position was " + mPosition + ", now " + position);
        }
        */
        mPosition = position;

        Bitmap bitmap = null;
        if (path != null)
        {
            //Log.d(LOG_TAG, "getView() : art path \"" + theAlbum.album_picture_path + "\"");
            // cache mechanism
            if (bitmapCache.containsKey(position))
            {
                // cache hit: re-use
                // note that bitmap maybe null for invalid album art files
                bitmap = bitmapCache.get(position);
                //Log.d(LOG_TAG, "setPicturePath() : got bitmap from cache");
            }
            else
            if (sbDeferred)
            {
                // create image in background thread

                //Log.d(LOG_TAG, "setPicturePath() : send request for position " + position + " to worker thread");
                CreateBitmapMsg payload = new CreateBitmapMsg(mContext, position, path, bmFolderMask, this, theAudioObject, bitmapCache);
                sWorkerThreadHandler.sendMessage(sWorkerThreadHandler.obtainMessage(MSG_CREATE_BITMAP, payload));
            }
            else
            {
                bitmap = createBitmap(mContext, path, bmFolderMask, imageSize);
                if (bitmap != null)
                {
                    bitmapCache.put(position, bitmap);
                }
                else
                {
                    Log.d(LOG_TAG, "getView() : no picture for object " + theAudioObject.getName());
                    theAudioObject.setPicturePathInvalid();
                }
            }
        }

        drawBitmap(this, bitmap, bmFolderMask, defaultDrawable);
    }

    /************************************************************************************
     *
     * create bitmap from media Uri (API 29 or newer)
     *
     ***********************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.Q)
    static Bitmap createBitmapFromMediaContentUri
    (
        Context context,
        final String path,
        final int imageSize
    )
    {
        Bitmap bitmap = null;

        // Android 11 media path from:
        //          ContentUris.withAppendedId(
        //                        MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
        //                        thisAlbumId);
        ContentResolver resolver = context.getContentResolver();
        Uri contentUri = Uri.parse(path);
        Size wh = new Size(imageSize, imageSize);
        try
        {
            bitmap = resolver.loadThumbnail(contentUri, wh, null);
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "createBitmapFromMediaContentUri() : invalid uri \"" + path + "\": " + e);
        }

        return bitmap;
    }


    /************************************************************************************
     *
     * draw bitmap or default image (running in UI thread)
     *
     ***********************************************************************************/
    private static void drawBitmap
    (
        final ImageViewDeferred view,
        final Bitmap bitmap,
        final Bitmap bmFolderMask,
        final Drawable defaultDrawable
    )
    {
        view.setBackgroundResource(0);
        if (bitmap != null)
        {
            view.setImageBitmap(bitmap);
            if (bmFolderMask != null)
            {
                view.setBackgroundResource(R.drawable.folder_frame);
                view.setBackgroundResource(R.drawable.folder_2);
            }
        }
        else
        if (defaultDrawable != null)
        {
            view.setImageDrawable(defaultDrawable);
            // causes out of memory error:
            //  albumCoverImageView.setImageResource(R.drawable.no_album_art);
        }
    }


    /************************************************************************************
     *
     * create bitmap from SAF Uri
     *
     ***********************************************************************************/
    private static Bitmap createBitmapFromSafPath
    (
        Context context,
        final String path
    )
    {
        Bitmap bitmap = null;

        DocumentFile df = SafUtilities.getDocumentFileFromUriString(context, path);
        if (df != null)
        {
            InputStream is;
            try
            {
                is = context.getContentResolver().openInputStream(df.getUri());
                bitmap = BitmapFactory.decodeStream(is);
            } catch (Exception e)
            {
                Log.e(LOG_TAG, "createBitmap() : image file not found: " + path);
            }
        }

        return bitmap;
    }


    /************************************************************************************
     *
     * create bitmap from path (usually running in thread context)
     *
     * If not in debug mode (sbDeferred == false), this function is called in
     * thread context. Returns null in case of failure.
     *
     ***********************************************************************************/
    private static Bitmap createBitmap
    (
        Context context,
        final String path,
        final Bitmap bmFolderMask,
        final int imageSize
    )
    {
        Bitmap bitmap = null;
        if (path.startsWith("content://media/"))
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            {
                bitmap = createBitmapFromMediaContentUri(context, path, imageSize);
            }
        }
        else
        if (path.startsWith("content://"))
        {
            bitmap = createBitmapFromSafPath(context, path);
        } else
        {
            // traditional path
            bitmap = BitmapUtils.decodeSampledBitmapFromFile(path, imageSize, imageSize);
        }

        if ((bitmap != null) && (bmFolderMask != null))
        {
            bitmap = BitmapUtils.makeMaskedBitmap(bitmap, bmFolderMask, imageSize, imageSize);
        }

        return bitmap;
    }

}
