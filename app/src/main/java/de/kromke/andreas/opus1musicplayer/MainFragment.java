/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import de.kromke.andreas.utilities.*;

//import static java.lang.System.identityHashCode;


/**
 * This class manages the sub-views, called fragments
 *
 * According to http://stackoverflow.com/questions/6510550/android-listview-in-fragment we
 * need a ListFragment here. But I think this is not necessary.
 * To make things more complicated, we explicitly need the v4 support ListFragment.
 *
 *  Static nested classes do not have access to other members of the enclosing class
 */
@SuppressWarnings({"Convert2Lambda", "JavadocBlankLines", "JavadocLinkAsPlainText"})
public class MainFragment extends Fragment
        implements View.OnClickListener /* for footer */,
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener,
        MyListView.OnNoItemClickListener
{
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    static private final String LOG_TAG = "MainFragment";

    //public String mUniqueHash;   // for debugging purposes, to check if there are multiple instances

    public static int sImageSizeForListPictures;
    public static int sImageSizeForHeaderPictures;
    public static int sImageSizeForGrid;
    public static boolean sGridView;

    public static final int theAlbumFragment = 1;
    public static final int theWorkFragment = 2;            // works consist of tracks
    public static final int theTracksFragment = 3;          // selected tracks that can be copied to playing list
    public static final int thePlaylistFragment = 4;        // stored playlists, named
    public static final int theFolderFragment = 5;
    public static final int theComposerFragment = 6;
    public static final int theGenreFragment = 7;
    public static final int thePerformerFragment = 8;
    public static final int thePlayingFragment = 9;         // tracks and other objects that can currently be played
    public static final int nFragments = 9;

    private static int sPrevSectionNumber = -1;     // to be able to go back
    private static int sPrevPrevSectionNumber = -1;
    private static int sPrevPrevPrevSectionNumber = -1;

    public int mSectionNumber = -1;
    private boolean mBackKeyAlreadyPressed; // for folder view to go back to album view
    private String mCurrSearchText = "";
    public FilteredAudioObjectList mAudioObjectList;  // all objects

    private AbsListView mTheItemsView;
    AudioBaseListAdapter mTheAdapter;
    @SuppressWarnings("FieldCanBeLocal")
    private boolean mbGridView;

    TextView mFooterView;
    View mDividerView;
    ProgressBar mProgressBarView;
    private int mProgressBarHeight = 0;
    private int mImageSize;
    private static boolean mProgressBarShow = false;    // shared among all fragments
    private static int mProgressBarMax = 0;
    private static int mProgressBarProgress = 0;

    /**************************************************************************
     *
     * constructor
     *
     * Note that this is called either by our newInstance() or internally
     * by the onCreate() method of the activity in case a state had been
     * stored in the savedInstanceState parameter.
     *
     *************************************************************************/
    public MainFragment()
    {
        //mUniqueHash = String.format("0x%08x", identityHashCode(this));
        //Log.d(LOG_TAG, "MainFragment(" + mUniqueHash + ")");
    }


    /**************************************************************************
     *
     * Returns a new instance of this fragment for the given section
     * number. Kind of constructor.
     *
     *************************************************************************/
    public static MainFragment newInstance(int sectionNumber)
    {
        //Log.d(LOG_TAG, "newInstance(" + sectionNumber + ")");
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);  // in fact unnecessary because of next line
        fragment.mSectionNumber = sectionNumber;
        return fragment;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    public static FilteredAudioObjectList getAudioObjectList(int secNo)
    {
        FilteredAudioObjectList theList;

        switch (secNo)
        {
            case MainFragment.theAlbumFragment:     theList = AudioFilters.audioAlbumList; break;
            case MainFragment.theWorkFragment:      theList = AudioFilters.audioWorkList; break;
            case MainFragment.theTracksFragment:    theList = AudioFilters.audioTracksList; break;
            case MainFragment.thePlaylistFragment:  theList = AudioFilters.audioPlaylistList; break;
            case MainFragment.theComposerFragment:  theList = AudioFilters.audioComposerList; break;
            case MainFragment.thePerformerFragment: theList = AudioFilters.audioPerformerList; break;
            case MainFragment.theGenreFragment:     theList = AudioFilters.audioGenreList; break;
            case MainFragment.theFolderFragment:
                if (FolderHelper.mCurrFolderList != null)
                {
                    theList = FolderHelper.mCurrFolderList;
                }
                else
                {
                    theList = AudioFilters.audioFolderList;
                }
                break;
            case MainFragment.thePlayingFragment:   theList = AudioFilters.audioPlayingList; break;
            default: theList = null;
        }

        return theList;
    }


    /**************************************************************************
     *
     * Wait for background task and show toast, if it's still busy
     *
     *************************************************************************/
    private int waitForDbCompletion()
    {
        int ret = AppGlobals.waitForBackgroundTaskWithTimeout();
        if (ret == 1)
        {
            Toast.makeText(getActivity(), R.string.str_be_patient, Toast.LENGTH_SHORT).show();
        }
        return ret;
    }


    /**************************************************************************
     *
     * return a View for the current fragment
     *
     *************************************************************************/
    @Override
    public View onCreateView
    (
        @NonNull LayoutInflater inflater,
        ViewGroup container,
        Bundle savedInstanceState
    )
    {
        //Log.d(LOG_TAG, "onCreateView(" + mUniqueHash + ", " + mSectionNumber + ")");
        if (mSectionNumber == -1)
        {
            PrivateLog.w(LOG_TAG, "onCreateView() -- deny auto restored fragment, return null view");
            return null;        // deny auto generated fragments
        }
        final Bundle arguments = getArguments();
        mSectionNumber = (arguments != null) ? arguments.getInt(ARG_SECTION_NUMBER) : 1;

        View rootView = initViews(inflater, container);

        updateProgressBar(mProgressBarShow, mProgressBarProgress, mProgressBarMax);

        boolean bFileViewInsteadOfTrackView = false;
        boolean bPlaylistMode = false;
        mAudioObjectList = getAudioObjectList(mSectionNumber);

        switch(mSectionNumber)
        {
            //noinspection DefaultNotLastCaseInSwitch
            default:
            case theAlbumFragment:
                // we need all albums here, not yet all tracks
// too late, need album list for background task                AppGlobals.initAudioAlbumList();
                // list of objects to show
                showFilterFooter();
                mFooterView.setOnClickListener(this);
                break;

            case theWorkFragment:
            case theComposerFragment:
            case thePerformerFragment:
            case theGenreFragment:
                // we need all tracks here to get the respective list
                AppGlobals.waitForBackgroundTask();
                // work: shall we choose the album picture or the composer picture?
                // list of objects to show
                showFilterFooter();
                mFooterView.setOnClickListener(this);
                break;

            case thePlaylistFragment:
                // no footer
                mDividerView.setVisibility(View.GONE);
                mFooterView.setVisibility(View.GONE);
                mImageSize = sImageSizeForHeaderPictures;
                // list of objects to show
                break;

            case theFolderFragment:
                // we need all tracks here to get the folder list
                AppGlobals.waitForBackgroundTask();
                //RelativeLayout theLayout = (RelativeLayout) mRootView;
                FolderHelper.initCommonFolderPrefix();
                // FolderHelper.currentPath = FolderHelper.rootPath;    no, don't, otherwise path is reset on fragment change
                mFooterView.setOnClickListener(this);
                FolderHelper.enterCurrentPath();
                mFooterView.setText(FolderHelper.getCurrentPath());
                // special handling of BACK key
                mBackKeyAlreadyPressed = false;

                // list of objects to show
                int scrolledIndexShown = -1;
                int scrolledTopShown = -1;
                if (mAudioObjectList != null)
                {
                    // copy saved scroll position, if any, the new list
                    scrolledIndexShown = mAudioObjectList.scrolledIndexShown;
                    scrolledTopShown = mAudioObjectList.scrolledTopShown;
                }
                mAudioObjectList = FolderHelper.mCurrFolderList;
                mAudioObjectList.scrolledIndexShown = scrolledIndexShown;
                mAudioObjectList.scrolledTopShown = scrolledTopShown;

                // folder symbols
                bFileViewInsteadOfTrackView = true;     // show tracks as files

                // update number of items in popup title
                updateSpinner();
                break;

            case theTracksFragment:
            case thePlayingFragment:
                // no footer
                mDividerView.setVisibility(View.GONE);
                mFooterView.setVisibility(View.GONE);
                mImageSize = sImageSizeForHeaderPictures;
                // list of objects to show
                bPlaylistMode = true;
                break;
        }

        mTheAdapter = new AudioBaseListAdapter(
                getActivity(),
                mAudioObjectList.getListShown(),
                mImageSize, bFileViewInsteadOfTrackView, bPlaylistMode, mbGridView);

        if (bPlaylistMode)
        {
            Context context = getActivity();
            if (context != null)
            {
                mTheAdapter.setHeaderColoursForPlaylistMode(
                        UserSettings.getThemedColourFromResId(context, R.attr.colourTextLine1AsHeader),
                        UserSettings.getThemedColourFromResId(context, R.attr.colourTextLine2AsHeader),
                        UserSettings.getThemedColourFromResId(context, R.attr.colourTextTrackComposer));
            }
        }

        /*
        if (mSectionNumber == theFolderFragment)
        {
            mAudioObjectList = FolderHelper.currFolderList;
            mTheAdapter.setObjectList(mAudioObjectList.getListShown());
        }
         */

        mTheItemsView.setAdapter(mTheAdapter);
        mTheItemsView.setOnItemClickListener(this);

        return rootView;
    }


    /**************************************************************************
     *
     * The fragment is now visible to the user.
     *
     * Called after onActivityCreated().
     * After onStart() the method onResume() is called.
     *
     *************************************************************************/
    @Override
    public void onStart()
    {
        //Log.d(LOG_TAG, "onStart(" + mUniqueHash + ", " + mSectionNumber + ")");
        super.onStart();
        if (sPrevPrevSectionNumber == mSectionNumber)
        {
            // probably BACK key was pressed, undo the assignment in onStop
            PrivateLog.d(LOG_TAG, "BACK detected");
            sPrevSectionNumber = sPrevPrevPrevSectionNumber;
            sPrevPrevSectionNumber = -1;
        }
        else
        if (sPrevSectionNumber == mSectionNumber)
        {
            // screen has been rotated?
            sPrevSectionNumber = sPrevPrevSectionNumber;    // undo the assignment in onStop
        }
        PrivateLog.d(LOG_TAG, "prev = " + sPrevSectionNumber + ", prev prev = " + sPrevPrevSectionNumber);
        PrivateLog.d(LOG_TAG, "onStart() - done");
    }


    /**************************************************************************
     *
     * Pendant to onStart()
     *
     * unfortunately this is also called in case the screen is being rotated...
     *
     *************************************************************************/
    @Override
    public void onStop()
    {
        //Log.d(LOG_TAG, "onStop(" + mUniqueHash + ", " + mSectionNumber + ")");
        super.onStop();
        saveScrollPosition();
        sPrevPrevPrevSectionNumber = sPrevPrevSectionNumber;    // needed to handle BACK key
        sPrevPrevSectionNumber = sPrevSectionNumber;    // needed to handle screen rotation
        sPrevSectionNumber = mSectionNumber;    // remember section number to be able to go back later
        PrivateLog.d(LOG_TAG, "prev = " + sPrevSectionNumber + ", prev prev = " + sPrevPrevSectionNumber);
        PrivateLog.d(LOG_TAG, "onStop() - done");
    }


    /**************************************************************************
     *
     * show footer in order to present the filter
     *
     *************************************************************************/
    private void showFilterFooter()
    {
        // text for all filters except that of the current list
        String filterText = AudioFilters.getFilterText(mAudioObjectList);
        if (filterText.isEmpty())
        {
            mDividerView.setVisibility(View.GONE);
            mFooterView.setVisibility(View.GONE);
        }
        else
        {
            mDividerView.setVisibility(View.VISIBLE);
            mFooterView.setVisibility(View.VISIBLE);
            mFooterView.setText(filterText);
        }
//                mFooterView.setMinHeight(50);
//                mFooterView.setHeight(0);
//                mFooterView.setVisibility(View.VISIBLE);
//                mFooterView.setOnClickListener(this);
                /*
                // for list:
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewToLayout.getLayoutParams();
                params.addRule(RelativeLayout.BELOW, R.id.below_id);
                //
                You can't remove a rule because all rules are always stored in a fixed-size java array. But you can set a rule to 0. For example

                layoutParams.addRule(RelativeLayout.RIGHT_OF, 0);
                layoutParams.addRule(RelativeLayout.BELOW, R.id.new_ref_LinearLayout);
                 */
    }


    /**************************************************************************
     *
     * standard Fragment callback
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
    {
        PrivateLog.d(LOG_TAG, "onViewCreated()");
        super.onViewCreated(view, savedInstanceState);
        // instead of the standard ListView we have the extended MyListView
        if (mTheItemsView instanceof MyListView)
        {
            MyListView myList = (MyListView) mTheItemsView;
            myList.setOnNoItemClickListener(this);
            myList.setOnItemLongClickListener(this);
        }
        /*
        myList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3)
            {
                Toast.makeText(getActivity(), "On long click listener", Toast.LENGTH_LONG).show();
                return true;
            }
        });
        */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            restoreScrollPosition();
        }
        else
        {
            restoreScrollPosition_Api19();
        }
        PrivateLog.d(LOG_TAG, "onViewCreated() - done");
    }


    /**************************************************************************
     *
     * helper for folder fragment
     *
     *************************************************************************/
    private boolean GotoParentFolder()
    {
        if (FolderHelper.gotoParent())
        {
            mFooterView.setText(FolderHelper.getCurrentPath());
            // force redraw by re-assigning the adapter
            mAudioObjectList = FolderHelper.mCurrFolderList;
            mTheAdapter.setObjectList(mAudioObjectList.getListShown());
            mTheItemsView.setAdapter(mTheAdapter);
            updateSpinner();        // update number of items shown in spinner title
            return true;
        }
        else
        {
            return false;
        }
    }


    /**************************************************************************
     *
     * A filter has been removed, update the list to be displayed
     *
     *************************************************************************/
    public void onRemoveFilters(boolean removeComposerFilter, boolean removeGenreFilter, boolean removePerformerFilter)
    {
        if (removeComposerFilter)
        {
            AudioFilters.setFilterComposer(null);
        }
        if (removeGenreFilter)
        {
            AudioFilters.setFilterGenre(null);
        }
        if (removePerformerFilter)
        {
            AudioFilters.setFilterPerformer(null);
        }
        showFilterFooter();

        updateSpinner();

        // force redraw by re-assigning the adapter
        mTheAdapter.setObjectList(mAudioObjectList.getListShown());
        mTheItemsView.setAdapter(mTheAdapter);
    }


    /**************************************************************************
     *
     * click to footer in folder view plays all tracks in that folder
     *
     *************************************************************************/
    private void playCurrentFolder()
    {
        PrivateLog.d(LOG_TAG, "playCurrentFolder");
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null)
        {
            if (FolderHelper.mCurrentFolder == null)
            {
                activity.dialogNoAudioFilesInPath();
            } else
            {
                activity.dialogConditionallyOverwriteOrAppendToPlayingList(FolderHelper.mCurrentFolder, -1);
            }
        }
    }


    /**************************************************************************
     *
     * click to footer
     *
     *************************************************************************/
    @Override
    public void onClick(View v)
    {
        PrivateLog.d(LOG_TAG, "onClick");
        //noinspection SwitchStatementWithTooFewBranches
        switch(mSectionNumber)
        {
            case theFolderFragment:
                mBackKeyAlreadyPressed = false;     // special handling of BACK key
                //GotoParentFolder();
                playCurrentFolder();
                break;

            default:
                MainActivity activity = (MainActivity) getActivity();
                if (activity != null)
                {
                    activity.dialogRemoveFilters(AudioFilters.isComposerFilter(), AudioFilters.isGenreFilter(), AudioFilters.isPerformerFilter());
                }
                break;
        }
    }


    /**************************************************************************
     *
     * Listview callback: user list item longpressed
     *
     *************************************************************************/
    @Override
    public boolean onItemLongClick(AdapterView<?> l, View v,
                                   final int position, long id)
    {
        PrivateLog.d(LOG_TAG, "onItemLongClick(pos = " + position + ", id = " + id + ")");
        AudioBaseObject theListObject = mAudioObjectList.getListShown().list.get(position);

        boolean ret = false;
        MainActivity activity = (MainActivity) getActivity();
        switch(mSectionNumber)
        {
            case theWorkFragment:
            case theAlbumFragment:
            case theTracksFragment:
            case thePlayingFragment:
                if (activity != null)
                {
                    ret = activity.onLongpressToMusicList(theListObject, position);
                }
                break;

            case thePlaylistFragment:
                if (activity != null)
                {
                    ret = activity.onLongpressToPlaylist(theListObject, position);
                }
                break;

            default:
                break;
        }

        return ret;
    }


    /**************************************************************************
     *
     * Fragment callback: user list item selected
     *
     *************************************************************************/
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id)
    {
        PrivateLog.d(LOG_TAG, "onListItemClick(pos = " + position + ", id = " + id + ")");
        AudioBaseObject theListObject = mAudioObjectList.getListShown().list.get(position);
        int currSelection = mAudioObjectList.getShownSelectedIndex();
        MainActivity activity = (MainActivity) getActivity();

        switch(mSectionNumber)
        {
            case theAlbumFragment:
            case theWorkFragment:
                if ((waitForDbCompletion() == 0) && (activity != null))
                {
                    activity.putToTrackList(theListObject, position);
                }
                break;

            case thePlaylistFragment:
            case theTracksFragment:
                if ((waitForDbCompletion() == 0) && (activity != null))
                {
                    activity.dialogConditionallyOverwriteOrAppendToPlayingList(theListObject, position);
                }
                break;

            case theFolderFragment:
                mBackKeyAlreadyPressed = false;     // special handling of BACK key
                if (position >= 0)
                {
                    //AudioBaseObject theObject = FolderHelper.currFolderList.getListShown().list.get(position);
                    if (theListObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_FOLDER_OBJECT)
                    {
                        // folder selected, enter new path

                        FolderHelper.gotoSubdir(theListObject.name);
                        mFooterView.setText(FolderHelper.getCurrentPath());
                        // force redraw by re-assigning the adapter
                        mAudioObjectList = FolderHelper.mCurrFolderList;
                        mTheAdapter.setObjectList(mAudioObjectList.getListShown());
                        mTheItemsView.setAdapter(mTheAdapter);
                        //mRootView.invalidate();   // does not suffice
                        if (activity != null)
                        {
                            activity.updateSpinnerAdapter();
                        }
                    }
                    else
                    {
                        // track selected. Play it.
                        //((MainActivity) getActivity()).conditionalDialogPlayTrackOrCompleteFolder(theListObject, position);
                        if (activity != null)
                        {
                            activity.dialogConditionallyOverwriteOrAppendToPlayingList(theListObject, position);
                        }
                    }
                }
                break;

            case theComposerFragment:
                if (position == currSelection)
                {
                    // remove selection
                    position = -1;
                    theListObject = null;
                }
                onSetCurrSelection(position);
                AudioFilters.setFilterComposer(theListObject);
                if (activity != null)
                {
                    activity.updateSpinnerAdapter();
                    if (position >= 0)
                    {
                        activity.dialogConditionallyBranchToAlbumsOrWorks();
                    }
                }
                break;

            case thePerformerFragment:
                if (position == currSelection)
                {
                    // remove selection
                    position = -1;
                    theListObject = null;
                }
                onSetCurrSelection(position);
                AudioFilters.setFilterPerformer(theListObject);
                if (activity != null)
                {
                    activity.updateSpinnerAdapter();
                    if (position >= 0)
                    {
                        activity.dialogConditionallyBranchToAlbumsOrWorks();
                    }
                }
                break;

            case theGenreFragment:
                if (position == currSelection)
                {
                    // remove selection
                    position = -1;
                    theListObject = null;
                }
                onSetCurrSelection(position);
                AudioFilters.setFilterGenre(theListObject);
                if (activity != null)
                {
                    activity.updateSpinnerAdapter();
                    if (position >= 0)
                    {
                        activity.dialogConditionallyBranchToAlbumsOrWorks();
                    }
                }
                break;

            case thePlayingFragment:
                if (activity != null)
                {
                    activity.onPlayingListTrackPicked(position, false);
                }
                break;
        }
    }


    /**************************************************************************
     *
     * callback for custom ListView that reports clicks to empty space
     *
     *************************************************************************/
    @Override
    public void onNoItemClicked()
    {
        PrivateLog.d(LOG_TAG, "onNoItemClicked()");
        if (mSectionNumber == thePlayingFragment)
        {
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null)
            {
                activity.onPlayingListTrackPicked(-1, false);
            }
        }
    }


    /**************************************************************************
     *
     * search text entered
     *
     *************************************************************************/
    public void onQueryTextSubmit(final String searchText)
    {
        PrivateLog.d(LOG_TAG, "onQueryTextSubmit(searchText = " + searchText + ")");
        mBackKeyAlreadyPressed = false;     // special handling of BACK key
        if (!mCurrSearchText.equals(searchText))
        {
            applyFilter(searchText);
        }
    }


    /**************************************************************************
     *
     * force redraw
     *
     *************************************************************************/
    public void forceRedraw()
    {
        PrivateLog.d(LOG_TAG, "forceRedraw()");
        if (mAudioObjectList != null)
        {
            // force redraw by re-assigning the adapter
            mTheAdapter.setObjectList(mAudioObjectList.getListShown());
            mTheItemsView.setAdapter(mTheAdapter);
        }
    }


    /**************************************************************************
     *
     * apply search text filter
     *
     *************************************************************************/
    public void applyFilter(final String searchText)
    {
        PrivateLog.d(LOG_TAG, "applyFilter(searchText = " + searchText + ")");
        boolean bCaseSensitive = UserSettings.getBool(UserSettings.PREF_CASE_SENSITIVE_SEARCH, false);
        mAudioObjectList.setSearchText(searchText, bCaseSensitive);
        // new filter set
        mCurrSearchText = searchText;
        forceRedraw();
    }


    /**************************************************************************
     *
     * BACK key was pressed
     *
     *************************************************************************/
    public boolean onKeyBack()
    {
        PrivateLog.d(LOG_TAG, "onKeyBack()");
        MainActivity activity = (MainActivity) getActivity();

        if ((sPrevSectionNumber == mSectionNumber) || (sPrevSectionNumber < 0))
        {
            sPrevSectionNumber = theAlbumFragment;
            sPrevPrevSectionNumber = -1;
        }

        switch(mSectionNumber)
        {
            case theFolderFragment:
                if (GotoParentFolder())
                {
                    // event consumed, i.e. path changed in direction to root
                    return true;
                }

                // in case we already are in root directory, press BACK a second
                // time to go to the Album view.

                if (!mBackKeyAlreadyPressed)
                {
                    Toast.makeText(getActivity(), R.string.str_press_back_key_twice, Toast.LENGTH_SHORT).show();
                    mBackKeyAlreadyPressed = true;
                }
                else
                {
                    // otherwise go to album fragment
                    if (activity != null)
                    {
                        activity.changeFragment(theAlbumFragment);
                    }
                }
                return true;    // event consumed

            case theTracksFragment:
                if ((sPrevSectionNumber == thePlayingFragment) && (sPrevPrevSectionNumber > 0))
                {
                    if (activity != null)
                    {
                        activity.changeFragment(sPrevPrevSectionNumber);
                        return true;
                    }
                }
                // fall through
            case thePlayingFragment:
                if (activity != null)
                {
                    activity.changeFragment(sPrevSectionNumber);
                }
                // event consumed
                return true;

            case theAlbumFragment:
                // exit application
                break;

            default:
                // otherwise go to album fragment
                if (activity != null)
                {
                    activity.changeFragment(theAlbumFragment);
                    return true;
                }
                else
                {
                    // exit application
                    break;
                }
        }

        // event not consumed
        return false;
    }


    /**************************************************************************
     *
     * get the View element of a list entry at given position
     *
     *************************************************************************/
    public View getViewByPosition(int pos, ListView listView)
    {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition)
        {
            return listView.getAdapter().getView(pos, null, listView);
        } else
        {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }


    /**************************************************************************
     *
     *  If title or group shows a bookmark "*", remove it
     *
     *************************************************************************/
    private void updateBookmarkAsterisk(View v, AudioTrack theTrack)
    {
        CharSequence title;
        TextView titleView = v.findViewById(R.id.track_title);
        if (titleView != null)
        {
            title = titleView.getText();
            if (title.length() == 0)
            {
                titleView = v.findViewById(R.id.track_composer_and_work);
            }
            if (titleView != null)
            {
                title = titleView.getText();
                String titleStr = title.toString();
                String newTitleStr = AudioBaseListAdapter.getTrackTitle(theTrack);
                if (!newTitleStr.equals(titleStr))
                {
                    titleView.setText(newTitleStr);
                }
            }
        }
    }


    /**************************************************************************
     *
     *  Helper for updating an item
     *
     *************************************************************************/
    private View getItemView(int index)
    {
        if (index >= 0)
        {
            if (mTheItemsView instanceof MyListView)
            {
                MyListView myList = (MyListView) mTheItemsView;
                return getViewByPosition(index, myList);
                // unfortunately the BaseAdapter has no notifyItemChange() method
                // v is of type LinearLayout
            }
        }
        return null;
    }

    /**************************************************************************
     *
     *  Change the item's background colour from "selected" back to "normal"
     *  and remove the bookmark asterisk, if necessary.
     *
     *************************************************************************/
    private void toNormal(int index)
    {
        View v = getItemView(index);
        if (v != null)
        {
            AudioBaseObject theObject = mAudioObjectList.getListShown().list.get(index);
            if (theObject instanceof AudioTrack)
            {
                AudioTrack theTrack = (AudioTrack) theObject;
                updateBookmarkAsterisk(v, theTrack);
            }
            // back to normal (depends on odd and even position)
            // HACK: find feasible solution for playlist
            int colourId = ((theObject.alternating_colour == 0)) ? R.attr.colourRowNormal : R.attr.colourRowNormal2;
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null)
            {
                v.setBackgroundColor(UserSettings.getThemedColourFromResId(activity, colourId));
            }
        }
    }


    /**************************************************************************
     *
     *  Change the item's background colour to "selected".
     *
     *************************************************************************/
    private void toSelected(int index)
    {
        View v = getItemView(index);
        if (v != null)
        {
            // selected
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null)
            {
                v.setBackgroundColor(UserSettings.getThemedColourFromResId(activity, R.attr.colourRowSelected));
            }
        }
    }


    /**************************************************************************
     *
     *  Update the item's bookmark asterisk, if necessary
     *
     *************************************************************************/
    private void toBookmark(int index)
    {
        View v = getItemView(index);
        if (v != null)
        {
            AudioBaseObject theObject = mAudioObjectList.getListShown().list.get(index);
            if (theObject instanceof AudioTrack)
            {
                AudioTrack theTrack = (AudioTrack) theObject;
                updateBookmarkAsterisk(v, theTrack);
            }
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void toScroll(int prevIndex, int newIndex)
    {
        if (newIndex != prevIndex)
        {
            if (newIndex >= 0)
            {
                // TODO: See http://stackoverflow.com/questions/14479078/smoothscrolltopositionfromtop-is-not-always-working-like-it-should/20997828#20997828
                //   getListView().smoothScrollToPosition(newIndex);   is buggy
                //   getListView().setSelection(newIndex); is ugly
                smoothScrollToPositionFromTop(mTheItemsView, newIndex);
            }
        }
    }


    /**************************************************************************
     *
     * the current selection may have changed, also called from MainActivity
     *
     * newSelection may be -1, meaning to deselect everything
     *
     *************************************************************************/
    public void onSetCurrSelection(int newSelection)
    {
        if (mSectionNumber == theTracksFragment)
        {
            return;
        }

        if (isVisible())
        {
            int prevSelection = mAudioObjectList.getShownSelectedIndex();
            toNormal(prevSelection);
            toSelected(newSelection);
            toScroll(prevSelection, newSelection);
        }
        else
        {
            // see https://stackoverflow.com/questions/22295164/content-view-not-yet-created
            PrivateLog.w(LOG_TAG, "onSetCurrSelection() -- not visible, try to avoid crash");
        }
        mAudioObjectList.setShownSelectedIndex(newSelection);
    }


    /**************************************************************************
     *
     * The current selection in the raw (unfiltered) list may have changed,
     * called from MainActivity on track change
     *
     * Note that the raw selection indices address the raw, unfiltered list,
     * and possibly the selected or deselected index is not shown, due
     * to active filters.
     *
     * Note that selected index has already been set by caller.
     *
     *************************************************************************/
    public void onUpdateRawSelection(int prevRawSelection, int newRawSelection)
    {
        if (isVisible())
        {
            int prevShownSelection = mAudioObjectList.getShownIndexFromRawIndex(prevRawSelection);
            toNormal(prevShownSelection);
            int newShownSelection = mAudioObjectList.getShownIndexFromRawIndex(newRawSelection);
            toSelected(newShownSelection);
            toScroll(prevRawSelection, newRawSelection);
        }
        else
        {
            // see https://stackoverflow.com/questions/22295164/content-view-not-yet-created
            PrivateLog.w(LOG_TAG, "onUpdateRawSelection() -- not visible, try to avoid crash");
        }
    }


    /**************************************************************************
     *
     * The current bookmark in the raw (unfiltered) list may have changed,
     * called from MainActivity on track change
     *
     * Note that the raw selection indices address the raw, unfiltered list,
     * and possibly the selected or deselected index is not shown, due
     * to active filters.
     *
     * Note that selected index has already been set by caller.
     *
     *************************************************************************/
    public void onUpdateRawBookmark(int indexRaw)
    {
        if (isVisible())
        {
            int shownBookmark = mAudioObjectList.getShownIndexFromRawIndex(indexRaw);
            toBookmark(shownBookmark);
        }
        else
        {
            // see https://stackoverflow.com/questions/22295164/content-view-not-yet-created
            PrivateLog.w(LOG_TAG, "onUpdateRawBookmark() -- not visible, try to avoid crash");
        }
    }


    /**************************************************************************
     *
     * Save the scroll position of the list, to be restored later
     * when we switch back to this fragment.
     *
     *************************************************************************/
    public void saveScrollPosition()
    {
        // see http://stackoverflow.com/questions/3014089/maintain-save-restore-scroll-position-when-returning-to-a-listview/3035521#3035521
        if (mAudioObjectList != null)
        {
            mAudioObjectList.scrolledIndexShown = mTheItemsView.getFirstVisiblePosition();
            View v = mTheItemsView.getChildAt(0);
            mAudioObjectList.scrolledTopShown = (v == null) ? 0 : (v.getTop() - mTheItemsView.getPaddingTop());
            PrivateLog.d(LOG_TAG, "saveScrollPosition() -- for section " + mSectionNumber + ": pos = " + mAudioObjectList.scrolledIndexShown + ", top = " + mAudioObjectList.scrolledTopShown);
        }
    }


    /**************************************************************************
     *
     * Restore the saved scroll position
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void restoreScrollPosition()
    {
        if (mAudioObjectList.scrolledIndexShown >= 0)
        {
            PrivateLog.d(LOG_TAG, "restoreScrollPosition() -- for section " + mSectionNumber + ": pos == " + mAudioObjectList.scrolledIndexShown + " top = " + mAudioObjectList.scrolledTopShown);
//          getListView().smoothScrollToPosition(mAudioObjectList.scrolledIndex); DOES NOT WORK
            if (mTheItemsView instanceof MyListView)
            {
                mTheItemsView.setSelectionFromTop(mAudioObjectList.scrolledIndexShown, mAudioObjectList.scrolledTopShown);
            }
            else
            {
//                mTheItemsView.setSelectionFromTop(2 * mAudioObjectList.scrolledIndexShown, mAudioObjectList.scrolledTopShown);
                //mTheItemsView.smoothScrollToPosition(mAudioObjectList.scrolledIndexShown); CRAP, DOES NOT WORK
                mTheItemsView.setSelection(mAudioObjectList.scrolledIndexShown);
//                smoothScrollToPositionFromTop(mTheItemsView, mAudioObjectList.scrolledIndexShown);
            }
        }
    }


    /**************************************************************************
     *
     * Restore the saved scroll position (Api 19 version)
     *
     *************************************************************************/
    public void restoreScrollPosition_Api19()
    {
        if ((mAudioObjectList.scrolledIndexShown >= 0) && (mTheItemsView instanceof MyListView))
        {
            PrivateLog.d(LOG_TAG, "restoreScrollPosition() -- for section " + mSectionNumber + ": pos == " + mAudioObjectList.scrolledIndexShown + " top = " + mAudioObjectList.scrolledTopShown);
//          getListView().smoothScrollToPosition(mAudioObjectList.scrolledIndex); DOES NOT WORK
            MyListView myList = (MyListView) mTheItemsView;
            myList.setSelectionFromTop(mAudioObjectList.scrolledIndexShown, mAudioObjectList.scrolledTopShown);
        }
    }


    /**************************************************************************
     *
     * view or hide progress bar
     *
     *************************************************************************/
    public void updateProgressBar(boolean bShow, int position, int duration)
    {
        //Log.d(LOG_TAG, "updateProgressBar(" + bShow + ", " + position + "/" + duration + ")");
        if (mProgressBarView != null)
        {
            ViewGroup.LayoutParams params;
            params = mProgressBarView.getLayoutParams();
            if (mProgressBarHeight == 0)
            {
                // not yet saved. Save now for later restore
                mProgressBarHeight = params.height;
            }

            // restore height or set to zero
            int newHeight = (bShow) ? mProgressBarHeight : 0;
            if (params.height != newHeight)
            {
                // change height
                params.height = newHeight;
                mProgressBarView.setLayoutParams(params);
            }

            if ((bShow) && (duration != 0))
            {
                //Log.d(LOG_TAG, "updateProgressBar(" + bShow + ", " + position + "/" + duration + ")");
                int progress = (position * 100) / duration;
                //Log.d(LOG_TAG, "updateProgressBar() -- progress in percent: " + progress);
                mProgressBarView.setProgress(progress);
            }
        }

        mProgressBarShow = bShow;   // in case mProgressBarView is null, the action is deferred
        mProgressBarMax = duration;
        mProgressBarProgress = position;

//      mProgressBarView.setVisibility(View.INVISIBLE);   // makes object invisible, but still takes place
//      mProgressBarView.setVisibility(View.GONE);        // makes footer to cover the whole screen?!?
    }


    /**************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    private void updateSpinner()
    {
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null)
        {
            activity.updateSpinnerAdapter();
        }
    }


    /**************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    public static View getChildAtPosition(final AbsListView view, final int position)
    {
        final int index = position - view.getFirstVisiblePosition();
        if ((index >= 0) && (index < view.getChildCount()))
        {
            return view.getChildAt(index);
        }
        else
        {
            return null;
        }
    }


    /**************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    public static boolean isGridViewAllowedForSection(final int section_number)
    {
        // grid view not allowed for tracks
        return ((section_number == theAlbumFragment) || (section_number == theComposerFragment) ||
               (section_number == theWorkFragment) || (section_number == thePerformerFragment) ||
               (section_number == theGenreFragment));
    }


    /**************************************************************************
     *
     * helper function
     *
     *************************************************************************/
    private View initViews(@NonNull LayoutInflater inflater, ViewGroup container)
    {
        // grid view not allowed for tracks
        if (isGridViewAllowedForSection(mSectionNumber))
        {
            mbGridView = sGridView;
        }
        else
        {
            mbGridView = false;
        }

        View rootView;

        if (mbGridView)
        {
            rootView = inflater.inflate(R.layout.basic_grid_with_footer, container, false);
            GridView audioAlbumView = rootView.findViewById(R.id.pictures_grid);

            // get screen width and derive number of columns for grid view
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Activity theActivity = getActivity();
            if (theActivity != null)
            {
                theActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                // make sure that in portrait mode there are at least two columns
                int width = displayMetrics.widthPixels;
                int height = displayMetrics.heightPixels;
                int dim = Math.min(width, height);    // get the smaller one of width and height
                int gridw;
                if (dim >= 2 * sImageSizeForGrid)
                {
                    gridw = sImageSizeForGrid;
                } else
                {
                    gridw = dim / 2;
                }

                audioAlbumView.setNumColumns(width / gridw);
            }

            mTheItemsView = audioAlbumView;
            mImageSize = sImageSizeForGrid;
        }
        else
        {
            rootView = inflater.inflate(R.layout.basic_list_with_footer, container, false);
            mTheItemsView = rootView.findViewById(R.id.pictures_list);
            mImageSize = sImageSizeForListPictures;
        }

        mFooterView = rootView.findViewById(R.id.footerText);
        mDividerView = rootView.findViewById(R.id.tracks_album_divider_line);
        mProgressBarView = rootView.findViewById(R.id.playProgressBar);

        return rootView;
    }


    /**************************************************************************
     *
     * helper function
     *
     * workaround for bug in smoothScrollToPosition()
     * See http://stackoverflow.com/questions/14479078/smoothscrolltopositionfromtop-is-not-always-working-like-it-should/20997828
     * Note that AbsListView is a base class of ListView, and AdapterView is a base class of AbsListView.
     *
     *************************************************************************/
    public static void smoothScrollToPositionFromTop(final AbsListView view, final int position)
    {
        View child = getChildAtPosition(view, position);
        // There's no need to scroll if child is already at top or view is already scrolled to its end
        if ((child != null) && ((child.getTop() == 0) || ((child.getTop() > 0) && !view.canScrollVertically(1))))
        {
            return;
        }

        view.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(final AbsListView view, final int scrollState)
            {
                if (scrollState == SCROLL_STATE_IDLE)
                {
                    view.setOnScrollListener(null);

                    // Fix for scrolling bug
                    new Handler().post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if ((view.getFirstVisiblePosition() <= position - 1) &&
                                (view.getLastVisiblePosition() >= position + 1))
                            {
                                // further scrolling should not be necessary
                                return;
                            }
                            view.setSelection(position);
                        }
                    });
                }
            }

            @Override
            public void onScroll(final AbsListView view, final int firstVisibleItem, final int visibleItemCount,
                                 final int totalItemCount) { }
        });

        // Perform scrolling to position
        new Handler().post(new Runnable()
        {
            @Override
            public void run()
            {
                //view.smoothScrollToPositionFromTop(position, 0);
                int boundPosition = (position > 0) ? position - 1 : 0;
                view.smoothScrollToPosition(position, boundPosition);
            }
        });
    }
}
