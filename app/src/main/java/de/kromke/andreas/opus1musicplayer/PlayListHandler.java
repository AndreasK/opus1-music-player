/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import java.util.ArrayList;

import de.kromke.andreas.utilities.AudioAlbum;
import de.kromke.andreas.utilities.AudioBaseObject;
import de.kromke.andreas.utilities.AudioFilters;
import de.kromke.andreas.utilities.AudioWork;
import de.kromke.andreas.utilities.AudioTrack;
import de.kromke.andreas.utilities.FilteredAudioObjectList;
import de.kromke.andreas.utilities.PrivateDbHandler;
import de.kromke.andreas.utilities.PrivateLog;

/**
 * Combines all play list handling used by both MainActivity and MediaPlayService
 */

@SuppressWarnings({"WeakerAccess", "JavadocBlankLines"})
public class PlayListHandler
{
    static private final String LOG_TAG = "PlayListHandler";
    static public class TrackNumInfo
    {
        int trackno = 0;
        int numOfTracks = 0;
    }
    @SuppressWarnings("WeakerAccess")
    static public int autoplayMode = 0;
    static public int numOfBookmarksInPlayingList = 0;


    /**************************************************************************
     *
     * get current entry index  0..n-1
     *
     * Note that non-playable entries, i.e. those not of type AudioTrack,
     * are not treated specially.
     *
     *************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public static int getCurrEntryNo()
    {
        FilteredAudioObjectList playList = AudioFilters.audioPlayingList;
        if (playList != null)
        {
            return playList.getRawSelectedIndex();
        }
        else
        {
            PrivateLog.e(LOG_TAG, "getCurrEntryNo() - list not initialised");
            return -1;
        }
    }


    /**************************************************************************
     *
     * set current entry index  0..n-1
     *
     * Note that non-playable entries, i.e. those not of type AudioTrack,
     * are not treated specially.
     *
     *************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public static void setCurrEntryNo(int index)
    {
        FilteredAudioObjectList playList = AudioFilters.audioPlayingList;
        if (playList != null)
        {
            playList.setRawSelectedIndex(index);
        }
        else
        {
            PrivateLog.e(LOG_TAG, "setCurrEntryNo() - list not initialised");
        }
    }


    /**************************************************************************
     *
     * get number of entries
     *
     * Note that non-playable entries, i.e. those not of type AudioTrack,
     * are not treated specially.
     *
     *************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public static int getNumOfEntries()
    {
        FilteredAudioObjectList playList = AudioFilters.audioPlayingList;
        if (playList != null)
        {
            return playList.getTotalSize(); // can be -1
        }
        else
        {
            PrivateLog.e(LOG_TAG, "getNumOfEntries() - list not initialised");
            return -1;
        }
    }


    /**************************************************************************
     *
     * get number of audio tracks and current index of tracks
     *
     * Note that non-playable entries, i.e. those not of type AudioTrack,
     * are ignored here.
     *
     *************************************************************************/
    @SuppressWarnings("WeakerAccess")
    public static TrackNumInfo getTrackNumInfo(int index)
    {
        TrackNumInfo n = new TrackNumInfo();
        int size = getNumOfEntries();
        for (int i = 0; i < size; i++)
        {
            if (isAudioTrack(i))
            {
                n.numOfTracks++;
                if (i < index)
                {
                    n.trackno++;    // count number of tracks before <index>
                }
            }
        }

        return n;
    }


    /**************************************************************************
     *
     * get n-th entry or null in case of an error
     *
     *************************************************************************/
    private static AudioBaseObject getEntry(int index)
    {
        if (index >= 0)
        {
            FilteredAudioObjectList playList = AudioFilters.audioPlayingList;
            if (playList != null)
            {
                int size = playList.getTotalSize(); // can be -1
                if (index < size)
                {
                    return playList.raw.list.get(index);
                }
            } else
            {
                PrivateLog.e(LOG_TAG, "getEntry() - list not initialised");
            }
        }

        return null;
    }


    /**************************************************************************
     *
     * check if n-th entry is an AudioTrack
     *
     *************************************************************************/
    private static boolean isAudioTrack(int index)
    {
        AudioBaseObject object = getEntry(index);
        //noinspection SimplifiableIfStatement
        if (object == null)
        {
            return false;
        }

        return object.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT;
    }

    @SuppressWarnings("WeakerAccess")
    public static AudioTrack getAudioTrack(int index)
    {
        AudioBaseObject object = getEntry(index);
        if ((object != null) && (object.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT))
        {
            return (AudioTrack) object;
        }

        return null;
    }

    private static int gotoTrackInternal(int index, int delta)
    {
        int size = getNumOfEntries();
        //skip invalid entries
        while ((index >= 0) && (index < size) && (!isAudioTrack(index)))
        {
            // skip entry which is not a track, but a header (like album or work)
            index += delta;
        }

        if (index >= size)
        {
            index = -1;
        }

        return index;
    }

    public static int gotoTrack(int index, int delta)
    {
        index = gotoTrackInternal(index, delta);
        setCurrEntryNo(index);
        return index;
    }

    public static int gotoTrackRelInternal(int delta, boolean bAutoRepeat)
    {
        int curr = getCurrEntryNo();
        int num = getNumOfEntries();
        if (curr >= 0)
        {
            // advance
            int newIndex = curr + delta;
            // auto repeat
            if ((num > 0) && (newIndex >= num) && bAutoRepeat)
            {
                PrivateLog.d(LOG_TAG, "gotoTrackRel() - auto repeat");
                newIndex = 0;
            }

            // check underflow and overflow
            if ((newIndex < 0) || (newIndex >= num))
            {
                // invalid
                newIndex = -1;
            }

            curr = gotoTrackInternal(newIndex, delta);
        }

        return curr;
    }

    public static int gotoTrackRel(int delta, boolean bAutoRepeat)
    {
        int index = gotoTrackRelInternal(delta, bAutoRepeat);
        setCurrEntryNo(index);
        return index;
    }

    public static int getNextTrack(boolean bAutoRepeat)
    {
        int curr = getCurrEntryNo();
        if (curr >= 0)
        {
            if (autoplayMode == 3)
            {
                // stop after list (this is default)
                curr = gotoTrackRelInternal(1, bAutoRepeat);
            } else if (autoplayMode == 2)
            {
                // stop after group
                AudioTrack theTrack = (AudioTrack) getEntry(curr);
                if ((theTrack != null) && (!theTrack.group_last))
                {
                    // current track was not the last in group
                    curr = gotoTrackRelInternal(1, bAutoRepeat);
                } else
                {
                    // stop
                    curr = -1;
                }
            }
            else
            {
                // mode 1
                if (!bAutoRepeat)
                {
                    // stop after track
                    curr = -1;
                }
            }
        }

        return curr;
    }

    @SuppressWarnings("WeakerAccess")
    public static void advanceToNextTrack(boolean bAutoRepeat)
    {
        int curr = getNextTrack(bAutoRepeat);
        setCurrEntryNo(curr);
        //Log.e(LOG_TAG, "advanceToNextTrack() - index changed to " + curr);
    }


    /**************************************************************************
     *
     * helper: removes anything which is not a track
     *
     *************************************************************************/
    private static void clean(ArrayList<AudioBaseObject> theList)
    {
        int size = theList.size();
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject theObject = theList.get(i);
            if (theObject.objectType != AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
            {
                // remove
                theList.remove(i);
                i--;
                size--;
            }
        }
    }


    /**************************************************************************
     *
     * helper: Returns 0: nothing, n: album, -n: work
     *
     *************************************************************************/
    private static int isAlbumOrWork(ArrayList<AudioBaseObject> theList, int startIndex, int size)
    {
        //Log.d(LOG_TAG, "isAlbumOrWork() - check index " + startIndex + " of " + size);

        AudioBaseObject theObject = theList.get(startIndex);
        if (theObject.objectType != AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
        {
            // should not happen, ignore
            PrivateLog.e(LOG_TAG, "isAlbumOrWork() - index " + startIndex + " is no track but " + theObject.objectType);
            return 0;
        }
        AudioTrack theTrack = (AudioTrack) theObject;

        //
        // get work and album
        //

        AudioWork theWork = theTrack.mWork;
        if (theWork == null)
        {
            // should not happen, ignore
            PrivateLog.e(LOG_TAG, "isAlbumOrWork() - index " + startIndex + " is not part of work");
            return 0;
        }
        int lenWork = theWork.no_of_tracks;

        AudioAlbum theAlbum = theWork.mAlbum;
        if (theAlbum == null)
        {
            // should not happen, ignore
            PrivateLog.e(LOG_TAG, "isAlbumOrWork() - index " + startIndex + " is not part of album");
            return 0;
        }
        int lenAlbum = theAlbum.no_of_tracks;

        //
        // check subsequent tracks if they belong to same album and work
        //

        //Log.d(LOG_TAG, "isAlbumOrWork() - check album of size " + lenAlbum + " or work of size " + lenWork);
        int nWork = 1;
        int nAlbum = 1;
        boolean bSameWork = true;
        for (int i = startIndex + 1; i < size; i++)
        {
            theObject = theList.get(i);
            if (theObject.objectType != AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
            {
                // should not happen, ignore
                break;
            }
            theTrack = (AudioTrack) theObject;

            if ((theTrack.mWork == null) || (theTrack.mWork.mAlbum != theAlbum))
            {
                break;  // break at first album mismatch
            }
            nAlbum++;    // album match

            if (bSameWork)
            {
                if (theTrack.mWork == theWork)
                {
                    nWork++;    // work match
                }
                else
                {
                    bSameWork = false;  // no longer check work
                }
            }
        }

        if (nAlbum >= lenAlbum)
        {
            return nAlbum;      // subsequent tracks form a complete album
        }

        if (nWork >= lenWork)
        {
            return -nWork;      // subsequent tracks form a complete work
        }

        return 0;
    }


    /**************************************************************************
     *
     * The playing list or the track list has been changed and has to be tidied up.
     *
     * First remove everything which is not a track, then
     * add album and work entries to the playing list.
     *
     *************************************************************************/
    private static void normalise(FilteredAudioObjectList playList)
    {
        //FilteredAudioObjectList playList = AudioFilters.audioPlayingList;
        int size = (playList == null) ? 0 : playList.getTotalSize(); // can be -1
        if (size <= 0)
        {
            return;
        }

        ArrayList<AudioBaseObject> theList = playList.raw.list;
        clean(theList);     // remove everything but tracks

        size = theList.size();          // may have been changed by clean()
        for (int i = 0; i < size; i++)
        {
            int n = isAlbumOrWork(theList, i, size);
            if (n != 0)
            {
                // album or work
                AudioBaseObject theObject = theList.get(i);
                AudioTrack theTrack = (AudioTrack) theObject;   // type checked in isAlbumOrWork()
                AudioBaseObject theObjectToInsert;

                if (n > 0)
                {
                    // album
                    theObjectToInsert = theTrack.mWork.mAlbum;
                }
                else
                {
                    // work
                    theObjectToInsert = theTrack.mWork;
                    n = -n;
                }
                theList.add(i, theObjectToInsert);
                i += n;     // skip remaining album or work tracks and inserted object
                size++;
            }

        }
    }


    /**************************************************************************
     *
     * The playing list has been changed and has to be tidied up.
     *
     *************************************************************************/
    public static void normalisePlayingList()
    {
        normalise(AudioFilters.audioPlayingList);

        // update bookmarks
        int size = getNumOfEntries();
        numOfBookmarksInPlayingList = 0;
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject object = AudioFilters.audioPlayingList.raw.list.get(i);
            if (object instanceof AudioTrack)
            {
                AudioTrack theAudioTrack = (AudioTrack) object;
                if (theAudioTrack.play_pos_ms == -1)
                {
                    // unknown, ask db
                    PrivateDbHandler.Bookmark theBookmark = PrivateDbHandler.readBookmark(theAudioTrack.getPath());
                    if (theBookmark != null)
                    {
                        // bookmark found
                        theAudioTrack.play_pos_ms = theBookmark.pos;
                        PrivateLog.d(LOG_TAG, "normalisePlayingList() - bookmark for \"" + theAudioTrack.getPath() + "\": " + theBookmark.pos);
                    }
                    else
                    {
                        // no bookmark, start with position 0
                        theAudioTrack.play_pos_ms = 0;
                    }
                    theAudioTrack.play_pos_ms_original = theAudioTrack.play_pos_ms;     // must detect changes
                }

                if (theAudioTrack.play_pos_ms > 0)
                {
                    numOfBookmarksInPlayingList++;
                }
            }
        }
    }


    /**************************************************************************
     *
     * Get index of next bookmark
     *
     *************************************************************************/
    public static int getNextBookmarkInPlayingList(int index)
    {
        if (index >= 0)
        {
            int size = getNumOfEntries();
            for (int i = index; i < size; i++)
            {
                AudioBaseObject object = AudioFilters.audioPlayingList.raw.list.get(i);
                if (object instanceof AudioTrack)
                {
                    AudioTrack theAudioTrack = (AudioTrack) object;
                    if (theAudioTrack.play_pos_ms > 0)
                    {
                        return i;
                    }
                }
            }
        }

        return -1;
    }


    /**************************************************************************
     *
     * Get index of first bookmark
     *
     *************************************************************************/
    public static int getFirstBookmarkInPlayingList()
    {
        int size = getNumOfEntries();
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject object = AudioFilters.audioPlayingList.raw.list.get(i);
            if (object instanceof AudioTrack)
            {
                AudioTrack theAudioTrack = (AudioTrack) object;
                if (theAudioTrack.play_pos_ms > 0)
                {
                    return i;
                }
            }
        }

        return -1;
    }


    /**************************************************************************
     *
     * Update bookmark and tell, if bookmark was added or removed
     *
     *************************************************************************/
    public static boolean updateBookmarkInPlayingList(int index, long bookmarkPos)
    {
        AudioTrack theTrack = PlayListHandler.getAudioTrack(index);
        if (theTrack != null)
        {
            PrivateLog.d(LOG_TAG, "update bookmark of playing index " + index + " from pos " + theTrack.play_pos_ms + " to " + bookmarkPos);
            long oldBookmarkPos = theTrack.play_pos_ms;
            theTrack.play_pos_ms = bookmarkPos;
            if ((oldBookmarkPos <= 0) && (bookmarkPos > 0))
            {
                //theToast = Toast.makeText(getApplicationContext(), "Bookmark set for " + theTrack.name, Toast.LENGTH_LONG);
                //theToast.show();
                PlayListHandler.numOfBookmarksInPlayingList++;
                return true;
            }
            else
            if ((oldBookmarkPos > 0) && (bookmarkPos <= 0))
            {
                //theToast = Toast.makeText(getApplicationContext(), "Bookmark removed for " + theTrack.name, Toast.LENGTH_LONG);
                //theToast.show();
                //Log.d(LOG_TAG, "updateBookmarkInPlayingList() - removed for " + theTrack.name);
                PlayListHandler.numOfBookmarksInPlayingList--;
                return true;
            }
            /*
            else
            if (oldBookmarkPos != bookmarkPos)
            {
                //theToast = Toast.makeText(getApplicationContext(), "Bookmark changed for " + theTrack.name, Toast.LENGTH_LONG);
                //theToast.show();
            }
            */
        }
        return false;
    }


    /**************************************************************************
     *
     * remove all bookmarks in playing list
     *
     *************************************************************************/
    public static void removeAllBookmarks()
    {
        int size = getNumOfEntries();
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject object = AudioFilters.audioPlayingList.raw.list.get(i);
            if (object instanceof AudioTrack)
            {
                ((AudioTrack) object).play_pos_ms = 0;      // remove bookmark
            }
        }

        numOfBookmarksInPlayingList = 0;
    }


    /**************************************************************************
     *
     * save all bookmarks to database
     *
     *************************************************************************/
    public static void saveBookmarksPlayingList()
    {
        PrivateLog.d(LOG_TAG, "saveBookmarksPlayingList()");
        int size = getNumOfEntries();
        for (int i = 0; i < size; i++)
        {
            AudioBaseObject object = AudioFilters.audioPlayingList.raw.list.get(i);
            if (object instanceof AudioTrack)
            {
                AudioTrack theAudioTrack = (AudioTrack) object;
                if (theAudioTrack.play_pos_ms != theAudioTrack.play_pos_ms_original)
                {
                    // change detected
                    if (theAudioTrack.play_pos_ms > 0)
                    {
                        PrivateLog.d(LOG_TAG, " update bookmark(" + theAudioTrack.getPath() + "," + theAudioTrack.play_pos_ms + ")");
                        PrivateDbHandler.updateBookmark(theAudioTrack.getPath(), theAudioTrack.play_pos_ms);
                    }
                    else
                    if (theAudioTrack.play_pos_ms_original > 0)
                    {
                        PrivateLog.d(LOG_TAG, " delete bookmark(" + theAudioTrack.getPath() + ")");
                        PrivateDbHandler.deleteBookmark(theAudioTrack.getPath());
                    }
                }
            }
        }
    }


    /**************************************************************************
     *
     * The "tracks" list has been changed and has to be tidied up.
     *
     *************************************************************************/
    public static void normaliseTracksList()
    {
        normalise(AudioFilters.audioTracksList);
    }


    /**************************************************************************
     *
     * first remove everything which is not a track, then
     * add album and work entries to the playing list
     *
     *************************************************************************/
    public static boolean moveInPlayingList(int pos, int num, int delta)
    {
        // convert index in list (containing tracks and works and albums) to pure track number
        TrackNumInfo info = getTrackNumInfo(pos);
        // XXX---- (trackno == 0, numtracks == 7, num == 3)
        if ((info.trackno == 0) && (delta < 0))
        {
            return false;       // track is already first track
        }
        // ----XXX (trackno == 4, numtracks == 7, num == 3)
        if ((info.trackno + num >= info.numOfTracks) && (delta > 0))
        {
            return false;       // tracks are already last tracks
        }

        // remove anything but tracks
        ArrayList<AudioBaseObject> list = AudioFilters.audioPlayingList.raw.list;
        clean(list);

        // perform move operation
        if (delta == 1)
        {
            // move down the num tracks, so remove the track after the group and re-insert it before the group
            AudioBaseObject temp = list.get(info.trackno + num);
            list.remove(info.trackno + num);
            list.add(info.trackno, temp);
        }
        else
        {
            // move up the num tracks, so remove the track before the group and re-insert it after the group
            AudioBaseObject temp = list.get(info.trackno - 1);
            list.remove(info.trackno - 1);
            list.add(info.trackno - 1 + num, temp);
        }

        // re-add albums and works
        normalisePlayingList();
        return true;
    }
}
