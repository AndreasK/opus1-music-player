/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceScreen;

import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import de.kromke.andreas.utilities.AppGlobals;
import de.kromke.andreas.utilities.Utilities;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static android.os.Environment.getExternalStorageState;
import static de.kromke.andreas.opus1musicplayer.UserSettings.PREF_ALWAYS_GET_PATHS_FOR_ANDROID_DB;
import static de.kromke.andreas.opus1musicplayer.UserSettings.PREF_DEBUG_ALWAYS_RECREATE_DB;
import static de.kromke.andreas.opus1musicplayer.UserSettings.PREF_MERGE_ALBUMS_OF_SAME_NAME;
import static de.kromke.andreas.opus1musicplayer.UserSettings.PREF_THEME;
import static de.kromke.andreas.opus1musicplayer.UserSettings.PREF_USE_OWN_DATABASE;
import static de.kromke.andreas.opus1musicplayer.UserSettings.PREF_WHITE_BLACK_LIST_PATHS;
import static de.kromke.andreas.opus1musicplayer.UserSettings.getVal;

/**
 * combines the three preferences pages
 *  (@keep: otherwise removed by optimiser)
 */
@SuppressWarnings("JavadocBlankLines")
public class UserSettingsActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "O1M : UsrSetAct";
    private int mColourTheme = -1;

    static public class MySettingsFragment extends PreferenceFragmentCompat
    {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.preferences, rootKey);
        }
    }

    /*
     * the BEHAVIOUR settings
     */

    @SuppressWarnings({"unused", "RedundantSuppression"})
    @Keep
    static public class MyPreferenceFragmentBehaviour extends PreferenceFragmentCompat
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_behaviour, rootKey);
        }
    }

    /*
     * the VIEW settings
     */

    @SuppressWarnings({"unused", "RedundantSuppression"})
    @Keep
    static public class MyPreferenceFragmentView extends PreferenceFragmentCompat
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_view, rootKey);
        }
    }

    /*
     * the SCALE AND COLOUR settings
     * (Note that we monitor changes directly here to instantly switch the theme)
     */

    @SuppressWarnings({"unused", "RedundantSuppression"})
    @Keep
    static public class MyPreferenceFragmentScaleColourRotate extends PreferenceFragmentCompat
            implements SharedPreferences.OnSharedPreferenceChangeListener
    {
        int mFragmentColourTheme = -1;

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            mFragmentColourTheme = getVal(PREF_THEME, 0);
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_scale_and_themes, rootKey);
        }

        @Override
        public void onResume() {
            super.onResume();
            SharedPreferences shPref = getPreferenceScreen().getSharedPreferences();
            if (shPref != null)
            {
                // Set up a listener whenever a key changes
                shPref.registerOnSharedPreferenceChangeListener(this);
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            SharedPreferences shPref = getPreferenceScreen().getSharedPreferences();
            if (shPref != null)
            {
                // free listener
                shPref.unregisterOnSharedPreferenceChangeListener(this);
            }
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
        {
            if ((key != null) && key.equals(PREF_THEME))
            {
                UserSettingsActivity theActivity = (UserSettingsActivity) getActivity();
                if (theActivity != null)
                {
                    mFragmentColourTheme = theActivity.applyTheme(mFragmentColourTheme);
                }
            }
        }
    }

    /*
     * the BLUETOOTH settings
     */

    @SuppressWarnings({"unused", "RedundantSuppression"})
    @Keep
    static public class MyPreferenceFragmentBluetooth extends PreferenceFragmentCompat
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_bluetooth, rootKey);
        }
    }

    /*
     * the METADATA settings
     */

    @SuppressWarnings({"unused", "RedundantSuppression", "Convert2Lambda", "JavadocBlankLines"})
    @Keep
    static public class MyPreferenceFragmentMetadata extends PreferenceFragmentCompat
            implements SharedPreferences.OnSharedPreferenceChangeListener, ActivityCompat.OnRequestPermissionsResultCallback
    {
        private long mDownloadID;
        private BroadcastReceiver mOnDownloadCompleteReceiver;
        //private final static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_AND_INTERNET = 12;
        //private ActivityResultLauncher<String> mPermissionActivityLauncher;     // for single permission
        private ActivityResultLauncher<String[]> mPermissionActivityLauncher;
        private final static String[] sPermissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET};



        /**************************************************************************
         *
         * PreferenceFragment method
         *
         *************************************************************************/
        @Override
        @SuppressLint("UnspecifiedRegisterReceiverFlag")
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            registerPermissionCallback();
            FragmentActivity activity = getActivity();
            Preference startPictureDownload = findPreference("prefDownloadComposerPictures");
            if ((startPictureDownload != null) && (activity != null))
            {
                startPictureDownload.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(@NonNull Preference preference)
                    {
                        FragmentActivity activity = getActivity();
                        if (activity != null)
                        {
                            int permissionCheckWrite = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                            int permissionCheckInternet = ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET);

                            if ((permissionCheckWrite == PackageManager.PERMISSION_GRANTED) &&
                                    (permissionCheckInternet == PackageManager.PERMISSION_GRANTED))
                            {
                                Log.d(LOG_TAG, "write and internet permission immediately granted");
                                downloadComposers();
                            } else
                            {
                                Log.d(LOG_TAG, "request write and internet permission");
                                // deprecated:
                                /*
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET},
                                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_AND_INTERNET);
                                 */
                                mPermissionActivityLauncher.launch(sPermissions);
                            }

                            return true;
                        } else
                        {
                            return false;
                        }
                    }
                });

                mOnDownloadCompleteReceiver = new BroadcastReceiver()
                {
                    @Override
                    public void onReceive(Context context, Intent intent)
                    {
                        long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                        if (mDownloadID == id)
                        {
                            installComposers(id);
                        }
                    }
                };

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                {
                    activity.registerReceiver(mOnDownloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE), Context.RECEIVER_NOT_EXPORTED);
                }
                else
                {
                    activity.registerReceiver(mOnDownloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                }
                updateEnabledStates();
            }
        }

        /**************************************************************************
         *
         * PreferenceFragment method
         *
         *************************************************************************/
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
        {
            setPreferencesFromResource(R.xml.settings_metadata, rootKey);
            handleRestorePaths();
        }

        /**************************************************************************
         *
         * PreferenceFragment method
         *
         *************************************************************************/
        @Override
        public void onResume()
        {
            super.onResume();
            SharedPreferences shPref = getPreferenceScreen().getSharedPreferences();
            if (shPref != null)
            {
                // Set up a listener whenever a key changes
                shPref.registerOnSharedPreferenceChangeListener(this);
            }
        }

        /**************************************************************************
         *
         * PreferenceFragment method
         *
         *************************************************************************/
        @Override
        public void onPause()
        {
            super.onPause();
            SharedPreferences shPref = getPreferenceScreen().getSharedPreferences();
            if (shPref != null)
            {
                // free listener
                shPref.unregisterOnSharedPreferenceChangeListener(this);
            }
        }

        /**************************************************************************
         *
         * PreferenceFragment method
         *
         *************************************************************************/
        @Override
        public void onDestroy()
        {
            super.onDestroy();
            FragmentActivity activity = getActivity();
            if (activity != null)
            {
                activity.unregisterReceiver(mOnDownloadCompleteReceiver);
            }
        }

        /**************************************************************************
         *
         * PreferenceFragment method
         *
         *************************************************************************/
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
        {
            if ((key != null) && key.equals(PREF_USE_OWN_DATABASE))
            {
                updateEnabledStates();
            }
        }


        /************************************************************************************
         *
         * Helper to change the paths
         *
         ***********************************************************************************/
        private boolean changePaths(Context context, String newOwnDatabasePath, String newOwnPersonsPath)
        {
            boolean ret = false;
            UserSettings.setContextIfNecessary(context);

            // get current values
            String currOwnDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);
            String currOwnPersonsPath = UserSettings.getString(UserSettings.PREF_OWN_PERSONS_PATH);

            // check if there are changes
            if (Utilities.stringsDiffer(currOwnDatabasePath, newOwnDatabasePath))
            {
                UserSettings.removeVal(UserSettings.PREF_OWN_DATABASE_PATH);
                UserSettings.getAndPutString(UserSettings.PREF_OWN_DATABASE_PATH, newOwnDatabasePath);
                ret = true;
            }
            if (Utilities.stringsDiffer(currOwnPersonsPath, newOwnPersonsPath))
            {
                UserSettings.removeVal(UserSettings.PREF_OWN_PERSONS_PATH);
                UserSettings.getAndPutString(UserSettings.PREF_OWN_PERSONS_PATH, newOwnPersonsPath);
                ret = true;
            }

            return ret;
        }


        /************************************************************************************
         *
         * Helper to reset the paths
         *
         ***********************************************************************************/
        private boolean resetPaths(Context context)
        {
            boolean ret = false;
            if (getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
            {
                // get new default values
                String defaultOwnDatabasePath = Utilities.getDefaultSharedDbFilePath(context);
                String defaultOwnPersonsPath = Utilities.getDefaultOwnPersonsPicturePath(context);

                // check if there are changes
                ret = changePaths(context, defaultOwnDatabasePath, defaultOwnPersonsPath);
            }
            return ret;
        }


        /************************************************************************************
         *
         * Update GUI on path change
         *
         ***********************************************************************************/
        private void updateGui()
        {
            /* unfortunately there is no way to update elements from stored preferences
            Preference pref = findPreference("prefMusicBasePath");
            String key = pref.getKey();
            pref.setKey(key);  // update!
            pref = findPreference("prefDataBasePath");
            key = pref.getKey();
            pref.setKey(key);  // update!
            */

            /*
            // this is the method from mediascanner, but that one does not use fragments
            setPreferenceScreen(null);  // is this necessary?
            addPreferencesFromResource(R.xml.preferences);
            */
            onCreatePreferences(null, null);
            handleRestorePaths();
        }


        /************************************************************************************
         *
         * Handle the "Restore Paths" button
         *
         ***********************************************************************************/
        private void handleRestorePaths()
        {
            Preference restoreDefaultPaths = findPreference("prefRestoreDefaultPaths");
            if (restoreDefaultPaths != null)
            {
                restoreDefaultPaths.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(@NonNull Preference preference)
                    {
                        FragmentActivity activity = getActivity();
                        if (activity != null)
                        {
                            if (resetPaths(activity))
                            {
                                Toast.makeText(activity, R.string.str_paths_updated, Toast.LENGTH_LONG).show();
                                updateGui();
                            }
                            else
                            {
                                Toast.makeText(activity, R.string.str_paths_unchanged, Toast.LENGTH_LONG).show();
                            }

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                });
            }
        }


        /**************************************************************************
         *
         * helper for deprecated startActivityForResult()
         *
         *************************************************************************/
        private void registerPermissionCallback()
        {
            mPermissionActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.RequestMultiplePermissions(),
                new ActivityResultCallback<Map<String, Boolean>>()
                {
                    @Override
                    public void onActivityResult(Map<String, Boolean> result)
                    {
                        // walk through the map
                        boolean bAllGranted = true;
                        for (Map.Entry<String, Boolean> x : result.entrySet())
                        {
                            final String key = x.getKey();
                            boolean v = x.getValue();
                            Log.d(LOG_TAG, "onActivityResult(): permission " + key + ((v) ? " granted" : " denied"));
                            bAllGranted = (bAllGranted && v);       // one denied -> bAllGranted == false
                        }

                        if (bAllGranted)
                        {
                            Log.d(LOG_TAG, "onActivityResult(): all permissions granted");
                            downloadComposers();
                        }
                        else
                        {
                            Log.d(LOG_TAG, "onActivityResult(): some permission denied");
                            Toast.makeText(getActivity(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                        }
                    }
                });
        }


        /* *************************************************************************
         *
         * called when access request has been denied or granted
         *
         *************************************************************************/
        /*
        @Override
        public void onRequestPermissionsResult
        (
            int requestCode,
            @NonNull String[] permissions,
            @NonNull int[] grantResults
        )
        {
            Log.d(LOG_TAG, "onRequestPermissionsResult()");

            //noinspection SwitchStatementWithTooFewBranches
            switch (requestCode)
            {
                case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE_AND_INTERNET:
                {
                    // If request is cancelled, the result arrays are empty.
                    if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                    {
                        Log.d(LOG_TAG, "onRequestPermissionsResult(): permission granted");
                        downloadComposers();
                    } else {
                        Log.d(LOG_TAG, "onRequestPermissionsResult(): permission denied");
                        Toast.makeText(getActivity(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                    }
                }
                break;
            }
        }
         */


        /**************************************************************************
         *
         * pass a request to the Android Download Manager to get the composer
         * names zip file.
         *
         *************************************************************************/
        private void downloadComposers()
        {
            // https://androidclarified.com/android-downloadmanager-example/
            Uri uri = Uri.parse("https://gitlab.com/AndreasK/assets/-/raw/master/composers/Komponisten_gpl.zip");
            //noinspection ExtractMethodRecommender
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle("Composer Pictures");
            request.setDescription("Downloading");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setVisibleInDownloadsUi(true);
            //request.setDestinationUri(Uri.parse("file://" + folderName + "/myfile.mp3"));
            //request.setDestinationInExternalFilesDir(getActivity(), null, "Komponisten_gpl.zip");
            request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, "Komponisten_gpl.zip" /*AppGlobals.sOwnDatabasePath*/);
            FragmentActivity activity = getActivity();
            if (activity != null)
            {
                DownloadManager downloadmanager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                mDownloadID = downloadmanager.enqueue(request);
                Log.d(LOG_TAG, "downloadComposers() : request ID is " + mDownloadID);
            }
        }


        /**************************************************************************
         *
         * helper
         *
         *************************************************************************/
        private void installComposers(long id)
        {
            FragmentActivity activity = getActivity();
            if (activity != null)
            {
                DownloadManager downloadmanager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                File destDir = new File(AppGlobals.sOwnDatabasePath);
                int numFiles;
                // WARN: try (bla) automatically closes bla outside this block!
                try (ParcelFileDescriptor pfd = downloadmanager.openDownloadedFile(id))
                {
                    InputStream zipFile = new FileInputStream(pfd.getFileDescriptor());
                    numFiles = Utilities.unzip(zipFile, destDir);
                    downloadmanager.remove(id);     // remove file
                } catch (IOException e)
                {
                    //noinspection CallToPrintStackTrace
                    e.printStackTrace();
                    numFiles = -1;
                }

            /*
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                AppGlobals.createAliases(destDir);
            }
            */

                String text;
                if (numFiles >= 0)
                {
                    text = numFiles + getString(R.string.str_files_downloaded);
                } else
                {
                    text = numFiles + getString(R.string.str_download_failure);
                }
                Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
            }
        }


        /**************************************************************************
         *
         * enables and disables settings due to preference changes
         *
         *************************************************************************/
        private void updateEnabledStates()
        {
            PreferenceScreen scPref = getPreferenceScreen();
            SharedPreferences shPref = scPref.getSharedPreferences();
            if (shPref != null)
            {
                boolean bUseOwn = shPref.getBoolean(PREF_USE_OWN_DATABASE, false);
                Preference pref = scPref.findPreference(PREF_MERGE_ALBUMS_OF_SAME_NAME);
                Preference pref2 = scPref.findPreference(PREF_ALWAYS_GET_PATHS_FOR_ANDROID_DB);
                Preference pref3 = scPref.findPreference(PREF_DEBUG_ALWAYS_RECREATE_DB);
                Preference pref4 = scPref.findPreference(PREF_WHITE_BLACK_LIST_PATHS);
                if (pref != null)
                {
                    pref.setEnabled(!bUseOwn);
                }
                if (pref2 != null)
                {
                    pref2.setEnabled(!bUseOwn);
                }
                if (pref3 != null)
                {
                    pref3.setEnabled(!bUseOwn);
                }
                if (pref4 != null)
                {
                    pref4.setEnabled(!bUseOwn);
                }
            }
        }
    }

    /*
    @Override
    protected void onPause()
    {
        Log.d(LOG_TAG, "onPause()");
        super.onPause();
    }
    */

    // sophisticated method to apply theme changed by fragment also to main preferences
    @Override
    protected void onResume()
    {
        Log.d(LOG_TAG, "onResume()");
        int colourTheme = getVal(PREF_THEME, 0);
        if (colourTheme != mColourTheme)
        {
            mColourTheme = applyTheme(mColourTheme);
        }
        super.onResume();
    }

    /* THIS IS NEEDED IN CASE THERE ARE TWO LEVELS OF SETTINGS, A HEADER AND SOME FRAGMENTS */
    /*
    @Override
    public void onBuildHeaders(List<Header> target)
    {
        loadHeadersFromResource(R.xml.headers_settings, target);
    }
    */

    /*
    @Override
    protected boolean isValidFragment(String fragmentName)
    {
        return MyPreferenceFragmentBehaviour.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentView.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentScaleColourRotate.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentBluetooth.class.getName().equals(fragmentName) ||
                MyPreferenceFragmentMetadata.class.getName().equals(fragmentName);
    }
    /*
     */

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        UserSettings.init(this);    // in case we are the first activity to run
        mColourTheme = applyTheme(-1);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new MySettingsFragment())
                .commit();
    }


    /**************************************************************************
     *
     * update colour theme etc., if necessary, and return new value
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    private int applyTheme(int prevTheme)
    {
        int colourTheme = getVal(PREF_THEME, 0);
        if ((prevTheme < 0) || (prevTheme != colourTheme))
        {
            mColourTheme = colourTheme;
            int resId;
            switch (mColourTheme)
            {
                //noinspection DefaultNotLastCaseInSwitch
                default:
                case 0:
                    resId = R.style.UserSettingsTheme_BlueLight;
                    break;
                case 1:
                    resId = R.style.UserSettingsTheme_OrangeLight;
                    break;
                case 2:
                    resId = R.style.UserSettingsTheme_GreenLight;
                    break;
                case 3:
                    resId = R.style.UserSettingsTheme_LightGrey;
                    break;
                case 4:
                    resId = R.style.UserSettingsTheme_DarkGrey;
                    break;
                case 5:
                    resId = R.style.UserSettingsTheme_Blue;
                    break;
            }
            setTheme(resId);

            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                getWindow().setStatusBarColor(UserSettings.getThemedColourFromResId(this, R.attr.colorPrimaryDark));
            }
        }

        return colourTheme;
    }

}
