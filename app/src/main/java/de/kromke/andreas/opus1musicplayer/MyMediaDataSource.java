/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.content.ContentResolver;
import android.media.MediaDataSource;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.RequiresApi;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Byte buffer source for MediaPlayer
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class MyMediaDataSource extends MediaDataSource
{
    private byte[] mBuf;

    /*
    public MyMediaDataSource(byte[] buf)
    {
        super();
        mBuf = buf;
        mInputStream = new ByteArrayInputStream(buf);
    }
    */

    MyMediaDataSource(ContentResolver theContentResolver, Uri uri) throws IOException
    {
        InputStream is = theContentResolver.openInputStream(uri);
        if (is == null)
        {
            throw new FileNotFoundException();
        }
        mBuf = readCompleteStream(is);
        is.close();
    }

    public long getSize()
    {
        return mBuf.length;
    }

    public int readAt(long position, byte[] buffer, int offset, int size)
    {
        // avoid source buffer overflow, i.e. do not read behind end
        long src_remaining = mBuf.length - position;
        if (src_remaining <= 0)
        {
            return -1;      // end-of-file
        }
        if (size > src_remaining)
        {
            size = (int) src_remaining;
        }

        // avoid destination buffer overflow, i.e. do not write behind end
        long dst_remaining = buffer.length - offset;
        if (dst_remaining <= 0)
        {
            return 0;      // destination buffer overflow
        }
        if (size > dst_remaining)
        {
            size = (int) dst_remaining;
        }

        System.arraycopy(mBuf, (int) position, buffer, offset, size);
        return size;
    }

    @Override
    public void close()
    {
        mBuf = null;
    }

    // helper
    private byte[] readCompleteStream(InputStream is) throws IOException
    {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = is.read(data, 0, data.length)) != -1)
        {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        return buffer.toByteArray();
    }
}
