/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import de.kromke.andreas.utilities.*;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import androidx.appcompat.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.content.Context;

import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import static android.provider.MediaStore.getExternalVolumeNames;
import static android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;
import static de.kromke.andreas.utilities.AppGlobals.getAudioTrack;
import static de.kromke.andreas.utilities.AppGlobals.getTracksOfObject;
import static de.kromke.andreas.utilities.SafUtilities.isSafPath;
import static de.kromke.andreas.utilities.UriToPath.getPathFromIntentUri;
import static java.lang.System.identityHashCode;
import de.kromke.andreas.utilities.PrivateLog;


/** @noinspection CommentedOutCode, ExtractMethodRecommender */
@SuppressWarnings({"Convert2Lambda", "JavadocBlankLines", "JavadocLinkAsPlainText"})
public class MainBasicActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener,
        OnItemSelectedListener
{
    private static final String LOG_TAG = "MainBasicActivity";
    private static final String STATE_FRAGMENT = "fragment";

    private static final String sIntentKeyDatabasePath = "databasePath";

    public String mUniqueHash;   // for debugging purposes, to check if there are multiple instances
    private final static int MY_PERMISSIONS_REQUEST = 11;
    private static final int timerFrequency = 1000;         // milliseconds
    static boolean isInitialised = false;                   // a process restart will reset this variable to "false"
    protected boolean bRestarting = false;
    private AlertDialog mPendingDialog = null;       // test code

    boolean mbBackgroundInitialisationIsRunning = false;
    //int initProgressCountdown;
    //int initProgressReload;
    static Toast theToast;
    Toolbar mToolbar;
    SearchView mSearchView;
    Spinner mSpinner;                       // the drop down menu that switches between fragments
    MainSpinnerAdapter mSpinnerAdapter;     // adapter for the drop down menu
    MainFragment mCurrFragment = null;
    MenuItem mMenuItemSavePlaylist;         // must be dynamically enabled and disabled
    MenuItem mMenuItemAutoRepeat;
    MenuItem mMenuItemStartTagger;
    MenuItem mMenuItemRebuildDb;
    MenuItem mMenuItemReloadDb;
    MenuItem mMenuItemScan;
    MenuItem mMenuItemGridView;
    MenuItem mMenuItemManageFiles;
    MenuItem mMenuItemOpenPath;
    protected ActionMode.Callback mActionModeCallback = null;
    protected ActionMode mActionMode = null;
    private AudioBaseObject mActionModeSelectedObject = null;
    private int mActionModeSelectedPos = -1;
    int mRequestedFragmentId = 0;
    protected ProgressBar mBusyIndicatorProgressBar;

    // cached settings
    int mColourTheme = -1;
    int mRotationMode = -1;
    int mGenderismMode = 0;
    boolean mbUseOwnDatabase = false;
    private static String sSafDatabasePath = null;     // if set, use this one and not the shared one
    String mOwnDatabasePath = "";
    boolean mbMergeAlbums;
    String mFilterPaths = null;
    boolean mbUsePathsForAndroidDbAudioFiles;
    int mHandlePersonsLists;
    boolean mbComposerDecompose;
    boolean mbComposerLastNameFirst;
    int mWorkPictureMode;
    String mOwnPersonsPicturePath = "";
    private boolean mbShowTrackNumbers;
    private int mShowConductorMode;
    private boolean mbShowSubtitle;
    private boolean mbGridView;
    boolean mbShowAlbumDuration;
    int mImageSizeForListPictures;
    int mImageSizeForHeaderPictures;
    int mImageSizeForGrid;

    // timer
    Timer mTimer;
    MyTimerTask mTimerTask;
    Timer mProgressTimer;
    MyProgressTimerTask mProgressTimerTask;

    //activity and playback pause flags
    private boolean mActivityPaused = false;
    private boolean mbMustRecreate = false;

    //data exchange with dialogue
    boolean[] mCheckedItems = { false, false, false };
    static boolean rba_checked = true;
    static boolean rbw_checked = false;
    static boolean rbn_checked = false;

    ActivityResultLauncher<Intent> mPreferencesActivityLauncher;
    ActivityResultLauncher<Intent> mRequestDirectorySelectActivityLauncher;
    ActivityResultLauncher<Intent> mRequestFileSelectActivityLauncher;
    ActivityResultLauncher<Intent> mStorageAccessPermissionActivityLauncher;
    boolean mbDirectorySelectForSaf = false;    // otherwise: path

    //static boolean rbf_checked = true;
    //static boolean rbc_checked = false;


    /**************************************************************************
     *
     * activity is about to be destroyed
     *
     *************************************************************************/
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        PrivateLog.d(LOG_TAG, "onSaveInstanceState()");
        savedInstanceState.putInt(STATE_FRAGMENT, mRequestedFragmentId);
        PrivateLog.d(LOG_TAG, "onSaveInstanceState() : save fragment no " + mRequestedFragmentId);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    /************************************************************************************
     *
     * Activity method: create the activity, holding all tabs
     *
     *  Note that this is also called in case the device had been turned, or
     *  in case the device had been shut off for a while. In this case we only have
     *  to re-initialise the GUI, but not the tables, database etc.
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        mUniqueHash = String.format("0x%08x", identityHashCode(this));
        PrivateLog.start();
        PrivateLog.d(LOG_TAG, "onCreate(" + mUniqueHash + ") -- API level = " + Build.VERSION.SDK_INT);

        // BUG: This call has a side effect: It tries to restore the previous state and instantiates
        //      a fragment without calling newInstance(). This leads to a fragment that will be destroyed
        //      again some moments later, in onResume(). Note that the fragment manager has stored the argument and
        //      will pass it to the newly created fragment.
        super.onCreate(savedInstanceState);

        // registering for callback must be done here, not later
        registerPreferencesCallback();
        registerDirectorySelectCallback();
        registerFileSelectCallback();
        registerStorageAccessPermissionCallback();

        // restore state
        if (savedInstanceState != null)
        {
            mRequestedFragmentId  = savedInstanceState.getInt(STATE_FRAGMENT);
            PrivateLog.d(LOG_TAG, "onCreate() : restore fragment id " + mRequestedFragmentId + " from saved instance state");
        }
        else
        {
            mRequestedFragmentId = MainFragment.theAlbumFragment;
        }
/*
        {
            PrivateLog.v("AppGlobals", "################ HACK ##################");
            AudioFileInfo t = AudioFileInfo.getFromPath("/storage/sdcard0/Music/Rodrigo - Concierto Heroico (.aac)/Joaquin-Rodrigo - Concierto Heroico - 1. Satz.aac");
            assert(false);
        }
*/
        /*
        // handle restart due to database rebuild
        if (rebuildInternalDb)
        {
            PrivateLog.d(LOG_TAG, "complete restart due to internal db recreation");
            isInitialised = false;  // run like newly started
        }
        */

        // all needed preference settings
        UserSettings.init(this);
        boolean bRememberSafMode = UserSettings.getBool(UserSettings.PREF_REMEMBER_LATEST_SAF_DB_PATH, false);
        String savedSafDatabasePath = bRememberSafMode ? UserSettings.getString(UserSettings.PREF_LATEST_SAF_DB_PATH) : null;

        if (sSafDatabasePath == null)
        {
            Intent intent = getIntent();
            if (intent != null)
            {
                // if that was a restart, get saf db path from intent
                sSafDatabasePath = intent.getStringExtra(sIntentKeyDatabasePath);
                if (sSafDatabasePath != null)
                {
                    PrivateLog.d(LOG_TAG, "onCreate() : got db path from intent:" + sSafDatabasePath);
                }
            }
        }

        if (sSafDatabasePath == null)
        {
            sSafDatabasePath = savedSafDatabasePath;
            if (sSafDatabasePath != null)
            {
                PrivateLog.d(LOG_TAG, "onCreate() : got db path from settings:" + sSafDatabasePath);
            }
        }

        //
        // check saf db path
        //

        if (sSafDatabasePath != null)
        {
            File f = new File(sSafDatabasePath);
            if (f.canRead())
            {
                PrivateLog.d(LOG_TAG, "onCreate() : use saf db path: " + sSafDatabasePath);
            }
            else
            {
                PrivateLog.d(LOG_TAG, "onCreate() : remove invalid stored saf db path " + sSafDatabasePath);
                sSafDatabasePath = null;
                if (savedSafDatabasePath != null)
                {
                    UserSettings.putString(UserSettings.PREF_LATEST_SAF_DB_PATH, null);
                }
            }
        }

        /* DEBUG CODE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            java.util.Set<String> volumes = getExternalVolumeNames(this);
            PrivateLog.d(LOG_TAG, "onCreate() : external volume names: " + volumes);
        }
        */ //END DEBUG CODE

        applySettingsChanges();     // ignore return value as we are already starting
        //updateSettings();

        setContentView(R.layout.activity_main);

        // Setup Toolbar
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        // setup global "busy" indicator
        mBusyIndicatorProgressBar = findViewById(R.id.busyProgressIndicator);
        if (!mbBackgroundInitialisationIsRunning)
        {
            mBusyIndicatorProgressBar.setProgress(0);
            mBusyIndicatorProgressBar.setVisibility(View.GONE);
        }

        initActionModeContextMenu();

        // We need the old (pre-Android-11) file read permissions at once, otherwise
        // we cannot run on older Android versions. Later we may ask for full
        // file access on Android 11 and above.
        requestForPermissionOld();

        /*
        // search stuff
        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction()))
        {
            String query = intent.getStringExtra(SearchManager.QUERY);
            PrivateLog.d(LOG_TAG, "query = " + query);
        }
        */

        // Check if app has been updated. If yes, show list of changes.
        // If preference value does not exist, yet, also show the dialogue

        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);
        int newVersionCode = info.versionCode;
        int oldVersionCode = UserSettings.updateVal(UserSettings.PREF_INSTALLED_APP_VERSION, newVersionCode, -1);
        if (oldVersionCode != newVersionCode)
        {
            dialogChanges();
        }

        PrivateLog.d(LOG_TAG, "onCreate() - done");
    }


    /**************************************************************************
     *
     * called after onCreate() or after the application has been put back to
     * the foreground.
     * After onStart() the method onResume() is called.
     *
     *************************************************************************/
    @Override
    protected void onStart()
    {
        PrivateLog.d(LOG_TAG, "onStart(" + mUniqueHash + ")");

        // manage progress bar via timer
        mTimerTask = new MyTimerTask();
        //delay 1000ms, repeat in <timerFrequency>ms
        mTimer = new Timer();
        mTimer.schedule(mTimerTask, 1000, timerFrequency);

        super.onStart();
        PrivateLog.d(LOG_TAG, "onStart() - done");
    }

    @Override
    protected void onResume()
    {
        PrivateLog.d(LOG_TAG, "onResume(" + mUniqueHash + ")");
        super.onResume();

        // make sure Volume up/down keys change music volume, not ringer volume
        // (moved from onCreate() to here, hoping that it will work more reliably)
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        if (mActivityPaused)
        {
            mActivityPaused = false;
            int currFragment = (mCurrFragment != null) ? mCurrFragment.mSectionNumber : -1;
            if (currFragment != mRequestedFragmentId)
            {
                // could not change fragment in PAUSED state. Do it now.
                setFragment(mRequestedFragmentId);
            }
        }

        PrivateLog.d(LOG_TAG, "onResume() - done");
    }


    /**************************************************************************
     *
     * Called after onResume(), but only once, i.e. not called after pause/resume (?)
     *
     *************************************************************************/
    @Override
    public void onAttachedToWindow()
    {
        PrivateLog.d(LOG_TAG, "onAttachedToWindow(" + mUniqueHash + ")");
        super.onAttachedToWindow();
    }


    /**************************************************************************
     *
     * Called before the application is put to the background.
     * After onPause() the method onStop() is called.
     *
     *************************************************************************/
    @Override
    protected void onPause()
    {
        PrivateLog.d(LOG_TAG, "onPause(" + mUniqueHash + ")");
        super.onPause();
        mActivityPaused = true;
        PrivateLog.d(LOG_TAG, "onPause() - done");
    }

    @Override
    protected void onStop()
    {
        PrivateLog.d(LOG_TAG, "onStop(" + mUniqueHash + ")");

        // stop timer
        mTimer.cancel();    // note that a cancelled timer cannot be re-scheduled. Why not?
        mTimer = null;
        mTimerTask = null;

        super.onStop();
        PrivateLog.d(LOG_TAG, "onStop() - done");
    }

    @Override
    protected void onDestroy()
    {
        PrivateLog.d(LOG_TAG, "onDestroy(" + mUniqueHash + ")");

        if (mPendingDialog != null)
        {
            if (mPendingDialog.isShowing())
            {
                mPendingDialog.dismiss();
            }
            mPendingDialog = null;
        }

        super.onDestroy();
        PrivateLog.d(LOG_TAG, "onDestroy() - done");
    }


    /************************************************************************************
     *
     * Activity method: got new intent
     *
     * This function may be called in case of a "open with" or "share" operation.
     *
     ***********************************************************************************/
    @Override
    protected void onNewIntent(Intent theIntent)
    {
        PrivateLog.d(LOG_TAG, "onNewIntent(): intent action is " + theIntent.getAction());
        setIntent(theIntent);

        if (!handleIntent())
        {
            super.onNewIntent(theIntent);
        }
    }


    /************************************************************************************
     *
     * open passed files (tracks) in selection view
     *
     ***********************************************************************************/
    private boolean handleIntent()
    {
        Intent theIntent = getIntent();
        if (theIntent == null)
        {
            return true;
        }

        PrivateLog.d(LOG_TAG, "handleIntent(): intent action is " + theIntent.getAction());

        String action = theIntent.getAction();
        ArrayList<Uri> newObjectList = new ArrayList<>();

        //
        // official messages like SHARE and OPEN_WITH
        //

        if ((Intent.ACTION_VIEW.equals(action)) ||
                (Intent.ACTION_EDIT.equals(action)))
        {
            Uri theUri = theIntent.getData();
            if (theUri != null)
            {
                newObjectList.add(theUri);
            }
        }
        else
        if ((Intent.ACTION_SEND.equals(action)))
        {
            Uri theUri = theIntent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (theUri != null)
            {
                newObjectList.add(theUri);
            }
        }
        else
        if ((Intent.ACTION_SEND_MULTIPLE.equals(action)))
        {
            ArrayList<Uri> uriList = theIntent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
            if (uriList != null)
            {
                newObjectList.addAll(uriList);
            }
        }
        else
        {
            return false;
        }

        ArrayList<AudioBaseObject> addList = new ArrayList<>();
        int trackNo = 1;
        for (Uri uri : newObjectList)
        {
            AudioTrack theTrack;
            final String thePath = getPathFromIntentUri(this, uri);
            if (thePath != null)
            {
                // search path in track list
                theTrack = getAudioTrack(thePath);
            }
            else
            {
                // search Uri in track list
                theTrack = getAudioTrack(uri.toString());
            }

            if (theTrack == null)
            {
                DocumentFile df = DocumentFile.fromSingleUri(this, uri);
                if (df != null)
                {
                    theTrack = new AudioTrack(
                            -1, trackNo,
                            df.getName(),   // title
                            null,           // album
                            -1,             // album id
                            uri.toString(),
                            null,
                            -1, null, 0);
                }
            }

            if (theTrack != null)
            {
                addList.add(theTrack);
                trackNo++;
            }
        }

        putToTrackList(addList);

        return true;
    }


    /**************************************************************************
     *
     * setup spinner
     *
     *************************************************************************/
    private void initSpinner()
    {
        String[] fragmentText = new String[]
                {
                        getString(R.string.str_albums_uc),
                        getString(R.string.str_works_uc),
                        getString(R.string.str_selection_uc),
                        getString(R.string.str_playlists_uc),
                        getString(R.string.str_folders_uc),
                        AppGlobals.getGenderedComposers(this, mGenderismMode),
                        getString(R.string.str_genres_uc),
                        AppGlobals.getGenderedPerformers(this, mGenderismMode),
                        getString(R.string.str_playing_uc)
                };
        //noinspection ConstantConditions
        if ((BuildConfig.DEBUG) && (fragmentText.length != MainFragment.nFragments))
        {
            throw new AssertionError("wrong dimension");
        }

        mSpinnerAdapter = new MainSpinnerAdapter(
                mToolbar.getContext(),
                fragmentText);
        mSpinner = findViewById(R.id.spinner);
        // TEST CODE
        /*
        PrivateLog.d(LOG_TAG, "initAlbumList() - spinner offset = " + mSpinner.getDropDownHorizontalOffset());
        mSpinner.setDropDownHorizontalOffset(3);
        PrivateLog.d(LOG_TAG, "initAlbumList() - spinner offset = " + mSpinner.getDropDownHorizontalOffset());
        */

        mSpinner.setAdapter(mSpinnerAdapter);
        mSpinner.setOnItemSelectedListener(this);
    }


    /**************************************************************************
     *
     * called either at activity creation or when request has been
     * granted
     *
     *************************************************************************/
    private void initAlbumList()
    {
        PrivateLog.d(LOG_TAG, "initAlbumList(isInitialised = " + isInitialised + ")");

        if (!isInitialised)
        {
            //
            // the process has been either started or restarted
            // create global variables
            //
            boolean bDebugRecreate = UserSettings.getBool(UserSettings.PREF_DEBUG_ALWAYS_RECREATE_DB,false);
            boolean rebuildInternalDb = (UserSettings.getVal(UserSettings.PREF_REBUILD_DB, 0) != 0) ||
                                        (bDebugRecreate);
            if (rebuildInternalDb)
            {
                PrivateLog.d(LOG_TAG, "initAlbumList(): shall rebuild internal db");
                String text = (bDebugRecreate ? "DEBUG: " : "") +
                        getString(R.string.str_rebuilding_internal_db);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
                UserSettings.putVal(UserSettings.PREF_REBUILD_DB, 0);
            }

            String dbPath;
            boolean bOwnDb;
            boolean bOwnDbIsSaf;
            if (sSafDatabasePath != null)
            {
                bOwnDb = true;
                bOwnDbIsSaf = true;
                dbPath = sSafDatabasePath;
            }
            else
            {
                bOwnDbIsSaf = false;
                bOwnDb = mbUseOwnDatabase;
                dbPath = mOwnDatabasePath;
            }
            /* jaudiotagger removed */
            //boolean bUseJaudioTaglib = UserSettings.getBool(UserSettings.PREF_USE_JAUDIOTAGGER_LIB, false);
            boolean bUsePathsForAndroidDbAudioFiles = UserSettings.getBool(UserSettings.PREF_ALWAYS_GET_PATHS_FOR_ANDROID_DB, false);

            AppGlobals.initAppGlobals(
                    this,
                    getContentResolver(),
                    rebuildInternalDb,
                    mFilterPaths,
                    mbMergeAlbums,
                    mHandlePersonsLists,
                    bOwnDb, bOwnDbIsSaf,
                    // bUseJaudioTaglib, /* jaudiotagger removed */
                    bUsePathsForAndroidDbAudioFiles,
                    dbPath);

            if (mbUseOwnDatabase && !AppGlobals.bOwnDatabaseIsActive)
            {
                Toast.makeText(getApplicationContext(), R.string.str_open_db_failure, Toast.LENGTH_LONG).show();
            }

            // we need the album list for background initialisation, for linking work and album etc.
            AppGlobals.initAudioAlbumList(); // do this in foreground

            // background initialisation
            AppGlobals.theBackgroundInitialisation = new BackgroundInitialisation().execute(this /* arbitrary number of arguments */);

            isInitialised = true;   // this variable will stay "true" until process restart
        }
        else
        if (!AppGlobals.bBackgroundInitialisationDone)
        {
            PrivateLog.d(LOG_TAG, "initAlbumList(): background init is running");
            //initProgressCountdown = 3;
            //initProgressReload = 7;
            mbBackgroundInitialisationIsRunning = true;
            startProgressBar();
        }
        else
        {
            handleIntent();
        }

        initSpinner();

        PrivateLog.d(LOG_TAG, "initAlbumList(): done");
    }


    /**************************************************************************
     *
     * permission was granted, immediately or later
     *
     *************************************************************************/
    private void onPermissionGranted()
    {
        initAlbumList();
        //handleIntent();
    }


    /**************************************************************************
     *
     * Ask for READ_EXTERNAL_STORAGE permission
     * (Code for Android 4.4 .. 10, but also used for later versions)
     *
     ************************************************************************
     * @noinspection ExtractMethodRecommender*/
    private void requestForPermissionOld()
    {
        String[] requests;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            // Android 13 needs different requests than 4..12
            requests = new String[]{
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_IMAGES,
                    Manifest.permission.POST_NOTIFICATIONS
            };
        }
        else
        {
            // this one is invalid in Android 13
            requests =  new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        }

        //
        // Count number of already granted permissions
        //

        int granted = 0;
        for (String request : requests)
        {
            int permissionCheck = ContextCompat.checkSelfPermission(this, request);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED)
            {
                granted++;
            }
        }

        //
        // if not all have been granted, request them all
        //

        if (granted == requests.length)
        {
            PrivateLog.d(LOG_TAG, "permission(s) immediately granted");
            onPermissionGranted();
        } else
        {
            PrivateLog.d(LOG_TAG, "request permission(s)");
            ActivityCompat.requestPermissions(this, requests, MY_PERMISSIONS_REQUEST);
        }
    }


    /**************************************************************************
     *
     * Permission request for Android 11 (API 30)
     *
     * Note that we open the permission dialogue of the system, regardless of
     * the current permission state, so that the user can not only grant, but
     * also revoke the permission.
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.R)
    private void requestForManageAllFiles()
    {
        Intent intent = new Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        mStorageAccessPermissionActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * called when READ_EXTERNAL_STORAGE access request has been denied or
     * granted, not used for MANAGE_ALL_FILES permission request
     *
     *************************************************************************/
    @Override
    public void onRequestPermissionsResult
    (
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults
    )
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PrivateLog.d(LOG_TAG, "onRequestPermissionsResult()");

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    PrivateLog.d(LOG_TAG, "onRequestPermissionsResult(): permission granted");
                    initAlbumList();
                } else
                {
                    PrivateLog.e(LOG_TAG, "onRequestPermissionsResult(): permission denied");
                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                }
            }
            break;

            /*
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
            {
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    PrivateLog.d(LOG_TAG, "onRequestPermissionsResult(): write permission granted");
                } else {
                    PrivateLog.d(LOG_TAG, "onRequestPermissionsResult(): write permission denied");
                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                }
            }

            case MY_PERMISSIONS_REQUEST_INTERNET:
            {
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    PrivateLog.d(LOG_TAG, "onRequestPermissionsResult(): internet permission granted");
                } else {
                    PrivateLog.d(LOG_TAG, "onRequestPermissionsResult(): internet permission denied");
                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                }
            }
           */
        }
    }


    /**************************************************************************
     *
     * Ask to proceed to Play Store
     *
     *************************************************************************/
    public void dialogOpenPlayStore(final String packageName)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_app_install);
        builder.setMessage(getString(R.string.str_app_install_msg) + "\n(" + packageName + ")");

        builder.setPositiveButton(R.string.str_play_store, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                try
                {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                } catch (android.content.ActivityNotFoundException anfe)
                {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)));
                }
            }
        });

        builder.setNeutralButton(R.string.str_fdroid, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://f-droid.org/packages/" + packageName)));
            }
        });

        builder.setNegativeButton(R.string.str_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                PrivateLog.d(LOG_TAG, "cancelled");
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * start scanner or tagger
     *
     *************************************************************************/
    private void startOtherApp(final String packageName, ArrayList<String> audioPathList)
    {
        // create an Intent with CATEGORY_LAUNCHER
        Intent theIntent = getPackageManager().getLaunchIntentForPackage(packageName);
        if (theIntent != null)
        {
            //
            // tagger or scanner is installed, start it
            //

            ArrayList<Uri> uriList = new ArrayList<>();

            if (audioPathList != null)
            {
                // Do not use foreach() when modifying the list inside the loop,
                // thus use iterator!
                Iterator<String> itr = audioPathList.iterator();
                while (itr.hasNext())
                {
                    String path = itr.next();
                    if (isSafPath(path))
                    {
                        uriList.add(Uri.parse(path));
                        itr.remove();
                    }
                }

                if (!uriList.isEmpty())
                {
                    // replace category to match that in Tagger's Manifest, otherwise
                    // an exception will occur, but FWFR not if app has full file access
                    theIntent.removeCategory(Intent.CATEGORY_LAUNCHER);
                    theIntent.addCategory(Intent.CATEGORY_DEFAULT);

                    theIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
                    theIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList);
                    theIntent.setType("audio/*");
                }
                if (!audioPathList.isEmpty())
                {
                    theIntent.putExtra("pathTable", audioPathList);
                }
            }

            try
            {
                startActivity(theIntent);
            }
            catch (Exception e)
            {
                // This crash only occurs if "full file access" is NOT granted.
                // See also:
                //  https://github.com/android/android-test/issues/1412
                //  https://medium.com/androiddevelopers/making-sense-of-intent-filters-in-android-13-8f6656903dde
                PrivateLog.e(LOG_TAG, "startOtherApp(): " + e);
                Toast.makeText(getApplicationContext(), getString(R.string.error_starting) + packageName, Toast.LENGTH_LONG).show();
            }
        } else
        {
            //
            // tagger or scanner is not installed, ask if to proceed to play store
            //

            dialogOpenPlayStore(packageName);
        }
    }


    /**************************************************************************
     *
     * start tagger with list
     *
     *************************************************************************/
    public void startTagger(ArrayList<String> audioPathList)
    {
        if (!audioPathList.isEmpty())
        {
            startOtherApp("de.kromke.andreas.musictagger", audioPathList);
        }
    }


    /**************************************************************************
     *
     * start tagger with Audio Object
     *
     *************************************************************************/
    public void startTaggerWithObject(AudioBaseObject theObject)
    {
        ArrayList<AudioBaseObject> tempList = getTracksOfObject(theObject);
        ArrayList<String> audioPathList = new ArrayList<>();
        int n = tempList.size();
        for (int i = 0; i < n; i++)
        {
            AudioBaseObject theTrack = tempList.get(i);
            if (theTrack != null)
            {
                audioPathList.add(theTrack.getPath());
            }
        }

        startTagger(audioPathList);
    }


    /**************************************************************************
     *
     * remove this item (with subitems) from playing list
     *
     *************************************************************************/
    public void removeFromPlayingList(AudioBaseObject theObject)
    {
        ArrayList<AudioBaseObject> tempList = getTracksOfObject(theObject);
        if (AudioFilters.removeObjectsFromPlayingList(tempList))
        {
            PlayListHandler.normalisePlayingList();        // (re-)arrange album and work rows
            if ((mCurrFragment != null) && (mCurrFragment.mSectionNumber == MainFragment.thePlayingFragment))
            {
                mCurrFragment.forceRedraw();
                // force redraw
                mSpinnerAdapter.notifyDataSetChanged();
            }
        }
    }


    /**************************************************************************
     *
     * move this item (with subitems) up or down playing list
     *
     *************************************************************************/
    public void moveInPlayingList(AudioBaseObject theObject, int pos, int delta)
    {
        ArrayList<AudioBaseObject> tempList = getTracksOfObject(theObject);
        int num = tempList.size();
        if (AudioFilters.audioPlayingList != null)
        {
            boolean bDone = PlayListHandler.moveInPlayingList(pos, num, delta);
            if ((bDone) && (mCurrFragment != null) && (mCurrFragment.mSectionNumber == MainFragment.thePlayingFragment))
            {
                mCurrFragment.forceRedraw();
                // force redraw
                mSpinnerAdapter.notifyDataSetChanged();
            }
        }
    }


    /**************************************************************************
     *
     * start tagger
     *
     *************************************************************************/
    public void startTaggerWithPlayingList()
    {
        ArrayList<String> audioPathList = new ArrayList<>();
        int n = PlayListHandler.getNumOfEntries();
        for (int i = 0; i < n; i++)
        {
            AudioTrack theTrack = PlayListHandler.getAudioTrack(i);
            if (theTrack != null)
            {
                audioPathList.add(theTrack.getPath());
            }
        }
        startTagger(audioPathList);
    }


    /**************************************************************************
     *
     * helper function to change the fragment
     *
     * Note that due to a strange design commit() can throw an exception like this:
     *
     * java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
     *  E AndroidRuntime:     at android.support.v4.app.FragmentManagerImpl.checkStateLoss(SourceFile:1842)
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    protected void setFragment(int fragmentId)
    {
        if (mCurrFragment != null)
        {
            // save scroll position of previous fragment
            mCurrFragment.saveScrollPosition();
        }

        // close search view on fragment change
        if ((mSearchView != null) && !mSearchView.isIconified())
        {
            // search string will automatically be reset on
            // fragment change
            mSearchView.setIconified(true);
        }

        // close context menu
        if (mActionMode != null)
        {
            mActionMode.finish();
            mActionMode = null;
        }

        mRequestedFragmentId = fragmentId;
        if (mActivityPaused)
        {
            PrivateLog.w(LOG_TAG, "setFragment(): avoid crash: ignore fragment change due to paused activity");
            // TODO: remember fragmentId in onSaveInstanceState() and
            //       restore it in onRestoreInstanceState()
        }
        else
        {
            mCurrFragment = MainFragment.newInstance(fragmentId);
            FragmentManager theFragmentManager = getSupportFragmentManager();
            theFragmentManager.beginTransaction()
                    .replace(R.id.container, mCurrFragment)
                    .commit();
            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                Window window = getWindow();
                int colourId = (fragmentId == MainFragment.thePlayingFragment) ? R.attr.colourNavigationBarPlaying : R.attr.colourNavigationBar;
                window.setNavigationBarColor(UserSettings.getThemedColourFromResId(this, colourId));
            }
        }
    }


    /**************************************************************************
     *
     * Wait for background task and show toast, if it's still busy
     *
     *************************************************************************/
    private int waitForDbCompletion()
    {
        int ret = AppGlobals.waitForBackgroundTaskWithTimeout();
        if (ret == 1)
        {
            Toast.makeText(getApplicationContext(), R.string.str_be_patient, Toast.LENGTH_SHORT).show();
        }
        return ret;
    }


    /**************************************************************************
     *
     * callback from AdapterView (for Spinner):
     *
     * dropdown item selected
     *
     * Note that this is also automatically called on activity start with
     * position == 0 to show the first fragment.
     *
     *************************************************************************/
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        // When the given dropdown item is selected, show its contents in the
        // container view.
        PrivateLog.f_enter(LOG_TAG, "onItemSelected(pos = " + position + ", id = " + id + ")");
        if ((position == 0) || (waitForDbCompletion() == 0))
        {
            setFragment(position + 1);
            // also inform derived classes
            onChangeFragment(position + 1);
        }
        else
        {
            if (mSpinner != null)
            {
                // in case of a problem select "Album" line in spinner
                mSpinner.setSelection(0);
            }
        }
        PrivateLog.f_leave(LOG_TAG, "onItemSelected(pos = " + position + ", id = " + id + ")");
    }


    /**************************************************************************
     *
     * to be overridden, to inform derived class about fragment change
     *
     *************************************************************************/
    protected void onChangeFragment(int fragmentId)
    {
        PrivateLog.d(LOG_TAG, "onChangeFragment(fragmentId = " + fragmentId + ")");
    }


    /**************************************************************************
     *
     * to be overridden, to inform derived class about playlist change
     *
     * derived class should stop playing
     *
     *************************************************************************/
    protected void onChangePlaylist(boolean bAppend)
    {
        PrivateLog.d(LOG_TAG, "onChangePlaylist()");
    }


    /**************************************************************************
     *
     * to be overridden
     *
     *************************************************************************/
    protected void onKeyCodeMediaPlay()
    {
        Toast.makeText(getApplicationContext(), "MEDIA BUTTON PLAY", Toast.LENGTH_SHORT).show();
        PrivateLog.d(LOG_TAG, "onKeyCodeMediaPlay()");
    }


    /**************************************************************************
     *
     * callback from MainFragment to change fragments
     *
     * Used when an album or work was selected to switch to the playlist and
     * when BACK was pressed or the green floating button.
     *
     *************************************************************************/
    public void changeFragment(int fragmentId)
    {
        PrivateLog.d(LOG_TAG, "changeFragment(fragmentId = " + fragmentId + ")");
        // change spinner
        if ((mSpinner != null) && (fragmentId >= 1))
        {
            // ATTENTION: This call will initiate the onItemSelected() callback,
            //            and this one will call setFragment() and onChangeFragment()
            mSpinner.setSelection(fragmentId - 1);
        }
        // change fragment
        //setFragment(fragmentId);
        // also inform derived classes
        //onChangeFragment(fragmentId);
    }


    /**************************************************************************
     *
     * callback from AdapterView (for Spinner):
     *
     * nothing selected
     *
     *************************************************************************/
    public void onNothingSelected(AdapterView<?> parent)
    {
        PrivateLog.d(LOG_TAG, "onNothingSelected()");
    }


    /**************************************************************************
     *
     * callback from SearchView.OnQueryTextListener:
     *
     * This function is called whenever a character has been entered
     * in the search field, also on deletion of a character etc.. When the
     * search field is closed, an empty string will be sent.
     *
     *************************************************************************/
    public boolean onQueryTextChange(String newText)
    {
        PrivateLog.d(LOG_TAG, "onQueryTextChange() : query = " + newText);
        // send query to current fragment
        if (mCurrFragment != null)
        {
            mCurrFragment.onQueryTextSubmit(newText);
        }
        else
        {
            PrivateLog.e(LOG_TAG, "onQueryTextChange() : no fragment?!?");
        }
        return true;
    }

    /**************************************************************************
     *
     * callback from SearchView.OnQueryTextListener:
     *
     * It's unclear when this function will be called, or if at all. Maybe
     * it's not called because we do not have a "submit" button.
     *
     *************************************************************************/
    public boolean onQueryTextSubmit(String query)
    {
        PrivateLog.d(LOG_TAG, "onQueryTextSubmit() : query = " + query);
        // send query to current fragment
        mCurrFragment.onQueryTextSubmit(query);
        return true;
    }

    /* ************************************************************************
     *
     * callback from SearchView.OnCloseListener:
     *
     *************************************************************************/
    /*
    public boolean onClose()
    {
        PrivateLog.d(LOG_TAG, "onClose()");
        mCurrFragment.onQueryTextSubmit("");
        return false;
    }
    */


    /**************************************************************************
     *
     * method from Activity
     *
     * This is called once, while onPrepareOptionsMenu is called everytime
     * the menu opens.
     *
     *************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        PrivateLog.d(LOG_TAG, "onCreateOptionsMenu()");
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null)
        {
            MenuItem searchItem = menu.findItem(R.id.menu_search);
            mMenuItemSavePlaylist = menu.findItem(R.id.action_save_playlist);
            mMenuItemAutoRepeat   = menu.findItem(R.id.check_autorepeat);
            mMenuItemStartTagger  = menu.findItem(R.id.action_tag);
            mMenuItemRebuildDb    = menu.findItem(R.id.action_rebuild_db);
            mMenuItemReloadDb     = menu.findItem(R.id.action_reload_db);
            mMenuItemScan         = menu.findItem(R.id.action_scan);
            mMenuItemGridView     = menu.findItem(R.id.check_gridview);
            mMenuItemManageFiles  = menu.findItem(R.id.action_manage_external_files);
            mMenuItemOpenPath     = menu.findItem(R.id.action_path_select);

            //SearchView mSearchView = (SearchView) searchItem.getActionView();
            mSearchView = (SearchView) searchItem.getActionView();
            if (mSearchView != null)
            {
                ComponentName theComponentName = getComponentName();
                if (theComponentName != null)
                {
                    // Assumes current activity is the searchable activity
                    mSearchView.setSearchableInfo(searchManager.getSearchableInfo(theComponentName));
                    //mSearchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
                    mSearchView.setOnQueryTextListener(this);
                    //mSearchView.setOnCloseListener(this);
                }
            }
        }

        return true;
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * called everytime the menu opens
     * Enable and disable menu items according to state and context
     *
     *************************************************************************/
    @Override
    public boolean onPrepareOptionsMenu(@NonNull Menu menu)
    {
        int currFragment = (mCurrFragment != null) ? mCurrFragment.mSectionNumber : -1;
        //TODO: Playlist referenced by MainActivity, not MainBasicActivity
        boolean b = (currFragment == MainFragment.thePlayingFragment) && (PlayListHandler.getNumOfEntries() > 0);

        mMenuItemSavePlaylist.setEnabled(b);
        mMenuItemAutoRepeat.setChecked(UserSettings.getBool(UserSettings.PREF_AUTOREPEAT, false));
        mMenuItemGridView.setChecked(UserSettings.getBool(UserSettings.PREF_GRIDVIEW, false));
        mMenuItemGridView.setEnabled(MainFragment.isGridViewAllowedForSection(currFragment));
        mMenuItemAutoRepeat.setEnabled(b);
        mMenuItemStartTagger.setEnabled(b);

        boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
        boolean bOwnDb = (bUseOwnDatabase) || (sSafDatabasePath != null);
        mMenuItemRebuildDb.setVisible(!bOwnDb);
        mMenuItemReloadDb.setVisible(bOwnDb);
        mMenuItemScan.setVisible(bOwnDb);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            mMenuItemManageFiles.setCheckable(true);
            mMenuItemManageFiles.setEnabled(true);
            if (Environment.isExternalStorageManager())
            {
                // full file access already granted
                mMenuItemManageFiles.setChecked(true);
                mMenuItemOpenPath.setEnabled(true);
            }
            else
            {
                mMenuItemManageFiles.setChecked(false);
                mMenuItemOpenPath.setEnabled(false);
            }
        }
        else
        {
            // The "manage files" permission is available for Android 10 and above
            mMenuItemManageFiles.setEnabled(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * shall react on SEARCH key on Nexus-S, but does not
     *
     *************************************************************************/
    @Override
    public boolean onSearchRequested()
    {
        PrivateLog.d(LOG_TAG, "onSearchRequested()");
        return super.onSearchRequested();
    }


    /**************************************************************************
     *
     * Dialogue
     *
     *************************************************************************/
    private void dialogAbout()
    {
        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);

        final String strTitle = "Classical Music Player Opus One";
        final String strDescription = getString(R.string.str_app_description);
        final String strAuthor = getString(R.string.str_author);
        @SuppressWarnings("ConstantConditions") boolean bIsPlayStoreVersion = ((BuildConfig.BUILD_TYPE.equals("release_play")) || (BuildConfig.BUILD_TYPE.equals("debug_play")));
        @SuppressWarnings("ConstantConditions") final String tStore = (bIsPlayStoreVersion) ? "   [Play Store]" : "   [free]";
        final String strVersion = "Version " + info.versionName + (((info.isDebug) ? " DEBUG" : "") + tStore);

        AlertDialog alertDialog = new AlertDialog.Builder(MainBasicActivity.this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setIcon(R.drawable.app_icon_noborder);
        alertDialog.setMessage(
                strDescription + "\n\n" +
                        strAuthor + "Andreas Kromke" + "\n\n" +
                        "Version " + strVersion + "\n" +
                        "(" + info.strCreationTime + ")");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        mPendingDialog = null;
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * restart the app
     *
     *************************************************************************/
    public void restart()
    {
        PrivateLog.d(LOG_TAG, "restart()");
        UserSettings.commit();      // important: flush write cache for changed settings
        bRestarting = true;
        onDestroy();
        /*
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
        if (Build.VERSION.SDK_INT >= 16 /o Build.VERSION_CODES.JELLY_BEAN o/)
        {
            this.finishAffinity();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "this Android version is too old", Toast.LENGTH_SHORT).show();
        }
        */
        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
        if (intent != null)
        {
            ComponentName componentName = intent.getComponent();
            Intent mainIntent = Intent.makeRestartActivityTask(componentName);
            mainIntent.putExtra(sIntentKeyDatabasePath, sSafDatabasePath);
            startActivity(mainIntent);
            AppGlobals.exit();  // close database
            System.exit(0);
        }
    }


    /**************************************************************************
     *
     * "Not Available in Play Store Version" dialogue
     *
     *************************************************************************/
    private void dialogNotAvailableInPlayStoreVersion()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.str_NeedManageFilesPermission));
        alertDialog.setMessage(getString(R.string.str_NotAvailableInPlayStoreVersion));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * Dialogue
     *
     *************************************************************************/
    private void dialogLeaveSafMode()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_leave_stay_saf_mode);
        builder.setMessage(R.string.str_msg_return_to_file_mode);
        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;
                sSafDatabasePath = null;
                UserSettings.putString(UserSettings.PREF_LATEST_SAF_DB_PATH, null);
                restart();
            }
        });

        builder.setNegativeButton(getString(R.string.str_no), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * Dialogue
     *
     *************************************************************************/
    private void dialogRebuildDb()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_rebuild_db));
        builder.setMessage(getString(R.string.str_text_rebuild_db));
        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;
                Toast.makeText(getApplicationContext(), getString(R.string.str_confirm_rebuild_db), Toast.LENGTH_SHORT).show();
                UserSettings.putVal(UserSettings.PREF_REBUILD_DB, 1);
                restart();
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Toast.makeText(getApplicationContext(), "abgekanzelt", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * Dialogue to show any html text
     *
     *************************************************************************/
    private void dialogHtml(final String filename)
    {
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/html-" + getString(R.string.locale_prefix) + "/" + filename);
//        webView.loadData(SampleStats.boxScore1, "text/html", "utf-8");
        AlertDialog alertDialog = new AlertDialog.Builder(MainBasicActivity.this).create();
        alertDialog.setView(webView);
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * Dialogue to show help text
     *
     *************************************************************************/
    private void dialogHelp()
    {
        dialogHtml("help.html");
    }


    /**************************************************************************
     *
     * Dialogue to show list of changes
     *
     *************************************************************************/
    private void dialogChanges()
    {
        dialogHtml("changes.html");
    }


    /**************************************************************************
     *
     * "There is no Database File" dialogue
     *
     *************************************************************************/
    private void DialogNoDatabaseFile()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(MainBasicActivity.this).create();
        alertDialog.setTitle(getString(R.string.str_NeedDatabaseFile));
        alertDialog.setMessage(getString(R.string.str_NoDatabaseFileDescription));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * A main menu item has been selected
     *
     * (method from Activity)
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        PrivateLog.d(LOG_TAG, "onOptionsItemSelected()");
        int id = item.getItemId();
        if (id == R.id.action_about)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_about");
            dialogAbout();
        }
        else if (id == R.id.action_help)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_help");
            dialogHelp();
        }
        else if (id == R.id.action_changes)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_changes");
            dialogChanges();
        }
        else if (id == R.id.action_settings)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_settings");
            // User chose the "Settings" item, show the app settings UI...
            Intent intent = new Intent(this, UserSettingsActivity.class);
            // DEPRECATED startActivityForResult(intent, RESULT_SETTINGS);
            mPreferencesActivityLauncher.launch(intent);
        }
        else if (id == R.id.action_tag)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_tag");
            startTaggerWithPlayingList();
        }
        else if (id == R.id.action_scan)
        {
            String app_id = (sSafDatabasePath != null) ?
                    "de.kromke.andreas.safmediascanner" : "de.kromke.andreas.mediascanner";
            startOtherApp(app_id, null);
            return true;
        }
        else if (id == R.id.action_rebuild_db)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_rebuild_db");
            // User chose the "Reset Db" item, open confirmation dialogue...
            dialogRebuildDb();
        }
        else if (id == R.id.action_saf_select)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                startSafTreePicker(true);
            }
            return true;
        }
        else if (id == R.id.action_path_select)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                startSafTreePicker(false);
            }
            return true;
        }
        else if (id == R.id.action_manage_external_files)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            {
                // Android 10 and newer: MANAGE EXTERNAL FILES
                //noinspection ConstantConditions
                if ((BuildConfig.BUILD_TYPE.equals("release_play")) || (BuildConfig.BUILD_TYPE.equals("debug_play")))
                {
                    dialogNotAvailableInPlayStoreVersion();
                }
                else
                {
                    requestForManageAllFiles();
                }
            }
            return true;
        }
        else if (id == R.id.action_reload_db)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_reload_db");
            restart();
        }
        else if (id == R.id.action_save_playlist)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- action_save_playlist");
            dialogSavePlaylist();
        }
        else if (id == R.id.check_autorepeat)
        {
            PrivateLog.d(LOG_TAG, "onOptionsItemSelected() -- check_autorepeat");
            boolean b = mMenuItemAutoRepeat.isChecked();
            b = !b;     // invert
            mMenuItemAutoRepeat.setChecked(b);
            UserSettings.putVal(UserSettings.PREF_AUTOREPEAT, b);
        }
        else if (id == R.id.check_gridview)
        {
            boolean b = mMenuItemGridView.isChecked();
            b = !b;     // invert
            mMenuItemGridView.setChecked(b);
            UserSettings.putVal(UserSettings.PREF_GRIDVIEW, b);
            restart();
            return true;
        }
        else if (id == R.id.action_exit)
        {
            System.exit(0);
            return true;
        }
        else
        {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            return super.onOptionsItemSelected(item);
        }
        return true;
    }


    /**************************************************************************
     *
     * update colour theme and rotation mode, if necessary
     *
     *************************************************************************/
    @SuppressLint({"SourceLockedOrientationActivity", "ObsoleteSdkInt"})
    private boolean updateSettings()
    {
        int colourTheme = UserSettings.getVal(UserSettings.PREF_THEME, 0);
        if ((mColourTheme < 0) || (mColourTheme != colourTheme))
        {
            mColourTheme = colourTheme;
            switch (mColourTheme)
            {
                //noinspection DefaultNotLastCaseInSwitch
                default:
                case 0:
                    setTheme(R.style.Theme_BlueLight);
                    break;
                case 1:
                    setTheme(R.style.Theme_OrangeLight);
                    break;
                case 2:
                    setTheme(R.style.Theme_GreenLight);
                    break;
                case 3:
                    setTheme(R.style.Theme_LightGrey);
                    break;
                case 4:
                    setTheme(R.style.Theme_DarkGrey);
                    break;
                case 5:
                    setTheme(R.style.Theme_Blue);
                    break;
            }

            //noinspection StatementWithEmptyBody
            if (android.os.Build.VERSION.SDK_INT >= 21)
            {
                Window window = getWindow();
//                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                //window.setStatusBarColor(Color.parseColor("#000000"));
                window.setStatusBarColor(UserSettings.getThemedColourFromResId(this, R.attr.colorPrimaryDark));
            } else
            {
            }

            // activity recreate necessary, if not called in onCreate()
            return true;
        }

        int rotationMode = UserSettings.getVal(UserSettings.PREF_ROTATION, 0);
        if ((mRotationMode < 0) || (mRotationMode != rotationMode))
        {
            mRotationMode = rotationMode;
            if (mRotationMode == 1)
            {
                // freeze orientation, if requested
                PrivateLog.d(LOG_TAG, "SCREEN_ORIENTATION_PORTRAIT fixed");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else if (mRotationMode == 2)
            {
                // freeze orientation, if requested
                PrivateLog.d(LOG_TAG, "SCREEN_ORIENTATION_LANDSCAPE fixed");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            } else
            {
                // free orientation
                PrivateLog.d(LOG_TAG, "SCREEN_ORIENTATION_UNSPECIFIED");
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            }
        }

        // no recreate necessary
        return false;
    }


    /**************************************************************************
     *
     * check for changed settings (also called on activity creation)
     *
     * return 0: do nothing
     *        1: recreate activity
     *        2: restart app
     *        3: recreate internal db
     *
     *************************************************************************/
    private int applySettingsChanges()
    {
        boolean bRecreateActivity = false;
        boolean bRestartApp = false;
        boolean bRecreateDb = false;

        //noinspection UnnecessaryLocalVariable
        String defaultSharedDbBasePath = Utilities.getDefaultSharedDbFileBasePath(this);
        AppGlobals.sOwnDatabasePath = defaultSharedDbBasePath;

        boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
        if (bUseOwnDatabase != mbUseOwnDatabase)
        {
            mbUseOwnDatabase = bUseOwnDatabase;
            bRestartApp = true;
        }

        String defaultSharedDbPath = Utilities.getDefaultSharedDbFilePath(this);
        String ownDatabasePath = UserSettings.getAndPutString(UserSettings.PREF_OWN_DATABASE_PATH, defaultSharedDbPath);
        if (Utilities.stringsDiffer(ownDatabasePath, mOwnDatabasePath))
        {
            mOwnDatabasePath = ownDatabasePath;
            bRestartApp = true;
        }

        String filterPaths = UserSettings.getString(UserSettings.PREF_WHITE_BLACK_LIST_PATHS);
        if (Utilities.stringsDiffer(filterPaths, mFilterPaths))
        {
            mFilterPaths = filterPaths;
            if (!bUseOwnDatabase)
            {
                bRecreateDb = true;
            }
        }

        boolean bMergeAlbums = UserSettings.getBool(UserSettings.PREF_MERGE_ALBUMS_OF_SAME_NAME, false);
        if (bMergeAlbums != mbMergeAlbums)
        {
            mbMergeAlbums = bMergeAlbums;
            if (!bUseOwnDatabase)
            {
                bRecreateDb = true;
            }
        }

        boolean bUsePathsForAndroidDbAudioFiles = UserSettings.getBool(UserSettings.PREF_ALWAYS_GET_PATHS_FOR_ANDROID_DB, false);
        if (bUsePathsForAndroidDbAudioFiles != mbUsePathsForAndroidDbAudioFiles)
        {
            mbUsePathsForAndroidDbAudioFiles = bUsePathsForAndroidDbAudioFiles;
            if (!bUseOwnDatabase)
            {
                bRecreateDb = true;
            }
        }

        int handlePersonsLists = UserSettings.getVal(UserSettings.PREF_HANDLE_PERSON_LISTS, 1);
        if (handlePersonsLists != mHandlePersonsLists)
        {
            mHandlePersonsLists = handlePersonsLists;
            bRestartApp = true;
        }

        boolean bComposerDecompose = UserSettings.getBool(UserSettings.PREF_COMPOSER_SORT_BY_LAST_NAME, true);
        if (bComposerDecompose != mbComposerDecompose)
        {
            mbComposerDecompose = bComposerDecompose;
            bRestartApp = true;
        }

        boolean bComposerLastNameFirst = UserSettings.getBool(UserSettings.PREF_SHOW_COMPOSER_LAST_NAME_FIRST, false);
        if (bComposerLastNameFirst != mbComposerLastNameFirst)
        {
            mbComposerLastNameFirst = bComposerLastNameFirst;
            bRestartApp = true;
        }

        int workPictureMode = UserSettings.getVal(UserSettings.PREF_WORK_PICTURE_MODE, 0);
        if (workPictureMode != mWorkPictureMode)
        {
            mWorkPictureMode = workPictureMode;
            bRecreateActivity = true;
        }

        String defaultOwnPersonsPicturePath = Utilities.getDefaultOwnPersonsPicturePath(this);
        String ownPersonsPicturePath = UserSettings.getAndPutString(UserSettings.PREF_OWN_PERSONS_PATH, defaultOwnPersonsPicturePath);
        if (!ownPersonsPicturePath.equals(mOwnPersonsPicturePath))
        {
            mOwnPersonsPicturePath = ownPersonsPicturePath;
            bRecreateActivity = true;
        }

        boolean bShowAlbumDuration = UserSettings.getBool(UserSettings.PREF_SHOW_ALBUM_AND_OPUS_DURATION, false);
        if (bShowAlbumDuration != mbShowAlbumDuration)
        {
            mbShowAlbumDuration = bShowAlbumDuration;
            bRecreateActivity = true;
        }

        boolean bGridView = UserSettings.getBool(UserSettings.PREF_GRIDVIEW, false);
        if (bGridView != mbGridView)
        {
            mbGridView = bGridView;
            bRecreateActivity = true;
        }

        // at least 80 pixels, at least 8 mm
        int imageSizeForGrid = UserSettings.getVal(UserSettings.PREF_SIZE_OF_GRID_PICTURES, 512);
        if (imageSizeForGrid != mImageSizeForGrid)
        {
            mImageSizeForGrid = imageSizeForGrid;
            bRecreateActivity = true;
        }

        // at least 80 pixels, at least 8 mm
        int imageSizeForListPictures = UserSettings.getAndPutVal(UserSettings.PREF_SIZE_OF_LIST_PICTURES, UserSettings.pixelsOrMillimeters(this, 80, 8));
        if (imageSizeForListPictures != mImageSizeForListPictures)
        {
            mImageSizeForListPictures = imageSizeForListPictures;
            bRecreateActivity = true;
        }

        // at least 120 pixels, at least 12 mm
        int imageSizeForHeaderPictures = UserSettings.getAndPutVal(UserSettings.PREF_SIZE_OF_ALBUM_HEADER, UserSettings.pixelsOrMillimeters(this, 120, 12));
        if (imageSizeForHeaderPictures != mImageSizeForHeaderPictures)
        {
            mImageSizeForHeaderPictures = imageSizeForHeaderPictures;
            bRecreateActivity = true;
        }

        int genderismMode = UserSettings.getVal(UserSettings.PREF_GENDERISM_NONSENSE, 0);
        if (genderismMode != mGenderismMode)
        {
            mGenderismMode = genderismMode;
            bRecreateActivity = true;
        }

        boolean bShowTrackNumbers = UserSettings.getBool(UserSettings.PREF_SHOW_TRACK_NUMBER, false);
        if (bShowTrackNumbers != mbShowTrackNumbers)
        {
            mbShowTrackNumbers = bShowTrackNumbers;
            bRecreateActivity = true;
        }

        int showConductorMode = UserSettings.getVal(UserSettings.PREF_SHOW_CONDUCTOR, 0);
        if (showConductorMode != mShowConductorMode)
        {
            mShowConductorMode = showConductorMode;
            bRecreateActivity = true;
        }

        boolean bShowSubtitle = UserSettings.getBool(UserSettings.PREF_SHOW_SUBTITLE, false);
        if (bShowSubtitle != mbShowSubtitle)
        {
            mbShowSubtitle = bShowSubtitle;
            bRecreateActivity = true;
        }

        AudioBaseObject.setShowAlbumDuration(mbShowAlbumDuration);
        AudioBaseObject.setComposerDecompMode(mbComposerDecompose, mbComposerLastNameFirst);
        AudioBaseObject.sWorkPictureMode = mWorkPictureMode;

        String[] pathArray = mOwnPersonsPicturePath.split("\n");
        File dir = new File(pathArray[0]);
        if (dir.isDirectory() && dir.canRead())
        {
            PrivateLog.d(LOG_TAG, "applySettingsChanges() : use picture directory " + dir.getPath());
            AudioBaseObject.sOwnPersonsPicturePath = mOwnPersonsPicturePath;
        }
        else
        {
            PrivateLog.w(LOG_TAG, "applySettingsChanges() : ignore invalid picture directory " + dir.getPath());
            AudioBaseObject.sOwnPersonsPicturePath = null;
        }
        MainFragment.sImageSizeForListPictures = mImageSizeForListPictures;
        MainFragment.sImageSizeForHeaderPictures = mImageSizeForHeaderPictures;
        MainFragment.sImageSizeForGrid = mImageSizeForGrid;
        MainFragment.sGridView = mbGridView;
        AudioBaseObject.SetContext(this, mGenderismMode);
        AudioBaseListAdapter.sbShowTrackNumbers = mbShowTrackNumbers;
        AudioBaseListAdapter.sShowConductorMode = mShowConductorMode;
        AudioBaseListAdapter.sbShowSubtitle = mbShowSubtitle;
        AudioBaseListAdapter.sGenderismMode = mGenderismMode;

        if (updateSettings())
        {
            // colour theme has changed, recreate activity
            bRecreateActivity = true;
        }

        if (bRecreateDb)
        {
            return 3;
        }
        if (bRestartApp)
        {
            return 2;
        }
        if (bRecreateActivity)
        {
            return 1;
        }
        return 0;
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerPreferencesCallback()
    {
        mPreferencesActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int res = applySettingsChanges();
                    if (res == 3)
                    {
                        // db mode must be rebuilt, restart app
                        Toast.makeText(getApplicationContext(), getString(R.string.str_confirm_rebuild_db), Toast.LENGTH_SHORT).show();
                        UserSettings.putVal(UserSettings.PREF_REBUILD_DB, 1);
                        restart();
                    }
                    else
                    if (res == 2)
                    {
                        // db mode changed, restart app
                        restart();
                    }
                    else
                    if (res == 1)
                    {
                        // colour theme has changed, recreate activity
                        Toast.makeText(getApplicationContext(), getString(R.string.str_applyGuiChanges), Toast.LENGTH_LONG).show();
                        AboutToRecreate();
                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)
                        {
                            // Nexus 5 has problems when recreate() is called in any onXXX() function,
                            // in fact the new activity will go back to paused state for whatever reason
                            mbMustRecreate = true;
                        }
                        else
                        {
                            recreate();
                        }
                    }
                }
            });
    }


    /************************************************************************************
     *
     * Helper to change the db path
     * Return true when path was successfully changed and is active
     *
     ***********************************************************************************/
    private boolean changeDbPath(String newMusicBasePath)
    {
        boolean ret = false;
        String newOwnDatabasePath = Utilities.getDatabasePathFromMusicBasePath(newMusicBasePath);
        File dbf = new File(newOwnDatabasePath);
        if (dbf.exists())
        {
            // get current values
            boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
            String currOwnDatabasePath = UserSettings.getString(UserSettings.PREF_OWN_DATABASE_PATH);

            // check if there are changes
            if (Utilities.stringsDiffer(currOwnDatabasePath, newOwnDatabasePath))
            {
                PrivateLog.d(LOG_TAG, " new DB path = " + newOwnDatabasePath);
                if (!bUseOwnDatabase)
                {
                    PrivateLog.d(LOG_TAG, " enable using own DB");
                    bUseOwnDatabase = true;
                    UserSettings.putVal(UserSettings.PREF_USE_OWN_DATABASE, bUseOwnDatabase);
                }
                UserSettings.putStringAndCommit(UserSettings.PREF_OWN_DATABASE_PATH, newOwnDatabasePath);
                ret = bUseOwnDatabase;
            }
        }

        return ret;
    }


    /**************************************************************************
     *
     * Callback for file tree selector
     *
     *************************************************************************/
    private void registerDirectorySelectCallback()
    {
        mRequestDirectorySelectActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @SuppressLint("ObsoleteSdkInt")
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri treeUri = data.getData();
                        if (treeUri != null)
                        {
                            PrivateLog.d(LOG_TAG, " Tree Uri = " + treeUri.getPath());

                            if (mbDirectorySelectForSaf)
                            {
                                grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

                                // copy and open database
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                {
                                    // remember URI for the next time the selector is opened
                                    UserSettings.putString(UserSettings.PREF_LATEST_SAF_URI, treeUri.toString());
                                    openSafTree(treeUri);
                                } else
                                {
                                    PrivateLog.e(LOG_TAG, "This function needs Android 5 or higher");
                                }
                            }
                            else
                            {
                                // /tree/primary:Music/bla
                                // /tree/12F0-3703:Music/bla
                                String newMusicBasePath = Utilities.pathFromTreeUri(treeUri);
                                PrivateLog.d(LOG_TAG, " real Path = " + newMusicBasePath);
                                if (newMusicBasePath != null)
                                {
                                    File f = new File(newMusicBasePath);
                                    if (f.exists())
                                    {
                                        if (changeDbPath(newMusicBasePath))
                                        {
                                            // db path has been changed
                                            restart();
                                            return;
                                        }
                                        // db path unchanged
                                        return;
                                    }
                                }
                                DialogNoDatabaseFile();
                            }
                        }
                    }
                    else
                    {
                        PrivateLog.d(LOG_TAG, "registerDirectorySelectCallback() - cancelled");
                        if (sSafDatabasePath != null)
                        {
                            dialogLeaveSafMode();
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * callback for startSafFilePicker()
     *
     *************************************************************************/
    private void registerFileSelectCallback()
    {
        mRequestFileSelectActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @SuppressLint("ObsoleteSdkInt")
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri fileUri = data.getData();
                        if (fileUri != null)
                        {
                            PrivateLog.d(LOG_TAG, " File Uri = " + fileUri.getPath());
                            // open database
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            {
                                openDatabaseFile(fileUri);
                            }
                            else
                            {
                                PrivateLog.e(LOG_TAG, "This function needs Android 5 or higher");
                            }
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     * Note that this function is not called when then the permission is
     * revoked! Instead, the process ends and is restarted immediately
     * at the moment the user disables the permission in the system dialogue.
     *
     * Otherwise the function is called, i.e. when the user did not change
     * the setting (either denied or permitted) or when the user granted
     * permission.
     *
     *************************************************************************/
    private void registerStorageAccessPermissionCallback()
    {
        mStorageAccessPermissionActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                @RequiresApi(api = Build.VERSION_CODES.R)
                public void onActivityResult(ActivityResult result)
                {
                    // Note that the resultCode is always RESULT_CANCELED, thus not helpful here, fwr
                    if (Environment.isExternalStorageManager())
                    {
                        PrivateLog.d(LOG_TAG, "registerStorageAccessPermissionCallback(): permission granted");

                        boolean bUseOwnDatabase = UserSettings.getBool(UserSettings.PREF_USE_OWN_DATABASE, false);
                        if (bUseOwnDatabase && AppGlobals.isAndroidDb())
                        {
                                PrivateLog.d(LOG_TAG, "registerStorageAccessPermissionCallback(): restart to activate own db");
                            restart();
                        }

                        onPermissionGranted();
                    }
                    else
                    {
                        PrivateLog.d(LOG_TAG, "registerStorageAccessPermissionCallback(): permission denied");
                        Toast.makeText(getApplicationContext(), R.string.str_manage_permission_denied, Toast.LENGTH_LONG).show();
                    }
                }
            });
    }


    /**************************************************************************
     *
     * start SAF tree selector
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void startSafTreePicker(boolean bIsSaF)
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        // specify initial directory tree position

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String uriString = UserSettings.getString(UserSettings.PREF_LATEST_SAF_URI);
            if (uriString != null)
            {
                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uriString);
            }
        }

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

        // callback: see registerDirectorySelectCallback()
        mbDirectorySelectForSaf = bIsSaF;
        mRequestDirectorySelectActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * start SAF file selector
     *
     * Note that dumb API cannot select file name extension, but only
     * MIME types, and Android does not know about "application/vnd.sqlite3".
     * Further, any usable documentation about supported MIME types is missing.
     *
     * Note that EXTRA_INITIAL_URI does not accept file URIs, but only those
     * generated from SAF. Thus this function uses a hack.
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void startSafFilePicker()
    {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");

        /*
        // THIS DOES NOT WORK DUE TO POOR API DESIGN
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String bla = map.getMimeTypeFromExtension("db");
        String bla2 = MimeTypeMap.getSingleton().getMimeTypeFromExtension("sqlite");
        String bla3 = MimeTypeMap.getSingleton().getMimeTypeFromExtension("sqlite3");
        String blu = MimeTypeMap.getSingleton().getExtensionFromMimeType("application/vnd.sqlite3");
        intent.setType("application/x-sqlite3");
        */

        // specify initial directory tree position
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            // This is a hack ...
            Uri uri = Uri.parse("content://com.android.externalstorage.documents/document/primary%3A" + Utilities.sDbPath);
            // ... because that one does not work:
            // Uri uri = Uri.fromFile(new File(getSharedDbFilePath(this)));
            if (uri != null)
            {
                intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
            }
        }

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        /*
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
         */
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

        // callback in registerFileSelectCallback()
        mRequestFileSelectActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * copy database from here and open it
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openSafTree(Uri treeUri)
    {
        DocumentFile treeDf = DocumentFile.fromTreeUri(this, treeUri);
        if (treeDf == null)
        {
            PrivateLog.e(LOG_TAG, "cannot open treeUri: " + treeUri);
            Toast.makeText(getApplicationContext(), R.string.str_nonAccessiblePath, Toast.LENGTH_LONG).show();
            return;
        }

        DocumentFile ddf = treeDf.findFile(Utilities.sDbPath);
        if (ddf == null)
        {
            PrivateLog.e(LOG_TAG, "cannot find directory " + Utilities.sDbPath + " in treeUri: " + treeUri);
            dialogPickDatabaseFile();
            return;
        }

        DocumentFile df = ddf.findFile(Utilities.sDbSafName);
        if (df == null)
        {
            PrivateLog.e(LOG_TAG, "cannot find file " + Utilities.sDbSafName + " in directory " + Utilities.sDbPath + " in treeUri: " + treeUri);
            dialogPickDatabaseFile();
            return;
        }

        InputStream is;
        try
        {
            is = getContentResolver().openInputStream(df.getUri());
            if (is == null)
            {
                PrivateLog.e(LOG_TAG, "cannot open file for input " + Utilities.sDbSafName + " in directory " + Utilities.sDbPath + " in treeUri: " + treeUri);
                return;
            }
        } catch (FileNotFoundException e)
        {
            PrivateLog.e(LOG_TAG, "cannot create output stream");
            return;
        }

        boolean bResult = false;
        File tempDbFile = new File(getFilesDir(), "tempSaf.db");
        //noinspection ResultOfMethodCallIgnored
        tempDbFile.delete();
        FileOutputStream os;
        try
        {
            if (!tempDbFile.createNewFile())
            {
                PrivateLog.e(LOG_TAG, "cannot create temporary file");
                SafUtilities.closeStream(is);
                return;
            }
            os = new FileOutputStream(tempDbFile);
            bResult = SafUtilities.copyFileFromTo(is, os);
        } catch (IOException e)
        {
            SafUtilities.closeStream(is);
            PrivateLog.e(LOG_TAG, "cannot create temporary file: " + e);
        }

        if (bResult)
        {
            Toast.makeText(getApplicationContext(), R.string.database_retrieved, Toast.LENGTH_SHORT).show();
            sSafDatabasePath = tempDbFile.getPath();
            UserSettings.putString(UserSettings.PREF_LATEST_SAF_DB_PATH, sSafDatabasePath);
            restart();
        }
        else
        {
            sSafDatabasePath = null;
            Toast.makeText(getApplicationContext(), R.string.error_database_not_retrieved, Toast.LENGTH_LONG).show();
        }
    }


    /**************************************************************************
     *
     * open database from internal memory
     *
     * The file Uri looks like this:
     *  content://com.android.externalstorage.documents/document/primary%3AClassicalMusicDb%2F6D9B-B0E6%20Music.db
     *
     * The file generated from it looks like
     *  /document/primary:ClassicalMusicDb/6D9B-B0E6 Music.db
     *
     * There is no conversion so that we have to use a hack here.
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openDatabaseFile(Uri fileUri)
    {
        String path = fileUri.getPath();
        File f;
        if (path == null)
        {
            f = null;
        }
        else
        {
            // TODO: is this correct?
            f = new File(Utilities.getDefaultSharedDbFileBasePath(this), new File(path).getName());
        }

        if ((f != null) && (f.canRead()))
        {
            sSafDatabasePath = f.getPath();
            restart();
        }
        else
        {
            sSafDatabasePath = null;
            Toast.makeText(getApplicationContext(),"ERROR: Database not retrieved", Toast.LENGTH_LONG).show();
        }
    }


    /**************************************************************************
     *
     * "Select Database File" dialogue
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void dialogPickDatabaseFile()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.str_NeedDatabaseFile));
        alertDialog.setMessage(getString(R.string.str_NeedDatabaseFileDescription));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        startSafFilePicker();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * ask if end app
     *
     *************************************************************************/
    private void dialogEndApp()
    {
        final boolean bShowGreetings = UserSettings.getBool(UserSettings.PREF_SHOW_GREETINGS, true);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_program_exit));
        builder.setMessage("To be or not to be, that is the question...");
        builder.setPositiveButton(getString(R.string.str_exit), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;
                if (bShowGreetings)
                {
                    Toast.makeText(getApplicationContext(), R.string.str_program_exiting, Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;
                if (bShowGreetings)
                {
                    Toast.makeText(getApplicationContext(), R.string.str_program_keeping, Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /* *************************************************************************
     *
     * helper function to play an "object"
     *
     *************************************************************************/
    /*
    private void enterPlaylist(final AudioBaseObject theSelectedObject, boolean bAppend)
    {
        AppGlobals.playObject(theSelectedObject, bAppend);
        changeFragment(MainFragment.thePlayingFragment);
    }
    */


    /**************************************************************************
     *
     * helper function to play an "object"
     *
     *************************************************************************/
    private void enterPlaylist(final AudioBaseObject[] theSelectedObject, boolean bAppend)
    {
        AppGlobals.playObjects(theSelectedObject, bAppend);
        changeFragment(MainFragment.thePlayingFragment);
    }


    /**************************************************************************
     *
     * initialise context menu
     *
     * -> https://developer.android.com/guide/topics/ui/menus.html#CAB
     *
     *************************************************************************/
    private void initActionModeContextMenu()
    {
        mActionMode = null;
        mActionModeCallback = new ActionMode.Callback()
        {
            // Called when the action mode is created; startActionMode() was called
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu)
            {
                int sectionNumber = mCurrFragment.mSectionNumber;
                boolean bIsPlayingList = (sectionNumber == MainFragment.thePlayingFragment);
                boolean bIsAlbumOrWorkOrTracksList = (sectionNumber == MainFragment.theAlbumFragment) ||
                        (sectionNumber == MainFragment.theWorkFragment) ||
                        (sectionNumber == MainFragment.theTracksFragment);
                int num;

                // Inflate a menu resource providing context menu items
                MenuInflater inflater = mode.getMenuInflater();
                if (bIsPlayingList)
                {
                    inflater.inflate(R.menu.menu_context_playing, menu);
                    num = 5;
                }
                else
                {
                    inflater.inflate(R.menu.menu_context, menu);
                    num = 3;
                }

                int i;
                for (i = 0; i < num; i++)     // info/remove or play/tagger/up/down
                {
                    MenuItem item = menu.getItem(i);
                    if (i == 1)
                    {
                        // "remove" and "up/down" menu entry only for playing fragment
                        // "play" menu entry only for albums and works fragments

                        if (!bIsPlayingList && !bIsAlbumOrWorkOrTracksList)
                        {
                            item.setVisible(false);
                        }
                    }
                    if (i < 3)
                    {
                        // stupid method to use unused space, as we do not have a title here:
                        // -> https://stackoverflow.com/questions/23253475/remove-title-from-contextual-action-bar
                        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                    }
                }

                return true;
            }

            // Called each time the action mode is shown. Always called after onCreateActionMode, but
            // may be called multiple times if the mode is invalidated.
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu)
            {
                return false; // Return false if nothing is done
            }

            // Called when the user selects a contextual menu item
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item)
            {
                if (mActionModeSelectedObject != null)
                {
                    boolean bIsPlayingList = (mCurrFragment.mSectionNumber == MainFragment.thePlayingFragment);
                    int id = item.getItemId();
                    if (bIsPlayingList)
                    {
                        if (id == R.id.menu_context_playing_info)
                        {
                            PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_info");
                            dialogShowInfo(mActionModeSelectedObject);
                        }
                        else if (id == R.id.menu_context_playing_remove_from_list)
                        {
                            PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_remove");
                            removeFromPlayingList(mActionModeSelectedObject);
                        }
                        else if (id == R.id.menu_context_playing_tagger)
                        {
                            PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_tagger");
                            startTaggerWithObject(mActionModeSelectedObject);
                        }
                        else if (id == R.id.menu_context_playing_up)
                        {
                            PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_up");
                            moveInPlayingList(mActionModeSelectedObject, mActionModeSelectedPos, -1);
                        }
                        else if (id == R.id.menu_context_playing_down)
                        {
                            PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_down");
                            moveInPlayingList(mActionModeSelectedObject, mActionModeSelectedPos, 1);
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    if (id == R.id.menu_context_info)
                    {
                        PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_info");
                        dialogShowInfo(mActionModeSelectedObject);
                    }
                    else if (id == R.id.menu_context_play)
                    {
                        PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_play");
                        dialogConditionallyOverwriteOrAppendToPlayingList(mActionModeSelectedObject, -1);
                    }
                    else if (id == R.id.menu_context_tagger)
                    {
                        PrivateLog.d(LOG_TAG, "mActionModeCallback.onActionItemClicked() : menu_context_tagger");
                        startTaggerWithObject(mActionModeSelectedObject);
                    }
                    else
                    {
                        return false;
                    }
                }

                mode.finish(); // Action picked, so close the CAB
                return true;
            }

            // Called when the user exits the action mode
            @Override
            public void onDestroyActionMode(ActionMode mode)
            {
                mActionMode = null;
            }
        };
    }


    /**************************************************************************
     *
     * delete playlist after confirmation
     *
     ************************************************************************
     * @noinspection unused*/
    public boolean onLongpressToPlaylist(final AudioBaseObject theSelectedObject, final int position)
    {
        dialogAskDeletePlaylist((AudioPlaylist) theSelectedObject);
        return true;
    }


    /**************************************************************************
     *
     * show object information
     *
     *************************************************************************/
    public boolean onLongpressToMusicList(final AudioBaseObject theSelectedObject, final int position)
    {
        if (mActionMode != null)
        {
            return false;
        }

        // Start the CAB using the ActionMode.Callback defined above
        mActionModeSelectedObject = theSelectedObject;              // no intelligent way to pass parameters
        mActionModeSelectedPos = position;              // no intelligent way to pass parameters
        mActionMode = startSupportActionMode(mActionModeCallback);

        // found no way to avoid the unused space for title
        // -> https://stackoverflow.com/questions/23253475/remove-title-from-contextual-action-bar
        if (mActionMode != null)
        {
            mActionMode.setCustomView(null);
            mActionMode.setTitleOptionalHint(true);
            mActionMode.setTitle("");
            //mActionMode.setSubtitle("Subtitle");
        }

        return true;
    }


    /**************************************************************************
     *
     * Put to track list
     *
     * object can be track, work, album, folder, playlist
     *
     *************************************************************************/
    public void putToTrackList
    (
        final ArrayList<AudioBaseObject> theSelectedObjects
    )
    {
        AppGlobals.copyObjectToTracks(theSelectedObjects);
        changeFragment(MainFragment.theTracksFragment);
    }


    /**************************************************************************
     *
     * Put to track list
     *
     * object can be track, work, album, folder, playlist
     *
     *************************************************************************/
    public void putToTrackList
    (
        final AudioBaseObject theSelectedObject,
        final int position
    )
    {
        if ((position >= 0) && (mCurrFragment != null))
        {
            // do selection in source list (album or work or ...)
            mCurrFragment.onSetCurrSelection(position);
        }

        AppGlobals.copyObjectToTracks(theSelectedObject);
        changeFragment(MainFragment.theTracksFragment);
    }


    /**************************************************************************
     *
     * if playing list is not empty, ask if to overwrite
     *
     * object can be track, work, album, folder, playlist
     *
     *************************************************************************/
    public void dialogConditionallyOverwriteOrAppendToPlayingList
    (
        final AudioBaseObject[] theSelectedObjects,
        final int position
    )
    {
        if (AudioFilters.audioPlayingList.getTotalSize() < 1)
        {
            // current playlist is empty
            if (position >= 0)
            {
                // do selection in source list (album or work or ...)
                mCurrFragment.onSetCurrSelection(position);
            }
            // playlist was empty, just add and play
            enterPlaylist(theSelectedObjects, false /* overwrite */);
            return;
        }

        // playlist is not empty and does not contain the object.
        // ask for overwrite

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_overwrite_playing_list));
        builder.setMessage(getString(R.string.str_message_overwrite_playing_list));
        builder.setPositiveButton(getString(R.string.str_replace), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;
                // do selection in album or work list
                if (position >= 0)
                {
                    mCurrFragment.onSetCurrSelection(position); // inform fragment
                }
                enterPlaylist(theSelectedObjects, false);   // change playlist
                onChangePlaylist(false); // inform derived class to stop playing etc.
            }
        });

        boolean bOneIsNotInPlayingList = false;
        for (AudioBaseObject theObject : theSelectedObjects)
        {
            if (!AppGlobals.isInPlayingList(theObject))
            {
                bOneIsNotInPlayingList = true;
                break;
            }
        }
        if (bOneIsNotInPlayingList)
        {
            // only offer "append" when object is not in playlist
            builder.setNeutralButton(getString(R.string.str_append), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    mPendingDialog = null;
                    // do selection in album or work list
                    if (position >= 0)
                    {
                        mCurrFragment.onSetCurrSelection(position); // inform fragment
                    }
                    enterPlaylist(theSelectedObjects, true);   // change playlist
                    onChangePlaylist(true); // inform derived class to stop playing etc.
                }
            });
        }

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;
                Toast.makeText(getApplicationContext(), R.string.str_keep_playlist, Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * if playing list is not empty, ask if to overwrite
     *
     * object can be track, work, album, folder, playlist
     *
     *************************************************************************/
    public void dialogConditionallyOverwriteOrAppendToPlayingList
    (
        final AudioBaseObject theSelectedObject,
        final int position
    )
    {
        final AudioBaseObject[] theSelectedObjects = new AudioBaseObject[] {theSelectedObject};
        dialogConditionallyOverwriteOrAppendToPlayingList(theSelectedObjects, position);
    }


    /**************************************************************************
     *
     * Composer, Performer or Genre was selected, and this dialogue asks
     * if the user wants to branch to albums or works or if he just
     * wanted to set a filter.
     *
     * There are four choices:
     * - go to albums
     * - go to works
     * - do not do anything
     * - do not do anything and do not ask again
     *
     *************************************************************************/
    private void dialogBranchToAlbumsOrWorks()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_branch_to_view));
        //builder.setMessage("You can either continue settings filters or go to albums or works");

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null)
        {
            @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.dlg_filter_branch, null);
            final RadioButton rba = view.findViewById(R.id.gotoAlbums);
            final RadioButton rbw = view.findViewById(R.id.gotoWorks);
            final RadioButton rbn = view.findViewById(R.id.gotoNowhere);
            final CheckBox     cb = view.findViewById(R.id.doNotAskAgain);

            rba.setChecked(rba_checked);
            rbw.setChecked(rbw_checked);
            rbn.setChecked(rbn_checked);
            //cb.setChecked(false);

            builder.setView(view);

            builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface arg0, int arg1)
                {
                    // ask and remember
                    mPendingDialog = null;
                    rba_checked = rba.isChecked();
                    rbw_checked = rbw.isChecked();
                    rbn_checked = rbn.isChecked();
                    if (cb.isChecked())
                    {
                        // switch and do not ask again
                        int val = rbn_checked ? 1 : ((rba_checked) ? 2 : 3);
                        UserSettings.putVal(UserSettings.PREF_BRANCH_MODE, val);
                    }
                    if (rba_checked || rbw_checked)
                    {
                        changeFragment((rba_checked) ? MainFragment.theAlbumFragment : MainFragment.theWorkFragment);
                    }
                }
            });
/*
            builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
            {
        mPendingDialog = null;       // test code
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    // ask and remember
                    rba_checked = rba.isChecked();
                    if (cb.isChecked())
                    {
                        // do nothing and do not ask again
                        UserSettings.putVal(UserSettings.PREF_BRANCH_MODE, 1);
                    }
                }
            });
*/
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            mPendingDialog = alertDialog;       // test code
        }
    }


    /**************************************************************************
     *
     * Composer, Performer or Genre was selected, and this dialogue asks
     * if the user wants to branch to albums or works or if he just
     * wanted to set a filter.
     *
     * There are four choices:
     * - go to albums
     * - go to works
     * - do not do anything
     * - do not do anything and do not ask again
     *
     *************************************************************************/
    public void dialogConditionallyBranchToAlbumsOrWorks()
    {
        int branchMode = UserSettings.getVal(UserSettings.PREF_BRANCH_MODE, 0);
        switch(branchMode)
        {
            case 0:
                // ask
                dialogBranchToAlbumsOrWorks();
                break;

            case 1:
                // do nothing
                break;

            case 2:
                changeFragment(MainFragment.theAlbumFragment);
                break;

            case 3:
                changeFragment(MainFragment.theWorkFragment);
                break;
        }
    }


    /**************************************************************************
     *
     * tell error
     *
     *************************************************************************/
    public void dialogNoAudioFilesInPath()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_cannot_play_folder));
        builder.setMessage(getString(R.string.str_no_audio_files_in_folder));
        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * ask if remove filter. Note that this runs asynchronously, i.e. the
     * function will return while the dialogue is still being displayed.
     *
     *************************************************************************/
    public void dialogRemoveFilters(final boolean isComposerFilter, final boolean isGenreFilter, final boolean isPerformerFilter)
    {
        // count number of filters
        int numFilters = (isComposerFilter ? 1 : 0) + (isGenreFilter ? 1 : 0) + (isPerformerFilter ? 1 : 0);
        CharSequence[] filterItems = new CharSequence[numFilters];
        boolean[] filterChecked = new boolean[numFilters];
        // fill arrays
        int i = 0;
        if (isComposerFilter)
        {
            filterItems[i] = getString(R.string.str_composer_uc);
            filterChecked[i] = false;
            i++;
        }
        if (isGenreFilter)
        {
            filterItems[i] = getString(R.string.str_genre_uc);
            filterChecked[i] = false;
            i++;
        }
        if (isPerformerFilter)
        {
            filterItems[i] = getString(R.string.str_performer_uc);
            filterChecked[i] = false;
        }

        // reset result
        mCheckedItems[0] = false;
        mCheckedItems[1] = false;
        mCheckedItems[2] = false;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_remove_filters));
        /*
        if a message is set, the multi choice items will not be displayed
        alertDialogBuilder.setMessage("Select the filters to be removed...");
        */

        builder.setPositiveButton(R.string.str_ok, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                //Toast.makeText(getApplicationContext(), "Filter ist dann mal wech...", Toast.LENGTH_SHORT).show();
                // check which filters to remove
                int i = 0;
                boolean removeComposerFilter = false;
                if (isComposerFilter)
                {
                    removeComposerFilter = mCheckedItems[i++];
                }
                boolean removeGenreFilter = false;
                if (isGenreFilter)
                {
                    removeGenreFilter = mCheckedItems[i++];
                }
                boolean removePerformerFilter = false;
                if (isPerformerFilter)
                {
                    removePerformerFilter = mCheckedItems[i];
                }
                mCurrFragment.onRemoveFilters(removeComposerFilter, removeGenreFilter, removePerformerFilter);
            }
        });

        builder.setNeutralButton(R.string.str_all, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
                mCurrFragment.onRemoveFilters(isComposerFilter, isGenreFilter, isPerformerFilter);
            }
        });

        builder.setNegativeButton(R.string.str_cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
                //Toast.makeText(getApplicationContext(), "Filter bleibt dann mal...", Toast.LENGTH_SHORT).show();
            }
        });

        // have to use member variable mCheckedItems here, otherwise it cannot be write accessed
        builder.setMultiChoiceItems(filterItems, filterChecked, new DialogInterface.OnMultiChoiceClickListener()
        {
            public void onClick(DialogInterface dialog, int which, boolean isChecked)
            {
                // remember which ones to remove
                mCheckedItems[which] = isChecked;
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * ask if playlist shall be deleted
     *
     *************************************************************************/
    private void dialogAskDeletePlaylist(final AudioPlaylist thePlaylist)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_delete_saved_playlist_1) + thePlaylist.name + getString(R.string.str_question_delete_saved_playlist_2));
        builder.setMessage(getString(R.string.str_message_delete_saved_playlist));
        builder.setPositiveButton(getString(R.string.str_delete), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                Playlists.deletePlaylist(thePlaylist);
                mCurrFragment.forceRedraw();
                updateSpinnerAdapter();     // force spinner redraw (update number of lines)
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
                Toast.makeText(getApplicationContext(), R.string.str_keep_playlist, Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * ask if playlist shall be overwritten
     *
     *************************************************************************/
    private void dialogAskOverwritePlaylist(final String name)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_question_replace_saved_playlist));
        builder.setMessage(getString(R.string.str_message_replace_saved_playlist));
        builder.setPositiveButton(getString(R.string.str_replace), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                Playlists.writePlaylistFromPlayingList(name, true /* overwrite */);
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
                Toast.makeText(getApplicationContext(), R.string.str_keep_playlist, Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * ask for name and save playlist to database
     *
     *************************************************************************/
    private void dialogSavePlaylist()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_playlist_save_dlg_title));

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null)
        {
            @SuppressLint("InflateParams") final View view = inflater.inflate(R.layout.dlg_save_playlist, null);
            builder.setView(view);

            builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface arg0, int arg1)
                {
                    PrivateLog.d(LOG_TAG, "dialogSavePlaylist() : OK");
                    mPendingDialog = null;       // test code
                    final EditText textItem = view.findViewById(R.id.edit_playlist_name);
                    final String name = textItem.getText().toString();
                    if (!name.isEmpty())
                    {
                        PrivateLog.d(LOG_TAG, "dialogSavePlaylist() : save playlist " + name);
                        boolean bDone = Playlists.writePlaylistFromPlayingList(name, false /* do not overwrite */);
                        if (!bDone)
                        {
                            // name already exists. Ask for overwrite confirmation
                            dialogAskOverwritePlaylist(name);
                        }
                    }
                }
            });

            builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    mPendingDialog = null;       // test code
                    PrivateLog.d(LOG_TAG, "dialogSavePlaylist() : Cancel");
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            mPendingDialog = alertDialog;       // test code
        }
    }


    /**************************************************************************
     *
     * info dialogue: helper to get the title text
     *
     *************************************************************************/
    private String dialogInfoGetTitle(final AudioBaseObject theAudioObject)
    {
        String theTitle;

        if (theAudioObject instanceof AudioTrack)
        {
            theTitle = getString(R.string.str_track_info);
        }
        else
        if (theAudioObject instanceof AudioWork)
        {
            theTitle = getString(R.string.str_work_info);
        }
        else
        if (theAudioObject instanceof AudioAlbum)
        {
            theTitle = getString(R.string.str_album_info);
        }
        else
        {
            theTitle = "ERROR: unsupported object";
        }

        return theTitle;
    }


    /**************************************************************************
     *
     * helper to get info dialogue message text for AudioTrack
     *
     *************************************************************************/
    private String dialogInfoGetMessageTextForAudioTrack(final AudioBaseObject theAudioObject)
    {
        String thePath = theAudioObject.getRealPath(this);
        if (thePath == null)
        {
            return "ERROR: invalid path";
        }

        String theMessage;
        /*
        AudioFileInfo audioInfo = new AudioFileInfo(thePath);
        AudioFileInfo.Info info = audioInfo.getInfo();
        if (info != null)
        {
            theMessage = info.format;
            String strLocChannels = (info.channels < 2) ? getString(R.string.str_audiochannel) : getString(R.string.str_audiochannels);
            if (info.lossless)
            {
                theMessage += " (" + getString(R.string.str_lossless) + ")";
            }
            theMessage += "\n" + info.bitRateInKbitPerSecond + " " + getString(R.string.str_kbits_per_sec);
            theMessage += "\n" + info.bitsPerSample + " " + getString(R.string.str_bits_per_sample);
            theMessage += "\n" + info.strChannels + " (" + info.channels + " " + strLocChannels + ")";
            theMessage += "\n" + info.durationInSeconds + " s";
            theMessage += "\n\n" + info.absPath;
        }
        else
        */
        {
            int pos = thePath.lastIndexOf('.');
            if (pos >= 0)
            {
                theMessage = "format: " + thePath.substring(pos + 1);
            }
            else
            {
                theMessage = "unknown format";
            }
            theMessage += "\n" + "duration: " + (theAudioObject.duration / 1000) + " s";
            theMessage += "\n\n" + thePath;
        }

        return theMessage;
    }


    /**************************************************************************
     *
     * helper to get info dialogue message text for AudioWork
     *
     *************************************************************************/
    private String dialogInfoGetMessageTextForAudioWork(final AudioBaseObject theAudioObject)
    {
        String theMessage;
        String thePath = theAudioObject.getRealPath(this);

        AudioWork theWork = (AudioWork) theAudioObject;
        theMessage = theWork.getComposerName(true) + ":" + "\n"
                + theWork.getName() + "\n"
                + "(" + theWork.getPerformerName(true) + ")" + "\n"
                + theWork.no_of_tracks + " " + getString(R.string.str_parts) + "\n"
                + getString(R.string.str_album) + ": " + theWork.getAlbumName() + "\n"
                + theWork.getDurationAsString()
                + "\n\n" + thePath;

        return theMessage;
    }


    /**************************************************************************
     *
     * helper to get info dialogue message text for AudioAlbum
     *
     *************************************************************************/
    private String dialogInfoGetMessageTextForAudioAlbum(final AudioBaseObject theAudioObject)
    {
        String theMessage;
        String thePath = theAudioObject.getRealPath(this);

        AudioAlbum theAlbum = (AudioAlbum) theAudioObject;
        String artist = theAlbum.getAlbumArtist();
        if (!artist.isEmpty())
        {
            artist = "(" + artist + ")" + "\n";
        }
        String composer = theAlbum.getComposerName(false);
        if (!composer.isEmpty())
        {
            composer += ":" + "\n";
        }
        theMessage = composer
                + theAlbum.getTitle() + "\n"
                + artist
                + theAlbum.no_of_tracks + " " + getString(R.string.str_parts) + "\n"
                + theAlbum.getDurationAsString()
                + "\n\n" + thePath;

        return theMessage;
    }


    /**************************************************************************
     *
     * info dialogue: helper to get the message text
     *
     *************************************************************************/
    private String dialogInfoGetMessageText(final AudioBaseObject theAudioObject)
    {
        if (theAudioObject instanceof AudioTrack)  // instanceof includes null pointer check
        {
            return dialogInfoGetMessageTextForAudioTrack(theAudioObject);
        }
        else
        if (theAudioObject instanceof AudioWork)
        {
            return dialogInfoGetMessageTextForAudioWork(theAudioObject);
        }
        else
        if (theAudioObject instanceof AudioAlbum)
        {
            return dialogInfoGetMessageTextForAudioAlbum(theAudioObject);
        }
        else
        {
            return "";
        }
    }


    /**************************************************************************
     *
     * show detailed track info
     *
     *************************************************************************/
    private void dialogShowInfo(final AudioBaseObject theAudioObject)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(dialogInfoGetTitle(theAudioObject));
        builder.setMessage(dialogInfoGetMessageText(theAudioObject));

        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                mPendingDialog = null;       // test code
                PrivateLog.d(LOG_TAG, "dialogShowInfo() : OK");
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * show fatal error message for Android bug
     *
     *************************************************************************/
    private void dialogShowAndroidOsAccessGrantBug()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_android_os_bug));
        builder.setMessage(getString(R.string.str_android_os_bug_message));

        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                restart();
            }
        });
        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;       // test code
                Toast.makeText(getApplicationContext(), getString(R.string.str_android_os_bug_warn), Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * to be overwritten by child class
     *
     *************************************************************************/
    protected void dialogUseBookmarkResult(boolean bUseBookmark)
    {
    }


    /**************************************************************************
     *
     * Show alert asking for using a bookmark
     *
     *************************************************************************/
    protected void dialogUseBookmark()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_execute_bookmark);
        builder.setMessage(R.string.str_execute_bookmark_explanation);

        // start with first bookmarked track at bookmarked position
        builder.setPositiveButton(R.string.str_bookmark_jumpto, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;
                dialogUseBookmarkResult(true);
            }
        });
        // remove bookmarks and start at beginning of first track
        builder.setNegativeButton(R.string.str_bookmark_ignore, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                mPendingDialog = null;
                PlayListHandler.removeAllBookmarks();
                dialogUseBookmarkResult(false);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        mPendingDialog = alertDialog;       // test code
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * Note that keys are sent to MyMediaController first, in case it is
     * visible. BACK, MENU and SEARCH are always forwarded from MyMediaController
     * to here, the others may be processed by MediaController. It seems
     * that MediaController interprets STOP as PAUSE, so the program might
     * behave differently, depending on MediaController is currently
     * visible or not.
     *
     *************************************************************************/
    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event != null)      // got some inexplicable null pointer exceptions in this function
        {
            int keyCode = event.getKeyCode();
            int actionCode = event.getAction();
            PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : keyCode = " + keyCode);

            switch(keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if (actionCode == KeyEvent.ACTION_UP)
                    {
                        if ((mCurrFragment != null) && (mCurrFragment.onKeyBack()))
                        {
                            // event was consumed
                            return true;
                        }
                        dialogEndApp();
                        return true; // event was consumed
                    }
                    break;

                // reserved for future use
                case KeyEvent.KEYCODE_MENU:
                    break;

                case KeyEvent.KEYCODE_SEARCH:
                    if (actionCode == KeyEvent.ACTION_UP)
                    {
                        if (mSearchView != null)
                        {
                            // show search view
                            mSearchView.setIconified(false);
                            return true;    // event was consumed
                        }
                    }
                    break;

                //
                // we can ignore these here and handle them in media session callback instead
                //

                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_PLAY_PAUSE");
                    break;

                case KeyEvent.KEYCODE_MEDIA_STOP:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_STOP");
                    break;

                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_NEXT");
                    break;

                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_PREVIOUS");
                    break;

                case KeyEvent.KEYCODE_MEDIA_REWIND:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_REWIND");
                    break;

                case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_FAST_FORWARD");
                    break;

                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_PLAY");
                    if (actionCode == KeyEvent.ACTION_UP)
                    {
                        onKeyCodeMediaPlay();
                        return true;    // event was consumed
                    }
                    break;

                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    PrivateLog.d(LOG_TAG, "dispatchKeyEvent() : KEYCODE_MEDIA_PAUSE");
                    break;
            }

            return super.dispatchKeyEvent(event);
        }

        return true; // null event was consumed
    }


    /**********************************************************************************************
     *
     *  number of items in a list has changed
     *
     **********************************************************************************************/
    public void updateSpinnerAdapter()
    {
        // force a redraw of the spinner to show the number of albums and titles
        if (mSpinnerAdapter != null)
        {
            mSpinnerAdapter.notifyDataSetChanged();
        }
    }


    /**********************************************************************************************
     *
     *  init progress bar (in fact a circle) for background initialisation
     *
     **********************************************************************************************/
    private void startProgressBar()
    {
        if (mProgressTimerTask == null)
        {
            mProgressTimerTask = new MyProgressTimerTask();
            mProgressTimer = new Timer();
            mProgressTimer.schedule(mProgressTimerTask, 5, 50);
        }
        mBusyIndicatorProgressBar.setProgress(0);
        mBusyIndicatorProgressBar.setVisibility(View.VISIBLE);
    }


    /**********************************************************************************************
     * The three type arguments are (use "Void" if not used):
     *  Params
     *  Progress
     *  Result
     **********************************************************************************************/
    @SuppressLint("StaticFieldLeak")
    private class BackgroundInitialisation extends AsyncTask<Context, Void, String>
    {
        @Override
        protected String doInBackground(Context... params)
        {
//            PrivateLog.d("BackgroundInit", "doInBackground() : load all tracks");
            AppGlobals.loadAllTracksTable();
            int nTracks = AppGlobals.getAudioTrackListSize();
            // also initialize other tables here, to get information needed
//            PrivateLog.d("BackgroundInit", "doInBackground() : init filter lists");
            AppGlobals.initAudioComposersPerformersGenreWorkFolderList(/*params[0] NOT NEEDED ANY MORE */);
            AppGlobals.buildComposerAliasesMap();
            Playlists.initAudioPlaylistsList();
            AudioFilters.updateMatchingWorks();    // for composer, filter and genre init no_of_matching_works
            return nTracks + getString(R.string.str_tracks_loaded);
        }

        /*
         * Called in UI thread after background thread has ended
         */
        @Override
        protected void onPostExecute(String result)
        {
            mbBackgroundInitialisationIsRunning = false;
            AppGlobals.bBackgroundInitialisationDone = true;

            if (mProgressTimerTask != null)
            {
                mProgressTimer.cancel();
                mProgressTimer = null;
                mProgressTimerTask.cancel();
                mProgressTimerTask = null;
            }
            mBusyIndicatorProgressBar.setVisibility(View.GONE);
            mBusyIndicatorProgressBar.setProgress(0);

            theToast.cancel();
            theToast = Toast.makeText(getApplicationContext(), getString(R.string.str_dots_done) + result, Toast.LENGTH_SHORT);
            theToast.show();

            updateSpinnerAdapter();

            boolean bRedraw = false;

            // TODO: remove albums that are empty due to path filters
            //if (AppGlobals.sNumOfFilteredFiles > 0)   // does only work for first start, when cache db is built
            if (!AppGlobals.bOwnDatabaseIsActive)
            {
                ArrayList<AudioBaseObject> list = AudioFilters.audioAlbumList.raw.list;
                int i = 0;
                int nAlbumsRemoved = 0;
                while (i < list.size())
                {
                    AudioBaseObject theObject = list.get(i);
                    AudioAlbum theAlbum = (AudioAlbum) theObject;
                    if (theAlbum.isEmpty())
                    {
                        list.remove(i);
                        nAlbumsRemoved++;
                    }
                    else
                    {
                        i++;
                    }
                }
                if (nAlbumsRemoved > 0)
                {
                    final String text = nAlbumsRemoved + getString(R.string.str_empty_albums_removed);
                    theToast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
                    theToast.show();
                    bRedraw = true;
                }
            }

            if (AppGlobals.sVirtualAlbumsList != null)
            {
                // There were virtual albums added for orphaned audio files during background initialisation.
                // Now add these to album list and force Adapter rebuild.

                int numVirtualAlbums = AppGlobals.sVirtualAlbumsList.size();
                int numOrphans = 0;
                // get number of orphaned files
                for (int i = 0; i < numVirtualAlbums; i++)
                {
                    AudioAlbum theAlbum = (AudioAlbum) AppGlobals.sVirtualAlbumsList.get(i);
                    numOrphans += theAlbum.no_of_tracks;
                }
                final String text = numOrphans + getString(R.string.str_orphaned_files_sorted_to) + numVirtualAlbums + getString(R.string.str_to_virtual_albums);
                theToast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG);
                theToast.show();

                // append virtual albums to "normal" ones
                AudioFilters.audioAlbumList.raw.list.addAll(AppGlobals.sVirtualAlbumsList);
                AppGlobals.sVirtualAlbumsList = null;
                bRedraw = true;
            }

            if (bRedraw && (mCurrFragment != null))
            {
                // rebuild Adapter, mainly notifyDataSetChanged()
                mCurrFragment.forceRedraw();
            }

            if (AppGlobals.getBackgroundInitialisationFailedDueToAndroidBug())
            {
                dialogShowAndroidOsAccessGrantBug();
            }

            // handle parameters ("share" or "open with...")
            handleIntent();
        }

        /*
         * Called in UI thread before background thread is started
         */
        @Override
        protected void onPreExecute()
        {
            //initProgressCountdown = 5;
            //initProgressReload = 7;

            AppGlobals.bBackgroundInitialisationDone = false;
            mbBackgroundInitialisationIsRunning = true;
            startProgressBar();
            theToast = Toast.makeText(getApplicationContext(), R.string.str_loading_dots, Toast.LENGTH_LONG);
            theToast.show();
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }


    /**********************************************************************************************
     *
     * to be overridden
     *
     *********************************************************************************************/
    public void AboutToRecreate()
    {
        // should be overridden...
    }


    /**********************************************************************************************
     *
     * timer callback is run once per second, to be overwritten by child class
     *
     *********************************************************************************************/
    public void TimerCallback()
    {
        /* HACK FOR ANDROID BUG. NOT NECESSARY WITH ANDROID 7.1 AND 8.1 */
        if (mbMustRecreate)
        {
            PrivateLog.d(LOG_TAG, "onResume() -- recreate");
            recreate();
        }
    }


    /**********************************************************************************************
     *
     * timer callback for progress circle
     *
     *********************************************************************************************/
    private void ProgressTimerCallback()
    {
        if (mbBackgroundInitialisationIsRunning)
        {
            // update progress bar
            /*
            int numOfTracksTotal = AppGlobals.getBackGroundInitialisationNumberOfTracksTotal();
            int numOfTracksDone = AppGlobals.getBackGroundInitialisationNumberOfTracksDone();
            //mBusyIndicatorProgressBar.setIndeterminate(false);
            //Log.d(LOG_TAG, "progress, max = " + numOfTracksTotal + ", done = " + numOfTracksDone);
            mBusyIndicatorProgressBar.setMax(numOfTracksTotal);
            mBusyIndicatorProgressBar.setProgress(numOfTracksDone);
            */
            mBusyIndicatorProgressBar.setProgress(AppGlobals.getBackGroundInitialisationProgressPercent());
            //Log.d(LOG_TAG, "progress [%] = " + AppGlobals.getBackGroundInitialisationProgressPercent());

            /*
            // update toast
            initProgressCountdown--;
            if (initProgressCountdown == 0)
            {
                initProgressCountdown = initProgressReload;
                int total = AppGlobals.getBackGroundInitialisationNumberOfTracksTotal();
                int done = AppGlobals.getBackGroundInitialisationNumberOfTracksDone();
                if (done < total)
                {
                    theToast.cancel();
                    String theText = "" + done + "/" + total;
                    theToast = Toast.makeText(getApplicationContext(), theText + getString(R.string.str_tracks_loaded), Toast.LENGTH_SHORT);
                    theToast.show();
                }
            }
            */

            if (AppGlobals.bBackgroundInitialisationDone)
            {
                // this is needed in case the device was rotated and the TimerTask is connected
                // to an older instance of the activity
                mbBackgroundInitialisationIsRunning = false;
                mBusyIndicatorProgressBar.setVisibility(View.GONE);
            }
        }
    }


    // timer for progress circle, higher frequency
    class MyProgressTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(MainBasicActivity.this::ProgressTimerCallback);
        }
    }

    // general UI timer, once per second
    class MyTimerTask extends TimerTask
    {
        @Override
        public void run()
        {
            runOnUiThread(MainBasicActivity.this::TimerCallback);
        }
    }
}
