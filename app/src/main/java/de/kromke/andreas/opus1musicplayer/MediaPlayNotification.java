/*
 * Copyright (C) 2016-2023 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;


import static androidx.core.app.NotificationCompat.CATEGORY_TRANSPORT;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.media.session.MediaSession;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;


@SuppressWarnings({"WeakerAccess", "JavadocBlankLines", "JavadocLinkAsPlainText"})
public class MediaPlayNotification
{
    private static final String LOG_TAG = "O1M : MediaPlayNotif";

    public final static String NOTIFICATION_MAIN_ACTION = "de.kromke.andreas.opus1musicplayer.customnotification.action.main";
    public final static String NOTIFICATION_PREV_ACTION = "de.kromke.andreas.opus1musicplayer.customnotification.action.prev";
    public final static String NOTIFICATION_PLAY_ACTION = "de.kromke.andreas.opus1musicplayer.customnotification.action.play";
    public final static String NOTIFICATION_NEXT_ACTION = "de.kromke.andreas.opus1musicplayer.customnotification.action.next";
    public final static String NOTIFICATION_STOP_ACTION = "de.kromke.andreas.opus1musicplayer.customnotification.action.stop";

    public final static String NOTIFICATION_TRACK_ARRAY_NO = "de.kromke.andreas.opus1musicplayer.customnotification.track_array_no";
    public final static String NOTIFICATION_TRACK_ID = "de.kromke.andreas.opus1musicplayer.customnotification.track_id";

    private final PendingIntent mPendingNotificationIntent;
    private final static String NOTIFICATION_CHANNEL_ID = "de.kromke.andreas.opus1musicplayer";
    private final static String channelName = "Playback Service";
    private final static int MY_NOTIFICATION_ID = 520;      // arbitrary number, as we only have on at a time
    private final NotificationManager mNotificationManager;
    private RemoteViews mCustomBigViewForAndroid6;          // only needed for Android 6 and older



    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    static int getPendingFlags()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            return PendingIntent.FLAG_UPDATE_CURRENT + PendingIntent.FLAG_IMMUTABLE;
        }
        else
        {
            return PendingIntent.FLAG_UPDATE_CURRENT;
        }
    }


    /**************************************************************************
     *
     * constructor
     *
     *************************************************************************/
    @SuppressLint("UnspecifiedImmutableFlag")
    MediaPlayNotification
    (
        Context context,
        @SuppressWarnings("rawtypes") Class theClass
    )
    {
        //
        // Create pendingIntent for later use.
        // Note that this intent will be sent to the activity, not to us
        //
        Intent notificationIntent = new Intent(context, theClass);
        notificationIntent.setAction(NOTIFICATION_MAIN_ACTION);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mPendingNotificationIntent = PendingIntent.getActivity(
                context, 0, notificationIntent, getPendingFlags());
        //
        // The Notification Manager is needed to create a Notification Channel and
        // to update existing notifications.
        //
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    /**************************************************************************
     *
     * adds essential necessary information to intent, so that the
     * handler can get any needed information to recover from
     * process restart, ie. loss of any global or static variables.
     *
     *************************************************************************/
    @SuppressLint("UnspecifiedImmutableFlag")
    private PendingIntent createMediaPlayIntent
    (
        final String action,
        int currTrackArrayNumber,
        long currTrackId,
        Context context
    )
    {
        Intent theIntent = new Intent(context, MediaPlayService.class);
        theIntent.setAction(action);
        theIntent.putExtra(NOTIFICATION_TRACK_ARRAY_NO, currTrackArrayNumber);
        theIntent.putExtra(NOTIFICATION_TRACK_ID, currTrackId);
        // for whatever reason PendingIntent.FLAG_UPDATE_CURRENT is needed to pass extra parameters, otherwise they are ignored
        return PendingIntent.getService(context, 0, theIntent, getPendingFlags());
    }


    /**************************************************************************
     *
     * add action to notification builder
     *
     *************************************************************************/
    @TargetApi(23)
    private void addAction
    (
        Notification.Builder builder,
        int resId,
        final String title,     // no idea what this is used for
        final String action,
        int currTrackArrayNumber,
        long currTrackId,
        Context context
    )
    {
        Icon icon = Icon.createWithResource(context, resId);
        Notification.Action theAction = new Notification.Action.Builder(icon, title,
                createMediaPlayIntent(action, currTrackArrayNumber, currTrackId, context)).build();
        builder.addAction(theAction);
    }


    /**************************************************************************
     *
     * add Media Style to notification builder
     *
     *************************************************************************/
    private void addMediaStyle
    (
        Notification.Builder builder,
        boolean isPlay,
        int currTrackArrayNumber,
        long currTrackId,
        Drawable albumImageDrawable,
        MediaSession mediaSession,
        Context context
    )
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            //builder.setSmallIcon(R.drawable.app_icon_noborder);
            if (albumImageDrawable != null)
            {
                builder.setLargeIcon(((BitmapDrawable) albumImageDrawable).getBitmap());
            }
            // On Android 6 the notification will be tinted in an ugly dark gray.
            // Does not happen with Android 7
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M)
            {
                builder.setColor(0x9e9e9e);
            }
            Notification.MediaStyle mediaStyle = new Notification.MediaStyle();
            mediaStyle.setShowActionsInCompactView(1);

            builder.setStyle(mediaStyle);
            if (mediaSession != null)
            {
                builder.setStyle(new Notification.MediaStyle().setMediaSession(mediaSession.getSessionToken()));
            }

            int playPauseId = (!isPlay) ?
                    R.drawable.media_notification_play :
                    R.drawable.media_notification_pause;
            addAction(builder, R.drawable.media_notification_prev,  "PREV", NOTIFICATION_PREV_ACTION, currTrackArrayNumber, currTrackId, context);
            addAction(builder, playPauseId,                         "PLAY", NOTIFICATION_PLAY_ACTION, currTrackArrayNumber, currTrackId, context);
            addAction(builder, R.drawable.media_notification_next,  "NEXT", NOTIFICATION_NEXT_ACTION, currTrackArrayNumber, currTrackId, context);
            addAction(builder, R.drawable.media_notification_close, "STOP", NOTIFICATION_STOP_ACTION, currTrackArrayNumber, currTrackId, context);
        }
    }


    /**************************************************************************
     *
     * Android >= 8.1 needs notification channels
     *
     *************************************************************************/
    private Notification.Builder createNotificationBuilder(Context context)
    {
        Notification.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            builder = new Notification.Builder(context, NOTIFICATION_CHANNEL_ID);
            // note that IMPORTANCE_NONE works fine on any device except Samsung. Damn!
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_LOW);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            if (mNotificationManager != null)
            {
                mNotificationManager.createNotificationChannel(chan);
            }
            else
            {
                Log.e(LOG_TAG, "no Notification Manager");
            }
        }
        else
        {
            // deprecated, but needed for older APIs
            builder = new Notification.Builder(context);
        }

        return builder;
    }


    /**************************************************************************
     *
     * create a generic (non custom) Notification Builder
     *
     * Note that with API 33 (Android 13) for Media Notifications:
     *
     *  setSmallIcon() shows the icon at the upper left corner, but not in status bar
     *  setTicker(), setContentTitle(), setContent(Sub)Text() are ignored
     *
     * For API 33 Media Style Notifications some information is instead taken
     * from Media Session Metadata:
     *
     *  METADATA_KEY_DISPLAY_TITLE is preferred to
     *  METADATA_KEY_TITLE for the track name
     *  METADATA_KEY_ARTIST is shown
     *  METADATA_KEY_ALBUM is ignored
     *  METADATA_KEY_COMPOSER is ignored
     *  everything else is ignored
     *
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    private Notification.Builder createGenericNotificationBuilder
    (
        boolean isPlay,
        boolean isMediaStyle,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        long currTrackId,
        Drawable albumImageDrawable,
        MediaSession mediaSession,
        Context context
    )
    {
        Notification.Builder builder = createNotificationBuilder(context);
        int playPauseIcon = (isPlay) ?
                R.drawable.ic_play_notification :
                R.drawable.ic_pause_notification;
        if (Build.VERSION.SDK_INT >= 33)
        {
            if (isMediaStyle)
            {
                playPauseIcon = R.drawable.app_icon_noborder_white;
            }
        }

        builder.setContentIntent(mPendingNotificationIntent)    // intent to be sent when the notification is clicked on
                .setSmallIcon(playPauseIcon)                    // MANDATORY: used for menu bar and, with Android N, inside notification
                .setTicker(audioInformationSecondLine)          // (for accessibility services or ignored)
                .setOngoing(true)                               // notification cannot be dismissed
                .setShowWhen(false)                             // do not show time of the day when notification was sent
                .setContentTitle(audioInformationFirstLine)     // "composer:", ignored in Android 13
                .setContentText(audioInformationSecondLine)     // work, ignored in Android 13
                .setSubText(audioInformationThirdLine);         // movement, ignored in Android 13
        if (Build.VERSION.SDK_INT > 20)
        {
            // show notification on lock screen
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            // seems to be totally ignored in Android 10
            builder.setColor(UserSettings.getThemedColourFromResId(context, R.attr.colorAccent));
        }

        if (isMediaStyle)
        {
            // Media Style (extended generic)
            addMediaStyle(builder, isPlay, currTrackArrayNumber, currTrackId, albumImageDrawable, mediaSession, context);
        }

        return builder;
    }


    /************************************************************************************
     *
     * helper to handle API differences (Android 7 or older)
     *
     * @noinspection RedundantSuppression
     ***********************************************************************************/
    @SuppressWarnings("deprecation")
    private Notification.Builder createNotificationBuilderWithCustomViews
    (
        Context context,
        RemoteViews customSmallView,
        RemoteViews customBigView
    )
    {
        Notification.Builder builder = createNotificationBuilder(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            builder.setCustomContentView(customSmallView);
            if (customBigView != null)
            {
                builder.setCustomBigContentView(customBigView);
            }
        }
        else
        {
            // Deprecated, but needed for Android 6 and older.
            // Ignore big view here, will be added later to notification.
            builder.setContent(customSmallView);
        }

        return builder;
    }


    /**************************************************************************
     *
     * helper for compact notification
     *
     *************************************************************************/
    private int getCompactViewPlayPauseIcon(boolean isPlay)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
        {
            // Notifications in Android 12 and newer have large white margins and
            // thus less space for content
            return (isPlay) ? R.drawable.ic_pause_82px : R.drawable.ic_play_82px;
        }
        else
        {
            return (isPlay) ? R.drawable.ic_pause_circle_outline_black115_24dp : R.drawable.ic_play_circle_outline_black115_24dp;
        }
    }


    /**************************************************************************
     *
     * helper for custom notification
     *
     *************************************************************************/
    private int getCustomSmallViewLayout(boolean bUseCompactNotification)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
        {
            // Notifications in Android 12 and newer have large white margins and
            // thus less space for content
            return (bUseCompactNotification) ? R.layout.notification_compact_api33 : R.layout.notification_api33;
        }
        else
        {
            return (bUseCompactNotification) ? R.layout.notification_compact : R.layout.notification;
        }
    }


    /**************************************************************************
     *
     * helper for custom notifications
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    private RemoteViews createCustomSmallView
    (
        boolean isPlay,
        boolean bUseCompactNotification,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        Context context,
        Bitmap theAlbumBitmap,
        PendingIntent ppreviousIntent,
        PendingIntent pplayIntent,
        PendingIntent pnextIntent,
        PendingIntent pcloseIntent
    )
    {
        RemoteViews customSmallView = new RemoteViews(context.getPackageName(), getCustomSmallViewLayout(bUseCompactNotification));

        // showing album image or default album image
        if (theAlbumBitmap != null)
        {
            customSmallView.setImageViewBitmap(R.id.album_art, theAlbumBitmap);
        }

        // showing play or pause, Note that !isPlay means status pause, so show play icon and vice versa
        int playPauseId = (isPlay) ? R.drawable.notification_pause : R.drawable.notification_play;
        if (bUseCompactNotification)
        {
            customSmallView.setImageViewResource(R.id.button_play_pause, getCompactViewPlayPauseIcon(isPlay));
        } else
        {
            customSmallView.setImageViewResource(R.id.button_play_pause, playPauseId);
        }

        // three text lines

        customSmallView.setTextViewText(R.id.composer_name, audioInformationFirstLine);
        customSmallView.setTextViewText(R.id.work_name, audioInformationSecondLine);
        customSmallView.setTextViewText(R.id.track_name, audioInformationThirdLine);

        // HACK. No idea why text colour is white by default
        if (Build.VERSION.SDK_INT > 20)
        {
            customSmallView.setTextColor(R.id.composer_name, Color.BLACK);
            customSmallView.setTextColor(R.id.work_name, Color.DKGRAY);
            customSmallView.setTextColor(R.id.track_name, Color.GRAY);
        }

        customSmallView.setOnClickPendingIntent(R.id.button_play_pause, pplayIntent);
        if (!bUseCompactNotification)
        {
            customSmallView.setOnClickPendingIntent(R.id.button_next, pnextIntent);
            customSmallView.setOnClickPendingIntent(R.id.button_prev, ppreviousIntent);
            customSmallView.setOnClickPendingIntent(R.id.button_close, pcloseIntent);
        }

        return customSmallView;
    }


    /**************************************************************************
     *
     * helper for custom notifications
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    private RemoteViews createCustomBigView
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        Context context,
        Bitmap theAlbumBitmap,
        PendingIntent ppreviousIntent,
        PendingIntent pplayIntent,
        PendingIntent pnextIntent,
        PendingIntent pcloseIntent
    )
    {
        RemoteViews customBigView = new RemoteViews(context.getPackageName(), R.layout.notification_expanded);
        if (theAlbumBitmap != null)
        {
            customBigView.setImageViewBitmap(R.id.album_art, theAlbumBitmap);
        }
        // showing play or pause, Note that !isPlay means status pause, so show play icon and vice versa
        int playPauseId = (isPlay) ? R.drawable.notification_pause : R.drawable.notification_play;
        customBigView.setImageViewResource(R.id.button_play_pause, playPauseId);
        customBigView.setTextViewText(R.id.composer_name, audioInformationFirstLine);
        customBigView.setTextViewText(R.id.work_name, audioInformationSecondLine);
        customBigView.setTextViewText(R.id.track_name, audioInformationThirdLine);

        // HACK. No idea why text colour is white by default
        if (Build.VERSION.SDK_INT > 20)
        {
            customBigView.setTextColor(R.id.composer_name, Color.BLACK);
            customBigView.setTextColor(R.id.work_name, Color.BLACK);
            customBigView.setTextColor(R.id.track_name, Color.BLACK);
        }

        customBigView.setOnClickPendingIntent(R.id.button_play_pause, pplayIntent);
        customBigView.setOnClickPendingIntent(R.id.button_next, pnextIntent);
        customBigView.setOnClickPendingIntent(R.id.button_prev, ppreviousIntent);
        customBigView.setOnClickPendingIntent(R.id.button_close, pcloseIntent);

        return customBigView;
    }


    /**************************************************************************
     *
     * create a compact or verbose notification
     *
     * As side effect sets mCustomBigViewForAndroid6 if appropriate
     *
     * read about colours of material design icons:
     * http://stackoverflow.com/questions/30544657/material-design-icon-colors
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    private Notification.Builder createCustomNotificationBuilder
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        long currTrackId,
        Drawable albumImageDrawable,
        boolean bUseCompactNotification,
        boolean bNoBigView,
        Context context
    )
    {
        // intents sent to service, not to activity

        PendingIntent ppreviousIntent = createMediaPlayIntent(NOTIFICATION_PREV_ACTION, currTrackArrayNumber, currTrackId, context);
        PendingIntent pplayIntent     = createMediaPlayIntent(NOTIFICATION_PLAY_ACTION, currTrackArrayNumber, currTrackId, context);
        PendingIntent pnextIntent     = createMediaPlayIntent(NOTIFICATION_NEXT_ACTION, currTrackArrayNumber, currTrackId, context);
        PendingIntent pcloseIntent    = createMediaPlayIntent(NOTIFICATION_STOP_ACTION, currTrackArrayNumber, currTrackId, context);

        Bitmap theAlbumBitmap;
        if ((albumImageDrawable != null) && !(bUseCompactNotification && bNoBigView))
        {
            theAlbumBitmap = ((BitmapDrawable) albumImageDrawable).getBitmap();
        }
        else
        {
            theAlbumBitmap = null;
        }

        // Using RemoteViews to bind custom layouts into Notification

        //
        // create small view
        //

        RemoteViews customSmallView = createCustomSmallView(
                    isPlay,
                    bUseCompactNotification,
                    audioInformationFirstLine,
                    audioInformationSecondLine,
                    audioInformationThirdLine,
                    context,
                    theAlbumBitmap,
                    ppreviousIntent,
                    pplayIntent,
                    pnextIntent,
                    pcloseIntent);

        //
        // create big view
        //

        RemoteViews customBigView;
        if (bNoBigView)
        {
            customBigView = null;
        }
        else
        {
            customBigView = createCustomBigView(
                                    isPlay,
                                    audioInformationFirstLine,
                                    audioInformationSecondLine,
                                    audioInformationThirdLine,
                                    context,
                                    theAlbumBitmap,
                                    ppreviousIntent,
                                    pplayIntent,
                                    pnextIntent,
                                    pcloseIntent);
        }

        // Note that big view is ignored here on Android 6 and older, but will be added
        // later, see below.
        Notification.Builder builder = createNotificationBuilderWithCustomViews(
                                                context, customSmallView, customBigView);

        builder.setContentIntent(mPendingNotificationIntent);
        // note that beginning with Android 5 this icon should be nothing but white:
        builder.setSmallIcon((isPlay) ? R.drawable.play : R.drawable.pause);
        /*
        // does this change anything?
        builder.setLargeIcon(theAlbumBitmap);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            builder.setStyle(new Notification.DecoratedCustomViewStyle());
        }
        */


        if (Build.VERSION.SDK_INT > 20)
        {
            // show notification on lock screen
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
        {
            // For Android N this had been done in builder.
            // Deprecated, but needed for Android 6 and older
            mCustomBigViewForAndroid6 = customBigView;     // may be null
        }

        return builder;
    }


    /**************************************************************************
     *
     * Create generic or custom Notification Builder that can be used
     * to create or update notifications.
     *
     * notification style:
     *
     *  0 = compact
     *  1 = system
     *  2 = verbose
     *  3 = compact small (developer)
     *  4 = verbose small (developer)
     *  5 = system (media)
     *
     *************************************************************************/
    private Notification.Builder createMyNotificationBuilder
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        long currTrackId,
        Drawable albumImageDrawable,        // maybe null
        MediaSession mediaSession,
        Context context
    )
    {
        Notification.Builder theNotificationBuilder;      // to create and update notifications
        int notificationStyle = UserSettings.getVal(UserSettings.PREF_NOTIFICATION_STYLE, 0);

        if ((notificationStyle == 1) || (notificationStyle == 5))
        {
            // generic notification
            theNotificationBuilder = createGenericNotificationBuilder(
                    isPlay,
                    (notificationStyle == 5),       // Media Style (extended generic)
                    audioInformationFirstLine,
                    audioInformationSecondLine,
                    audioInformationThirdLine,
                    currTrackArrayNumber,
                    currTrackId,
                    albumImageDrawable,
                    mediaSession,
                    context);
        }
        else
        {
            boolean bNoBigView = false;     // for debugging
            boolean bCompact;

            if (notificationStyle == 0)
            {
                bCompact = true;
            }
            else
            if (notificationStyle == 3)
            {
                bCompact = true;
                bNoBigView = true;
            }
            else
            if (notificationStyle == 4)
            {
                bCompact = false;
                bNoBigView = true;
            }
            else
            {
                bCompact = false;
            }

            theNotificationBuilder = createCustomNotificationBuilder(
                    isPlay,
                    audioInformationFirstLine,
                    audioInformationSecondLine,
                    audioInformationThirdLine,
                    currTrackArrayNumber,
                    currTrackId,
                    albumImageDrawable,
                    bCompact, bNoBigView, context);
        }

        return theNotificationBuilder;
    }


    /**************************************************************************
     *
     * set notification or update current one, if any
     *
     *************************************************************************/
    public void removeNotification()
    {
        if (mNotificationManager != null)
        {
            mNotificationManager.cancel(MY_NOTIFICATION_ID);
        }
        else
        {
            Log.e(LOG_TAG, "no Notification Manager");
        }
    }


    /**************************************************************************
     *
     * set notification or update current one, if any
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    public void createAndShowNotification
    (
        boolean isPlay,
        final String audioInformationFirstLine,
        final String audioInformationSecondLine,
        final String audioInformationThirdLine,
        int currTrackArrayNumber,
        long currTrackId,
        Drawable albumImageDrawable,        // maybe null
        MediaSession mediaSession,
        Context context
    )
    {
        // Theoretically the Notification Builder could be created once and
        // updated accordingly when audio information or play state changes.
        // But this would be highly complicated, thus we rebuild the builder
        // whenever the notification is requested.

        Notification.Builder theNotificationBuilder = createMyNotificationBuilder(
                            isPlay,
                            audioInformationFirstLine,
                            audioInformationSecondLine,
                            audioInformationThirdLine,
                            currTrackArrayNumber,
                            currTrackId,
                            albumImageDrawable,
                            mediaSession,
                            context);

        Notification theNotification = theNotificationBuilder.build();

        // register the notification with an id so that we can later update or remove it
        //mNotificationManager.notify(MY_NOTIFICATION_ID, theNotification);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            theNotification.category = CATEGORY_TRANSPORT;
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
        {
            // For Android N this had been done in builder.
            // Deprecated, but needed for Android 6 and older
            theNotification.bigContentView = mCustomBigViewForAndroid6;     // may be null
        }

        theNotification.flags = Notification.FLAG_ONGOING_EVENT;    // ?

        // register the notification with an id so that we can later update or remove it

        mNotificationManager.notify(MY_NOTIFICATION_ID, theNotification);
    }
}
