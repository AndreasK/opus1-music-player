/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import de.kromke.andreas.utilities.AppGlobals;
import de.kromke.andreas.utilities.AudioBaseObject;
import de.kromke.andreas.utilities.AudioFilters;
import de.kromke.andreas.utilities.AudioTrack;
import de.kromke.andreas.utilities.FilteredAudioObjectList;
import de.kromke.andreas.utilities.PrivateLog;

/**
 * Helper class for the main activity, dealing with player
 */
@SuppressWarnings({"Convert2Lambda", "JavadocBlankLines"})
public class MainActivity extends MainBasicActivity implements ServiceConnection,
        MediaController.MediaPlayerControl, MediaPlayService.MediaPlayServiceControl
{
    private static final String LOG_TAG = "MainActivity";
    private static final String STATE_PLAY_STATE = "playState";
    private static final String STATE_POSITION = "position";
    private static final String STATE_DURATION = "duration";

    static private boolean mServiceStarted = false;
    static private boolean wasPlaying = false;       // hack to let the playing state survive activity recreation until service rebind

    enum FloatingButtonActionType
    {
        NONE,
        PLAY,
        PAUSE,
        CONTINUE,
        GOTO_PLAYLIST
    }

    private FloatingActionButton mFloatingButton2;      // upper button
    private FloatingActionButton mFloatingButton1;      // lower button
    private FloatingButtonActionType mFloatingButton1Action = FloatingButtonActionType.NONE;

    // our activity gets deleted and created without any sense and we must be able to change the
    // callback as soon as possible to the new activity instance, otherwise the old one will be called.
    private static MediaPlayService mMediaPlayService = null;
    private Intent mPlayIntent;

    //mMediaController (showing the prev/next/play/pause buttons inside the activity window)
    private MyMediaController mMediaController;

    // these variables must survive e.e. a screen rotation
    private int mCurrTrackNo = -1;
    private int mBufferedPosition = 0;
    private int mBufferedDuration = 0;

    // settings
    private int mAutohideModeMs = 0;
    private int mChooseOtherTrackMode = 0;


    /**************************************************************************
     *
     * Activity is about to be destroyed
     *
     *************************************************************************/
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        PrivateLog.d(LOG_TAG, "onSaveInstanceState()");
        if (mCurrTrackNo != -1)
        {
            savedInstanceState.putBoolean(STATE_PLAY_STATE, isPlaying());
            savedInstanceState.putInt(STATE_POSITION, mBufferedPosition);
            savedInstanceState.putInt(STATE_DURATION, mBufferedDuration);
            PrivateLog.d(LOG_TAG, "onSaveInstanceState() : save track no " + mCurrTrackNo);
        }

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }


    /**************************************************************************
     *
     * Activity is created
     *
     * call order:
     *
     * onCreate() -> onStart -> onResume ->onAttachedToWindow
     * -> onPause -> onStop -> onDestroy
     *
     * Some recommend that onStart() binds to the service, onStop() unbinds,
     * but due to a bug in Samsung's Android "interpretation" we MUST do it
     * in create/destroy, otherwise our service will be killed by Samsung after unbind.
     * There is no problem with Google, Motorola, Nokia and Huawei. On the other
     * hand, Google's example also uses onCreate()/onDestroy() for bind/unbind.
     *
     *************************************************************************/
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        PrivateLog.d(LOG_TAG, "onCreate(" + mUniqueHash + ")");

        // restore state
        if (savedInstanceState != null)
        {
            //mBufferedPlayState = savedInstanceState.getBoolean(STATE_PLAY_STATE);
            mBufferedPosition  = savedInstanceState.getInt(STATE_POSITION);
            mBufferedDuration  = savedInstanceState.getInt(STATE_DURATION);
            PrivateLog.d(LOG_TAG, "onCreate() : buffered position/duration " + mBufferedPosition + "/" + mBufferedDuration + " from saved instance state");
        }
        else
        {
            //mBufferedPlayState = false;
            mBufferedPosition = 0;
            mBufferedDuration = 0;
        }

        // In case of Activity re-creation the current track number of the playlist should survive in PlayListHandler,
        // because the PlayListHandler is static.
        // In case of process recreation, the current track number will be invalid.
        mCurrTrackNo = PlayListHandler.getCurrEntryNo();
        PrivateLog.d(LOG_TAG, "onCreate() : restore track no " + mCurrTrackNo + " from PlayListHandler");

        initFloatingActionButtons();
        //updateFloatingActionButtons();    // too early, see onAttachedToWindow()

        // this is used for communication with the service
        mPlayIntent = new Intent(this, MediaPlayService.class);
        mPlayIntent.setAction(MediaPlayService.SERVICE_START_ACTION);   // not really necessary, could also be null

        //setup mMediaController
        // after screen rotation the media controller should be made visible,
        // but this is not possible even in resume, because of bad Android design
        createMusicController();

        bindToService();
    }


    /**************************************************************************
     *
     * Called after onStart()
     *
     *************************************************************************/
    @Override
    protected void onResume()
    {
        PrivateLog.d(LOG_TAG, "onResume(" + mUniqueHash + ")");
        super.onResume();

        // read the settings here, not in onCreate(), because they might have just
        // changed, as we returned from the settings activity
        PlayListHandler.autoplayMode = UserSettings.getVal(UserSettings.PREF_AUTOPLAY_MODE, 3);
        mAutohideModeMs = UserSettings.getVal(UserSettings.PREF_AUTOHIDE_CONTROLLER, 3000);
        if (mAutohideModeMs < 1000)
        {
            mAutohideModeMs = 1000;      // prevent the old setting "show forever"
        }
        mChooseOtherTrackMode = UserSettings.getVal(UserSettings.PREF_CHOOSE_OTHER_TRACK, 0);
        PrivateLog.d(LOG_TAG, "onResume() - done");
    }


    /**************************************************************************
     *
     * Called after onResume()
     *
     *************************************************************************/
    @Override
    public void onAttachedToWindow()
    {
        PrivateLog.d(LOG_TAG, "onAttachedToWindow(" + mUniqueHash + ")");
        super.onAttachedToWindow();
        if ((PlayListHandler.getCurrEntryNo() >= 0) && (mMediaController != null) && !(mMediaController.isShowing()))
        {
            // media controller should be visible, but is not. Show it.
            PrivateLog.d(LOG_TAG, "onAttachedToWindow() -- show Media Controller with timeout = " + mAutohideModeMs);
            mMediaController.show(mAutohideModeMs);
        }
        updateFloatingActionButtons();
    }


    /**************************************************************************
     *
     * Called before the application is put to the background.
     * After onPause() the method onStop() is called.
     *
     *************************************************************************/
    @Override
    protected void onPause()
    {
        PrivateLog.d(LOG_TAG, "onPause(" + mUniqueHash + ")");
        super.onPause();
        PrivateLog.d(LOG_TAG, "onPause() - done");
    }


    /**************************************************************************
     *
     * Called before the activity object will be deallocated, not only
     * at program end, but also e.g. when HOME button has been pressed.
     *
     * In case this function has been called due to program exit,
     * the call isFinishing() returns true.
     *
     *************************************************************************/
    @Override
    protected void onDestroy()
    {
        PrivateLog.d(LOG_TAG, "onDestroy(" + mUniqueHash + ")");

        PlayListHandler.saveBookmarksPlayingList();

        wasPlaying = isPlaying();       // hack to let the playing state survive activity recreation

        if (bRestarting || isFinishing())
        {
            PrivateLog.d(LOG_TAG, "onDestroy() -- is finishing");
            if (mMediaPlayService != null)
            {
                mMediaPlayService.stopPlayer();
            }
            unbindFromService();
            if (mServiceStarted)
            {
                // only stop service in case app is finishing
                stopService(mPlayIntent);
                mServiceStarted = false;
            }

            AudioFilters.audioPlayingList.setRawSelectedIndex(-1);
        }
        else
        {
            unbindFromService();
        }

        if (mMediaController != null)
        {
            mMediaController.reallyHide();
            mMediaController.setEnabled(false);
            mMediaController = null;
        }

        super.onDestroy();
        PrivateLog.d(LOG_TAG, "onDestroy(" + mUniqueHash + ") - done");
    }


    /* ####################################
     * ### MediaPlayerControl callbacks ###
     * ####################################
     */

    @Override
    public boolean canPause()
    {
        return true;
    }

    @Override
    public boolean canSeekBackward()
    {
        return true;
    }

    @Override
    public boolean canSeekForward()
    {
        return true;
    }

    @Override
    public int getAudioSessionId()
    {
        return 0;
    }

    @Override
    public int getBufferPercentage()
    {
        return 0;
    }

    @Override
    public int getCurrentPosition()
    {
        if (isReallyPlaying())
        {
            mBufferedPosition = mMediaPlayService.getPosition();
        }

        //PrivateLog.d(LOG_TAG, "getCurrentPosition() => " + mBufferedPosition);
        return mBufferedPosition;
    }

    @Override
    public int getDuration()
    {
        if (isReallyPlaying())
        {
            mBufferedDuration = mMediaPlayService.getDuration();
        }

        //PrivateLog.d(LOG_TAG, "getDuration() => " + mBufferedDuration);
        return mBufferedDuration;
    }

    // Callback from MediaController, asking if to show PAUSE or PLAY control
    @Override
    public boolean isPlaying()
    {
        boolean ret;
        //noinspection SimplifiableIfStatement
        if (mMediaPlayService != null)
        {
            ret = mMediaPlayService.isPlaying();
        }
        else
        {
            //PrivateLog.d(LOG_TAG, "isPlaying() : no service, yet");
            ret = wasPlaying;   // hack for the time while service reconnection is pending
            //ret = false;
        }

        //PrivateLog.d(LOG_TAG, "isPlaying() => " + ret);
        return ret;
    }

    // called when the media mMediaController PAUSE button had been pressed
    @Override
    public void pause()
    {
        PrivateLog.f_enter(LOG_TAG, "pause()");
        if (mMediaPlayService != null)
        {
            setBookmarkForCurrentTrack(true);     // also update buffered position
            mMediaPlayService.pausePlayer();
        }
        updateFloatingActionButtons();
        PrivateLog.f_leave(LOG_TAG, "pause()");
    }

    @Override
    public void seekTo(int pos)
    {
        PrivateLog.d(LOG_TAG, "seekTo()");
        mBufferedPosition = pos;
        if (mMediaPlayService != null)
        {
            mMediaPlayService.seek(pos);
        }
    }

    // called when the media mMediaController PLAY button had been pressed
    @Override
    public void start()
    {
        PrivateLog.v(LOG_TAG, "start()");
        if (mMediaPlayService != null)
        {
            mMediaPlayService.continuePlayer();
        }
        updateFloatingActionButtons();
    }

    // called when the media mMediaController PLAY_NEXT button had been pressed
    private void playNext()
    {
        PrivateLog.d(LOG_TAG, "playNext()");
        int prevTrackNo = PlayListHandler.getCurrEntryNo();
        mCurrTrackNo = PlayListHandler.gotoTrackRel(1, false);
        changedTrack(prevTrackNo, false, true);
    }

    // called when the media mMediaController PLAY_PREV button had been pressed
    private void playPrev()
    {
        PrivateLog.d(LOG_TAG, "playPrev()");
        int prevTrackNo = PlayListHandler.getCurrEntryNo();
        mCurrTrackNo = PlayListHandler.gotoTrackRel(-1, false);
        changedTrack(prevTrackNo, false, true);
    }


    /* ##############################
     * ### MediaPlayService callbacks ###
     * ##############################
     */

    /**************************************************************************
     *
     * informational callback from MediaPlayService, called after it has been
     * handled by the service
     *
     *************************************************************************/
    @Override
    public void onNotificationPlayPauseChanged(int newTrackArrayNo)
    {
        PrivateLog.f_enter(LOG_TAG, "onNotificationPlayPauseChanged()");

        //HACK: handle sudden play requests from car radio
        if (mCurrTrackNo != newTrackArrayNo)
        {
            PrivateLog.w(LOG_TAG, "onNotificationPlayPauseChanged() : Track changed");
            onNotificationTrackChanged(mCurrTrackNo, 0, newTrackArrayNo);
            PrivateLog.f_leave(LOG_TAG, "onNotificationPlayPauseChanged()");
            return;
        }

        // in case of pause, set bookmark
        setBookmarkForCurrentTrack(false);     // also update buffered position
        updateFloatingActionButtons();

        // trying to force redraw of play/pause button (one of the media controller's bugs)
        if (mMediaController != null)
        {
            mMediaController.show(mAutohideModeMs);
        }

        PrivateLog.f_leave(LOG_TAG, "onNotificationPlayPauseChanged()");
    }


    /**************************************************************************
     *
     * informational callback from MediaPlayService, called after it has been
     * handled by the service
     *
     * newTrackNo == -1 means STOP
     *
     * Note: Bookmark handling has already been done by MediaPlayService,
     *
     *************************************************************************/
    @Override
    public void onNotificationTrackChanged(int prevTrackArrayNumber, long prevPosition, int newTrackArrayNo)
    {
        PrivateLog.f_enter(LOG_TAG, "onNotificationTrackChanged(" + newTrackArrayNo + ")");
        if (prevTrackArrayNumber != mCurrTrackNo)
        {
            PrivateLog.e(LOG_TAG, "prev track mismatch (" + prevTrackArrayNumber + " <> " + mCurrTrackNo);
        }

        int prevTrackNo = mCurrTrackNo;
        mCurrTrackNo = newTrackArrayNo;
        changedTrack(prevTrackNo, false, false);
        PrivateLog.f_leave(LOG_TAG, "onNotificationTrackChanged()");
    }


    /* ########################
     * ### helper functions ###
     * ########################
     */

    /**************************************************************************
     *
     * start service and catch exception
     *
     **************************************************************************/
    private boolean myStartService()
    {
        try
        {
            startService(mPlayIntent);
            return true;
        } catch (Exception e)
        {
            // With Android S and above this can cause a security exception.
            PrivateLog.e(LOG_TAG, "myStartService(" + mUniqueHash + ") : cannot start service: " + e);
            return false;
        }
    }


    /**************************************************************************
     *
     * initiate binding to Media Play Service.
     * Process will be completed in callback onServiceConnected()
     *
     * Called when activity changes from pause to resume state
     *
     **************************************************************************/
    private void bindToService()
    {
        // + TEST CODE : trying workaround for issue #10
        // (Android destroys Service after some time without reason)
        myStartService();
        // - TEST CODE
        if (mServiceStarted)
        {
            // HACK:
            // As callback onServiceConnected tends to be too late
            // and may be overtaken by onPause and unbindService
            // we must register for callbacks here. Otherwise the
            // callback of a former instance of the activity will
            // be called. This is bad Android design.
            if (mMediaPlayService != null)
            {
                // register here, in case activity instance changed
                mMediaPlayService.setController(this);
            }
            else
            {
                PrivateLog.e(LOG_TAG, "bindToService(" + mUniqueHash + ") : mMediaPlayService is NULL, cannot set controller");
            }
        }
        else
        {
            // this is the first start. Start background service.
            // make sure service keeps running when activity is unbound
            PrivateLog.d(LOG_TAG, "bindToService(" + mUniqueHash + ") : start service");
            if (myStartService())
            {
                mServiceStarted = true;
            }
        }

        if (mServiceStarted)
        {
            // bind in order to be able to control the service
            PrivateLog.d(LOG_TAG, "bindToService(" + mUniqueHash + ") : bind to service");
            bindService(mPlayIntent, this, Context.BIND_AUTO_CREATE);
        }
    }


    /**************************************************************************
     *
     * initiate unbinding from Media Play Service.
     * Process will be completed in callback onServiceDisconnected()
     *
     * Called when activity changes from resume to pause state
     *
     **************************************************************************/
    private void unbindFromService()
    {
        PrivateLog.d(LOG_TAG, "unbindFromService(" + mUniqueHash + ")");
        try
        {
            unbindService(this);
        }
        catch (Exception e)
        {
            PrivateLog.e(LOG_TAG, "unbindFromService(" + mUniqueHash + ") : cannot unbind from service: " + e);
        }
        mMediaPlayService = null;
    }


    /**************************************************************************
     *
     * Binding to Media Play Service is asynchronous. This function is
     * initiated in bindToService() and finally called when the binding was successful.
     *
     **************************************************************************/
    @Override
    public void onServiceConnected(ComponentName name, IBinder service)
    {
        PrivateLog.f_enter(LOG_TAG, "onServiceConnected(" + mUniqueHash + ")");
        MediaPlayService.MediaPlayServiceBinder binder = (MediaPlayService.MediaPlayServiceBinder) service;
        //get service
        mMediaPlayService = binder.getService();
        // reset stored intermediate state (asked while service reconnection is pending)
        wasPlaying = false;
        // register for callbacks
        mMediaPlayService.setController(this);
        // in case it's a re-bind, i.e. activity was resumed, force update duration, position and buttons
        updateAllControls(true);
        PrivateLog.f_leave(LOG_TAG, "onServiceConnected()");
    }


    /**************************************************************************
     *
     * unbinding from Media Play Service is asynchronous. This function is
     * initiated from unbindFromService() and finally called when
     * the unbinding was successful.
     *
     **************************************************************************/
    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        PrivateLog.d(LOG_TAG, "onServiceDisconnected(" + mUniqueHash + ")");
        mMediaPlayService = null;   // unnecessary, because already done ...
    }


    /**************************************************************************
     *
     * Setup the Media Controller (the window showing play, pause, progress,
     * time etc. of a music track being played.
     *
     **************************************************************************/
    private void createMusicController()
    {
        PrivateLog.d(LOG_TAG, "createMusicController(" + mUniqueHash + ")");
        mMediaController = new MyMediaController(this);
        //set previous and next button listeners
        mMediaController.setPrevNextListeners(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playNext();
            }
        }, new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                playPrev();
            }
        });
        //set and show
        mMediaController.setMediaPlayer(this);
        View v;
        /*
        v = findViewById(R.id.tracks_album_header);
        PrivateLog.d(LOG_TAG, "createMusicController() : header w = " + v.getMeasuredWidth() + " h = " + v.getMeasuredHeight());
        v = findViewById(R.id.audioTrack_all);
        PrivateLog.d(LOG_TAG, "createMusicController() : space  w = " + v.getMeasuredWidth() + " h = " + v.getMeasuredHeight());
        */
        v = findViewById(R.id.main_content);
        //PrivateLog.d(LOG_TAG, "createMusicController() : list   w = " + v.getMeasuredWidth() + " h = " + v.getMeasuredHeight());
        mMediaController.setAnchorView(v);
        mMediaController.setEnabled(true);
    }


    /**************************************************************************
     *
     * Set new "playing track number" and update list with selection colours
     * and remove the bookmark asterisk, if necessary.
     *
     **************************************************************************/
    private void updateList(int prevTrackNo)
    {
        if (currFragmentIsPlayingFragment())
        {
            mCurrFragment.onUpdateRawSelection(prevTrackNo, PlayListHandler.getCurrEntryNo());
        }
    }


    /**************************************************************************
     *
     * for skip forward and backward. Also called while the play list is not
     * the current fragment
     *
     * Note that is is also called directly from the MediaPlayService,
     * with tellMediaPlayService = false, because MediaPlayService will handle
     * autonomously.
     *
     *************************************************************************/
    private void changedTrack(int prevTrackNo, boolean use_bookmark, boolean tellMediaPlayService)
    {
        PrivateLog.f_enter(LOG_TAG, "changedTrack(" + prevTrackNo + " -> " + mCurrTrackNo + ")");
        mBufferedPosition = 0;
        mBufferedDuration = 0;
        int currTrackNo = PlayListHandler.getCurrEntryNo();

        // Track has been completed, thus remove all old bookmarks.
        removeBookmarks(use_bookmark ? currTrackNo : -1);

        if ((prevTrackNo >= 0) && (currTrackNo < 0))
        {
            if (mMediaController != null)
            {
                // hide the controller overlay view
                mMediaController.reallyHide();
            }
        }
        updateList(prevTrackNo);

        //
        // last check if something is playing or not
        //

        // now getRawSelectedIndex() has been updated with newTrackNo
        boolean playTrack = (currTrackNo >= 0);

        //
        // Start or stop playing (if not called by MediaPlayService, because
        // in this case the action has already been taken.
        //

        if (tellMediaPlayService )
        {
            if (mMediaPlayService != null)
            {
                if (playTrack)
                {
                    // note that this runs asynchronously, i.e. isPlaying() will
                    // continue to return false until the callback onPrepared() has been called
                    mMediaPlayService.playAudioTrack(0);
                } else
                {
                    mMediaPlayService.stopPlayer();
                }
            }
            else
            {
                PrivateLog.e(LOG_TAG, "changedTrack() : mMediaPlayService is null");
            }
        }

        //
        // Now we can show the media controller, if appropriate.
        // Do not show it before, because callback tells it "not playing".
        //

        if (playTrack)
        {
            if (currFragmentIsPlayingFragment() && (mMediaController != null))
            {
                // make sure media controller is not shown in other fragments
                PrivateLog.d(LOG_TAG, "changedTrack() : show media controller");
                mMediaController.show(mAutohideModeMs);
            }
        }

        updateFloatingActionButtons();
        updateProgressBar();

        PrivateLog.f_leave(LOG_TAG, "changedTrack()");
    }


    /**************************************************************************
     *
     * helper to stop playing
     *
     * Called on click to STOP button or when choosing a different track.
     * Note that the media controller does not have a STOP icon.
     *
     **************************************************************************/
    private void StopPlaying()
    {
        PrivateLog.f_enter(LOG_TAG, "StopPlaying()");
        if (mMediaPlayService != null)
        {
            setBookmarkForCurrentTrack(true);     // also update buffered position
            mMediaPlayService.stopPlayer();
        }
        if (mMediaController != null)
        {
            mMediaController.reallyHide();
        }

        int prevTrackNo = PlayListHandler.getCurrEntryNo();
        PlayListHandler.setCurrEntryNo(-1);     // new active track number is invalid
        updateList(prevTrackNo);
        mBufferedPosition = 0;
        mBufferedDuration = 0;
        updateFloatingActionButtons();
        updateProgressBar();
        PrivateLog.f_leave(LOG_TAG, "StopPlaying()");
    }


    /**************************************************************************
     *
     * helper, called when a track is chosen during play
     *
     **************************************************************************/
    private void showMediaControls()
    {
        if ((mAutohideModeMs != 0) && (PlayListHandler.getCurrEntryNo() >= 0))
        {
            // currently playing, and controls are auto hidden
            if (mMediaController != null)
            {
                // show controller
                mMediaController.show(mAutohideModeMs);
            }
        }
    }


    /**************************************************************************
     * kind of callback from fragment, but also called for the green
     * PLAY floating button
     *
     * @param trackNo either the list position (0, ...) or -1
     *
     * Is only called, if the current fragment is the playing fragment
     **************************************************************************/
    public void onPlayingListTrackPicked(int trackNo, boolean bUseBookmark)
    {
        int nTracks = PlayListHandler.getNumOfEntries();
        PrivateLog.f_enter(LOG_TAG, "onPlayingListTrackPicked(" + trackNo + "/" + nTracks + ")");
        boolean trackValid = (trackNo >= 0) && (trackNo < nTracks);

        // in case no track is being played always start the selected track
        int mMode = (PlayListHandler.getCurrEntryNo() >= 0) ? mChooseOtherTrackMode : 0;

        switch (mMode)
        {
            case 0:     // play selected track
                if ((trackValid) && (trackNo != PlayListHandler.getCurrEntryNo()))
                {
                    // play selected track. if necessary, stop the running one before automatically
                    int prevTrackNo = PlayListHandler.getCurrEntryNo();
                    mCurrTrackNo = PlayListHandler.gotoTrack(trackNo, 1);
                    changedTrack(prevTrackNo, bUseBookmark, true);
                }
                else
                {
                    showMediaControls();
                }
            break;

            case 1:     // pause
                pause();
                break;

            case 2:     // stop
                StopPlaying();
                break;

            case 3:     // show controls
                showMediaControls();
                break;
        }
        PrivateLog.f_leave(LOG_TAG, "onPlayingListTrackPicked()");
    }

    // note that this will never be called once we call setMovementMethod() for the album title view
    /*
    public void onSpacePicked(View view)
    {
        PrivateLog.d(LOG_TAG, "onSpacePicked()");
        if (mAutohideModeMs != 0)
        {
            if (getCurrTrack() >= 0)
            {
                // currently playing
                if (mMediaController != null)
                {
                    mMediaController.show(mAutohideModeMs);
                }
            }
        }
    }
    */

    /*
        @Override
        // callback for custom ListView that reports clicks to empty space
        public void onNoItemClicked()
        {
            PrivateLog.d(LOG_TAG, "onNoItemClicked()");
            if (mAutohideModeMs != 0)
            {
                if (mCurrTrackNo >= 0)
                {
                    // currently playing
                    if (mMediaController != null)
                    {
                        mMediaController.show(mAutohideModeMs);
                    }
                }
            }
        }
    */


    /**************************************************************************
     *
     * get track info for the "snackbar" showed when Floating Buttons are touched
     *
     *************************************************************************/
    String getAudioTrackInfo(AudioBaseObject theObject)
    {
        String theText;

        if (theObject.objectType == AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT)
        {
            AudioTrack currAudioTrack = (AudioTrack) theObject;

            // set information lines
            theText = currAudioTrack.getComposerName(true) + ":";

            if (currAudioTrack.grouping != null)
            {
                theText += "\n" + currAudioTrack.grouping;
            }
            theText += "\n" + currAudioTrack.name;
        }
        else
        {
            theText = "unhandled audio object type";
        }

        return theText;
    }


    /**************************************************************************
     *
     * set icon and colour of floating button #1 according to
     * mFloatingButton1Action
     *
     *************************************************************************/
    private void updateFloatingButton1()
    {
        // BUG: https://stackoverflow.com/questions/49587945/setimageresourceint-doesnt-work-after-setbackgroundtintcolorlistcolorstateli
        mFloatingButton1.hide();
        switch(mFloatingButton1Action)
        {
            case PAUSE:
                mFloatingButton1.setImageResource(android.R.drawable.ic_media_pause);
                mFloatingButton1.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.action_button_pause_colour));
                break;

            case GOTO_PLAYLIST:
                mFloatingButton1.setImageResource(R.drawable.app_ic_media_goto_playlist);
                mFloatingButton1.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.action_button_goto_playlist_colour));
                break;

            case PLAY:
            case CONTINUE:
                mFloatingButton1.setImageResource(android.R.drawable.ic_media_play);
                mFloatingButton1.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.action_button_play_colour));
                break;
        }
        mFloatingButton1.show();
    }


    /**************************************************************************
     *
     * set visibility, icon and colour of floating button #2 according to
     * mFloatingButton1Action
     *
     *************************************************************************/
    private void updateFloatingButton2()
    {
        switch(mFloatingButton1Action)
        {
            case PAUSE:
            case CONTINUE:
                // BUG: https://stackoverflow.com/questions/49587945/setimageresourceint-doesnt-work-after-setbackgroundtintcolorlistcolorstateli
                mFloatingButton2.hide();
                mFloatingButton2.setImageResource(R.drawable.app_ic_media_stop);
                //mFloatingButton2.setImageResource(android.R.drawable.ic_media_stop);      // does not work for whatever reason
                mFloatingButton2.setBackgroundTintList(AppCompatResources.getColorStateList(this, R.color.action_button_stop_colour));
                mFloatingButton2.show();
                break;

            case PLAY:
            case GOTO_PLAYLIST:
                mFloatingButton2.hide();
                break;
        }
    }


    /**************************************************************************
     *
     * hide or show the Floating Buttons
     *
     *************************************************************************/
    private void updateFloatingActionButtons()
    {
        //PrivateLog.d(LOG_TAG, "updateFloatingActionButtons()");
        int currTrackNo = PlayListHandler.getCurrEntryNo();
        int currFragment = (mCurrFragment != null) ? mCurrFragment.mSectionNumber : -1;
        FloatingButtonActionType newType;

        if (currFragment == MainFragment.thePlayingFragment)
        {
            // in playlist fragment either start, pause or continue
            if (currTrackNo >= 0)
            {
                if (isPlaying())
                    newType = FloatingButtonActionType.PAUSE;
                else
                    newType = FloatingButtonActionType.CONTINUE;
            }
            else
            {
                // no track is being played or paused
                newType = FloatingButtonActionType.PLAY;
            }
        }
        else
        {
            // from any other fragment go to playlist fragment
            newType = FloatingButtonActionType.GOTO_PLAYLIST;
        }

        if (mFloatingButton1Action != newType)
        {
            // mode has changed, update icons
            mFloatingButton1Action = newType;
            updateFloatingButton1();
            updateFloatingButton2();
        }
    }


    /**************************************************************************
     *
     * hide or show the Progress Bar
     *
     *************************************************************************/
    private void updateProgressBar()
    {
        if (mCurrFragment != null)
        {
            int currTrackNo = PlayListHandler.getCurrEntryNo();
            if (currTrackNo >= 0)
            {
                mCurrFragment.updateProgressBar(true, mBufferedPosition, mBufferedDuration);
            } else
            {
                // not playing: hide progress bar
                mCurrFragment.updateProgressBar(false, 0, 0);
            }
        }
    }


    /**************************************************************************
     *
     * show the snackbar
     *
     *************************************************************************/
    void showSnackbar(View view, String theText)
    {
        Snackbar theSnackbar = Snackbar.make(view, theText, Snackbar.LENGTH_LONG);
        theSnackbar.setAction("Action", null);
        View snackbarView = theSnackbar.getView();
        TextView textView = snackbarView.findViewById(R.id.snackbar_text);
        textView.setMaxLines(3);  // show multiple line
        theSnackbar.show();
    }


    /**************************************************************************
     *
     * fragment is being changed
     *
     * kind of message from base class
     *************************************************************************/
    @Override
    protected void onChangeFragment(int fragmentId)
    {
        PrivateLog.f_enter(LOG_TAG, "onChangeFragment(fragmentId = " + fragmentId + ")");
        updateFloatingActionButtons();
        updateProgressBar();
        PrivateLog.f_leave(LOG_TAG, "onChangeFragment()");
    }


    /**************************************************************************
     *
     * to be overridden, to inform derived class about playlist change
     *
     * derived class should stop playing
     *
     * kind of message from base class
     *************************************************************************/
    @Override
    protected void onChangePlaylist(boolean bAppend)
    {
        PrivateLog.d(LOG_TAG, "onChangePlaylist()");
        if ((!bAppend) && (mMediaPlayService != null))
        {
            if (isPlaying())
            {
                PlayListHandler.saveBookmarksPlayingList();
                mMediaPlayService.stopPlayer();
                if (mMediaController != null)
                {
                    mMediaController.reallyHide();
                }
            }
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private boolean currFragmentIsPlayingFragment()
    {
        return ((mCurrFragment != null) && (mCurrFragment.mSectionNumber == MainFragment.thePlayingFragment));
    }


    /**************************************************************************
     *
     * initialise the Floating Buttons
     *
     *************************************************************************/
    private void initFloatingActionButtons()
    {
        /*
         * PLAY symbol
         */
        mFloatingButton1 = findViewById(R.id.floating_play);
        mFloatingButton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int playingIndex = PlayListHandler.getCurrEntryNo();

                if (mFloatingButton1Action == FloatingButtonActionType.PAUSE)
                {
                    String theText = "PAUSE PAUSE PAUSE";
                    if (playingIndex >= 0)
                    {
                        AudioBaseObject theObject = AudioFilters.audioPlayingList.raw.list.get(playingIndex);
                        theText = getAudioTrackInfo(theObject);
                        pause();
                    }
                    Snackbar.make(view, theText, Snackbar.LENGTH_LONG)
                           .setAction("Action", null).show();
                } else
                if (mFloatingButton1Action == FloatingButtonActionType.GOTO_PLAYLIST)
                {
                    changeFragment(MainFragment.thePlayingFragment);
                }
                else
                {
                    if (playingIndex >= 0)
                    {
                        // currently there is a track being played
                        AudioBaseObject theObject = AudioFilters.audioPlayingList.raw.list.get(playingIndex);
                        String theText = getAudioTrackInfo(theObject);
                        showSnackbar(view, theText);

                        if (!isPlaying())
                        {
                            // continue paused file
                            start();
                        }
                    } else if (currFragmentIsPlayingFragment())
                    {
                        // playing fragment, no track being played: Start first track
                        if (PlayListHandler.getNumOfEntries() > 0)
                        {
                            showSnackbar(view, "PLAY");
                            if (PlayListHandler.numOfBookmarksInPlayingList > 0)
                            {
                                dialogUseBookmark();
                            }
                            else
                            {
                                onPlayingListTrackPicked(0, false);
                            }
                        }
                    } /*else if ((mCurrFragment != null) && (mCurrFragment.mSectionNumber != MainFragment.thePlayingFragment))
                       {
                           // no track being played, switch to playing fragment
                           changeFragment(MainFragment.thePlayingFragment);
                       } else
                       {
                           //TODO: no action here?
                       }*/
    /*
    Snackbar.make(view, theText, Snackbar.LENGTH_LONG)
    .setAction("Action", null).show();
    */
                   }
               }
           }
        );

        mFloatingButton1.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                if ((mFloatingButton1Action == FloatingButtonActionType.GOTO_PLAYLIST) &&
                    (mCurrFragment != null) &&
                    ((mCurrFragment.mSectionNumber == MainFragment.theTracksFragment) ||
                     (mCurrFragment.mSectionNumber == MainFragment.theWorkFragment) ||
                     (mCurrFragment.mSectionNumber == MainFragment.theAlbumFragment)))
                {
                    FilteredAudioObjectList.AudioObjectList theList = mCurrFragment.mAudioObjectList.getListShown();
                    if (theList == null)
                    {
                        return true;        // should not happen
                    }
                    int size = theList.getSize();
                    if (size < 1)
                    {
                        return true;        // should not happen
                    }

                    int position = 0;
                    AudioBaseObject theListObject = theList.list.get(position);
                    AudioBaseObject[] theArray;

                    if ((mCurrFragment.mSectionNumber == MainFragment.theTracksFragment) &&
                        !(theListObject instanceof AudioTrack))
                    {
                        // put all tracks to playing list (trick: for both album and work the first entry represents the whole list)
                        theArray = new AudioBaseObject[1];
                        theArray[0] = theListObject;
                    }
                    else
                    {
                        // Works View, Albums View, or Tracks View with audio files passed via "open with" or "share"
                        theArray = new AudioBaseObject[size];
                        for (int i = 0; i < size; i++)
                        {
                            theArray[i] = theList.list.get(i);
                        }
                    }

                    dialogConditionallyOverwriteOrAppendToPlayingList(theArray, 0);
                    return true;
                }

                return false;       // no long clicks
            }
        }
        );

        /*
         * upper button
         */
        mFloatingButton2 = findViewById(R.id.floating_pause);
        mFloatingButton2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //int playingIndex = getCurrTrack();
                String theText = "STOP";
                Snackbar.make(view, theText, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                StopPlaying();
            }
        }
        );
    }


    /**********************************************************************************************
     *
     * helper to update all controls
     *
     *********************************************************************************************/
    private boolean isReallyPlaying()
    {
        //noinspection SimplifiableIfStatement
        if (mMediaPlayService != null)
        {
            return mMediaPlayService.isReallyPlaying();
        }
        else
        {
            return false;
        }
    }


    /**********************************************************************************************
     *
     * helper to update all controls
     * Called from timer (force = false) and on service reconnection (force = true).
     *
     *********************************************************************************************/
    private void updateAllControls(boolean bForce)
    {
        boolean bIsReallyPlaying = isReallyPlaying();
        if (bForce || bIsReallyPlaying)
        {
            if (bIsReallyPlaying)
            {
                mBufferedDuration = mMediaPlayService.getDuration();
                mBufferedPosition = mMediaPlayService.getPosition();
            }
            else
            {
                mBufferedDuration = 0;
                mBufferedPosition = 0;
            }
            updateProgressBar();
        }

        if (bForce || isPlaying())
        {
            // check if the service was recently recreated and lost its media player.
            int pos = mMediaPlayService.getPosition();
            PrivateLog.d(LOG_TAG, "MediaPlayService reports position " + pos);
            // hack to solve state mismatch between Service and PlaylistHandler
            if ((mCurrTrackNo >= 0) && (pos == 0))
            {
                StopPlaying();
            }
            updateFloatingActionButtons();
        }
    }


    /**********************************************************************************************
     *
     * update the progress bar here
     *
     *********************************************************************************************/
    @Override
    public void TimerCallback()
    {
        super.TimerCallback();

        //Toast.makeText(getApplicationContext(), "TIMER BUMMS", Toast.LENGTH_SHORT).show();
        updateAllControls(false);
    }


    /**********************************************************************************************
     *
     * Activity is about to be recreated due to theme change.
     * Last chance to save the current scroll position
     *
     *********************************************************************************************/
    @Override
    public void AboutToRecreate()
    {
        if (mCurrFragment != null)
        {
            mCurrFragment.saveScrollPosition();
        }
    }


    /**************************************************************************
     *
     * media button, e.g. from Bluetooth car radio
     *
     * Note that usually media buttons are automatically forwarded to the
     * media session, but for whatever reason the PLAY command is not, in case
     * the media player has been stopped (not only paused).
     *
     * Thus we handle the Play command here manually
     *************************************************************************/
    protected void onKeyCodeMediaPlay()
    {
        PrivateLog.d(LOG_TAG, "onKeyCodeMediaPlay()");

        if ((PlayListHandler.getNumOfEntries() > 0) && (mMediaPlayService != null))
        {
            int prevTrackNo = PlayListHandler.getCurrEntryNo();

            if (prevTrackNo > 0)
            {
                // continue, if paused, otherwise ignore
                if (isPlaying())
                {
                    PrivateLog.w(LOG_TAG, "onKeyCodeMediaPlay() : Play command ignored, already playing");
                }
                else
                {
                    mMediaPlayService.continuePlayer();
                    updateFloatingActionButtons();
                }
            }
            else
            {
                // no track being played: Start first track
                int trackNo = 0;
                mCurrTrackNo = PlayListHandler.gotoTrack(trackNo, 1);
                changedTrack(prevTrackNo, false, true);
            }
        }
        else
        {
            PrivateLog.d(LOG_TAG, "onKeyCodeMediaPlay() : Playlist is empty");
        }
    }

    /* *************************************************************************
     *
     * media button, e.g. from Bluetooth car radio
     *
     *************************************************************************/
    /*
    protected void onKeyCodeMediaStop()
    {
        PrivateLog.d(LOG_TAG, "onKeyCodeMediaStop()");
        StopPlaying();
    }
    */


    /**************************************************************************
     *
     * overwritten from base class
     *
     *************************************************************************/
    @Override
    protected void dialogUseBookmarkResult(boolean bUseBookmark)
    {
        int index = 0;
        if (bUseBookmark)
        {
            index = PlayListHandler.getFirstBookmarkInPlayingList();
            if (index < 0)
            {
                index = 0;
            }
        }
        onPlayingListTrackPicked(index, bUseBookmark);
    }


    /**************************************************************************
     *
     * remove all bookmarks, except those in keepIndex
     *
     *************************************************************************/
    private void removeBookmarks(int keepIndex)
    {
        int index = PlayListHandler.getFirstBookmarkInPlayingList();
        while (index >= 0)
        {
            if (index != keepIndex)
            {
                if (PlayListHandler.updateBookmarkInPlayingList(index, 0))
                {
                    if (currFragmentIsPlayingFragment())
                    {
                        mCurrFragment.onUpdateRawBookmark(index);
                    }
                }
            }

            index++;
            index = PlayListHandler.getNextBookmarkInPlayingList(index);
        }
    }


    /**************************************************************************
     *
     * Set a bookmark for current track, also update buffered position
     *
     * Here this bookmark is not stored persistently, yet, but only in the
     * track object. Called if user selects PAUSE or STOP.
     *
     * Note that, if called indirectly from MediaPlayService, the PAUSE
     * state has already been updated, so we expect playing==false, otherwise
     * this function is called before the state is changed to PAUSE or STOP,
     * and in this case playing=true.
     *
     *************************************************************************/
    private void setBookmarkForCurrentTrack(boolean bExpectedPlayState)
    {
        PrivateLog.f_enter(LOG_TAG, "setBookmarkForCurrentTrack()");
        if ((mMediaPlayService != null) && (mMediaPlayService.isPlaying() == bExpectedPlayState))
        {
            // Remove all old bookmarks
            removeBookmarks(-1);

            // remove existing bookmark if current position is in the very first seconds
            mBufferedPosition = mMediaPlayService.getPosition();
            long bookmarkPos = (mBufferedPosition >= AppGlobals.bookmarkMinimumMs) ? mBufferedPosition : 0;
            int curr_index = PlayListHandler.getCurrEntryNo();
            PlayListHandler.updateBookmarkInPlayingList(curr_index, bookmarkPos);

            if (currFragmentIsPlayingFragment())
            {
                mCurrFragment.onUpdateRawBookmark(curr_index);
            }
        }
        PrivateLog.f_leave(LOG_TAG, "setBookmarkForCurrentTrack()");
    }
}
