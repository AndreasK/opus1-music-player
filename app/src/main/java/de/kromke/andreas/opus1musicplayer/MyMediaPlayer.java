/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import android.util.Log;

import java.io.FileNotFoundException;
import java.io.IOException;

import de.kromke.andreas.utilities.SafUtilities;


// this class encapsulates Android's MediaPlayer and adds try .. catch blocks
@SuppressWarnings("WeakerAccess")
public class MyMediaPlayer extends MediaPlayer implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener
{
    private static final String LOG_TAG = "O1M : MyMediaPlayer";
    private boolean bPreparedOrRunning = false;
    private OnErrorListener mOnErrorListener;
    private OnPreparedListener mOnPreparedListener;
    private OnCompletionListener mOnCompletionListener;
    private long mStartPosMs = 0;

    public interface OnErrorListener
    {
        boolean onError(MyMediaPlayer mp, int what, int extra);
    }

    public interface OnPreparedListener
    {
        void onPrepared(MyMediaPlayer mp);
    }

    public interface OnCompletionListener
    {
        void onCompletion(MyMediaPlayer mp);
    }

    public boolean onError(MediaPlayer mp, int what, int extra)
    {
        if (mOnErrorListener != null)
        {
            return mOnErrorListener.onError(this, what, extra);
        }
        else
        {
            return false;
        }
    }

    public void onPrepared(MediaPlayer mp)
    {
        if (mStartPosMs != 0)
        {
            mp.seekTo((int) mStartPosMs);
            mStartPosMs = 0;
        }
        bPreparedOrRunning = true;
        if (mOnPreparedListener != null)
        {
            mOnPreparedListener.onPrepared(this);
        }
    }

    public void onCompletion(MediaPlayer mp)
    {
        bPreparedOrRunning = false;
        if (mOnCompletionListener != null)
        {
            mOnCompletionListener.onCompletion(this);
        }
    }

    public void setOnErrorListener(OnErrorListener listener)
    {
        mOnErrorListener = listener;
        setOnErrorListener(this);
    }

    public void setOnPreparedListener(OnPreparedListener listener)
    {
        mOnPreparedListener = listener;
        setOnPreparedListener(this);
    }

    public void setOnCompletionListener(OnCompletionListener listener)
    {
        mOnCompletionListener = listener;
        setOnCompletionListener(this);
    }

    public boolean isPlayingEx()
    {
        try
        {
            //Log.d(LOG_TAG, "isPlayingEx() -> " + is);
            return super.isPlaying();
        } catch (Exception e)
        {
            Log.w(LOG_TAG, "Error on isPlaying()", e);
            return false;
        }
    }

    public boolean startEx()
    {
        try
        {
            super.start();
            return true;
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on start()", e);
            return false;
        }
    }

    public void pause()
    {
        if (!bPreparedOrRunning)
        {
            return;
        }

        try
        {
            super.pause();
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on pause()", e);
        }
    }

    public void seekTo(int msec)
    {
        try
        {
            super.seekTo(msec);
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on seekTo()", e);
        }
    }

    public void stop()
    {
        if (!bPreparedOrRunning)
        {
            return;
        }

        bPreparedOrRunning = false;
        try
        {
            super.stop();
        } catch (Exception e)
        {
            Log.w(LOG_TAG, "Error on stop()", e);
        }
    }

    public void reset()
    {
        bPreparedOrRunning = false;
        try
        {
            super.reset();
        } catch (Exception e)
        {
            Log.w(LOG_TAG, "Error on reset()", e);
        }
    }

    // used in combination with Android MediaStore
    public int setDataSourceExWithUri(@NonNull Context context, @NonNull Uri uri)
    {
        try
        {
            super.setDataSource(context, uri);
            return 0;
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "Error on setDataSource(): cannot find file", e);
            return 1;
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on setDataSource()", e);
            return -1;
        }
    }

    // used with own database in File mode
    public int setDataSourceExWithPath(String path)
    {
        try
        {
            super.setDataSource(path);
            return 0;
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "Error on setDataSource(): cannot find file", e);
            return 1;
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on setDataSource()", e);
            return -1;
        }
    }

    // used with own database in SAF mode
    public int setDataSourceWithUriString(Context context, String uriString)
    {
        DocumentFile df = SafUtilities.getDocumentFileFromUriString(context, uriString);
        if (df != null)
        {
            try
            {
                super.setDataSource(context, df.getUri());
                return 0;
            } catch (Exception e)
            {
                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)
                {
                    Log.e(LOG_TAG, "Error on setDataSource(Uri)", e);
                    return -1;
                }

                // With Uri and Android 6, we retry with byte buffer
                Log.w(LOG_TAG, "Error on setDataSource(Uri), reason: \"" + e.getMessage() + "\", retrying with byte buffer");
                try
                {
                    MyMediaDataSource dataSource = new MyMediaDataSource(context.getContentResolver(), df.getUri());
                    super.setDataSource(dataSource);
                    return 0;
                } catch (IOException e2)
                {
                    Log.e(LOG_TAG, "Error on setDataSource(byteBuffer)", e2);
                    return -1;
                }
            }
        }
        return -1;
    }

    public boolean prepareAsyncEx(long pos)
    {
        mStartPosMs = pos;
        try
        {
            super.prepareAsync();
            return true;
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on prepareAsync()", e);
            mStartPosMs = 0;
            return false;
        }
    }

    // handles the case that the same successor is set twice and
    // catches exceptions
    @SuppressWarnings("UnusedReturnValue")
    public boolean setNextMediaPlayerEx(MediaPlayer next)
    {
        //Log.d(LOG_TAG, "setNextMediaPlayerEx(mp = " + System.identityHashCode(this) + ", next = " + System.identityHashCode(next) + ")");
        try
        {
            super.setNextMediaPlayer(next);
            return true;
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on setNextMediaPlayer()", e);
            return false;
        }
    }

    public int getCurrentPosition()
    {
        if (!bPreparedOrRunning)
        {
            return (int) mStartPosMs;
        }

        try
        {
            return super.getCurrentPosition();
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on getCurrentPosition()", e);
            return 0;
        }
    }

    public int getDuration()
    {
        if (!bPreparedOrRunning)
        {
            return 0;
        }

        try
        {
            return super.getDuration();
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "Error on getDuration()", e);
            return 0;
        }
    }
}
