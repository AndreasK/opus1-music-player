/*
 * Copyright (C) 2016-2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.opus1musicplayer;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.ThemedSpinnerAdapter;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import de.kromke.andreas.utilities.AppGlobals;
import de.kromke.andreas.utilities.AudioBaseObject;
import de.kromke.andreas.utilities.AudioFilters;
import de.kromke.andreas.utilities.FilteredAudioObjectList;


/**************************************************************************************************
 *
 * This class manages the drop down menu. Originally a private sub-class of MainActivity.
 *
 * Note that the drop down menu does not know anything about the fragments. They
 * are switched in the activity when a Spinner item has been selected.
 *
 *************************************************************************************************/
@SuppressWarnings("JavadocBlankLines")
public class MainSpinnerAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter
{
    private final ThemedSpinnerAdapter.Helper mDropDownHelper;
    private static final int DARK_GREEN = 0xFF008000;
    static private final String LOG_TAG = "O1M : MSA";


    // constructor
    public MainSpinnerAdapter(Context context, String[] objects)
    {
        // note that if we want to change the colour or something else of a dropdown itemn,
        // we have to provide a custom layout instead of simple_list_item_1
        super(context, android.R.layout.simple_list_item_1 /* seems to be ignored */, objects);
        mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
    }

    /*
    // in case this is not overridden: counts the number of strings
    @Override
    public int getCount()
    {
        return 6;
    }
    */

    // helper


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @SuppressWarnings("DuplicateBranchesInSwitch")
    private int getColour(int position)
    {
        switch (position + 1)
        {
            case MainFragment.theAlbumFragment:     return Color.BLACK;
            case MainFragment.theWorkFragment:      return Color.BLACK;
            case MainFragment.theTracksFragment:    return Color.BLACK;
            case MainFragment.thePlaylistFragment:  return Color.BLACK;
            case MainFragment.theFolderFragment:    return Color.BLACK;
            case MainFragment.theComposerFragment:  return Color.BLUE;
            case MainFragment.theGenreFragment:     return Color.BLUE;
            case MainFragment.thePerformerFragment: return Color.BLUE;
            case MainFragment.thePlayingFragment:   return DARK_GREEN;
        }

        return Color.BLACK;
    }


    /**************************************************************************************************
     *
     * get total number and filtered number of objects in list
     *
     * return either "(n)" or "(n/m)"
     *
     *************************************************************************************************/
    private String getNumbers(FilteredAudioObjectList theList)
    {
        if ((theList == AudioFilters.audioPlayingList) || (theList == AudioFilters.audioTracksList))
        {
            int theTotalNumber = theList.getTotalSizeOfType(AudioBaseObject.AudioObjectType.AUDIO_TRACK_OBJECT);
            return "(" + theTotalNumber + ")";
        }

        String theText = "";
        if (theList != null)
        {
            int theTotalNumber = theList.getTotalSize();

            if (theTotalNumber >= 0)
            {
                int theFilteredNumber = theList.getFilteredSize();
                if ((theFilteredNumber != theTotalNumber) && (theFilteredNumber >= 0))
                {
                    theText += "(" + theFilteredNumber + "/" + theTotalNumber + ")";
                }
                else
                {
                    theText += "(" + theFilteredNumber + ")";
                }
            }
        }

        return theText;
    }


    /**************************************************************************
     *
     * used for both title text and text inside the drop down menu
     *
     *************************************************************************/
    private String getLine(int position)
    {
        FilteredAudioObjectList theList = MainFragment.getAudioObjectList(position + 1);
        return getItem(position) + " " + getNumbers(theList);
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private static Spanned fromHtml(String html)
    {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
        {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        }
        else
        {
            result = Html.fromHtml(html);
        }
        return result;
    }


    /**************************************************************************
     *
     * get the view of the Menu Title
     *
     * in case this is not overridden: get a simple text line
     *
     *************************************************************************/
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        //Log.v(LOG_TAG, "getView(position = " + position + ")");
        //return super.getView(position, convertView, parent);
        View view;
        TextView textView;

        if (convertView == null)
        {
            // Inflate the drop down using the helper's LayoutInflater
            LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
//                view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
            view = inflater.inflate(/*android.R.layout.simple_list_item_1*/R.layout.spinner_title, parent, false);
        } else
        {
            // recycle the view after scrolling?
            view = convertView;
        }

        // set text1 of the item (larger text)
        textView = view.findViewById(android.R.id.text1);
        // avoid vertical line break
        textView.setHorizontallyScrolling(true);
        /*
        Log.v("MainSpinnerAdapter", "getView() : PaddingLeft   = " + textView.getPaddingLeft());
        Log.v("MainSpinnerAdapter", "getView() : PaddingTop    = " + textView.getPaddingTop());
        Log.v("MainSpinnerAdapter", "getView() : PaddingRight  = " + textView.getPaddingRight());
        Log.v("MainSpinnerAdapter", "getView() : PaddingBottom = " + textView.getPaddingBottom());
        */
        //textView.setPadding(0, 0, 0, 0);      // no change ?!?
        /*
        String theText = getLine(position);
        textView.setText(theText);
        */
        String theItemText = getItem(position);
        if (theItemText == null)
        {
            theItemText = "MUSTNOTBE";
        }
        String theNumberText = getNumbers(MainFragment.getAudioObjectList(position + 1));

        // unsatisfying method to place the numbers and the "DB" marker

        String theHtmlString;
        boolean isDb = AppGlobals.bOwnDatabaseIsActive;
        int tlimit = (isDb) ? 13 : 17;
        int tlen = theItemText.length() + theNumberText.length();
        if (tlen > tlimit)
        {
            theHtmlString = theItemText + " " + "<small><small>" + theNumberText + "</small></small>";
            tlen -= theNumberText.length() / 2;
        } else
        {
            theHtmlString = theItemText + " " + theNumberText;
        }

        if (isDb)
        {
            //Log.v(LOG_TAG, "getView() -- tlen(" + theItemText + theNumberText + ") = " + tlen);
            String suffix = (AppGlobals.bOwnSafDatabaseIsActive) ? " SAF" : "DB";
            // &#x2003 is an "Em space"
            // &#x2005 is a "Four-Per-Em Space"

            theHtmlString += "&#x2005;<small><small><small>&#x2005;" + "∙&#x2005;" + suffix + "</small></small></small>";
        }

        textView.setText(fromHtml("<body>" + theHtmlString + "</body>"));

        //textView.setTextSize()

      //  final String theHtmlString = "<body>" + "InterpretenMMM" + "<small><small>" + " (8888/8888)" + "</small></small></body>";
      //  final String theHtmlString = "<body>" + "Interpreten" + "<font size=\"1\">" + " (888/888)" + "</font> </body>";
      //  final String theHtmlString = "<body>" + "WWWW" + "<small><small>" + "MMMM" + "</small></small>WWWW</body>";
      //  textView.setText(Html.fromHtml(theHtmlString));

        // set text2 of the item (smaller text)
            /*
            textView = (TextView) view.findViewById(android.R.id.text2);
            textView.setText(getItem(position*2 + 1));
            */

        return view;
    }


    /**************************************************************************
     *
     * callback from SpinnerAdapter which is parent of ThemedSpinnerAdapter
     *
     * create a view for the given line of the drop down menu
     *
     *************************************************************************/
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent)
    {
        //Log.v("MainSpinnerAdapter", "getDropDownView(position = " + position + ")");
        View view;

        if (convertView == null)
        {
            // Inflate the drop down using the helper's LayoutInflater
            LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
            // android.R.layout.simple_list_item_1 has one text line
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            // android.R.layout.simple_list_item_2 has a larger and a smaller text
            //view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
        } else
        {
            // recycle the view after scrolling?
            view = convertView;
        }

        // set text1 of the item (larger text)
        TextView textView = view.findViewById(android.R.id.text1);
        String theText = getLine(position);
        textView.setText(theText);
        textView.setTextColor(getColour(position));

        // set text2 of the item (smaller text)
        //((TextView) view.findViewById(android.R.id.text2)).setText("dummy text");

        return view;
    }

    // callback from ThemedSpinnerAdapter
    @Override
    public Resources.Theme getDropDownViewTheme()
    {
        return mDropDownHelper.getDropDownViewTheme();
    }

    // callback from ThemedSpinnerAdapter
    @Override
    public void setDropDownViewTheme(Resources.Theme theme)
    {
        mDropDownHelper.setDropDownViewTheme(theme);
    }
}
