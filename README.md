# Opus 1 Music Player
<img alt="Logo" src="app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" width="80">

An advanced music player especially for **classical music**, playing locally stored music files. The Opus 1 Player can be seen as a sophisticated enhancement of the Unpopular Music Player.

As "pop music" is an abbreviation for "popular music", any other music obviously is "unpopular".

***

<a href='https://play.google.com/store/apps/details?id=de.kromke.andreas.opus1musicplayer'><img src='public/google-play.png' alt='Get it on Google Play' height=45/></a>
<a href='https://f-droid.org/app/de.kromke.andreas.opus1musicplayer'><img src='public/f-droid.png' alt='Get it on F-Droid' height=45 ></a>

***

# Music Organisation

Other music player apps are meant for pop music only and deal with "Songs" and "Artists". This is not suitable for classical (baroque, renaissance, ...) music which is organised by composer, work, movements and performers.

Pop music:

Artist | Album  | Song
-------| ------ | -------------
Smith | Heaven  | 1. Love you, babe
 | | 2. Babe, love you
 | | 3. More love, babe, inne wadr
Jones  | Freedom | 1. Babe, I'm free
 | | 2. Free the innrnatt

Classical music needs more metadata ("tags"), especially the *composer tag* and *grouping tag*, the latter is used to group the movements together:

| Album  | Composer  | Work | Performer | Movement
|------- |-----------| -----| --- | ---
| Cello Concertos | Joseph Haydn | Concerto No. 1 | Fred Stone | 1. Allegro
|                 |              |                |            | 2. Adagio 
|                 |              |                |            | 3. Presto 
|                 | Hella Haydn  | Concerto No. 3 | Jean Water | 1. Allegro
|                 |              |                |            | 2. Adagio


For more information see the Google Play Store entry or the offline help text of the application.

# Usual Versus Opus 1 or Unpopular Music Player
<img alt="No" src="public/Tracks-View-generic-no.png" width="320">
<img alt="Yes" src="public/Tracks-View-UMP-yes.png" width="320">

# Other Screenshots

<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-01-Albums-white.png" width="220">
<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-02-Selection-white.png" width="220">
<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-03-Composers-white.png" width="220">

<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-04-Genres-white.png" width="220">
<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-05-Albums-orange.png" width="220">
<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-06-Albums-green.png" width="220">
<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-07-Albums-lgray.png" width="220">
<img alt="1" src="fastlane/metadata/android/en-US/images/phoneScreenshots/O1M-08-Albums-dgray.png" width="220">

<img alt="1" src="fastlane/metadata/android/en-US/images/sevenInchScreenshots/Screenshot_20190602-182821.png" width="220">


# Supported

* Starts with album list and cover art.
* Nowhere text is truncated (with "..."), even the longest work and movement descriptions get the space they need.
* Composer and grouping (work, movements)
* Views: albums, composers, works, genres, performers, folders
* Filter for combinations of composer, genre etc.
* Text search
* Play lists
* "gapless" playback (as far as supported by Android)
* If configured, uses database built by *Classical Music Scanner*, otherwise the system database.
* Call *Classical Music Tagger* or *Classical Music Scanner* directly from app
* Meta data transferred via Bluetooth to car radio
* Six colour themes
* Scalable UI, also for tablets
* Portrait and landscape orientation
* Perfect gendering-ism, especially designed for useresses and users from German civil service.

# Not Supported (Yet)

* Cover downloading
* (Un-)Equalizer
* "Songs" and "Artists"
* ...

# Permissions Needed

* Read storage
* Prevent phone from sleeping

# License

The Opus 1 Music Player is licensed according to GPLv3, see LICENSE file.

# External Licenses

**Jaudiotagger library:**  
Source: http://www.jthink.net/jaudiotagger/  
Copyright: paultaylor@jthink.net  
License: http://www.gnu.org/copyleft/lesser.html
